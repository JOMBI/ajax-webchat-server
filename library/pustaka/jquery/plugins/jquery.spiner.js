/**
 * @project [plugin $.fn.Spiner loding data on posted ]
 * @auth 	[omen]
 *
 * @class  [Register patient]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	 

;(function( $ ) {
	// check validation type : 
 if( typeof( $.fn.Spiner ) != 'function' ) { 
	// spiner extend : jQuery 	
	$.fn.Spiner = function( data ) {
		
		var UrlParams = { }, 
		UrlWindow = ( typeof( data.url ) == 'object' ? 
					  Ext.EventUrl( data.url ).Apply() : data.url );
					
	// check validation : 		
	if( typeof( data ) == 'object' )  {
		// get data type is object : 
		var row = data.param;
		if( typeof( row )== 'object' ) for( var i in row ){
			// jika data object [];
			if( typeof(row[i]) == 'object' ){
				UrlParams[i] = row.join(',')
			}
			// then :
			else{
				UrlParams[i] = row[i];
			}
		}
		
		
	// next process
		var pager = data.order;
		if( typeof(pager) == 'object' ){
			// set add object : 
			UrlParams['orderby'] = ( pager.order_by == 'undefined'?'':pager.order_by ) ;
			UrlParams['type'] 	 = ( pager.order_type == 'undefined'?'':pager.order_type ) ;
			UrlParams['page']    = ( pager.order_page == 'undefined'?'':pager.order_page ) ;
		}
		
		// get handleEvent
		var handleEvent = data.handler;	
		if( typeof(handleEvent) == 'string' && handleEvent != '' ){
			UrlParams['handler']  = handleEvent;
		} 
		
		// get callback :
		var callback = data.complete, callbacks = '';
		if( typeof(callback) == 'function' ){
			callbacks = callback;
		}
		
		//	var _call_func =  ( ( data.complete !=='undefined' && typeof ( data.complete ) == 'function') ? data.complete : '');
		
		$(this)
			.html('')
			.css({'height' : '120px', 'z-index' : '9999'})
			.addClass('ui-widget-ajax-spiner'); 
		
		// sent data via ajax : 
		
			$(this).load( UrlWindow, UrlParams,  function( response, textStatus, jqXhr ) {
				
				// check response : 
				if( typeof(textStatus) == 'string' && textStatus == 'success' ){
					if( typeof(callbacks) == 'function' ){
						$(this)
						.css({ 'height' : '100%'})
						.removeClass( 'ui-widget-ajax-spiner' );
						
						callbacks.apply( this, new Array($(this), response, jqXhr ) );
						
					}
				}
				// then if respnse true all false:
				else if( typeof(textStatus) == 'string' && textStatus == 'error' ){
					$(this)
					.html('Error 404')
					.removeClass( 'ui-widget-ajax-spiner');
				}	
				//end task : 
				$(this).removeClass( 'ui-widget-ajax-spiner');	
			}); 	
	  }
  }
  
  // spiner extend : jQuery 	
	$.fn.Paging = function( data ) {
		
		var UrlParams = { }, 
		UrlWindow = ( typeof( data.url ) == 'object' ? 
					  Ext.EventUrl( data.url ).Apply() : data.url );
					
	// check validation : 		
	if( typeof( data ) == 'object' )  {
		// get data type is object : 
		var row = data.param;
		if( typeof( row )== 'object' ) for( var i in row ){
			// jika data object [];
			if( typeof(row[i]) == 'object' ){
				UrlParams[i] = row.join(',')
			}
			// then :
			else{
				UrlParams[i] = row[i];
			}
		}
		
		
	// next process
		var pager = data.paging;
		if( typeof(pager) == 'object' ){
			// set add object : 
			UrlParams['orderby'] = ( pager.orderData == 'undefined'?'':pager.orderData ) ;
			UrlParams['type'] 	 = ( pager.orderType == 'undefined'?'':pager.orderType ) ;
			UrlParams['page']    = ( pager.orderPage == 'undefined'?'':pager.orderPage ) ;
		}
		
		// get callback :
		var callback = data.complete, callbacks = '';
		if( typeof(callback) == 'function' ){
			callbacks = callback;
		}
		
		// get handleEvent
		var handleEvent = data.handler;	
		if( typeof(handleEvent) == 'string' && handleEvent != '' ){
			UrlParams['handler']  = handleEvent;
		}  
		
		// then processing data on server side: 
		$(this)
			.html('')
			.css({'height' : '120px', 'z-index' : '9999'})
			.addClass('ui-widget-ajax-spiner'); 
		
		// sent data via ajax : 
		
			$(this).load( UrlWindow, UrlParams,  function( response, textStatus, jqXhr ) {
				
				// check response : 
				if( typeof(textStatus) == 'string' && textStatus == 'success' ){
					if( typeof(callbacks) == 'function' ){
						// if you have customize height: 
						$(this).removeClass( 'ui-widget-ajax-spiner');	
						$(this).css({'height' : '100%'}); 
						callbacks.apply( this, new Array($(this), textStatus, response, jqXhr ) );
					}
				}
				// then if respnse true all false:
				else if( typeof(textStatus) == 'string' && textStatus == 'error' ){
					$(this).html('Error 404');	 
					$(this).removeClass( 'ui-widget-ajax-spiner');	
				}	
				//end task : 
				$(this).removeClass( 'ui-widget-ajax-spiner');	
			}); 	
	  }
  }
  
 //loader extend : jQuery 
  $.fn.loader = function( data ) {
		var UrlParams = {}, 
		UrlWindow = ( typeof( data.url ) == 'object' ? 
					  Ext.EventUrl( data.url ).Apply() : data.url );
					
	// check validation : 		
	if( typeof( data ) == 'object' )  {
		// get data type is object : 
		var row = data.param;
		if( typeof( row )== 'object' ) for( var i in row ){
			// jika data object [];
			if( typeof(row[i]) == 'object' ){
				UrlParams[i] = row.join(',')
			}
			// then :
			else{
				UrlParams[i] = row[i];
			}
		}
		
	// next process
		var pager = data.order;
		if( typeof(pager) == 'object' ){
			// set add object : 
			UrlParams['orderby'] = ( pager.order_by != 'undefined' ? pager.order_by  : '' ) ;
			UrlParams['type'] 	 = ( pager.order_type != 'undefined' ? pager.order_type : '' ) ;
			UrlParams['page']    = ( pager.order_page != 'undefined' ? pager.order_page : '' ) ;
		}
		
		// get callback :
		var callback = data.complete, callbacks = '';
		if( typeof(callback) == 'function' ){
			callbacks = callback;
		}
		
		//	var _call_func =  ( ( data.complete !=='undefined' && typeof ( data.complete ) == 'function') ? data.complete : '');
		
		$(this)
			.html( '' )
			.css({ 'height' : '50px', 'z-index' : '9999' })
			.addClass('ui-widget-ajax-spiner-min'); 
		
		// sent data via ajax : 
		
			$(this).load( UrlWindow, UrlParams,  function( response, textStatus, jqXhr ) {
				
				// check response : 
				if( typeof(textStatus) == 'string' && textStatus == 'success' ){
					if( typeof(callbacks) == 'function' ){
						callbacks.apply( this, new Array($(this), response, jqXhr ) );
						$(this)
						.css({'height' : '100%'})
						.removeClass( 'ui-widget-ajax-spiner-min');	
					}
				}
				// then if respnse true all false:
				else if( typeof(textStatus) == 'string' && textStatus == 'error' ){
					$(this).html('Error 404');	 
					$(this).removeClass( 'ui-widget-ajax-spiner-min');	
				}	
				//end task : 
				$(this).removeClass( 'ui-widget-ajax-spiner-min');	
			}); 	
	  }
   } 
 }
})( jQuery );
 