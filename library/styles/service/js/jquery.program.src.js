/*!
 * Kawani -- An open source Smart Chat Public toolkit.
 *
 * Copyright (C) 2019 - 2023, (PDS) Peranti Digital Solusindo, PT.
 *
 * OMEN <jombi.php@gmail.com>
 *
 * See http://perantidigital.co.id for more information about
 * the Asterisk project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * <BEGIN>
 * @version 4.5
 * + $.revision 2023/12/11 00:00
 * + $.revision 2024/07/23 00:00
 * + $.revision 2024/07/24 00:00
 * </BEGIN>
 *
 */ 
var originalTitle = "", // parent title 
intval = 0, // int value 
ontype = 0,  // default on type 
timer = null, // null of timeer start 
delay = 3000, // that's 3 seconds of not typing 
offsetEmoji = 500,  // emoji offset layout
offsetHeight = 300, // ofsset height 
searchOnline = 0, // start find agent 	
chatMaxTimeout = 30, //  maximum timeout if not response 
chatTickTimeout = 1000, // all tick time 
chatAnimateTimeout = 1000, // animate timeout 
chatBusyTimeout = 5000, // if busy timeou to nex flow 
chatMaxRounting = 10,  //  maximum routing to agent 
chatTimeoutInterval = null, // id interval
chatActiveInterval = null, // id interval 
chatHeartbeatInterval = null; // id interval
(function($){
	// handleEvent if jQuery modul is empty 
	if (typeof $ === "undefined"){
		console.warn( "jQuery error" );
		return;
	} // default local storage
	// default users 
	$.user = {from: "", to: ""};
	// default configuration 
	$.config = {
		program: {
			title: "Smart Chat",
			version: "4.5",
			Revision: "2024/07/24 02:00:00 WIB"
		},
		sound: {}, 
		cookie: false,
		files: false,
		wss: {},
		api: {},
		session: {}, 
		timeout: 2000, /* timeout */
		timeone: 2000, /* timeone */ 
		timeact: 5000,  /* timeact */
	}; 
	
	/**
	 * @brief set config for local program
	 * @retval mixed return value
	 */
	$.chatConfigs = function(events){
		$.config.cookie = getWebhookSession(); // storage of database 
		$.config.files = getWebhookFiles(); // storage of file
		$.config.wss.url = getWebsockUrl(); // websocket url interface 
		$.config.api.url = getWebhookUrl(); // webhook url interface
		$.config.api.key = getWebhookToken(false); // no decrypt
		$.config.api.auth = getWebhookAuth(false); // no decrypt 
		$.config.sound.message = {incoming: getSound("message-alert.mp3")};			 
		if (typeof events	=== "function") {
			events.apply(this, [$.config]);
		} return $.config;
	};
	
	/**
	 * @brief chatLocalStorage | DBI
	 * @details local storage custom   
	 * @retval mixed return value
	 */
	$.chatLocalStorage = DBI.visitor = {
		open: (newsession) => { 
			var Constructor = typeof newsession == "object" ? newsession: {}, newupdated = 0;
			if (window.localStorage.getItem($.config.cookie) === null){
				window.localStorage.setItem($.config.cookie, JSON.stringify(Constructor));
			} else {
				Constructor = window.localStorage.getItem($.config.cookie);
				if (typeof Constructor === "string"){
					Constructor = JSON.parse(Constructor);
					// befor update will updated and compare 
					if (typeof newsession === "object"){
						// cehck if indicated update configuration server
						$.config.wss.url = getWebsockUrl(); 
						$.config.api.url = getWebhookUrl();
						$.config.api.key = getWebhookToken(false);
						$.config.api.auth = getWebhookAuth(false);  
						// update new url for websocket 
						if (Constructor.config.wss.url !== $.config.wss.url){ 
							Constructor.config.wss.url = $.config.wss.url;
							newupdated++;
						} // update new url for api 
						if (Constructor.config.api.url !== $.config.api.url){ 
							Constructor.config.api.url = $.config.api.url;
							newupdated++;
						} // update new url for api key 
						if (Constructor.config.api.key !== $.config.api.key){ 
							Constructor.config.api.key = $.config.api.key;
							newupdated++;
						} // update new url for api auth
						if (Constructor.config.api.auth !== $.config.api.auth){ 
							Constructor.config.api.auth = $.config.api.auth;
							newupdated++;
						} // version 
						if (Constructor.config.program.version !== $.config.program.version){
							Constructor.config.program.version = $.config.program.version;
							newupdated++;
						} // revision 
						if (Constructor.config.program.Revision !== $.config.program.Revision){
							Constructor.config.program.Revision = $.config.program.Revision;
							newupdated +=1;
						} // version 
						if (Constructor.program.version !== $.config.program.version){
							Constructor.program.version = $.config.program.version;
							newupdated++;
						} // Revision  
						if (Constructor.program.Revision !== $.config.program.Revision){
							Constructor.program.Revision = $.config.program.Revision;
							newupdated++;
						} // jika ada indikasi perbedaan maka update dulu datanya 
						if (newupdated){
							window.ctmClient.Utils.loger("Updated New Configuration");  
							window.localStorage.setItem($.config.cookie, JSON.stringify(Constructor)); 
						}
					}
				} 
			} return Constructor;
		},
		getApi: (key) => {
			var Constructor = DBI.visitor.open();
			if (Constructor.hasOwnProperty("config")){
				if (typeof Constructor.config === "object"){
					var config = Constructor.config;
					if (typeof Constructor.config === "object" &&(Constructor.config.hasOwnProperty("api"))){
						if (typeof Constructor.config.api === "object" &&(Constructor.config.api.hasOwnProperty(key))){
							return Constructor.config.api[key];
						}
					}
				}
			} return "";
		},
		getWss: (key) => {
			var Constructor = DBI.visitor.open();
			if (Constructor.hasOwnProperty("config")){
				if (typeof Constructor.config === "object"){
					var config = Constructor.config;
					if (typeof Constructor.config === "object" &&(Constructor.config.hasOwnProperty("wss"))){
						if (typeof Constructor.config.wss === "object" &&(Constructor.config.wss.hasOwnProperty(key))){
							return Constructor.config.wss[key];
						}
					}
				}
			} return "";
		},
		setSession: (key, value) => {
			var Constructor = DBI.visitor.open();
			if (Constructor.hasOwnProperty("session")){
				Constructor.session[key] = value;
			} if (typeof Constructor === "object"){
				window.localStorage.setItem($.config.cookie, JSON.stringify(Constructor));
			} return Constructor;
		},
		getSession: (key, defaults) => {
			var Constructor = DBI.visitor.open(); 
			if (typeof defaults === "undefined") {
				defaults = "";
			} if (Constructor.hasOwnProperty("session")){
				if (!Constructor.session.hasOwnProperty(key)){
					return defaults;
				} return Constructor.session[key];
			} return defaults;
		},
		removeSession: (key) => {
			var Constructor = DBI.visitor.open();
			if (Constructor.hasOwnProperty("session")){
				if (Constructor.session.hasOwnProperty(key)){
					delete Constructor.session[key];
					window.localStorage.setItem($.config.cookie, JSON.stringify(Constructor));
					return true;
				}
			} return false;
		},
		removeAll: (events) => {
			var Constructor = window.localStorage.getItem($.config.cookie);
			if (Constructor == null) {
				return false;
			} 
			window.localStorage.removeItem($.config.files);
			window.localStorage.removeItem($.config.cookie); 
			if (typeof events === "function"){
				events.apply(this, [Constructor]);
			} return true;
		}
	}; 
	
	/** 
	 * @brief Solution
	 * @details Run CODE snippet below & type as the textarea appears.
	 * @param See if this is what you wanted to do:
	 * @link https://stackoverflow.com/questions/42111342/js-change-class-of-div-while-typing-in-textarea-and-after-some-time-of-not-typi
	 */
	
	((AnotherSystem) => { 
		if (typeof AnotherSystem.localStorage === "undefined") { 
			AnotherSystem.localStorage = { 
				setItem: (ca, va) => { 
					DBI.lites[ca] = va;	
					return true;
				}, 
				getItem: (ca) => { 
					if (typeof DBI.lites[ca] == "undefined") return "";
					else if (typeof DBI.lites[va] !== "undefined"){
						return DBI.lites[ca];
					} return "";
				},
				removeItem: (ca) => {
					if (DBI.lites.hasOwnProperty(ca)){
						delete DBI.lites[va];
					} return true;
				}
			}
		} 	
	})(window);
	
	/**
	 * @brief chatHeader
	 * @details get user start online  
	 * @retval mixed return value
	 */
	$.chatHeader = function(events){ 
		var headers = {}; // set global header variabels 
		if (typeof DBI.visitor === "object"){
			headers["Authorization"] = getBinaryString(DBI.visitor.getApi("auth"));
			headers["ApiKey"] = getBinaryString(DBI.visitor.getApi("key"));
		} setAjaxHeader(headers, events);
	}	   
	/**
	 * @brief chatSoundReceiveMessage
	 * @details playing audio on chat incoming 
	 * @retval mixed return value
	 */
	$.chatSoundReceiveMessage= function(msgobj) {
		var newSoundRinging = 0; // chat panel in hidden 
		if (!getById("chatcontiner").is(":visible")){
			newSoundRinging++;
		} // if negative ringing 
		if (newSoundRinging <= 0){
			return;
		} // set document title proces notice: 
		try {
			var newAudioRinger = new Audio($.config.sound.message.incoming);
			newAudioRinger.console = DBI.visitor.getSession("console", 0);
			newAudioRinger.crossOrigin = "anonymous";
			newAudioRinger.preload = "auto"; 
			newAudioRinger.loop = false; 
			newAudioRinger.oncanplaythrough = (event) => {
				consoleEvents.notify(newAudioRinger.console, msgobj); 
				newAudioRinger.play().then(() => {
					ctmClient.Utils.loger("Audio Is Playing");  
				}).catch((e) => {
					console.warn("Unable to play audio file.", e);
				});	
			}  
		} catch(e) {
			console.warn("error: ", e);
		} return;
	}
	 
	/**
	 * @brief chatUrlParam
	 * @details get user start online 
	 * @retval mixed return value
	 */
	$.chatSetUrlParam = function(events) {
		var urlParam = new URLSearchParams(window.location.search);
		if (typeof urlParam === "object") {
			DBI.visitor.setSession("cid", urlParam.get("cid"));
			DBI.visitor.setSession("scheme", urlParam.get("auth"));
			DBI.visitor.setSession("domain", urlParam.get("host"));  
		}   
		if (typeof events === "function"){
			events.apply(this, [DBI.visitor]);
		} return urlParam;
	} 
	
	/**
	 * @brief chatConstruct
	 * @details get user start online  
	 * @retval mixed return value
	 */
	$.chatConstruct = function(events) {
		$.chatConfigs(response => {
			var Constructor = {
				config: typeof $.config === "object" ? $.config : {},
				program: typeof $.config.program === "object" ? $.config.program : {},
				session: typeof $.config.session === "object" ? $.config.session : {}
			} // "Constructor"  and check process 
			DBI.visitor.open(Constructor);
			$.chatSetUrlParam((visitor) => {
				visitor.setSession("to",visitor.getSession("to"));
				visitor.setSession("from",visitor.getSession("from"));
				visitor.setSession("email", visitor.getSession("email"));  
			});
			if (typeof events === "function"){
				events.apply(this, [DBI.visitor]);
			} return false; 
		}); 
	} 
	
	/**
	 * @brief chatWrite
	 * @details write of chat area
	 * @retval mixed return value
	 */ 
	$.chatWrite = Utils.sprintf = function(str) {
		for (var i=1; i < arguments.length; i++) {
			str = str.replace(/%s/, arguments[i] );
		} return str;
	};
	
	/** 
	 * @brief checkWithSpace
	 * @details get header for new session 
	 * @retval mixed return value
	 */
	$.checkWithSpace = Utils.withspace = function(s){
		return /\s/g.test(s);
	}
	
	/**
	 * @brief chatConversation
	 * @details message of conversation  
	 * @retval mixed return value
	 */  
	$.chatConversation = function(type, s){
		s.message = getRegexString(s.message); // quota html script on js 
		s.message = getBoldString(s.message); 
		if (type === "sender" && (typeof s === "object" )){ 
			var ourMessage = "", ourTypeMessage = getXor(s, "files", false) ? "file": "text";
			ourMessage += "<div id='message-body-"+ getXor(s, "msgid", "") +"' class='row message-body' value='"+ getXor(s, "msgid", "") +"'>";
			ourMessage += "<div id='message-main-sender-"+ getXor(s, "msgid", "") +"' class='col-sm-12 message-main-sender'>";
			
			// dropdown showon 
			if (ourTypeMessage === "text" &&(getDropdownMessage(["deleted", "quotes"]))){
				ourMessage += "<div id=\"message-body-sender-"+ getXor(s, "msgid", "") +"\" class=\"message-body-sender-"+ getXor(s, "msgid", "") +" sender\" onmouseenter=\"openChatOurMessageDropdown(this);\" onmouseleave=\"closeChatOurMessageDropdown(this);\">";
			} else if (ourTypeMessage === "file" && getDropdownMessage(["deleted", "quotes", "download", "preview", "playing"])){
				ourMessage += "<div id=\"message-body-sender-"+ getXor(s, "msgid", "") +"\" class=\"message-body-sender-"+ getXor(s, "msgid", "") +" sender\" onmouseenter=\"openChatOurMessageDropdown(this);\" onmouseleave=\"closeChatOurMessageDropdown(this);\">";
			} else {
				ourMessage += "<div id=\"message-body-sender-"+ getXor(s, "msgid", "") +"\" class=\"message-body-sender-"+ getXor(s, "msgid", "") +" sender\">";
			}  
			
			ourMessage += "<table class=\"senderMessageContainer\" border=\"0\">";
			ourMessage += "<tr>";
			ourMessage += "<td class=\"senderMessageContainerText\">";
			// if contains files 
			if (typeof getXor(s, "files", false) === "object"){
				messageStreamFiles(s.files, (stream) => {
					ourMessage += messageTriggerFiles(s.files, false, "OurMessage");
				}); 
			} // on text if exist OK 
			if (getXor(s, "message", "") !== ""){
				ourMessage += "<div id='message-text-"+ getXor(s, "msgid", "") +"' class='message-text message-text-sender'>"+ getXor(s, "message", "") +"</div>";
			} // if not have message 
			ourMessage += "<span id='message-time-"+ getXor(s, "msgid", "") +"' class='message-time pull-right'>"+ getXor(s, "sent", "") +" <i class='fa fa-check'></i></span>";
			ourMessage += "</td>";
			
			// dropdown showon 
			if (ourTypeMessage === "text" &&(getDropdownMessage(["deleted", "quotes"]))){
				ourMessage += "<td class=\"senderMessageMenu\">";
				ourMessage += "<span class=\"senderMessageDropdown\" onclick=\"openChatMessageMenu(this, '"+ getXor(s, "msgid", "") +"', false);\"style=\"display:none;\"><i class=\"fa fa-chevron-down\"></i></span>";
				ourMessage += "</td>"; 
			} else if (ourTypeMessage === "file" &&(getDropdownMessage(["deleted", "quotes", "download", "preview", "playing"]))){
				ourMessage += "<td class=\"senderMessageMenu\">";
				ourMessage += "<span class=\"senderMessageDropdown\" onclick=\"openChatMessageMenu(this, '"+ getXor(s, "msgid", "") +"', false);\"style=\"display:none;\"><i class=\"fa fa-chevron-down\"></i></span>";
				ourMessage += "</td>";
			}
			
			ourMessage += "</tr>";
			ourMessage += "</table>";
			ourMessage += "</div>";
			ourMessage += "</div>";
			ourMessage += "</div>"; 
			$(ourMessage).appendTo( $('#conversation'));
			$('#conversation').scrollTop($('#conversation')[0].scrollHeight);
		}
		// receive message from user app:
		else if (type === "receiver" && (typeof s === "object")){
			var theirMessage = "", theirTypeMessage = getXor(s, "files", false) ? "file": "text";
			theirMessage += "<div id=\"message-body-"+ getXor(s, "msgid", "") +"\" class=\"row message-body\" value=\"'"+ getXor(s, "msgid", "") +"\">";
			theirMessage += "<div id=\"message-main-receiver-"+ getXor(s, "msgid", "") +"\" class=\"col-sm-12 message-main-receiver\">";
			
			// dropdown showon 
			if (theirTypeMessage === "text" &&(getDropdownMessage(["deleted", "quotes"]))){ 
				theirMessage += "<div id=\"message-body-receiver-"+ getXor(s, "msgid", "") +"\" class=\"message-body-receiver-"+ getXor(s, "msgid", "") +" receiver\" onmouseenter=\"openChatTheirMessageDropdown(this);\" onmouseleave=\"closeChatTheirMessageDropdown(this);\" >";
			} else if (theirTypeMessage === "file" &&(getDropdownMessage(["deleted", "quotes", "download", "preview", "playing"]))) {
				theirMessage += "<div id=\"message-body-receiver-"+ getXor(s, "msgid", "") +"\" class=\"message-body-receiver-"+ getXor(s, "msgid", "") +" receiver\" onmouseenter=\"openChatTheirMessageDropdown(this);\" onmouseleave=\"closeChatTheirMessageDropdown(this);\" >";
			} else {
				theirMessage += "<div id=\"message-body-receiver-"+ getXor(s, "msgid", "") +"\" class=\"message-body-receiver-"+ getXor(s, "msgid", "") +" receiver\">";
			}
			
			theirMessage += "<table class=\"receiverMessageContainer\" border=\"0\">";
			theirMessage += "<tr>";
			theirMessage += "<td class=\"receiverMessageContainerText\">"; 
			// if contains files 
			if (typeof getXor(s, "files", false) === "object"){ 
				messageStreamFiles(s.files, (stream) => {
					theirMessage += messageTriggerFiles(s.files, false, "TheirMessage");
				});
			} if (getXor(s, "message", "") !== ""){
				theirMessage += "<div id='message-text-"+ getXor(s, "msgid", "") +"' class='message-text message-text-receiver'>"+ getXor(s, "message", "") +"</div>";
			} // if not have message 
			theirMessage += "<span id='message-time-"+ getXor(s, "msgid", "") +"' class='message-time pull-right'>"+ getXor(s, "sent", "") +" <i class='fa fa-check'></i></span>";
			theirMessage += "</td>"; 
			// dropdown showon 
			if (theirTypeMessage === "text" &&(getDropdownMessage(["deleted", "quotes"]))){ 
				theirMessage += "<td class=\"receiverMessageMenu\">";
				theirMessage += "<span class=\"receiverMessageDropdown\" onclick=\"openChatMessageMenu(this, '"+ getXor(s, "msgid", "") +"', false);\" style=\"display:none;\"><i class=\"fa fa-chevron-down\"></i></span>";
				theirMessage += "</td>"; 
			} else if (theirTypeMessage === "file" &&(getDropdownMessage(["deleted", "quotes", "download", "preview", "playing"]))) {
				theirMessage += "<td class=\"receiverMessageMenu\">";
				theirMessage += "<span class=\"receiverMessageDropdown\" onclick=\"openChatMessageMenu(this, '"+ getXor(s, "msgid", "") +"', false);\" style=\"display:none;\"><i class=\"fa fa-chevron-down\"></i></span>";
				theirMessage += "</td>";
			} 
			
			theirMessage += "</tr>";
			theirMessage += "</table>";
			theirMessage += "</div>";
			theirMessage += "</div>";
			theirMessage += "</div>";
			$(theirMessage).appendTo( $('#conversation'));
			$('#conversation').scrollTop($('#conversation')[0].scrollHeight);  
			
		}  // completed message if ok 
		else if (type === "completed" && (typeof s === "object")){
			
			var complete = "<div id=\"message-body-complete-"+  getXor(s, "session", 0) +"\" class=\"row message-body messageCompleteCover\">";
			complete += "<table class=\"messageCompleteContainer\" border=0><tr>";
			complete += "<td class=\"messageCompleteIcon\"><i class=\"fa fa-clock-o\"></i></td>";
			complete += "<td class=\"messageCompleteText\">";
			complete += " <div class=\"messageCompleteTextContainer\">Chat Session <b>"+ getXor(s, "session", "") +"</b> Complete !</div></td>"; 
			complete += "</tr></table>"; 
			complete +="</div>"; 
			$(complete).appendTo( $('#conversation'));
			$('#conversation').scrollTop($('#conversation')[0].scrollHeight);  
			
		}
		/* update scroll to width window */
		if (s.hasOwnProperty( "complete" )){
			if (typeof s.complete === "function"){
				s.complete.apply(this, [type]);
			}
		} return;
	}  
	
	/**
	 * @brief chatSentTyping
	 * @details sent info state on typing to agents   
	 * @retval mixed return value
	 */  
	$.chatSentTyping = function(typing){ 
		/* collabs with websocket */
		if (typeof window.ctmClient.SendTyping === "function"){
			window.ctmClient.SendTyping(typing);
		} 
	}  
	
	/**
	 * @brief chatForceClose
	 * @details sent info state on typing to agents   
	 * @retval mixed return value
	 */ 
	$.chatForceClose = function(msgstr){
		Alert(msgstr, getLang("alert_title_error"), (e) => {
			$.chatCleanup((ui) => {  
				if (typeof window.ctmClient !== "object"){
					getByClass("message-loaders").hide("slow", () => {
						window.location.reload();
					});
					return false;
				} window.ctmClient.EndSession((Visitor) => {
					Visitor.Disconnect(() => {
						getByClass("message-loaders").hide(() => { 
							window.location.reload()
						});
					});
				});  
			});  
		});  
	}
	
	/**
	 * @brief chatTryRouting
	 * @details sent message text by user 
	 * @retval mixed return value
	 */
	$.chatTryRouting = function(){
		getById("chatcontiner").block(blockingUI.content);   
		HTTP.header(() => {
			var routing = HTTP.routing({ "action": "signoutAppChat" }), request = {}; 
			request.uid = DBI.visitor.getSession("session"); // session from socket 
			request.from = DBI.visitor.getSession("from"); // from visitor 
			request.email = DBI.visitor.getSession("email"); // email of visitor 
			request.session = DBI.visitor.getSession("session"); // session from visitor 
			getAjaxPromise(routing, request).then((res) => {
				// get object false
				if (typeof res !== "object"){
					getById("chatcontiner").unblock();	
					return false;
				} // response invalid 
				if (!getXor(res, "success", 0)){
					return false;
				} // old session will remove from local storage 
				if (request.session !== ""){
					DBI.visitor.setSession("to", "0"); // remove local agents 
					DBI.visitor.setSession("nick", "0"); // remove local nick 
					DBI.visitor.setSession("session", "0"); // remove local session 
				} // then sent data Guest  
				var routing = HTTP.routing({action: "routeAppChat"});  
				request.session = DBI.visitor.getSession("session", ""); // session  
				getAjaxPromise(routing, request).then((response) => {
					// check if false process 
					var tsession = {}, tresponse = getXor(response, "data", false) 
					? response.data : false; 
					if (!getXor(response, "success", 0)){
						getById("chatcontiner").unblock();	
						$.chatForceClose(getLang("alert_fail_res_message", "alert_fail_res_message"));
						return;
					} // if success from this  
					if (!tresponse){
						getById("chatcontiner").unblock();	
						$.chatForceClose(getLang("alert_fail_res_object", "alert_fail_res_object"));
						return; 
					}  // session empty 
					if (getXor(tresponse, "session", "") === ""){
						getById("chatcontiner").unblock();	  
						$.chatForceClose(getLang("alert_fail_res_session", "alert_fail_res_session"));	
						return;
					} // set new session after process routing success
					tsession.session = tresponse.session;
					tsession.update = tresponse.update;
					tsession.nick = tresponse.nick;
					tsession.to = tresponse.to; 
					// then checn check successfuly 
					DBI.storage(tsession).then((resolve) => {
						getById("chatcontiner").unblock();	
						window.setTimeout((e) => {
							window.location.reload();
						}, 1000);
					}); 
				}); 
			}) // on error
			.catch((error) => {
				console.warn("error:", error);
			}); 
		});
	}
	
	/**
	 * @brief chatSentExpired
	 * @details sent message text by user 
	 * @retval mixed return value
	 */
	$.chatSentExpired = function(){
		var butttonConfirm = [];
		butttonConfirm.push({
			text: getLang("alert_button_ok"),
			click: (trying) => {
				try {
					closeWindow(true); // close from window all  
					var tryButtons = getXor(enableButtons, "tryrouting", false);
					if (!tryButtons){ 
						$.chatSignout(); // force logout from session 
					} else if (tryButtons){ 
						$.chatTryRouting(); // this active moduls to try get session;
					} return;
				} catch(e) {};
			}
		}); 
		butttonConfirm.push({
			text: getLang("alert_button_cancel"),
			click: (cancel) => {
				closeWindow(true);
			}			
		});  
		// get local before session 
		var agents = DBI.visitor.getSession("nick", "");
		var messageStr = "<b>"+ agents +"</b> "+ getLang("alert_end_session", "");
		Confirm(messageStr, getLang("alert_title_confirm"), butttonConfirm); 
		return false;
	}
	
	/**
	 * @brief chatSentHandler
	 * @details sent message text by user 
	 * @retval mixed return value
	 */
	$.chatSentHandler = function(routing, request, transport){ 
		$.ajax ({
			url: routing,
			type: "POST",
			crossDomain: true,
			dataType: "json",
			data:request,
			// get event handler
			xhr: function(events) {
				var myXhr = $.ajaxSettings.xhr();
				if (myXhr.upload){
					myXhr.upload.addEventListener( "progress", function(event){
						var percent = (event.loaded / event.total) * 100;
						getById("fileUploadProgress").html("Upload: "+ Math.round(percent) +" %");  
					}, false);
				} return myXhr;  
			}, 
			// success 
			success: (response) => {
				window.setTimeout((e) => {getById("fileUploadProgress").html("Upload: 0 %")}, 50); 
				if (typeof response !== "object"){
					return false;
				} // check response code 
				var tmessage = response.hasOwnProperty("msg") ? response.msg : {};
				var tresponse = getXor(response, "success", 0);
				if (!tresponse){
					Alert("Send message failed", getLang("alert_title_error"));
					return false;
				} // then will "expired session" or close by local channel or completed*/ 
				if (tresponse == 3){
					$.chatSentExpired();
					return false;
				} // if is object 
				if (typeof transport.success === "function"){
					transport.success.apply(this, [tmessage]);
				} // close preview progress 
				closeCameraTakePhoto();
				closeTriggerUpload((e) => {
					window.clearTimeout(timer);
					notTyping();
				});
			}, 
			// error
			error:(error) => {   
				console.warn("error:", error);
				Alert(getLang("error_bigest_file_upload", "..."), 
				getLang("alert_title_error", "..."), (events) => {
					getById("fileUploadProgress").html(""); 
					closeCameraTakePhoto();
					closeTriggerUpload((e) => {
						window.clearTimeout(timer);
						notTyping();
					});
				});
			}
		});
	}
	
	/**
	 * @brief chatSentMessage
	 * @details sent message text by user 
	 * @retval mixed return value
	 */
	$.chatSentMessage = function(event, chatboxtextarea, chatbox){  
		var from = DBI.visitor.getSession("from");
		var to = DBI.visitor.getSession("to");
		var message = false, 
		files = getTriggerUpload();
		// check channel "origin" 
		if (from === "") { 
			$(chatboxtextarea).val(""); 
			$(chatboxtextarea).focus(); 
			return false;
		} // chec channel "remote"
		if (to === "") { 
			$(chatboxtextarea).val(""); 
			$(chatboxtextarea).focus();  
			return false;
		}   
		// get message 
		var routing = HTTP.routing({action: "sendAppChat"});  
		var message = $(chatboxtextarea).val(); 
		var request = {}; 
		if (message.length !== ""){
			message = message.replace(/^\s+|\s+$/g,""); 
		} // set paramter header process 
		request.from = from;
		request.to = to; 
		request.files = files;
		request.message = message;  
		// event,chatboxtextarea,chatwhatsappboxtitle 
		if ((event.type === "keydown") &&(event.keyCode == 13 && event.shiftKey == 0)) {
			event.preventDefault(); 
			$(chatboxtextarea).val("").focus();  
			// validation paramter 
			if (request.message === "" && request.files === ""){
				return false;
			} // validation function 
			if (typeof $.chatConversation !== "function"){
				return false;
			} // set header request before send 
			HTTP.header(() => { 
				$.chatSentHandler(routing, request, { 
					success: (tmessage) => {
						$.chatConversation("sender", {
							message: getXor(tmessage, "m", ""),
							msgid: getXor(tmessage, "n", ""),
							files: getXor(tmessage, "x", false),
							sent: getXor(tmessage, "t", "") 
						}); 
					} 
				}); 
			}); 
			return false;
		} // on click buttton sent 
		if ((event.type === "click"))  {
			$(chatboxtextarea).val("").focus();  
			// validation paramter 
			if (request.message === "" &&  request.files === ""){
				return false;
			} // validation function 
			if (typeof $.chatConversation !== "function"){
				return false;
			} // set header request before send 
			HTTP.header(() => { 
				$.chatSentHandler(routing, request, { 
					success: (tmessage) => {
						$.chatConversation("sender", {
							message: getXor(tmessage, "m", ""),
							msgid: getXor(tmessage, "n", ""),
							files: getXor(tmessage, "x", false),
							sent: getXor(tmessage, "t", "") 
						}); 
					}
				}); 
			});
			return false;
		} 
	}
	
	/**
	 * @brief chatSentLeaveMessage
	 * @details send Leve Message
	 * @param object "data" paramter to sent 
	 * @retval mixed return value
	 */
	$.chatSentLeaveMessage = function(objects){
		var payload = {}, request = {}, routing = false;
		let notify = {
			buttons: [],
			body: {
				uid: null,
				timeout: 5000,
				counter: 10,
				close:(dispatchEvent) => {
					closeWindow(true);
					window.clearTimeout(notify.body.uid);
					if (typeof dispatchEvent === "function"){
						dispatchEvent.apply(this,[notify.body]);
					} return;
				},
				open: (dispatchEvent) => {
					window.clearTimeout(notify.body.uid);  
					notify.body.uid = window.setTimeout((e) => {
						notify.body.close((e) => {window.setBackFromLeaveMessage()});
					}, notify.body.timeout); 
				}
			}  
		};    
		payload.email = getById("txt_leave_email");
		payload.phone = getById("txt_leave_phone");
		payload.visitor = getById("txt_leave_username");
		payload.message = getById("txt_leave_message");    
		if (payload.visitor.val() === ""){
			payload.visitor.focus();
			return;
		} if (payload.email.val() === ""){
			payload.email.focus();
			return;
		} if (payload.phone.val() === ""){
			payload.phone.focus();
			return;
		} if (payload.message.val() === ""){
			payload.message.focus();
			return;
		}
		request.email = payload.email.val();
		request.phone = payload.phone.val();
		request.visitor = payload.visitor.val();
		request.message = payload.message.val(); 
		HTTP.header((xhr) => { 
			var msgstr = "<p class=\"text-left\">"+ getLang("alert_body_leavemsg_success", "...") +"</p>";
			var routing = HTTP.routing({action: "sendLeaveMessageChat"});
			getAjaxPromise(routing, request).then((res) => {
				if (res.success){
					Confirm(msgstr, getLang("alert_title_leavemsg_success", "..."), 
					null, null, notify.body.open);
					return;
				} 
				Alert(getLang("alert_body_leavemsg_failed", "..."), 
				getLang("alert_title_failed", "..."));  
			})  // on error
			.catch((error) => {
				console.warn("error:", error);  
				Alert(getLang("alert_body_leavemsg_failed", "..."), 
				getLang("alert_title_error", "..."));  
			});
		});  
	}
	
	/**
	 * @brief chatTitle
	 * @details get user start online 
	 * @param object "data" paramter to sent 
	 * @retval mixed return value
	 */
	$.chatTitle = function(events){
		getByClass("chat-agent-statement").show("show", function() {
			var useTo = DBI.visitor.getSession("nick", ""); 
			if (useTo){
				getById("smartagent").html(useTo);
			} if (typeof events === "function"){
				events.apply(this, []);
			} return;
		});
	} 
	 
	/**
	 * @brief chatCleanup
	 * @details get user start online 
	 * @param get from storage session 
	 * @retval mixed return value
	 */
	$.chatCleanup = function(events) { 
		DBI.visitor.removeAll(); 
		if (typeof events === "function"){
			events.apply(this, [events]);
		} return false;
	}
	
	/**
	 * @brief chatCleanSession
	 * @details clean on session only 
	 * @retval mixed return value
	 */
	$.chatCleanSession = function(events) { 
		DBI.visitor.removeSession(events);
		if (typeof events === "function"){
			events.apply(this, [$]);
		} return false;
	}  
	
	/**
	 * @brief chatBackFromLeaveMessage
	 * @details get user start online 
	 * @retval mixed return value
	 */ 
	$.chatBackFromLeaveMessage = function(objects){  
		var chatNotifyContainer = getByClass("message-notice");  // notice agent busy 
		var chatLeaveMessageContainer = getByClass("message-leave"); // leave message form 
		var chatVisitorMessageContainer = getByClass("message-guest"); // login form
		var chatVisitorUpdateContainer = {"leavemsg": 0}; // update leavemsg session 
		/* update session in local visitor then */
		DBI.storage(chatVisitorUpdateContainer).then((resolve) => {
			chatLeaveMessageContainer.hide((event) => {
				chatVisitorMessageContainer.show(chatAnimateTimeout,(event) => { 
					showCopyrightContainer(true);
				});
			}); 	
		}); 
	} 
	
	/**
	 * @brief chatSession
	 * @details get user start online 
	 * @retval mixed return value
	 */ 
	$.chatSignout = function(){ 
		HTTP.header((xhr) => {
			getById("conversation").scrollTop(0);
			getByClass("message-loaders").show( "slow", () => {
				getById("conversation").scrollTop(0);
			}); 
			// request to server destroy my session 
			// update 20240222: ketika logout harus di check sessionnya 
			var request = {}, routing = HTTP.routing({action: "signoutAppChat"});  
			request.uid = DBI.visitor.getSession("session", ""); // session from socket 
			request.from = DBI.visitor.getSession("from", ""); // from visitor 
			request.email = DBI.visitor.getSession("email", ""); // email of visitor 
			request.session = DBI.visitor.getSession("session", ""); // session from visitor 
			request.console = DBI.visitor.getSession("console", 0); // session from visitor 
			getAjaxPromise(routing, request).then((res) => {
				if (typeof res !== "object"){
					return false;
				} if (!getXor(res, "success", 0)){
					return false;
				} 
				// NOTE: 
				// + Jangan memanggil session setelah cleanup !
				// + Cleanup with singgle response context 
				$.chatCleanup((ui) => { 
					consoleEvents.logout(request.console, request);
					if (typeof window.ctmClient !== "object"){
						getByClass("message-loaders").hide("slow", () => {
							window.location.reload();
						});
						return false;
					} window.ctmClient.EndSession((Visitor) => {
						Visitor.Disconnect(() => {
							getByClass("message-loaders").hide(() => { 
								window.location.reload()
							});
						});
					});  
				}); 
			}) // on error
			.catch((error) => {
				console.warn("error:", error);
			}); 
		});
	}
	
	/**
	 * @brief chatSession
	 * @details get user start online 
	 * @retval mixed return value
	 */
	$.chatSession = function(events) {
		var routing = HTTP.routing({action: "sessionAppChat"});
		var request = {};
		request.to =  DBI.visitor.getSession("to");
		request.from = DBI.visitor.getSession("from");
		request.email = DBI.visitor.getSession("email");
		request.session = DBI.visitor.getSession("session"); 
		// on ajax settigs header 
		HTTP.header((xhr) => {	
			getAjaxPromise(routing, request).then((res) => {
				if (typeof events === "function"){
					events.apply(this, [res]);
				}
			}).catch((error) => {
				console.warn("error:", error);
			}); 
		});
	}
	
	/**
	 * @brief chatComment
	 * @details get handler comment if connection is closed 
	 * @retval mixed return value
	 */
	$.chatComment = function(objects, state) {
		/* 0: Error 1: connected /open */
		var replyButton = $(".reply");
		/* socket connected */
		if (typeof objects === "object" &&(state)){
			replyButton.show();
		} /* socket disconnected */
		else if (typeof objects === "object" &&(!state)){
			replyButton.hide();
		} return false;
	} 
	
	/** 
	 * @brief chatWorkFlowInVisitorLogout
	 * @details get user start online 
	 * @retval mixed return value
	 */
	$.chatWorkFlowInVisitorUnregister = function(events) {
		var chatLeaveMessageContainer = getByClass("message-leave");
		var chatVisitorMessageContainer = getByClass("message-guest"); 	
		var chatVisitorLeaveMessagSession = DBI.visitor.getSession("leavemsg", 0);  
		if (!chatVisitorLeaveMessagSession){
			maxScrollerContainer();
			return;
		} if (chatVisitorLeaveMessagSession){ 
			chatVisitorMessageContainer.hide((e) => { 
				chatLeaveMessageContainer.show((e) => {
					getById("txt_leave_username").val(DBI.visitor.getSession("from"));
					getById("txt_leave_phone").val(DBI.visitor.getSession("phone"));
					getById("txt_leave_email").val(DBI.visitor.getSession("email"));
					maxScrollerContainer();
				});
			});
		} return ;
	}
	
	/** 
	 * @brief chatWorkFlowInAgentBusy
	 * @details get user start online 
	 * @retval mixed return value
	 */
	$.chatWorkFlowInAgentBusy = function(events) {
		var chatNotifyContainer = getByClass("message-notice");
		var chatLoaderContainer = getByClass("message-loaders");
		var chatLeaveMessageContainer = getByClass("message-leave");
		var chatVisitorMessageContainer = getByClass("message-guest"); 
		// close notification text 
		if (chatNotifyContainer.length){
			chatNotifyContainer.hide((event) => { 
				if (!getButtonEnabledLeaveMessage()){
					DBI.visitor.removeSession("from"); // clean "from" of visitor
					DBI.visitor.removeSession("email"); // clean "email" of visitor 
				}
			}); 
		} // open send leave message form: 
		if (chatLeaveMessageContainer.length){
			if (getButtonEnabledLeaveMessage()){ 	
				chatLeaveMessageContainer.show(chatAnimateTimeout, () => {
					DBI.visitor.setSession("leavemsg", 1);
					getById("txt_leave_username").val(DBI.visitor.getSession("from"));
					getById("txt_leave_phone").val(DBI.visitor.getSession("phone"));
					getById("txt_leave_email").val(DBI.visitor.getSession("email"));
					chatLoaderContainer.hide((event) => {
						showCopyrightContainer();	
						maxScrollerContainer(); 
					}); 
				});
				return;
			}
		} // check enable or disable pending 
		if (chatVisitorMessageContainer.length){
			chatVisitorMessageContainer.show(chatAnimateTimeout,(event) => {
				DBI.visitor.setSession("leavemsg", 0);
				chatLoaderContainer.hide(() => {
					showCopyrightContainer();
				}); 
			});
		} return;
	}	
		
	/** 
	 * @brief chatOnline
	 * @details get user start online 
	 * @retval mixed return value
	 */
	$.chatOnline = function() { 
		window.searchOnline ++;
		if (window.searchOnline > chatMaxRounting) {
			window.searchOnline = 0;
			window.clearTimeout(window.chatTimeoutInterval);
			window.clearTimeout(window.chatActiveInterval); 
			var session = DBI.visitor.getSession("session", "");
			if (session == '') {
				getById("notice_message").html(getLang("alert_agent_busy", "busy"));
				getByClass("message-loaders").hide(10,() => { 
					getByClass("message-notice").show(chatAnimateTimeout, () => {
						window.setTimeout((event) => {
							$.chatWorkFlowInAgentBusy();
						}, chatBusyTimeout);
					});
				}); 
				return false;
			}
			// jika udah ada session :
			if (typeof $.chatHeartbeat === "function" ){
				getByClass("message-notice").hide();
				getByClass("message-loaders").hide(300, () => { 
					$.chatTitle(); 
				});	
			} 
			return false;
		}
		// get call HTTP.header before send message 
		HTTP.header((xhr) => {  
			/* NOTICE: 
			 * isue user clean window cache on state login then user reload  
			 * browser handler to remove locale cache 
			 */
			$.user.from = DBI.visitor.getSession("from");
			$.user.email = DBI.visitor.getSession("email"); 
			$.user.session = DBI.visitor.getSession("session");
			if ($.user.from === false && $.user.email === false) {
				window.localStorage.removeItem($.config.cookie);
				window.alert("Session storage has been remove !" );
				window.location.reload();
				return;
			} // then sent data Guest  
			var routing = HTTP.routing({action: "routeAppChat"}), request = {}; 
			request.session = $.user.session; // session 
			request.from = $.user.from; // from or visitor
			request.email = $.user.email; // email of visitor
			getAjaxPromise(routing, request).then((response) => {
				/* get error from server */
				var ret = response.hasOwnProperty('data') ? response.data : response;
				if (typeof response !== "object"){
					return;
				} /* if proces callback response is not success */
				if (parseInt(response.success) < 1){
					getByClass("text-loader").html($.chatWrite("Search user online in %s seconds", window.searchOnline));
					window.chatTimeoutInterval = window.setTimeout("$.chatOnline();", $.config.timeone); 
					return;
				}  
				if (ret.to == '') {
					getByClass("text-loader").html($.chatWrite("Search user online %s", window.searchOnline));
					window.chatTimeoutInterval = window.setTimeout("$.chatOnline();", $.config.timeone); 
					return;
				}  
				window.searchOnline = 0; // search routing data  
				showCopyrightContainer(false); // hide routing data 
				DBI.storage({"session": ret.session, "update": ret.update, "from": ret.from, "to": ret.to, "nick": ret.nick, "leavemsg": 0}).then((resolve) => { 
					/* FIXED: bug on lop show */
					window.delayAnimates = 0;
					getById("conversation").addClass("wallpaperBackground"); 
					getByClass("handler-display").show(function() {  
						window.delayAnimates ++;
						if (window.delayAnimates >= 5){
							getByClass("message-loaders").hide(() => { 
								$.chatTitle();
								$.chatHistory();
								UpdateUI(false);
							}); 
						} 
					});	  
					/* FIXED: Add new feturue  */
					var ctm_visitor_session = {
						"call":{}, "sock":{
							"url": DBI.visitor.getWss("url")
						},
						"user": {
							"channel" : DBI.visitor.getSession("cid"),
							"username": DBI.visitor.getSession("from"), 
							"email"   : DBI.visitor.getSession("email")
						} 
					};   
					// console.log(">> ctm_visitor_session: ", ctm_visitor_session);
					/* NOTE: collabs ajax with websocket interface */
					window.sessionStorage.setItem("ctm_visitor_session", JSON.stringify(ctm_visitor_session)); 
					if (window.ctmClient.helper.isFunc(window.ctmClient.Start)){ 
						window.ctmClient.Start((s) => {  
							window.ctmClient.label.state = getById("stateagent");
							window.ctmClient.label.socket = getById("stateagent");
							window.ctmClient.label.typing = getById("ontyping");  
							window.ctmClient.func.comment = $.chatComment;
							window.ctmClient.func.heartbeat = $.chatOnHeartbeat; 
							window.ctmClient.func.completed = $.chatOnCompleted; 
							window.ctmClient.func.deleted = $.chatOnDeleted; 
							window.ctmClient.func.timeout = $.chatOnTimeout;
							
							window.ctmClient.Init((ctm) => {  
								ctm.Utils.loger("Connection initialize on %s", ctm.cfg.sock.url);  
								ctm.Connect((ctm, o) => {  	
									ctm.Utils.loger("Wait Response Connection from %s ...", ctm.cfg.sock.url);
								}); 
							}); 
						});		
					}  
				}); 
				
			}) // if detected error from server 
			.catch((error) => { 
				console.warn("error:", getXor(error, "responseText", "Unknown")); 
				getByClass("text-loader").html("Search user online "+ window.searchOnline);
				window.chatTimeoutInterval = window.setTimeout("$.chatOnline();", $.config.timeone);  
			}); 
		}); 
	} 
	
	/** 
	 * @brief chatStorage
	 * @details get header for new session 
	 * @retval mixed return value
	 */ 
	$.chatStorage = DBI.storage = function(data, events){
		return new Promise((resolve, reject) => {
			let ptr = 0; 
			if (typeof data === "object"){
				for (var x in data){
					if (typeof DBI.visitor === "object"){ 
						DBI.visitor.setSession(x, data[x]);
						ptr++;
					}	
				}
			} if (ptr) {
				resolve(ptr);
			} else {
				reject(new Error("error write session")); 
			} if (typeof events === "function"){
				events.apply(this, [$(this), ptr]);
			}
		}); 
	}
	
	/** 
	 * @brief chatSaving
	 * @details get header for new session 
	 * @retval mixed return value
	 */
	$.chatSaving = function(objects){ 
		var username = getById('inputUsername').val();
		var email = getById('inputEmail').val();	  
		var phone = "";
		if (username == ''){ 
			getById('inputUsername').focus(); 
			return 0;	
		} if (email == '') { 
			getById('inputEmail').focus(); 
			return 0;
		} if ($.checkWithSpace(username)){ 
			getById('inputUsername').focus(); 
			return 0;
		} if ($.checkWithSpace(email)){ 
			getById('inputEmail').focus(); 
			return 0;
		} if (username.length<4) { 
			getById('inputUsername').focus(); 
			return 0;
		} if (email.indexOf('@') <0) { 
			getById('inputEmail').focus();  
			return 0;  
		} if (email.indexOf('.') <0) { 
			getById('inputEmail').focus(); 
			return 0; 
		} /* saving data to window storage from session user */ 
		DBI.storage({"from": username, "email": email, "phone": phone}).then((resolve) => { 
			getByClass("message-guest" ).hide();
			getByClass("message-loaders").show("show", () => {
				$.chatOnline();
			}); 
		});
	} 
	
	/** 
	 * @brief chatVisitorExport
	 * @details get header for new session 
	 * @param none description process here like chat 
	 * @retval mixed return value
	 */
	$.chatVisitorExport = function(events){
		HTTP.header((xhr) => { // set header process is hack process 
			let routing = HTTP.routing({action: "exportVisitorChat"}), request = {};   
			request.session = DBI.visitor.getSession("session");
			request.visitor = DBI.visitor.getSession("from");
			request.email = DBI.visitor.getSession("email"); // sent request to ajax process 
			getAjaxPromise(routing, request).then((response) => {
				if (!response.success){
					Alert(getLang("alert_body_download_nomessage", "..."), 
					getLang("alert_title_info", "..."));
					return; 
				} if (typeof events === "function"){
					events.apply(this, [response.data]);
				} return;
			}).catch((error) => {
				console.warn( "error:", error );
				Alert(getLang("alert_body_download_error", "..."), 
				getLang("alert_title_error", "..."));
				return; 
			}); 
		});
	}
	
	/** 
	 * @brief chatSaving
	 * @details get header for new session 
	 * @retval mixed return value
	 */
	$.chatHistory = function(){ 
		HTTP.header((xhr) => { // ME = FROM |TO : dari 
			var routing = HTTP.routing({action: "historyAppChat"}), request = {}; 
			request.session = DBI.visitor.getSession("session");
			request.email = DBI.visitor.getSession("email");
			request.me = DBI.visitor.getSession("from");
			request.to = DBI.visitor.getSession("to");  // sent request to ajax process 
			getAjaxPromise(routing, request).then((response) => {
				$.each(response.items, (i, msg) => {
					if (typeof msg !== "object"){
						return false;
					} // fix strange ie bug 
					var msgType = "receiver";
					if (msg.f == request.me){ 
						msgType = "sender";
					} // sent box conversation 	
					$.chatConversation(msgType, {
						msgid  : getXor(msg, "n", 0),
						message: getXor(msg, "m", ""),
						files  : getXor(msg, "x", false),
						sent   : getXor(msg, "t", "")
					}); 
				});
			}) // exceptions
			.catch((error) => { 
				console.warn("error:", error);
			}); 
		});  
	}
	
	/** 
	 * @brief chatSaving
	 * @details get header for new session 
	 * @retval mixed return value
	 */
	$.chatOnHeartbeat = function(){ 
		var routing = HTTP.routing({action: "heartbeatAppChat"}), 
		request = {}; // new constructor  
		request.to = DBI.visitor.getSession("to");
		request.from = DBI.visitor.getSession("from");
		request.userid = DBI.visitor.getSession("from");
		request.session = DBI.visitor.getSession("session");
		request.email = DBI.visitor.getSession("email");  
		// get heartbeat from server
		var heartbeat = getAjaxAsync(routing, request); 
		heartbeat.done((response) => {
			$.each(response.items, (i,msg) => {
				// fix strange ie bug 
				if (typeof msg !== "object"){
					return false;
				} // chat outgoing process 
				var msgType = "sender";
				if (msg.f != request.userid){
					msgType = "receiver";
				} // sent to conversation 
				$.chatConversation(msgType, {
					msgid: getXor(msg, "n", 0),
					message: getXor(msg, "m", ""),
					files: getXor(msg, "x", false),
					sent: getXor(msg, "t", ""),
					complete: (type) => { 
						if (getXor(msg, "f", false)){
							$.chatSoundReceiveMessage(msg); 
						}
					}
				});  
			}); 
		});
		// exceptions failed process 
		heartbeat.fail((error) => {
			console.warn("error:", error);
		}); 
	}
	
	/** 
	 * @brief chatOnTimeout
	 * @details get event timeout visitor not interactive 
     * with agent in long time wait 	 
	 * @retval mixed return value
	 */
	$.chatOnTimeout = function(ctm, stream) {
		var timeoutVisitor = DBI.visitor.getSession("timeout", 0);
		if (typeof stream !== "object") {
			return;
		} if (timeoutVisitor > 0){
			return;
		} /* then show message sent to visitor */
		let handler = {
			buttons: [],
			timeouts: {
				uid_timeout: null,
				max_timeout: chatTickTimeout,
				max_counter: chatMaxTimeout,
				close:(event) => {
					closeWindow(true); // close all window
					DBI.visitor.setSession("timeout", 0);// handler dont show 
					window.clearInterval(handler.timeouts.uid_timeout);
					if (typeof event === "function"){
						event.apply(this,[handler.timeouts]);
					} return;
				},
				open: (event) => {
					DBI.visitor.setSession("timeout", stream.data.timeout); // to default 
					window.clearInterval(handler.timeouts.uid_timeout);
					getByClass("ui-dialog-buttonset").css({"text-align": "center"});
					getById("dialogArgumentsTimer").html(handler.timeouts.max_counter);
					handler.timeouts.uid_timeout = window.setInterval((e) => {
						getById("dialogArgumentsTimer").html(handler.timeouts.max_counter);
						handler.timeouts.max_counter = handler.timeouts.max_counter - 1;
						if (handler.timeouts.max_counter < 0){
							handler.timeouts.close((e) => {
								$.chatSignout();
							});
						} return;
					}, handler.timeouts.max_timeout); 
				}
			}  
		}; 
		handler.buttons.push({
			text: getLang( "alert_button_ok" ),
			click: (event) => {
				try {
					handler.timeouts.close((e) => {  
						window.location.reload();
					}); 
				} catch(e) {};
			}
		});  
		timeoutTemplate((message) => {
			Confirm(message, getLang("alert_title_warning"), handler.buttons, null, handler.timeouts.open);
		});
		return;  
	}
	
	/** 
	 * @brief chatOnCompleted
	 * @details get event completed message from agent 
	 * @retval mixed return value
	 */
 	$.chatOnCompleted = function(ctm, stream) {
		if (typeof stream !== "object"){
			return;
		}  
		var request = stream.data;
		if (!request.hasOwnProperty("complete")){
			ctm.Utils.loger("Complete Message Session: %s", request.session);
			request.complete = function(event){
				getById("chatcontiner").block(blockingUI.content);
				window.setTimeout((e) => {
					getById("chatcontiner").unblock();
					$.chatSentExpired();
				},5000);
			}
		} if (typeof $.chatConversation === "function"){
			$.chatConversation("completed", request); 
		} return 0;
	}
	
	/** 
	 * @brief chatOnDeleted
	 * @details get event delete from agent 
	 * @retval mixed return value
	 */
	 $.chatOnDeleted = function(ctm, stream) {	 
		try {  
			ctm.Utils.loger("Delete Message ID: %s", stream.data.msgid);
			getById("message-body-"+ stream.data.msgid).hide();
		} catch(e){ console.warn("chatOnDeleted.error: ", e);}
		return 0;
	}  
	
	/** 
	 * @brief chatConsoleLogin
	 * @details request login by send message window
	 * @retval mixed return value
	 */
	$.chatConsoleLogin = function(cli, response){   
		// check if leavemsg == 1 
		var leaveMessageState = DBI.visitor.getSession("leavemsg", 0);
		if (leaveMessageState > 0){
			window.ctmClient.Utils.loger("Leave Message Active");
			return 0;
		} // validation 
		if (response.data.email === ""){ 
			response.message = getLang("console_error_email", "..");
			cli.send(response);
			return false;
		} // validation username 
		if (response.data.username === ""){ 
			response.message = getLang("console_error_username", "..");
			cli.send(response);
			return false;
		} // check session is still running 
		var consoleVisitorSession = DBI.visitor.getSession("session", ""); 
		if (consoleVisitorSession !== ""){ 
			response.message = getLang("console_error_session", ".."); 
			cli.send(response);
			return false;
		} // if proces success test to login
		showCopyrightContainer(false); /* hide copy right */
		DBI.visitor.setSession("console", cli.state);  
		window.setTimeout((e) => { 
			DBI.storage({"from": response.data.username, "email": response.data.email}).then((resolve) => {
				console.log(resolve);
				if (resolve){
					getByClass("message-guest" ).hide();
					getByClass("message-loaders").show("show", () => {
						$.chatOnline();
					});
				}
			}); 
		}, cli.timeout);
		return;
	} 
 
})(jQuery);

/** ================================================================
 *  Kawani -- An open source Smart Chat Public toolkit.
 *
 *  Copyright (C) 2019 - 2023, (PDS) Peranti Digital Solusindo, PT.
 *
 *  OMEN <jombi.php@gmail.com>
 *
 *  See http://perantidigital.co.id for more information about
 *  the Asterisk project. Please do not directly contact
 *  any of the maintainers of this project for assistance;
 *  the project provides a web site, mailing lists and IRC
 *  channels for your use.
 *
 *  This program is free software, distributed under the terms of
 *  the GNU General Public License Version 2. See the LICENSE file
 *  at the top of the source tree.
 */

/** 
 * @brief chatSaving
 * @details get header for new session 
 * @retval mixed return value
 */
 function notTyping() 
{
	ontype = 0;
	if (!ontype ){
		$.chatSentTyping(0);
	} return false;
}

/** 
 * @brief isTyping
 * @details Detect User typing from textarea 
 * @retval mixed return value
 */ 
 function isTyping() 
{
	window.clearTimeout(timer);
	var value = getById("comment").val();
	if (value !== ""){
		ontype ++;
		if (ontype == 1){  
			$.chatSentTyping(1)
		}
		timer = window.setTimeout(notTyping, delay);
		return 0;
	} else {
		notTyping();
	} return 0; 
} 

/* =============================================================================================================================
 * Window Document Ready
 * =============================================================================================================================
 */
$(document).ready(function(){
	setStylesIframe();
	start((desktop, body) => { // call init program && default hidden 
		getByObj(body).find(".message-notice").hide(); 
		getByObj(body).find(".handler-display").hide(); 
		$.chatConstruct((visitor) => { 
			getByClass("message-loaders").hide(() => {
				window.clearTimeout(window.chatHeartbeatInterval);
				window.clearTimeout(window.chatActiveInterval);
			});   
			/* public: start for session */
			var visitorTransport ={}, displayTransport = 0; 
			$.chatSession((transport) => {   
				$.chatCleanSession((ui) => {
					window.ctmClient.Utils.loger("Clear Session");  
				});  
				/* get session error */
				if (typeof transport !== "object" || (parseInt(transport.success) <= 0)){
					$.chatWorkFlowInVisitorUnregister();
					return false;
				} /* get data session from here */  
				showCopyrightContainer(false); /* hide copy right */ 
				visitorTransport.leavemsg = getXor(transport.data, "leavemsg", 0); /* if define of leave message */
				visitorTransport.session = getXor(transport.data, "session", ""); /* if get session success */
				visitorTransport.phone = getXor(transport.data, "phone", ""); /* if field contains phone number */
				visitorTransport.email = getXor(transport.data, "email", ""); /* if field contains email */
				visitorTransport.from = getXor(transport.data, "from", ""); /* this field is username */
				visitorTransport.to = getXor(transport.data, "to", ""); /* this field of agents 	*/
				
				// DBI.storage2
				DBI.storage(visitorTransport).then((ret) => {
					getByClass("message-leave").hide();
					getByClass("message-guest").hide(() => {  
						getByClass("message-loaders").show("show", () => { 
							if (displayTransport <1){
								displayTransport = displayTransport +1;
								$.chatOnline(); 
							}
						});	 
					});  
				}); 
				return false;
			});  
		}); 
		/* consoleEvents: message sent ready */
		consoleEvents.ready({});
	});  
});  

/* =============================================================================================================================
 * Window Document resize
 * =============================================================================================================================
 */
window._timeout = null;  
$(window).resize(function(e){ 
	window.clearTimeout(window._timeout);
	window._timeout = window.setTimeout(() => {
		getById("conversation").scrollTop(getById("conversation")[0].scrollHeight);
		window.resizeExpresion();
	}, 500); 
}); 
 
/* =============================================================================================================================
 * Window Disable Contextmenu 
 * =============================================================================================================================
 */
$(document)
.on("contextmenu", ".chat-front-logo-content", (e) => {
	if (!enableButtons.development){
		e.preventDefault();  
	}
})
.on("contextmenu", ".chat-front-form", (e) => {
	if (!enableButtons.development){
		e.preventDefault();  
	}  
})
.on("contextmenu", ".heading", (e) => {
	if (!enableButtons.development){
		e.preventDefault();  
	} 
})
.on("contextmenu", ".menutoolbar", (e) => {
	if (!enableButtons.development){
		e.preventDefault();  
	}
});

/* =============================================================================================================================
 * https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage
 * get listen object "message"
 * =============================================================================================================================
 */
window.addEventListener("message", function(event){
	try { 
		var SIGNAL = JSON.parse(event.data);
		window.ctmClient.Utils.loger("Child Got Message from Parent");
		if (SIGNAL === null) {
			return;
		} if (SIGNAL.login){
			consoleEvents.login(SIGNAL.data, (cli, events) => {
				$.chatConsoleLogin(cli, events);
			});
		} return; 
	} 
	catch (e){ console.warn("error:", e)}  
});
/* CTM: end of program moduls */