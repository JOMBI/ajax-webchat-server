/*!
 * Kawani -- An open source Smart Chat Public toolkit.
 *
 * Copyright (C) 2019 - 2023, (PDS) Peranti Digital Solusindo, PT.
 *
 * OMEN <jombi.php@gmail.com>
 *
 * See http://perantidigital.co.id for more information about
 * the Asterisk project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * <BEGIN>
 * @version 4.5
 * + $.revision 2023/12/11 00:00
 * + $.revision 2024/07/23 00:00
 * + $.revision 2024/07/24 00:00
 * + $.revision 2024/07/25 00:00
 * </BEGIN>
 *
 */  
 
/* =============================================================================================================================
 * Default config 
 * =============================================================================================================================
 */
let Utils = {}; 
let DBI = null;
let HTTP = null;  
let windowObj =  null;
let confirmObj = null;
let promptObj = null;
let alertObj = null;
let menuObj = null;
let menuCss = {
	min: 450, offset: 25, timeout: 50,
	expression : {
		"min-width": "90%",
		"max-width": "100%",
		"left": "0px", 
		"background-color": "#ffffff",
		"border-bottom": "0px solid #eee",
		"padding": "5px"
	}	
};
let enableLeaveMessage = false; // leave message if all user off 
let authorization = {}; /* config authorization */ 
let enableButtons = {}; /* global enable buttons */
let disableFeature = {}; /* disabled button on tolbar bottom per client */  
let imageSettings = {};  /* custom image settings on body chat */
let subscribes = {};  /* get client session */ 
let hostConfigs = {
	/*
	"development": {
        "directory": "smartchat-prod",
        "sock": "ws://192.168.10.236:18080/visitor/"
    },
    "production": {
		"directory": "",
        "sock": "wss://smartchat.visitor.peranti.solutions/"
    } */
}; /* host config websocket chat visitor */
let blockingUI = {
	timeout: 300,
	range: {start: 0, stop: 100},
	content: {
		message: "<h1><span class=\"fa fa-circle-o-notch fa-spin\"></span>"+
		"<span id=\"contentBlockingUI\" class=\"contentBlockingUI\"></span></h1>",
		css: {
			border: '0px solid #222', 
			backgroundColor: 'none', 
			'-webkit-border-radius': '10px', 
			'-moz-border-radius': '10px', 
			opacity: .8, 
			color: '#fff'
		}
	}
};
let MicrophoneRecognition = null; 
let consoleEvents = null;
let cameraTakePhoto = null;
let canvasTakePhoto = null;
let captureTakePhoto = null;
let streamTakePhoto = {width:320, height: 0, stream: false};
let constrainsTakePhoto = {video: true, audio: false}; 

	
	
/* =============================================================================================================================
 * DBI: moduls interface, Call This moduls in Program not for desktop 
 * =============================================================================================================================
 */
DBI = {
	lites: {},
	storage: {},
	visitor: {}, 
	desktop: {
		cookie: function(defaults){
			if (typeof defaults === "undefined"){
				defaults =  false;
			} try {
				var cookie = getWebhookSession(), cookies = window.localStorage.getItem(cookie);
				if (cookies === null ){
					return defaults;
				} cookies = JSON.parse(cookies);
				return cookies;
			} catch(e){
				return defaults;
			}
		}, 
		config: function(){
			var cookies = DBI.desktop.cookie(); 
			if (typeof cookies !== "object"){
				return {};
			} return getXor(cookies, "config", {});
		}
	}  
};
  
/* =============================================================================================================================
 * HTTP: moduls interface, call in program not in desktop 
 * ==============================================================================================================================
 */
HTTP = { 
	cookie: false, 
	index: function() { 
		HTTP.cookie = DBI.desktop.config();  
		if (HTTP.cookie.hasOwnProperty("api")){
			return getWebhookUrl();
		} return getXor(HTTP.cookie.api, "url", getWebhookUrl()); 
	},
	header: function(event){
		var headers = {}; /* set global header variabels */ 
		if (typeof DBI.visitor === "object"){
			headers["Authorization"] = getBinaryString(DBI.visitor.getApi("auth"));
			headers["ApiKey"] = getBinaryString(DBI.visitor.getApi("key"));
		} setAjaxHeader(headers, event);
	},
	compile: function(request){  
		let compiler = new URL(HTTP.index());
		compiler.search = new URLSearchParams(request).toString();
		return compiler.toString();
	},
	routing: function(objects){
		let builder = {}, requestor = "", ptr = 0;
		if (typeof objects !== "object"){
			objects = {};
		} for (var x in objects){
			builder[x] = objects[x];
			ptr ++; 
		} requestor = HTTP.compile(builder); 
		return encodeURI(requestor); 
	},
	post: function(key, value){}
};  

/* =============================================================================================================================
 * Console: Moduls interface for window event message child & parent "CORS"
 * =============================================================================================================================
 */
consoleEvents = {
	state: 1, // state of ok 
	timeout: 1000, // wait auto login in seconds 
	send: function(message){ 
		if (typeof message !== "object"){
			window.ctmClient.Utils.loger("Child Send: invalid message");
			return 0;
		} try {
			var msgstr = JSON.stringify(message);
			window.parent.postMessage(msgstr, "*");
		} catch(e){}
	},
	ready: function(){
		if (!getButtonEnabledConsole()){
			return false;
		} // then sen t parent from child 
		window.ctmClient.Utils.loger("Child Sent Ready State"); 
		var response = {ready: true, error: 0, message: "success", data:{}};  
		response.data = typeof data === "object" ? data: {};
		consoleEvents.send(response);
		return ;
	}, 
	notify: function(enabled, data){
		if (!enabled){
			return false;
		} if (!getButtonEnabledConsole()){
			return false;
		} // then sen t parent from child 
		window.ctmClient.Utils.loger("Child Sent Notify"); 
		var response = {notify: true, error: 0, message: "success", data:{}};  
		response.data = typeof data === "object" ? data: {};
		consoleEvents.send(response);
		return ;
	},
	login: function(data, callback){
		window.ctmClient.Utils.loger("Child Parent Request Login"); 
		var response = {login: true, error: 1, message: "failed", data:{email: "", username: ""}};
		response.data.email = getXor(data, "email", "");
		response.data.username = getXor(data, "username", ""); 
		if (!getButtonEnabledConsole()){
			response.message = getLang("console_error_moduls", "..");
			consoleEvents.send(response);
			return false;
		} if (typeof callback === "function"){
			callback.apply(this, [consoleEvents, response]);
		} return;
	},
	logout: function(enabled, data){
		if (!enabled) {
			return false;
		} if (!getButtonEnabledConsole()){
			return false;
		} // then sen t parent from child 
		window.ctmClient.Utils.loger("Child Sent Request Logout"); 
		var response = {logout: true, error: 0, message: "success", data:{email: false, username: false}};
		response.data.email = getXor(data, "email", "");
		response.data.username = getXor(data, "from", "");
		consoleEvents.send(response);
		return ;
	}
}; 

/* =============================================================================================================================
 * Program utilities 
 * =============================================================================================================================
 */

// http://www.navioo.com/javascript/tutorials/Javascript_microtime_1583.html
function strtotime(get_as_float) {  
    // Returns either a string or a float containing the current time in seconds and microseconds    
    //   
    // version: 812.316  
    // discuss at: http://phpjs.org/functions/microtime  
    // +   original by: Paulo Ricardo F. Santos  
    // *     example 1: timeStamp = microtime(true);  
    // *     results 1: timeStamp > 1000000000 && timeStamp < 2000000000  
    var now = new Date().getTime() / 1000;  
    var sec = parseInt(now);  
    var str = (get_as_float) ? now : (((now - sec) * 1000)/1000).toFixed(8) + ' ' + sec;  
	return str;
	
	// return str.toString().replace(/\./g,'');
}  

function getUniqueID(){
	var buildstr = strtotime(true);
	return buildstr.toString().replace(/\./g, "");
}
 
function format(strings){
	for (var i=1; i < arguments.length; i++) {
		strings = strings.replace(/%s/, arguments[i] );
	} return strings;
}  

function getLibUrl(){ 
	var subdirectory = window.location.pathname, libUrl = "", directory = ""; 
	if (subdirectory !== ""){
		var objectstr = subdirectory.substr(1, subdirectory.length);
		if (objectstr.indexOf("/") != -1){
			directory = objectstr.substr(0, objectstr.indexOf("/"));
			if (directory.indexOf(".") < 0){
				directory = "/"+ directory;
			} else {
				directory = "";
			}
		} 
	} 
	libUrl = window.location.protocol +"//"+ window.location.host + directory; 
	return libUrl;
}

function getLibDirectory(){
	var libDirectory = getLibUrl();
	if (enableButtons.development){
		subpath = getXor(hostConfigs.development, "directory", "");
		if (getXor(hostConfigs.development, "directory", "") !== ""){
			libDirectory = libDirectory + "/" + getXor(hostConfigs.development, "directory", ""); 
		} else if (getXor(hostConfigs.development, "directory", "") === ""){
			libDirectory = libDirectory;
		}
	} else if (!enableButtons.development){
		if (getXor(hostConfigs.production, "directory", "") !== ""){
			libDirectory = libDirectory + "/" + getXor(hostConfigs.production, "directory", ""); 
		} else if (getXor(hostConfigs.production, "directory", "") === ""){
			libDirectory = libDirectory;
		}  
	} return libDirectory;
} 

function getLibAssets(){ 
	var libAssets = getLibDirectory() +"/library";
	return libAssets;
} 

function getLibStyles(){ 
	var libStyles = getLibAssets() +"/styles";
	return libStyles;
} 

function getLibService(){ 
	var libStyle = getLibStyles() +"/service";
	return libStyle;
}  

function getImage(str){
	var libDirectoryImage = getLibService() +"/images";
	var libDirectoryImageUrl = libDirectoryImage + "/"+ str;
	return libDirectoryImageUrl;
}

function getBinaryString(str){  
	if (!str){
		return "";
	} if (str.indexOf('\\x') < 0){
		return str;
	} var s = str.split('\\x'), messageStr = "";
	for(var x in s){
		if (s[x] == '13') continue;
		else if (s[x] == '10') continue;
		else if (s[x] != '') {
			messageStr += String.fromCharCode(s[x]);
		}
	} return messageStr.toString();
}

function getStringBinary(str){ 
	var obj = str.split(""), bytes= [], messageStr = "";
	for(var x = 0; x < obj.length; x++){
		bytes.push(str.charCodeAt(x)); 
	} if (bytes.length > 1){
		messageStr = bytes.join('\\x');
	} return messageStr.toString();
}

function getSession(str, defaults){
	var retstr = getDbJson($.config.cookie, "{}");
	if (typeof retstr === "object" && retstr.hasOwnProperty(str)){
		return retstr[str];
	} return defaults;
}

 
function getByClass(str){
	return $("."+ str);
} 

function getByObj(obj){
	return $(obj);
} 

function getById(str){
	return $("#"+ str);
} 

function getDisplay(objects){
	var display = getById(objects);
	if (display.length <= 0){
		return false;
	} if (display.css( "display" ) === "none"){
		return false;
	} return display;
} 

function getIsLogin(str, defaults){
	var retstr = getSession("session", {});
	if (typeof retstr === "object" && retstr.hasOwnProperty(str)){
		return retstr[str];
	} return defaults;
}

function getWebsockUrl(){
	var retobj = enableButtons.development ? getXor(hostConfigs,"development",{}) : getXor(hostConfigs,"production",{});
	return getXor(retobj,"sock",""); 
} 

function getWebhookUrl(){
	var retstr = getLibDirectory() + "/index.php/Chat/index/";
	return retstr; 
}

function getWebhookIndex(){
	var retstr = getLibDirectory() + "/index.php";
	return retstr; 
} 

function getWebhookDownload(){ 
	var retstr = getWebhookIndex() + "/Document/file/?action=download";
	return retstr; 
}  

function getWebhookPreview(){ 
	var retstr = getWebhookIndex() + "/Document/file/?action=preview";
	return retstr; 
}  
  
function getWebhookModuls(moduls){ 
	var retstr = getWebhookIndex() + moduls;
	return retstr; 
}   

function getWebhookAuth(decrypts){ 
	var ret = false, clientid = getParam("cid");
	decrypts = typeof decrypts !== "undefined" ? decrypts : true;
	if (getXor(subscribes, "a", false)){
		ret = decrypts ? getBinaryString(getXor(subscribes, "a", "")) : getXor(subscribes, "a", "");
	} return ret; 
}

function getWebhookToken(decrypts){
	var ret = false, clientid = getParam("cid");
	decrypts = typeof decrypts !== "undefined" ? decrypts : true;
	if (getXor(subscribes, "t", false)){
		ret = decrypts ? getBinaryString(getXor(subscribes, "t", "")) : getXor(subscribes, "t", "");
	} return ret;  
}

function getWebhookSession(){  
	var ret = false, clientid = getParam("cid"); 
	if (getXor(subscribes, "s", false)){ 
		ret = getBinaryString(getXor(subscribes, "s", "")); 
	} return ret;  
}

function getWebhookFiles(){ 
	var ret = false, clientid = getParam("cid"); 
	if (getXor(subscribes, "f", false)){
		ret = getBinaryString(getXor(subscribes, "f", ""));  
	} return ret;  
}

function getSound(str){
	var libDirectorySound = getLibAssets() +"/sound";
	var libDirectorySoundUrl = libDirectorySound + "/"+ str;
	return libDirectorySoundUrl;
} 

function getLang(str, defaults){
	window.languages = {
		"alert_notice_refresh": "Are you sure, Refresh this window ?",
		"alert_notice_logout": "Are you sure, Logout from this session ?",
		"alert_agent_busy": "Maaf, Saat Ini Operator Kami Tidak Dapat Melayani Anda, Silahkan Coba Beberapa Saat lagi. Terimakasih",	
		"alert_end_session": "has ended the conversation session, do you want to start again ?",
		"alert_fail_res_message": "Failed, Response Message",
		"alert_fail_res_session": "Failed, Response Session",
		"alert_fail_res_object": "Failed, Response Object",
		"label_title_leave_message": "Leave Your Message",
		"label_username": "Your Name",
		"label_email": "Your Email Address",
		"label_phone": "Your Phone Number",
		"label_mobile_phone": "Mobile Phone",
		"label_message": "Message",
		"label_leave_message": "Leave Message", 
		"placeholder_username": "Visitor",
		"placeholder_email": "visitor@your.domain.com",
		"placeholder_textmessage": "Type your message here...", 
		"placeholder_mobile_phone": "0813-1234-5678", 
		"title_exit_from_session": "Exit from session",
		"title_audio_call": "Audio Call",
		"title_video_call": "Video Call",
		"title_return_main_page": "Return to the main page",
		"title_stop_speech_recognition": "Stop Speech Recognition",
		"error_bigest_file_upload": "Sorry, the uploaded file may be too large, try another file !",
		"button_leave_message": "Send Message", 
		"menu_item_cancel": "Cancel",
		"menu_item_expresion": "Select Expresion",
		"menu_item_fileshare": "Share File",
		"menu_item_takephoto": "Take / Photo", 
		"menu_item_dictate" : "Dictate Message", 
		"menu_item_refresh": "Refresh Window",
		"menu_item_export": "Export Mssage",
		"menu_item_logout": "Logout Session", 
		"menu_item_download": "Download",
		"menu_item_playing_audio": "Play Audio",
		"menu_item_playing_video": "Play Video",
		"menu_item_preview": "Preview",
		"menu_item_deleted": "Delete",
		"menu_item_image_show": "Show Picture",
		"menu_item_image_close": "Close Picture", 
		"menu_item_close_audio": "Close Audio",
		"menu_item_close_video": "Close Video", 
		"alert_button_ok": "OK",
		"alert_title_info": "Information",
		"alert_button_cancel": "Cancel", 
		"alert_title_failed": "Failed",
		"alert_title_error": "Error",
		"alert_title_warning": "Warning",
		"alert_title_confirm": "Confirmation",
		"alert_title_notice": "Notification",
		"alert_title_success": "Successfully",	
		"alert_title_leavemsg_success": "Message Sent", 
		"alert_body_download_error": "Sorry, Error download",
		"alert_body_download_nomessage": "Sorry, No Message to export",
		"alert_body_construction": "Under Construction",	
		"alert_body_leavemsg_success": "We will contact you soon, Thank You 🙏", 
		"alert_body_leavemsg_failed": "Sorry, Message failed to send.", 
		"delete_chat_message": "Message",
		"speech_recognition_notsupport" : "Sorry, Your browser does not support this feature",
		"speech_recognition_title" : "Speech Recognition",
		"speech_recognition_listening" : "I'm listening ...",  
		"media_device_title" : "Media Devices",
		"media_device_error" : "An error occurred: getUserMedia()",
		"media_device_notsupport" : "Sorry, Your browser does not support this feature", 
		"console_error_email": "Incorrect Email Address",
		"console_error_username": "Incorrect Username", 
		"console_error_session": "Session Is Alerdy Exist", 
		"console_error_moduls": "Console Disabled",  
		"button_login": "Chat",
		"bytes" : "Bytes",
		"kb" : "KB",
		"mb" : "MB",
		"gb" : "GB",
		"tb" : "TB",
		"pb" : "PB",
		"eb" : "EB",
		"zb" : "ZB",
		"yb" : "YB"
	};
	
	if (window.languages.hasOwnProperty(str)){
		return window.languages[str];
	} return defaults;
} 
 
function getParam(str, defaults){
	let params = new URLSearchParams(document.location.search);
	var retstr = params.get(str);
	if (retstr === null){
		if (typeof defaults !== "undefined"){
			return defaults;
		} return "";
	} return retstr;
}

function getClient(str, defaults){
	var retstr = getSession("session", false); 
	if (typeof retstr === "object" && retstr.hasOwnProperty(str)){
		return retstr[str];
	} return defaults;
} 
// collects[["download", "deleted", "preview", "playing"]]
function getDropdownMessage(collects) {
	var client = getParam("cid"), enableDropdownMessage = 0, enableDropdownMessageItem = false;
	if (!disableFeature.hasOwnProperty(client)){
		return false;
	} enableDropdownMessageItem = disableFeature[client] !== null ? disableFeature[client]: false;
	if (!enableDropdownMessageItem) return 0;	
	for (var f in enableDropdownMessageItem){
		var buttons = enableDropdownMessageItem[f];
		if (typeof buttons !== "object") continue;  
		if ($.inArray(f, collects) >= 0){
			if (!buttons.disabled){
				enableDropdownMessage ++;
			}
		} 
	} return enableDropdownMessage; 
} 

 
function getDbItem(str, defaults){
	var retstr = window.localStorage.getItem(str); 
	if (retstr === null || typeof retstr === "undefined"){
		if (typeof defaults !== "undefined"){
			return defaults;
		} return "";
	} return retstr; 
} 

function getDbJson(str, defaults){
	var retstr = getDbItem(str, defaults);
	if (retstr !== null){
		retstr = JSON.parse(retstr);
	} return retstr; 
}
 
function getFormatBytes(bytes, decimals){
	if (bytes === 0) return "0 "+ getLang("bytes");
    var k = 1024;
    var dm = (decimals && decimals >= 0)? decimals : 2;
    var sizes = [getLang("bytes"), getLang("kb"), getLang("mb"), getLang("gb"), getLang("tb"), getLang("pb"), getLang("eb"), getLang("zb"), getLang("yb")];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}

function getXor(obj, str, defaults){
	if (typeof defaults === "undefined"){
		defaults = "";
	} if (typeof obj !== "object"){
		return defaults;
	} if (obj.hasOwnProperty(str)){
		return obj[str];
	} return defaults;
}

 
function getButtonEnabledFeture(client, str, callback){
	var defaults = false;
	while (disableFeature.hasOwnProperty(client)){ 
		var fetureButtons = disableFeature[client] !== null ? disableFeature[client]: false;
		if (typeof fetureButtons !== "object"){
			return false;
		} 
		defaults = fetureButtons.hasOwnProperty(str) ? fetureButtons[str]: false;
		if (typeof defaults !== "object"){
			return false;
		} break;
	} 
	if (!defaults){
		return false;
	} if (defaults.disabled){
		return false;
	} if (typeof callback === "function"){
		callback.apply(this, [defaults]);
	} return defaults; 
}

// =====================================================================
// ==== start: enable button  ==========================================
// =====================================================================
function getButtonEnabledConsole(e){
	if (!getXor(enableButtons, "console", false)){
		return false;
	} return getButtonEnabledFeture(getParam("cid"), "console", e);
} 

function getButtonEnabledLeaveMessage(e){
	if (!getXor(enableButtons, "leavemsg", false)){
		return false;
	} return getButtonEnabledFeture(getParam("cid"), "leavemsg", e);
}

// =====================================================================
// ==== end: enable button  ============================================
// =====================================================================

function getImageSettings(client, callback){
	var defaults = {css: "chat-front-logo", img: "livechat.jpg"}, newimage = getXor(imageSettings, client, defaults);
	if (typeof callback === "function"){
		callback.apply(this, [newimage]);
	} return newimage;
	
}

function getAjaxPromise (routing, request){
	return new Promise((resolve, reject) => {
		$.ajax({
			url: routing,
			crossDomain:true,
			dataType: "json",
			cache: false,
			type: "POST",
			data: request,
			success: (response) => {
				resolve(response);
			},
			error: (error) => {
				reject(error);
			}
		});
	}); 
} 
	
function getAjaxAsync(routing, request){		
	return $.ajax({
		url: routing,
		crossDomain: true,
		cache: false,
		type: "POST",
		dataType: 'json',
		data: request
	}); 
} 

function getRegexString(str){
	if (typeof str === "undefined" || str === "object"){
		return "";
	} // get string only 
	str = str.replace(/\"/g,"&quot;"); 
	str = str.replace(/\\n/g, "<br>");
	return str;
} 

function getBoldString(str){
	var start = str.indexOf("*") +1, end = str.lastIndexOf("*"),
	value = str.substring(start, end);
	if (value.length > 0){
		var newstr = $.chatWrite("*%s*", value);
		str = str.replace(newstr, "<b>"+ value +"</b>");
	} return str;
}
 
function setAjaxHeader(headers, hook){	
	var settings = {
		crossDomain: true, 
		beforeSend: (xhr) => {  
			$.each(headers, (key, value) => {
				xhr.setRequestHeader(key, value);
			}); 
		}
	}; 
	$.ajaxSetup(settings); 
	if (typeof hook === "function"){
		hook.apply(this, [$]);
	} return false;
}

function setDbItem(str, value){
	window.localStorage.setItem(str, value);
}

function setStylesIframe(){
	var contentStyleSheetsUrl = getLibService() +"/default/iframe.css?time=20240327"; 
	if (getById("colorStyleScheme").length <= 0){
		$("head").append("<link rel='stylesheet' id='colorStyleScheme'/>");
	} if (getById("colorStyleScheme").length > 0){ 
		getById("colorStyleScheme").attr("href", contentStyleSheetsUrl);
	} 
} 
 
function setStyles(sheet){ 
	var styleSchemeColor = document.createElement("style");
	styleSchemeColor.id = sheet.message.id;
	styleSchemeColor.textContent = sheet.message.body; 	
	$(document)[0].head.appendChild(styleSchemeColor);  
} 

/* =============================================================================================================================
 * Popup Menu 
 * ============================================================================================================================= 
 */ 
function hidePopupMenu(timeout) {
    if (timeout){
        window.setTimeout(() => {
            if (menuObj != null){
                menuObj.menu("destroy");
                try{
                    menuObj.empty();
                } catch(e){}
                try {
                    menuObj.remove();
                } catch(e){}
                menuObj = null;
            }
        }, timeout);
    } else {
        if (menuObj != null){
            menuObj.menu("destroy");
            try {
                menuObj.empty();
            } catch(e){}
			
            try {
                menuObj.remove();
            } catch(e){}
            menuObj = null;
        }
    }
}

function closePopupMenu(hooks){
	if (menuObj != null){
        menuObj.menu("destroy");
        try{
            menuObj.empty();
        } catch (e){}
		
        try {
            menuObj.remove();
        } catch(e){}
        menuObj = null;
    } 
	if (typeof hooks === "function"){
		hooks.apply(this, []);
	} return;
}

function openPopupMenu(obj, menu){ 
    if (menuObj != null){
        menuObj.menu("destroy");
        menuObj.empty();
        menuObj.remove();
        menuObj = null;
    };
	// check this context menu Or display  
	var x = 0, y = 0, w = 0, h = 0, contextmenu = 0; 
	if (typeof obj=== "object" && obj.hasOwnProperty( "originalEvent" )){ 
		contextmenu ++;
	} else {
		x = $(obj).offset().left - $(document).scrollLeft();
		y = $(obj).offset().top - $(document).scrollTop();
		w = $(obj).outerWidth();
		h = $(obj).outerHeight();
	}
	menuObj = $("<ul></ul>");
    if (menu && menu.items){
        $.each(menu.items, (i, item) => {
            var header = (item.isHeader == true)? " class=\"ui-widget-header\"" : "";
            var disabled = (item.isDisabled == true)? " class=\"ui-state-disabled\"" : "";
            if (item.icon != null){
                menuObj.append("<li value=\""+ item.value +"\" text=\""+ item.text +"\" "+ header +" "+ disabled +"><div><span class=\""+ item.icon +" ui-icon\"></span>&nbsp;"+ item.text +"</div></li>");
            } else {
                menuObj.append("<li value=\""+ item.value +"\" text=\""+ item.text +"\" "+ header +" "+ disabled +"><div>&nbsp;"+ item.text +"</div></li>");
            }
        });
    }
    menuObj.append("<li><div>-</div></li>");
    menuObj.append("<li><div style=\"text-align:center; padding-right: 2em\">"+ getLang("menu_item_cancel", "") +"</div></li>");
	menuObj.appendTo(document.body);  // Attach UL to body
    menuObj.menu({}); // Create Menu
 
    // Event wireup
    if (menu && menu.selectEvent){
        menuObj.on("menuselect", menu.selectEvent);
    } if (menu && menu.createEvent){
        menuObj.on("menucreate", menu.createEvent);
    }
    menuObj.on('blur',function(){
		// HidePopup();
    });
    if (menu && menu.autoFocus == true) {
		menuObj.focus();
	} // Final Positions
    var menuWidth = menuObj.outerWidth(); 
    var left = x-((menuWidth/2)-(w/2)); 
    if (left + menuWidth + 10 > window.innerWidth){
        left = window.innerWidth - menuWidth - 10;
    }
	if (Math.round(left) < 0) left = 5;
	if (contextmenu){   
		// reposition if false with document 
		if ((window.innerWidth - event.pageX) < menuWidth) {
			left =  event.pageX - menuWidth; // turn right 
		} else { 
			left = event.pageX; // turn left
		} menuObj.css("left", left  +"px"); 
	} else if (!contextmenu){
		menuObj.css("left", left +"px");
	}  
    var menuHeight = menuObj.outerHeight()
    var top = y+h;
    if (top + menuHeight + 10 > window.innerHeight){
        top = window.innerHeight - menuHeight - 10;
    } 
	if (top < 0) {
		top = 0;
	} // for "contextmenu" must check to collapse of layer 
	var topLayer = top;
	if (contextmenu){
		topLayer = event.pageY;
		if ((menuHeight + event.pageY) > window.innerHeight){
			topLayer = event.pageY - menuHeight; 
		} 
	} 
	topLayer = (event.pageY > topLayer) ? (event.pageY - menuHeight) : topLayer;
	menuObj.css("top", topLayer + "px");   
}

// ==================================================================================================
// Upload OR File Attributes
// ==================================================================================================
function getTriggerUpload(){  
	return getById("TextUploadId").val();
}  
 
function getStreamFiles(cid){
	var fileStreamRet = false, 
	fileStreamObj = getDbJson(getXor($.config, "files", '{}'), '{}'); 
	if (getXor(fileStreamObj, "total", 0)){
		fileStreamRet = getXor(fileStreamObj.files, cid, false);
	} return fileStreamRet;
}

function getStreamFilePreview(str){
	var ret =  false;
	if (typeof str === "undefined") {
		return ret;
	} if (str.toLowerCase().endsWith(".png")) { 
        ret = true;
    } else if (str.toLowerCase().endsWith(".jpg")) { 
        ret = true;
    } else if (str.toLowerCase().endsWith(".jpeg")) { 
        ret = true;
    } else if (str.toLowerCase().endsWith(".bmp")) { 
        ret = true;
    } else if (str.toLowerCase().endsWith(".gif")) { 
        ret = true;
    } else if (str.toLowerCase().endsWith(".webp")) { 
        ret = true;
    } return ret;
} 

function getStreamFilePlay(str){
	var ret = {audio: false, video: false}; 
    if (str.toLowerCase().endsWith(".mov")){
		ret.video = true; 
	} if (str.toLowerCase().endsWith(".avi")) {
		ret.video = true; 
	} if (str.toLowerCase().endsWith(".mpeg")) {
		ret.video = true; 
	} if (str.toLowerCase().endsWith(".mp4")) {
		ret.video = true; 
	} if (str.toLowerCase().endsWith(".mvk")) {
		ret.video = true; 
	} if (str.toLowerCase().endsWith(".webm")) {
		ret.video = true; 
	} // audio stream
    if (str.toLowerCase().endsWith(".wav")){ 
		ret.audio = true;  
	} if (str.toLowerCase().endsWith(".mp3")) {
		ret.audio = true;  
	} if (str.toLowerCase().endsWith(".ogg")) {
		ret.audio = true;  
    } return ret;
}

function closeTriggerUpload(buttons){
	getById("menupreview").hide(0,function(){ 
		getById("TextUploadId")[0].value = "";
		getById("tdMenuPreviewContent").empty(); 
		UpdateUI(false);
		// check if buttons is function but not objects  
		if (typeof buttons === "function"){
			buttons.apply(this, [this]);
		} return;
	}); 
}

function typeTriggerUpload(str, uppercase){
	var retstr = str.split(".");
	if (retstr.length <= 0){
		return false;
	} var ret = retstr[retstr.length - 1];
	if (uppercase) {
		ret = ret.toUpperCase();
	} return ret;
}

function iconTriggerUpload(str){
	// image icon
	var ret = '<i class="fa fa-file"></i>';
	if (str.toLowerCase().endsWith(".png")) {
		ret = '<i class="fa fa-file-image-o"></i>';
    } if (str.toLowerCase().endsWith(".jpg")) {
		ret =  '<i class="fa fa-file-image-o"></i>';
    } if (str.toLowerCase().endsWith(".jpeg")) {
		ret = '<i class="fa fa-file-image-o"></i>';
    } if (str.toLowerCase().endsWith(".bmp")){ 
		ret =  '<i class="fa fa-file-image-o"></i>';
    } if (str.toLowerCase().endsWith(".gif")) {
		ret =  '<i class="fa fa-file-image-o"></i>'; 
	} // video Icons
    if (str.toLowerCase().endsWith(".mov")){
		ret =  '<i class="fa fa-file-video-o"></i>';
	} if (str.toLowerCase().endsWith(".avi")) {
		ret =  '<i class="fa fa-file-video-o"></i>';
	} if (str.toLowerCase().endsWith(".mpeg")) {
		ret =  '<i class="fa fa-file-video-o"></i>';
	} if (str.toLowerCase().endsWith(".mp4")) {
		ret =  '<i class="fa fa-file-video-o"></i>';
	} if (str.toLowerCase().endsWith(".mvk")) {
		ret =  '<i class="fa fa-file-video-o"></i>';
	} if (str.toLowerCase().endsWith(".webm")) {
		ret =  '<i class="fa fa-file-video-o"></i>';
	} // Audio Icons
    if (str.toLowerCase().endsWith(".wav")){ 
		ret =  '<i class="fa fa-file-audio-o"></i>';
	} if (str.toLowerCase().endsWith(".mp3")) {
		ret =  '<i class="fa fa-file-audio-o"></i>';
	} if (str.toLowerCase().endsWith(".ogg")) {
		ret =  '<i class="fa fa-file-audio-o"></i>';
    } // Compressed Icons
    if (str.toLowerCase().endsWith(".zip")){
		ret =  '<i class="fa fa-file-archive-o"></i>';
	} if (str.toLowerCase().endsWith(".rar")){ 
		ret =  '<i class="fa fa-file-archive-o"></i>';
	} if (str.toLowerCase().endsWith(".tar.gz")) {
		ret =  '<i class="fa fa-file-archive-o"></i>';
    } // Pdf Icons
    if (str.toLowerCase().endsWith(".pdf")){ 
		ret =  '<i class="fa fa-file-pdf-o"></i>';
	} return ret; 
}
 
function messageStreamFiles(files, hook){
	var fileStream = false, 
	fileStreamObj = getXor($.config, "files", false);
	if (fileStreamObj) {
		var fileStream = getDbJson(fileStreamObj, '{}');
		if (!fileStream.hasOwnProperty("files")){
			fileStream.files = {};
		} if (!fileStream.hasOwnProperty("total")){
			fileStream.total = 0;
		} if (!getXor(fileStream.files, files.cid, false)){
			fileStream.files[files.cid] = files;
		} fileStream.total = Object.keys(fileStream.files).length;
		setDbItem(fileStreamObj, JSON.stringify(fileStream)); 
	} if (typeof hook === "function"){
		hook.apply(this, [fileStream]);
	} return fileStream;
}

function messageTriggerFiles(sender, hook, type){ 
	if (typeof type === "undefined") {
		type = "";
	} // var senderFileDocument = "<div id=\"fileMessageCover-"+ getXor(sender, "cid", "") + "\" uid=\""+ getXor(sender, "uid", "") +"\" class=\"fileMessageCover file"+ type +"\" onmouseleave=\"closeChatMessageDropdown(this);\", onmouseenter=\"openChatMessageDropdown(this);\">";
	var senderFileDocument = "<div id=\"fileMessageCover-"+ getXor(sender, "cid", "") + "\" uid=\""+ getXor(sender, "uid", "") +"\" class=\"fileMessageCover file"+ type +"\">";
	senderFileDocument +="<table border=\"0\" class=\"fileMessageContainer\">";
	senderFileDocument +="<tr id=\"trMessageContainer-"+ getXor(sender, "cid", "") +"\">";
	senderFileDocument +="<td class=\"fileMessageIcon\" style=\"width:20px;\">"+ iconTriggerUpload(getXor(sender, "name", "")) +"</td>";
	senderFileDocument +="<td class=\"fileMessageName\">"+ getXor(sender, "name", "") +"</td>";
	// senderFileDocument +="<td class=\"fileMessageMenu\" rowspan=\"2\"><span class=\"fileMessageDropdown\" onclick=\"openChatMessageMenu(this, '"+ getXor(sender, "cid", "") +"', false);\"style=\"display:none;\"><i class=\"fa fa-chevron-down\"></i></span></td>"; 
	senderFileDocument +="</tr>";
	senderFileDocument +="<tr id=\"trMessagePreview-"+ getXor(sender, "cid", "") +"\" style=\"display:none;\">";
	senderFileDocument += "<td colspan=\"2\" id=\"tdMessagePreview-"+ getXor(sender, "cid", "") +"\" class=\"tdMessagePreview\">";
	senderFileDocument +="<div id=\"fileMessagePreview-"+ getXor(sender, "cid", "") +"\" class=\"fileMessagePreview\"></div></td>";
	senderFileDocument +="</tr>"; 
	senderFileDocument +="<tr id=\"trMessageSize-"+ getXor(sender, "cid", "") +"\"><td colspan=\"2\" class=\"fileMessageSize\">"+ typeTriggerUpload(getXor(sender, "type", ""), true) +" <span class=\"fileMessageDotted\"></span> "+ getFormatBytes(getXor(sender, "size",0)) +"</td></tr>";
	senderFileDocument += "</table></div>";  
	
	if (typeof callback === "function"){	
		hook.apply(this, [sender]);
	} return senderFileDocument;
}

function previewTriggerUpload(reader, hook){
var previewFileDocument = "<div class=\"filePreviewCover\"><table border=\"0\" class=\"previewContainer\">";
	previewFileDocument += "<tr><td class=\"filePreviewIcon\" style=\"width:20px;\">"+ getXor(reader, "Icon", "") +"</td><td class=\"filePreviewFile\">"+ getXor(reader, "Name", "Unknown") +"</td></tr>";
	previewFileDocument += "<tr><td colspan=\"2\" class=\"filePreviewSize\">"+ getXor(reader, "Type", "Unknown") +" <span class=\"filePreviewDotted\"></span> "+ getFormatBytes(getXor(reader, "Size", 0)) +"<span class=\"filePreviewDotted\"></span><span id=\"fileUploadProgress\">Upload: 0 %</span></td></tr>";
	previewFileDocument += "</table></div>"; 
	if (typeof hook === "function"){	
		hook.apply(this, [previewFileDocument]);
	} return previewFileDocument;
}

function openTriggerUpload(){ 
	if (getById("menuuploads").length <= 0){
		return;
	} getById("menuuploads").find("input[name=FileUploadId]").trigger("click"); 
	return;
}

function tempTriggerUpload(reader, hook){
	getById("TextUploadId")[0].value = JSON.stringify(reader);
	if (typeof hook === "function"){
		hook.apply(this, [reader]);
	} return;
} 

function changeTriggerUpload(fileUploads){
	var fileArray = $(fileUploads).prop('files'), fileAttr = {}, fileObj = false;
	getById("TextUploadId")[0].value = "";
	if (fileArray.length <= 0){
		console.warn("no file to selection")
	} // get all objects 
	fileObj = fileArray[fileArray.length - 1];
	if (typeof fileObj !== "object"){
		return false;
	} // tmp process 
	fileAttr.fileName = fileObj.name; 
	fileAttr.fileMime = fileObj.type;
	fileAttr.fileSize = fileObj.size; 
	fileAttr.fileIcon = iconTriggerUpload(fileObj.name);
	fileAttr.fileType = typeTriggerUpload(fileObj.name, true);
	fileAttr.fileModified = fileObj.lastModified; 
	// get to read of file 
	var documents = {}, 
	reader = new FileReader();
	reader.Icon = fileAttr.fileIcon; 
	reader.Name = fileAttr.fileName;
	reader.Size = fileAttr.fileSize; 
	reader.Mime = fileAttr.fileMime; 
	reader.Type = fileAttr.fileType; 
	
	// on updated progress bars 
	var onupdateprogress = function(percent, hook){
		getById("contentBlockingUI").html(Math.round(percent) +" %");
		if (typeof hook === "function"){
			hook.apply(this, [percent]);
		} return false;
	}
	// onloadstart
	reader.onloadstart = function(event){  
		onupdateprogress(blockingUI.range.start,(percent)=> {
			closeTriggerUpload((s) => {
				getById("chatcontiner").block(blockingUI.content);    
			});   
		}); 
	}, 
	// onloaded 
	reader.onloadend = (event) => {
		onupdateprogress(blockingUI.range.stop, (ret) => {
			documents.name = reader.Name;
			documents.size = reader.Size; 
			documents.mime = reader.Mime;  
			documents.type = reader.Type; 
			documents.data = reader.Data;  
			documents.url = "";
			window.setTimeout(() => {
				getById("chatcontiner").unblock();	
				getById("tdMenuPreviewContent").empty(); 
				previewTriggerUpload(reader,(output) => {
					getById("tdMenuPreviewContent").html(output);
					getById("menupreview").show(0, function(){  
						tempTriggerUpload(documents, (output) => {
							UpdateUI(true);
						}); 
					});
				});   
			},blockingUI.timeout); 
		});
	}, 
	// abort 
	reader.onabort = function(event) { 
		onupdateprogress(blockingUI.range.stop,(percent) => {
			Alert("File read cancelled", getLang("alert_title_warning", "...")); 
		}); return false;
	}, // on progresss 
	reader.onprogress = (event) => {
		if (event.lengthComputable){
			var percent = ((event.loaded / event.total) * 100);  
			if (percent < blockingUI.range.stop){			
				onupdateprogress(percent); 
			} // else onupdateprogress(percent);
		} return reader;
	}, // on onload 
	reader.onload = (event) => {  
		if (reader.result){
			reader.Data = reader.result;  
		} return reader; 
	}, reader.readAsDataURL(fileObj); 
	return 0; 
}

function openChatOurMessageDropdown(obj){
	$(obj).find("span.senderMessageDropdown").show();
}

function closeChatOurMessageDropdown(obj){
	$(obj).find("span.senderMessageDropdown").hide();
}

function openChatTheirMessageDropdown(obj){
	$(obj).find("span.receiverMessageDropdown").show();
}

function closeChatTheirMessageDropdown(obj){
	$(obj).find("span.receiverMessageDropdown").hide();
} 

/* =============================================================================================================================
 * Window Noticiofication  
 * =============================================================================================================================
 */
function Alert(messageStr, TitleStr, onOk)  {
    if (confirmObj != null) {
        confirmObj.dialog("close");
        confirmObj = null;
    } if (promptObj != null) {
        promptObj.dialog("close");
        promptObj = null;
    } if (alertObj != null) {
        return;
    } 
	var html = "<div class=NoSelect>";
    html += "<div class=UiText style=\"padding: 10px\" id=AllertMessageText>" + messageStr + "</div>";
    html += "</div>";
    alertObj = $('<div>').html(html).dialog({
        autoOpen: false,
        title: TitleStr,
        modal: true,
        width: 300,
        height: "auto",
        resizable: false,
        closeOnEscape : false,
        close: function (event, ui) {
            $(this).dialog("destroy");
            alertObj = null;
        }
    });
    var buttons = [];
    buttons.push({
        text: getLang("alert_button_ok", "OK"),
        click: function() { 
			if (typeof onOk === "function"){
				onOk.apply(this, [$(this)]);
			} 
			$(this).dialog("close");
            alertObj = null;
        }
    });
    alertObj.dialog("option", "buttons", buttons);
    alertObj.dialog("open"); 
    alertObj.dialog({ dialogClass: 'no-close' }); 
    // UpdateUI();

}

function Confirm(messageStr, TitleStr, onOk, onCancel, handleEvent) {
    if (alertObj != null) {
        alertObj.dialog("close");
        alertObj = null;
    } if (promptObj != null) {
        promptObj.dialog("close");
        promptObj = null;
    } if (confirmObj != null) {
       return;
    } 
	
	var eventButtons = []; 
	var html = "<div class=NoSelect>";
	if (typeof messageStr !== "object"){
		html += "<div class=UiText style=\"padding: 10px\" id=ConfirmMessageText>" + messageStr + "</div>";
	}   
	if (typeof messageStr === "object"){
		html +="<div class=UiText style=\"padding: 10px\" id=ConfirmMessageText>";
		html +="<table border=0 style=\"width:99%;\">"; 
		$.each(messageStr, (i, o) => {
			html+="<tr>";
			// content for HTML 
			if (getXor(o,"html", false)){
				html += "<td class=\"textLeftDialogConfirm\" style=\"text-align:right;\">"+ o.html + "</td>";
			} // content for buttons 
			if (getXor(o, "button", false)){
				html += "<td class=\"textRightDialogConfirm\" style=\"text-align:right;\">"
				html += "<button id=\""+ getXor(o.button, "id", "") +"\" class=\"buttonDialogs\" title=\""+ getXor(o.button, "text", "") +"\">" 
				html += "<span class=\"fa fa-trash-o\"></span></button>"
				html += "</td>"; 
				eventButtons.push({id: o.button.id, click: o.button.click}); 
			} html+="</tr>";
		});
		html +="</table>"; 
		html +="</div>"; 
	} 
    html += "</div>"; 

    confirmObj = $('<div>').html(html).dialog({
        autoOpen: false,
        title: TitleStr,
        modal: true,
        width: 300,
        height: "auto",
        resizable: false,
        closeOnEscape : false,
        close: function(event, ui) {
            $(this).dialog("destroy");
            confirmObj = null;
        }
    }); 
	// custom for new confirm buttons 
	var defaults = typeof onOk === "object" ? onOk : [];
	if (typeof onOk === "function"){ 
		defaults.push({
			text: getLang("alert_button_ok", "..."),
			click: function(){ 
				if (typeof onOk === "function"){
					onOk.apply(this, [$(this)]);
				} 
				$(this).dialog("close");
				confirmObj = null;
			}
		});
	}
	// default cancel button if 
	if (typeof onCancel === "function"){ 
		defaults.push({
			text: getLang("alert_button_cancel", "..."), 
			click: function(){ 
				if (typeof onCancel === "function"){
					onCancel.apply(this, [$(this)]);
				} 
				$(this).dialog("close");
				confirmObj = null;
			}
		}); 	
	} 
	var buttons = [];
	$.each(defaults, (item, button) => {
		buttons.push(button);
	});
    confirmObj.dialog("option", "buttons", buttons);
	confirmObj.on("dialogopen", (event, ui) => {  
		/* get event with buttons */
		if (eventButtons.length) {
			$.each(eventButtons, (s, events) => {
				if (events.id !== null) getById(events.id).on("click", function(){
					if (typeof events.click === "function"){
						events.click.apply(this, [$(this)]); 
					}
				});
			}); 
		} // callback in open process 
		if (typeof handleEvent === "function"){
			handleEvent.apply(this, [confirmObj]);
		} return;	
	}); 
    confirmObj.dialog("open");
    confirmObj.dialog({dialogClass: 'no-close'});
   // UpdateUI();
}

function closeWindow(all){
    if (windowObj != null){
        windowObj.dialog("close");
        windowObj = null;
    }
    if (all == true){
        if (confirmObj != null) {
            confirmObj.dialog("close");
            confirmObj = null;
        }
        if (promptObj != null) {
            promptObj.dialog("close");
            promptObj = null;
        }
        if (alertObj != null) {
            alertObj.dialog("close");
            alertObj = null;
        }
    }
}


/* =============================================================================================================================
 * Take Photo
 * ============================================================================================================================= 
 */ 
function canvasCameraTakePhoto() {	
	if (canvasTakePhoto === null) {
		return;
	}  // get context "2d"
	const context = canvasTakePhoto.getContext("2d"); 
	if (streamTakePhoto.width && streamTakePhoto.height) {
		canvasTakePhoto.width = streamTakePhoto.width;
		canvasTakePhoto.height = streamTakePhoto.height;
		context.drawImage(cameraTakePhoto, 0, 0, streamTakePhoto.width, streamTakePhoto.height);  
		getByObj(cameraTakePhoto).hide(30,(event) => {
			const data = canvasTakePhoto.toDataURL("image/jpeg");
			captureTakePhoto.setAttribute("src", data);
			getByObj(captureTakePhoto).show((event) => { 
				var imagename = format("CAM_%s.jpg", getUniqueID());
				getById("TextUploadId").val(JSON.stringify({name: imagename, mime: "image/webp", type: "jpg", data: data, size: 0, url: ""}));
			});
		}); 
    } return;
}

function resetCameraTakePhoto(){ 
	if (canvasTakePhoto === null){
		return;
	} if (getByObj(canvasTakePhoto).length) {
		getByObj(captureTakePhoto).hide((event) => {
			getByObj(cameraTakePhoto).show();
		}); 
	} return;
}  

function startCameraTakePhoto(events) { 
	cameraTakePhoto = document.getElementById("takeCoverCameraContent");
	canvasTakePhoto = document.getElementById("takeCoverCanvasContent");  
	captureTakePhoto = document.getElementById("takeCoverCaptureContent");    
	// get video.event "canplay"
	cameraTakePhoto.addEventListener("canplay", (event) => { 
		if (!streamTakePhoto.stream) {
			streamTakePhoto.height = cameraTakePhoto.videoHeight / (cameraTakePhoto.videoWidth / streamTakePhoto.width);
			// Firefox currently has a bug where the height can't be read from
			// the video, so we will make assumptions if this happens. 
			if (isNaN(streamTakePhoto.height)) {
				streamTakePhoto.height = streamTakePhoto.width /(4/3);
			} 
			cameraTakePhoto.setAttribute("width" , streamTakePhoto.width);
			cameraTakePhoto.setAttribute("height", streamTakePhoto.height); 
			canvasTakePhoto.setAttribute("width" , streamTakePhoto.width);
			canvasTakePhoto.setAttribute("height", streamTakePhoto.height);
			streamTakePhoto.stream = true;
		}
	});
	// try execCommand 
	try {
		navigator.mediaDevices.getUserMedia(constrainsTakePhoto).then((stream) => {
			cameraTakePhoto.srcObject = stream;
			cameraTakePhoto.play(); 
		}).catch((e) => {
			console.log("An error occurred:", e);
		}); 
	} catch (error) { 
		console.warn(error);
		if (typeof events === "function"){
			events.apply(this, [error]);
		} 
	} return;
}
 
function closeCameraTakePhoto(){
	var cameraTakePhotoContainer = getById("menurollphoto");
	if (cameraTakePhoto !== null){  
		try {
			const mediaStream = cameraTakePhoto.srcObject.getVideoTracks();
			mediaStream.forEach((mediaTrack) => {
				mediaTrack.stop();
			});
		} catch(e){}; 
	}  
	if (getDisplay("menurollphoto") && cameraTakePhotoContainer.length){
		cameraTakePhotoContainer.hide();
		cameraTakePhotoContainer.empty();
	} return 0;
}

function openCameraTakePhoto() {
	if (typeof navigator.mediaDevices === "undefined"){
		Alert(getLang("media_device_notsupport", "..."), getLang("media_device_title", "Warning"));
		return;
	} // started if not error 
	var cameraTakePhotoContainer = getById("menurollphoto");
	var cameraConversationContainer = getById("conversation");   
	var body = "<canvas id=\"takeCoverCanvasContent\" style=\"display:none;\"></canvas>";
	body += "<div id=\"takeCoverHeaderContent\" class=\"takeCoverHeaderContent\"></div>";
	body += "<div id=\"takeCoverBodyContent\" class=\"takeCoverBodyContent\">";
	body += "<video id=\"takeCoverCameraContent\" autoplay></video>";
	body += "<img id=\"takeCoverCaptureContent\"></img>";
	body += "</div>";  
	body += "<div id=\"takeCoverButtonContent\" class=\"takeCoverButtonContent\" style=\"\"> "
	body += "<button onclick=\"canvasCameraTakePhoto();\" class=\"round-buttons\" title=\"Take Photo\"><i class=\"fa fa-camera\"></i></button>&nbsp;";
	body += "<button onclick=\"resetCameraTakePhoto();\" class=\"round-buttons\" title=\"Reset Photo\"><i class=\"fa fa-refresh\"></i></button>";
	body += "<button onclick=\"closeCameraTakePhoto();\" class=\"round-buttons\" title=\"Close\"><i class=\"fa fa-close\"></i></button>";
	body += "</div>";    
	cameraTakePhotoContainer.empty();  
	cameraTakePhotoContainer.show(30,(event) => {
		cameraTakePhotoContainer.html(body).promise().done((event) => {
			startCameraTakePhoto((error) => { 
				Alert(error, getLang("media_device_title", "Error")); 
			});
		}); 
	});
}


/* =============================================================================================================================
 * Dicated Mic Try open support in crome 
 * ============================================================================================================================= 
 */ 
function closeDictatedRecognition() {
	if (MicrophoneRecognition != null){
		MicrophoneRecognition.abort();
		MicrophoneRecognition = null;
	} return;
} 

function openDictatedRecognition() {	
	if (MicrophoneRecognition != null){
		MicrophoneRecognition.abort();
		MicrophoneRecognition =  null;
	} 
	try {
        // Limitation: This object can only be made once on the page
        // Generally this is fine, as you can only really dictate one message at a time.
        // It will use the most recently created object.
        var SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition || window.speechRecognition;
        MicrophoneRecognition = new SpeechRecognition();
    } catch(e) {
        console.warn("SpeechRecognition.error:", e);
		Alert(getLang("speech_recognition_notsupport", "???"), getLang("speech_recognition_title", "Warning"));
        return;
    } 
	var dictateTextarea = getById("comment")
	var dictateContainer = getById("menudictate"); 
	var dictateInstruction = $("<div/>").addClass("dictateMessageContainer").css("width", "100%");
	MicrophoneRecognition.continuous = true;
	MicrophoneRecognition.onstart = function(event){ 
		var listening = "";
		listening += "<table class=\"dictateTableContainer\" align=\"center\" border=\"0\"><tr>";
		listening += "<td class=\"dictateButtonContainer\">";
		listening += "<button class=\"round-buttons\" onclick=\"closeDictatedRecognition();\">";
		listening += "	<i class=\"fa fa-microphone\" title=\""+ getLang("title_stop_speech_recognition", "...") +"\" style=\"font-size: 21px\"></i>";
		listening += "</button>";
		listening += "</td>";
		listening += "<td class=\"dictateTextContainer\" >";
		listening += "	<i class=\"fa fa-cog fa-spin\"></i> <span id=\"dictateInfoContainer\" class=\"dictateInfoContainer\">"+ getLang("speech_recognition_listening", "...") +"</span>";
		listening += "</td>";
		listening += "</tr></table>"; 
		dictateInstruction.html(listening);
    }
	MicrophoneRecognition.onspeechend = function(event){
		window.setTimeout(function(){ 
			dictateContainer.empty();
            dictateContainer.hide(function(){
				UpdateUI(false);
			});
        }, 1000);
	} 
	MicrophoneRecognition.onerror = function(event) {
		if (event.error === "no-speech") {
			if (getById("dictateInfoContainer").length){
				getById("dictateInfoContainer").html(event.error); 
			}
        } else {
            if (MicrophoneRecognition){
				console.warn("SpeechRecognition.error: ", event);
                MicrophoneRecognition.abort();
            } MicrophoneRecognition = null;
        }
        window.setTimeout(function(){
			dictateContainer.empty();
			dictateContainer.hide(function(){
			    UpdateUI(false);
			}); 
        }, 1000);
    } 
	MicrophoneRecognition.onresult = function(event){
		var transcript = event.results[event.resultIndex][0].transcript;
        if ((event.resultIndex == 1 && transcript == event.results[0][0].transcript) == false) {
            if ($.trim(dictateTextarea.val()).endsWith(".") || $.trim(dictateTextarea.val()) == "") {
                if (transcript == "\r" || transcript == "\n" || transcript == "\r\n" || transcript == "\t"){
                } else {
                    transcript = $.trim(transcript);
                    transcript = transcript.replace(/^./, " "+ transcript[0].toUpperCase());
                }
            } var bytestr = transcript.toLowerCase();
			if (bytestr.trim() === "stop"){
				closeDictatedRecognition(); 
				return false;
			} dictateTextarea.val(dictateTextarea.val() + transcript);
        }
	}  
	dictateContainer.empty();
    dictateContainer.append(dictateInstruction);
	dictateContainer.show(function(){ 
		MicrophoneRecognition.start();
		UpdateUI(true);
	}); 
	return;
}

/* =============================================================================================================================
 * Expresion Emoji Button 
 * ============================================================================================================================= 
 */
function openExpresion(obj, buddy) {
	// custom width dark 
	var windowoffset = $(window).innerWidth(), bottom = $("#toolbarBottom").outerHeight();
	menuCss.expression["display"] = "table";
	if (windowoffset > menuCss.min){
		menuCss.expression["max-width"] = "98%";
		menuCss.expression["left"] = "1%";	
	} 
	if (windowoffset <= menuCss.min){
		bottom = bottom + menuCss.offset;
	} 
	var maxConversation = $(".conversation").innerWidth() - 30;  
    var messageContainer = buddy.attr("class", "emojiContainer").css(menuCss.expression);
	var messageToolbars = $("<div/>").attr("class", "emojiToolbar").css("display", "table-row");
	var emojiCloser = $("<div/>").attr("class", "emojiCloser").html("<button id='emojiButtons' class='round-buttons buttonDialogs emojiButtons' style='float:right;' onclick=\"closeExpresion($(this));\" title=\""+ getLang("title_close_expresion", "Close expression") +"\"><i class='fa fa-keyboard-o'></button>");
	// fa-keyboard-o
	// fa-times-circle
	// fa-close
	var textarea = $("#comment");
	var menuBar = $("<div/>").css("width", "99%");
	
	/* Customize for new emoji */
	menuBar.attr("id", "emojiButton");
	menuBar.prop("class", "emojiButton"); 
    var emojis = ["😀","😁","😂","😃","😄","😅","😆","😇","😈","😉","😊","😋","😌","😍","😎","😏","😐","😑","😒","😓","😔","😕","😖","😗","😘","😙","😚","😛","😜","😝","😞","😟","😠","😡","😢","😣","😤","😥","😦","😧","😨","😩","😪","😫","😬","😭","😮","😯","😰","😱","😲","😳","😴","😵","😶","😷","🙁","🙂","🙃","🙄","🤐","🤑","🤒","🤓","🤔","🤕","🤠","🤡","🤢","🤣","🤤","🤥","🤧","🤨","🤩","🤪","🤫","🤬","🤭","🤮","🤯","🧐", "👍","🙏","🤲", "🤝","👈", "👉", "👆","💪", "👌","👏"];
    $.each(emojis, (i,e) => {
		var emojiButtonStyle = $("<button>");
		emojiButtonStyle.css({"background-color" : "#ffffff", "border": "0px solid #fff"});
        emojiButtonStyle.html(e); 
        emojiButtonStyle.on("click", function(events) {
            var i = textarea.prop('selectionStart');
            var v = textarea.val();
			textarea.val(v.substring(0,i) + $(this).html() + v.substring(i, v.length));  
			closeExpresion(getById("emojiButton"));
        });
        menuBar.append(emojiButtonStyle);
    }); 
    messageContainer.empty();
	messageToolbars.append(emojiCloser);
	messageContainer.append(messageToolbars); 
	messageContainer.append(menuBar);
    messageContainer.show(menuCss.timeout, function(e) {
		var offsets = $(window).innerWidth(), bottom = $("#toolbarBottom").outerHeight();
		if (offsets <= menuCss.min){
			bottom = bottom + menuCss.offset;
		} getByClass("emojiContainer").css("bottom", bottom + "px");  
	}); 
} 

function closeExpresion(obj, hook){
	var textarea = document.getElementById("comment");
	$(obj).parents("div.emojiContainer").hide(0, function(s){
		$(obj).parents("div.emojiContainer").css("display", "none");
		if (typeof textarea !== "undefined"){
			textarea.setSelectionRange(textarea.value.length, textarea.value.length);
		} getById("comment").focus();
	});  
	if (typeof hook === "function"){
		hook.apply(this, [obj]);
	} return;
}

function resizeExpresion() {
	var windowoffset = $(window).innerWidth(), bottom = $("#toolbarBottom").outerHeight(); 
	if (windowoffset > menuCss.min){
		menuCss.expression["max-width"] = "98%";
		menuCss.expression["left"] = "1%";
		menuCss.expression["bottom"] = bottom;
	} else if (windowoffset <= menuCss.min){ 
		menuCss.expression["max-width"] = "100%";
		menuCss.expression["left"] = "0%";
		menuCss.expression["bottom"] = bottom + menuCss.offset;
	}  
	var display = $(".emojiContainer").css("display"); 
	if (display === "undefined"){
		return;
	} else if (display === ""){
		return;
	} else if (display !== "none"){
		$(".emojiContainer").css(menuCss.expression); 
		$("#emojiButton").css("width", "99%");
	} return false;
}  

/* =============================================================================================================================
 * Window Template 
 * ============================================================================================================================= 
 */
function headingTemplate(){
var heading_template  ='<div class="row heading">';
	heading_template +='<table class="tableHeadingCover" border=0 width="100%">';
	heading_template +='<tr>';
	heading_template +='<td class="heading-avatar">';
	heading_template +='<div class="heading-avatar-icon chat-agent-statement">';
	heading_template +='<img src="'+ getImage("avatar6.png") +'" alt="">';
	heading_template +='</div>'; 
	heading_template +='</td>'; 
	heading_template +='<td class="heading-name">';
	heading_template +='<table class="tableHeadingCoverName" border=0>';
	heading_template +='<tr>';
	heading_template +='<td style="text-align:center;padding-left:3px;">'
	heading_template +='<span><a class="heading-name-meta chat-agent-statement" id="smartagent"></a></span>';
	heading_template +='</td> ';
	heading_template +='<td style="text-align:center;padding-left:5px;">';
	heading_template +='<span class="chat-agent-statement" id="stateagent">&nbsp;</span>';
	heading_template +='</td>';
	heading_template +='<td style="text-align:center;padding-left:3px;">';
	heading_template +='<span style="color:#d9d7d7;font-size:11px;" id="ontyping" ></span>';
	heading_template +='</td>'; 
	heading_template +='</tr>'; 
	heading_template +='</table>';
	heading_template +='</td>'; 
	heading_template +='<td class="heading-dot pull-right chat-agent-statement">'; 
	// Audio Call Button
	if (enableButtons.audiocall){
		getButtonEnabledFeture(getParam("cid"), "calls", (audio) => { 
			heading_template += "<button id=\"buttonAudioCall\" class=\"round-buttons\" onclick=\"setAudioCall('"+ getXor(audio.settings, "hunting", "") +"');\"><i class=\"fa fa-phone fa-2x\" aria-hidden=\"true\" title=\""+ getLang("title_audio_call", "Audio Call") +"\"></i></button>";
		});
	} // Video Call Button
	if (enableButtons.videocall){
		getButtonEnabledFeture(getParam("cid"), "video", (video) => { 
			heading_template +="<button id=\"buttonVideoCall\" class=\"round-buttons\" onclick=\"setVideoCall('"+ getXor(video.settings, "hunting", "") +"');\"><i class=\"fa fa-video-camera fa-2x\" aria-hidden=\"true\" title=\""+ getLang("title_video_call", "Video Call") +"\"></i></button>";
		});
	} // heading_template +='<button id="buttonSignout" class="round-buttons" onclick="setSignout();"><i class="fa fa-sign-out fa-2x" aria-hidden="true" title="'+ getLang("title_exit_from_session", "Exit Session") +'"></i></button>';
	heading_template +='<button id="buttonSettings" class="round-buttons" onclick="openSettingMenu(this);"><i class="fa fa-ellipsis-v fa-2x" aria-hidden="true" title="'+ getLang("title_exit_from_session", "Exit Session") +'"></i></button>';
	heading_template +='</td>';  
	heading_template +='</tr>';
	heading_template +='</table>';
	heading_template +='</div>';  
	// end: heading Area
	return heading_template;
}

function leaveMessageTemplate() { 
var pending_template = "<button class=\"round-buttons\" onclick=\"window.setBackFromLeaveMessage(this);\" title=\""+ getLang("title_return_main_page", "...") +"\" style=\"margin-top:-10px;\">";
	pending_template += "<i class=\"fa fa-arrow-circle-left fa-2x\" aria-hidden=\"true\"></i></button>";
	pending_template += "<p class=\"titleLeaveMessage\" >"+ getLang("label_title_leave_message", "...") +"</p>" 	
	pending_template += "<form id=\"frmLeveMessage\" name=\"frmLeveMessage\" onsubmit=\"return false;\">";
	pending_template += "<table id=\"tableLeaveMessage\" class=\"tableLeaveMessage\"  border=\"0\" align=\"center\">";    
	pending_template += "<tr> <td class=\"labelLeaveMessage\">"+ getLang("label_username", "...") +"</td></tr>"; 
	pending_template += "<tr>"; 
	pending_template += "<td class=\"fieldLeaveMessage\">";
	pending_template += "<input type=\"text\" name=\"txt_leave_username\" id=\"txt_leave_username\" value=\"\" onkeyup=\"DBI.visitor.setSession('from', this.value);\" class=\"form-control text-center\"  placeholder=\""+ getLang("placeholder_username", "") +"\"></td>";
	pending_template += "</tr>";  
	pending_template += "<tr><td class=\"labelLeaveMessage\">"+ getLang("label_email", "...") +"</td></tr>";
	pending_template += "<tr>"; 
	pending_template += "<td class=\"fieldLeaveMessage\" style=\"text-align:center;\">";
	pending_template += "<input type=\"text\" name=\"txt_leave_email\" id=\"txt_leave_email\" value=\"\" onkeyup=\"DBI.visitor.setSession('email', this.value);\" class=\"form-control text-center\" placeholder=\""+ getLang("placeholder_email", "") +"\"></td>";
	pending_template += "</tr>";  
	pending_template += "<tr><td class=\"labelLeaveMessage\">"+ getLang("label_phone", "...") +"</td></tr>"; 
	pending_template += "<tr>"; 
	pending_template += "<td class=\"fieldLeaveMessage\">";
	pending_template += "<input type=\"text\" name=\"txt_leave_phone\" id=\"txt_leave_phone\" value=\"\" onkeyup=\"DBI.visitor.setSession('phone', this.value);\" class=\"form-control text-center\" placeholder=\""+ getLang("placeholder_mobile_phone", "") +"\"></td>";
	pending_template += "</tr>";  
	pending_template += "<tr><td class=\"labelLeaveMessage\">"+ getLang("label_message", "...") +"</td></tr>"; 
	pending_template += "<tr>";
	pending_template += "<td class=\"fieldLeaveMessage\">";
	pending_template += "<textarea name=\"txt_leave_message\" id=\"txt_leave_message\" placeholder=\""+ getLang("placeholder_textmessage", "") +"\"></textarea></td>";
	pending_template += "</tr>";  
	pending_template += "<tr>";
	pending_template += "<td class=\"fieldLeaveMessage\" style=\"text-align:center;padding:6px 2px 4px 2px;\">";
	pending_template += "<button type=\"button\" class=\"btn btn-info btnLeaveMessage\" onclick=\"$.chatSentLeaveMessage(this);\" ><i class=\"fa fa-send\" aria-hidden=\"true\"></i>&nbsp;"+ getLang("button_leave_message", "...") +"</button>";
	pending_template += "</td></tr>"; 
	pending_template += "</form></table>"; 
	return pending_template;
}

 
function loginTemplate(){ 
var login_template  ='<div id=\"chatCoverFormLoginFront\" class="col-sm-12 guest text-center form-login-container">';
	login_template +='	<form onSubmit="return false;">';
	login_template +='		<div class="form-group text-center form-chat-login" >';
	login_template +='			<label for="inputUsername" class="col-sm-12 col-form-label text-center">'+ getLang("label_username", "") +'</label>';
	login_template +='			<div class="col-sm-12 text-center">';
	login_template +='				<input type="text" class="form-control text-center" id="inputUsername"  placeholder="'+ getLang("placeholder_username", "") +'">';
	login_template +='			</div>';
	login_template +='		</div>'; 
	login_template +='		<div class="form-group text-center form-chat-login">';
	login_template +='			<label for="inputEmail" class="col-sm-12 col-form-label text-center">'+ getLang("label_email", "") +'&nbsp;</label>';
	login_template +='			<div class="col-sm-12 text-center">';
	login_template +='				<input type="text" class="form-control text-center" id="inputEmail" placeholder="'+ getLang("placeholder_email", "") +'">';
	login_template +='			</div>';
	login_template +='		</div>'; 
	login_template +='		<div class="form-group text-center form-chat-login">';
	login_template +='			<div class="col-sm-12 button-form-control form-chat-login" style="margin-top:15px;">'
	login_template +='				<button type="button" class="btn btn-info" onclick="$.chatSaving(this);"><i class="fa fa-comment"></i> '+ getLang("button_login", "") +'</button>'
	login_template +='			</div>';
	login_template +='		</div>';
	login_template +='	</form>'; 
	login_template +='</div>'; 
	// end: main is login process
	return login_template; 
}
 
function conversTemplate(){  
var main_template = '<div class="row message scroll-container wallpaperBackground" id="conversation">';  // start: main
	if (!getIsLogin("session", false)){
		main_template = '<div class="row message scroll-container" id="conversation">';  // start: main
	} 
	main_template +='<div class="row message-loaders text-center" style="display:none;">';
	main_template +='<div class="col-sm-12 loaders text-center">';
	main_template +='<div class="form-group text-center">';
	main_template +='<div class="col-sm-11 text-center">';
	main_template +='<div class="loader"></div>';
	main_template +='</div>'; 
	main_template +='<div class="col-sm-11 text-center">';
	main_template +='<label for="inputTest" class="col-sm-11 col-form-label col-form-loader text-loader"> </label>';
	main_template +='</div>'; 
	main_template +='</div>';
	main_template +='</div>';
	main_template +='</div>'; 
	main_template +='<div class="row message-notice text-center" style="margin-left:50px;width:75%;">';
	main_template +='<p style="text-align:center; font-size:12px;color:#9e9e9f;" id="notice_message"></p>';
	main_template +='</div>'; 
	
	// start: form logo image custom 
	if (!getIsLogin("session", false)){
		getImageSettings(getParam("cid"),(s) => { 
			main_template +='<div id="chatCoverLogoImageFront" class="row message-guest '+ getXor(s, "css", "chat-front-logo") +' text-center">';
			main_template +=' <div class="chat-front-logo-content text-center"><img src="'+ getImage(getXor(s, "img", "livechat.jpg")) +'"></img></div>'
			main_template +='</div>';  
		});
	} // end: form logo image custom 
	
	// start: login form 
	main_template +='<div id="chatCoverBodyParentFront" class="row message-guest parent chat-front-form text-center">';
	if (!getIsLogin("session", false)){
		main_template += loginTemplate(); 
	} main_template +='</div>';
	// start: login form  
	
	// start: pending form  
	if (getButtonEnabledLeaveMessage()){
		main_template += "<div id=\"chatCoverLeaveMessageFront\" class=\"row message-leave text-center coverLeaveMessage\" style=\"display:none;\">";
		if (!getIsLogin("session", false)){
			main_template += leaveMessageTemplate();
		} main_template += "</div>";   
	} // end: pending form  
	
	// end: main is login process
	main_template +='</div>';   
	return main_template; 
}
 
function toolbarTemplate(){  
var toolbar_template = "";

	// start: camera capture 
	// toolbar_template += "<div id=\"cameratakerollon\" class=\"cameratakerollon\" style=\"display:none;\">";
	// toolbar_template += "<table border=\"1\"><tr>";
	// toolbar_template += "<td class=\"menuCameraBody\">";
	// toolbar_template += "<video id=\"cameraTakePhoto\" autoplay></video></td>";	
	// toolbar_template += "</tr>"; 
	// toolbar_template += "<tr>";
	// toolbar_template += "<td class=\"menuCameraToolbar\">";
	// toolbar_template += "<button class=\"round-buttons\">Capture</button>"
	// toolbar_template += "<button class=\"round-buttons\">Reset</button>"
	// toolbar_template += "</td>"
	// toolbar_template += "</tr></table>";
	// toolbar_template += "</div>"; 
	// stop: camera capture 
	
	
	// start: camera preview
	toolbar_template +="<div id=\"menurollphoto\" class=\"menurollphoto\" style=\"display:none;\"></div>"; 
	toolbar_template +="<div id=\"menuptakephoto\" class=\"menuptakephoto\" style=\"display:none;\"></div>"; 
	// stop:camera preview
	
	// start: preview	
	toolbar_template +='<div id="menupreview" class="menupreview" style="display:none;">';
	toolbar_template +='<table class="menuPreviewCover">';
	toolbar_template +='<tr>';
	toolbar_template +='<td class="tdMenuPreviewButton" id="tdMenuPreviewButton">';
	toolbar_template +='<button class="round-buttons" onclick="closeTriggerUpload(this);"><i class="fa fa-close"></i></button>';
	toolbar_template +='</td>';
	toolbar_template +='<td class="tdMenuPreviewContent" id="tdMenuPreviewContent"></td>'
	toolbar_template +='</tr>';
	toolbar_template +='</table>';
	toolbar_template +='</div>'; 
	// end: preview
	
	// start: dictate	
	toolbar_template +='<div id="menudictate" class="menudictate" style="display:none;"></div>';	
	// end: dictate
	
	// start: upload
	toolbar_template +='<div id="menuuploads" class="menuuploads" style="display:none;">';
	toolbar_template +='<input type="file" id="FileUploadId" name="FileUploadId" class="fileuploadid" onchange="changeTriggerUpload(this);"></input>';	
	toolbar_template +='<textarea id="TextUploadId" name="TextUploadId" class="textuploadid"></textarea>';	
	toolbar_template +='</div>'; 
	// end: upload
	
	// start: expression
	toolbar_template +='<div id="menuexpresion" class="menuexpresion" style="display:none;position:absolute;z-index:999;"></div>'; 
	// end: expression
	
	// start: toolbarBottom 
	toolbar_template +='<div id="toolbarBottom" class="menutoolbar row reply handler-display">';  
	toolbar_template +='<div id="toolbarItem" class="toolbaritem col-sm-1 col-xs-1 reply-emojis handler-display" onclick="openToolbarMenu(this);">';
	toolbar_template +='	<button class="round-buttons"><i class="fa fa-ellipsis-h"></i></button>';
	toolbar_template +='</div>';  
	toolbar_template +='<div class="col-sm-9 col-xs-10 reply-main handler-display">';
	toolbar_template +='	<textarea class="form-control handler-display" rows="1" id="comment" oninput="isTyping()" placeholder="'+ getLang("placeholder_textmessage", "")+'" onkeydown="$.chatSentMessage(event, $(this), \'TEST\');"></textarea>';
	toolbar_template +='</div>'; 
	toolbar_template +='<div class="col-sm-1 col-xs-1 reply-send handler-display" onclick="$.chatSentMessage(event, $(\'#comment\'), \'none\');">';
	toolbar_template +='	<button class="round-buttons"><i class="fa fa-send" aria-hidden="true"></i></button>';
	toolbar_template +='</div>';
	toolbar_template +='</div>';  
	// end: toolbarBottom 
	return toolbar_template;
}

 function timeoutTemplate(event)
{
	var timeout_template = "<p style=\"font-size:.9em;text-align:center;\">";
	timeout_template += "Sorry, We have detected that you have not carried out any activity for quite a long time.<br>";
	timeout_template += "If you agree to remain in the active window press [<b>OK</b>].<br>"; 
	timeout_template += "If you don't agree, we will close the session within ";
	timeout_template += "<span id=\"dialogArgumentsTimer\" style=\"font-weight:bold;\">0</span> seconds from now<br>";
	timeout_template += "Thank You 🙏";
	timeout_template += "</p>";
	if (typeof event === "function"){
		event.apply(this, [timeout_template]);
	} return timeout_template;
}

/* =============================================================================================================================
 * Open Popup Menu 
 * =============================================================================================================================
 */
function openSettingMenu(obj){
	var items = []; 
	// enable button refresh 
	if (enableButtons.refresh){
		getButtonEnabledFeture(getParam("cid"), "refresh" ,(refresh) => {
			items.push({icon: "fa fa-refresh", text: getLang("menu_item_refresh", "..."), value: 1});
		});
	} // enable button exports
	if (enableButtons.exports){
		getButtonEnabledFeture(getParam("cid"), "exports",(exports) => {
			items.push({icon: "fa fa-file", text: getLang("menu_item_export" , "Export Message"), value: 3});
		}); 
	} // enable button logout
	if (enableButtons.logout){
		items.push({icon: "fa fa-power-off", text: getLang("menu_item_logout" , "..."), value: 2});  
	} // check length buttons 
	if (items.length <= 0){
		return false;
	} // settups menu
	var menu = {
        selectEvent : (event, ui) => {
			var menuid = ui.item.attr("value"); 
			var menutitle = ui.item.attr("text");
			closePopupMenu((e) => { 
				// refresh window
				if (menuid == 1){
					setRefresh(menutitle);
				} // refresh window
				if (menuid == 3){
					setExports(menutitle);
				} // logout Session
				else if (menuid == 2){
					setSignout(menutitle);
				} return;
			});
		},
		createEvent : null,
        autoFocus : true,
        items : items
	}
	openPopupMenu(obj, menu); 
}

function downloadFile(msgid, s){ 
	var url = getXor(s, "url", false);
	if (url){
		try {
			window.open(url, "_blank", "height=5,width=5");
		} catch(e){ console.error(e)}
	} return;
}

function showImageFile(msgid, s){
	var messageSrc = encodeURI(getWebhookPreview() +"&id="+ getXor(s, "uid", "") +"&type="+ getXor(s, "type", "") +"&name="+ getXor(s, "name", ""));
	var messageStr = "<img src=\""+ messageSrc +"\" id=\"fileImagePreview-"+ msgid +"\" class=\"fileStreamPreview\"></img>";
	getById("trMessagePreview-"+ msgid).show(function(){
		getById("fileMessagePreview-"+ msgid).html(messageStr);
	});  
}

function showAudioFile(msgid, s){ 
	var messageStr = "<audio controls=true id=\"fileAudioPreview-"+ msgid +"\" class=\"fileStreamPreview\"></audio>";
	getById("trMessagePreview-"+ msgid).show(function(){
		getById("fileMessagePreview-"+ msgid).html(messageStr);
		window.setTimeout(() => {
			var url = encodeURI(getWebhookPreview() +"&id="+ getXor(s, "uid") +"&type="+ getXor(s, "type") +"&name="+ getXor(s, "name"));
			var localAudio = getById("fileAudioPreview-"+ msgid);
			fetch(url).then(response => response.blob()).then(blob => { 
				localAudio[0].type = getXor(s, "mime", blob.type);
				localAudio[0].src = URL.createObjectURL(blob); 
				localAudio[0].play();
			})
			.catch(error => {
				getById("trMessagePreview-"+ msgid).hide();
				getById("fileMessagePreview-"+ msgid).empty();
			});  
		}, blockingUI.timeout);
	});     
}

function showVideoFile(msgid, s){ 
	var messageStr = "<video controls=true id=\"fileVideoPreview-"+ msgid +"\" class=\"fileStreamPreview\"></video>";
	getById("trMessagePreview-"+ msgid).show(function(){
		getById("fileMessagePreview-"+ msgid).html(messageStr);
		window.setTimeout(() => {
			var audioUrl = encodeURI(getWebhookPreview() +"&id="+ getXor(s, "uid") +"&type="+ getXor(s, "type") +"&name="+ getXor(s, "name"));
			var localVideo = getById("fileVideoPreview-"+ msgid);
			fetch(audioUrl).then(response => response.blob()).then(blob => {
				try {
					localVideo[0].type = getXor(s, "mime", blob.type);
					localVideo[0].src = URL.createObjectURL(blob); 
					localVideo[0].play();
				} catch (e){}
			})
			.catch(error => {
				getById("trMessagePreview-"+ msgid).hide();
				getById("fileMessagePreview-"+ msgid).empty();
			});  
		}, blockingUI.timeout);
	});     
}

function closeImageFile(msgid, s){
	getById("trMessagePreview-"+ msgid).hide(function(){
		getById("fileMessagePreview-"+ msgid).empty();
	});
}

function closeAudioFile(msgid, s){
	if (getById("fileAudioPreview-"+ msgid).length > 0){
		getById("fileAudioPreview-"+ msgid)[0].pause();
	} getById("trMessagePreview-"+ msgid).hide(function(){
		getById("fileMessagePreview-"+ msgid).empty();
	});
}

function closeVideoFile(msgid, s){ 
	if (getById("fileVideoPreview-"+ msgid).length > 0){
		getById("fileVideoPreview-"+ msgid)[0].pause();
	} getById("trMessagePreview-"+ msgid).hide(function(){
		getById("fileMessagePreview-"+ msgid).empty();
	});
} 

function deleteChatMessage(msgid, s){
	var request = {}, routing = false;
	if (msgid === false || msgid === ""){
		return 0;
	} /* for public sent deleted via socket */
	try {
		getById("chatcontiner").block(blockingUI.content);
		window.ctmClient.Delete(msgid,(message) => {
			window.setTimeout(() => {
				getById("chatcontiner").unblock();	
				getById("message-body-"+ message.msgid).hide();
			}, 500); 
		});
	} /* catch error process */
	catch(e){ 
		getById("chatcontiner").unblock();	
		console.warn("delete.error:", e);
	} return;
	
	/*
	// set routing for deleted  via api 
	routing = getWebhookModuls("/Chat/?action=deleteAppChat"); // create new objects 
	request.cid = getClient("cid", ""); // client id 
	request.host = getClient("domain", ""); // host by parameter   
	request.email = getClient("email", ""); // host by parameter   
	request.msgid = getClient("msgid", msgid); // host by parameter   ;
	request.session = getClient("session", "");
	getById("chatcontiner").block(blockingUI.content);    
	getAjaxPromise(routing, request).then(response => {
		getById("chatcontiner").unblock();	
		if (!response.success){
			Alert(getLang("delete_chat_message", "") +": "+ response.message, getLang("alert_title_er", "..."));
			return false;
		} // window.location.reload();
		$("#message-body-"+ msgid).hide();
		return false; 
	}) // exceptions in error
	.catch(error => {
		getById("chatcontiner").unblock();	
		Alert(getLang("delete_chat_message", "") +": Unknown Error", getLang("alert_title_er", "..."));
		console.warn("deleteChatMessage.error():", error); 
	});
	*/
} 


function openChatMessageMenu(obj, msgid, url){
	var items = [], fileStream = getStreamFiles(msgid); 
	/* if type is image or "picture" */
	if (enableButtons.preview &&(getXor(fileStream, "name", false))){
		if (getStreamFilePreview(fileStream.name)){
			getButtonEnabledFeture(getParam("cid"), "preview", (download) => {
				items.push({icon: "fa fa-search", text: getLang("menu_item_image_show", "Show Picture"), value: 2});
				items.push({icon: "fa fa-close", text: getLang("menu_item_image_close", "Close Picture"), value: 5});
			});
		}
	} /* if type is Video/ Or Audio */
	if (enableButtons.playing &&(getXor(fileStream, "name", false))){
		var types = getStreamFilePlay(fileStream.name);
		getButtonEnabledFeture(getParam("cid"), "playing", (download) => {
			if (types &&(types.audio)){
				items.push({icon: "fa fa-play", text: getLang("menu_item_playing_audio", "Play Audio"), value: 3}); 
				items.push({icon: "fa fa-close", text: getLang("menu_item_close_audio", "Close Audio"), value: 6}); 
				
			} else if (types &&(types.video)){
				items.push({icon: "fa fa-play", text: getLang("menu_item_playing_video", "Play Video"), value: 4});
				items.push({icon: "fa fa-close", text: getLang("menu_item_close_video", "Close Video"), value: 7});
			}
		}); 
	} // for item download is all 
	if (enableButtons.download &&(getXor(fileStream, "name", false))){
		getButtonEnabledFeture(getParam("cid"), "download", (download) => {
			items.push({icon: "fa fa-download", text: getLang("menu_item_download", "Download"), value: 1});
		});
	} // delete of message if true 
	if (enableButtons.deleted){
		getButtonEnabledFeture(getParam("cid"), "deleted", (deleted) => {
			items.push({icon: "fa fa-trash-o", text: getLang("menu_item_deleted", "Delete Message"), value: 9});
		});
	} // check menu items
	if (items.length <= 0){
		return false;
	} // show list menu
	var menu = {
        selectEvent : (event, ui) => {
			var menuid = ui.item.attr("value"), textstr = ui.item.attr("text");
			closePopupMenu((e) => { 
				/* 1 download*/
				if (menuid == 1){ 
					downloadFile(msgid, fileStream);
				} /* 2 preview */
				else if (menuid == 2){
					showImageFile(msgid, fileStream); 
				} /* 3 play audio */
				else if (menuid == 3){
					showAudioFile(msgid, fileStream); 
				} /* 4 play video */
				else if (menuid == 4){
					showVideoFile(msgid, fileStream);
				} /* close image */
				else if (menuid == 5){
					closeImageFile(msgid, fileStream);  
				} /* close audio */
				else if (menuid == 6){
					closeAudioFile(msgid, fileStream);  
				} /* close video */
				else if (menuid == 7){
					closeVideoFile(msgid, fileStream);  
				} /* delete message */
				else if (menuid == 9){
					deleteChatMessage(msgid, fileStream);  
				}
			});
        },
        createEvent : null,
        autoFocus : true,
        items : items
    }
	openPopupMenu(obj, menu); 
}

function openToolbarMenu(obj){
	var items = []; 
	// expression menu toolbar
	if (enableButtons.expression){ 
		getButtonEnabledFeture(getParam("cid"), "expression", (expression) => {
			items.push({icon: "fa fa-smile-o", text: getLang("menu_item_expresion", "..."), value: 1});
		});
	} // fileshare menu toolbar
	if (enableButtons.fileshare){
		getButtonEnabledFeture(getParam("cid"), "files", (expression) => {
			items.push({icon: "fa fa-share-alt" , text: getLang("menu_item_fileshare", "..."), value: 2});
		}); 
	} // take photo
	if (enableButtons.takephoto){
		getButtonEnabledFeture(getParam("cid"), "photo", (expression) => {
			items.push({icon: "fa fa fa-camera" , text: getLang("menu_item_takephoto", "..."), value: 3});
		});	
	} // dictate reconize 
	if (enableButtons.dictate){
		getButtonEnabledFeture(getParam("cid"), "dictate", (expression) => {
			items.push({icon: "fa fa-microphone", text: getLang("menu_item_dictate"	 , "..."), value: 4}); 
		}); 
	} // check if buttons is Array.size ok 
	if (items.length < 1){
		return 0;
	} 
	var menu = {
        selectEvent : (event, ui) => {
			var menuid = ui.item.attr("value"), textstr = ui.item.attr("text");
			closePopupMenu((e) => { 
				textstr = textstr +" "+ getLang("alert_body_construction", "...");
				// get open expression
				if (menuid == 1){
					openExpresion(obj, $("#menuexpresion")); 
				} 
				// get files share 
				else if (menuid == 2){
					closeExpresion(getById("emojiButtons"), (e) => {
						openTriggerUpload(); 	
					}); 
				} // take a photo
				else if (menuid == 3){
					closeExpresion(getById("emojiButtons"), (e) => {
						openCameraTakePhoto();
						// if (typeof navigator.mediaDevices !== "undefined"){
							
						//} else 
					});  
				}// speech recognition
				else if (menuid == 4){
					closeExpresion(getById("emojiButtons"), (e) => {
						openDictatedRecognition();
					});  
				} return;
			});
        },
        createEvent : null,
        autoFocus : true,
        items : items
    }
	openPopupMenu(obj, menu); 
}

 
/* =============================================================================================================================
 * Local window function  
 * ============================================================================================================================= 
 */
function UpdateUI(auto) {
	var c = 140;
	var y = getByClass("scroll-container").innerHeight(); 
	var h = getByClass("menutoolbar").height();
	if (getByClass("menupreview").css("display") !== "none"){
		h = h + getByClass("menupreview").height();
	} if (auto){
		getByClass("scroll-container").css("height", (y - h) +"px"); 
	} else {
		getByClass("scroll-container").css("height", "calc(100% - "+ c +"px)");
	} return;
}

function maxScrollerContainer() {
	getById("conversation").css("height", "86%");  
	return;
}
 
function showCopyrightContainer(show) {
	if (typeof show === "undefined"){
		show = true;
	} if (show){
		getById("chatCoverCopyRight").show();
	} else if (!show){
		getById("chatCoverCopyRight").hide();
	}
}

function setBackFromLeaveMessage(button){
	if (typeof $.chatBackFromLeaveMessage === "function"){
		$.chatBackFromLeaveMessage(button);
	} return;
}

function setAudioCall(hunting){
	window.alert("Audio Call to: "+ hunting);
	return; 
}

function setVideoCall(hunting){
	window.alert("Video Call to: "+ hunting);
	return;
}

function setRefresh(msgtext) {
	var buttonLogouts = [];
	buttonLogouts.push({
		text: getLang("alert_button_ok"),
		click: (logout) => {
			try {
				closeWindow(true);
				window.location.reload();
			} catch(e) {};
		}
	}); 
	buttonLogouts.push({
		text: getLang("alert_button_cancel"),
		click: (cancel) => {
			closeWindow(true);
		}			
	}); 
	closeExpresion(getById("emojiButtons"), (e) => {
		Confirm(getLang("alert_notice_refresh"), getLang("alert_title_confirm"), buttonLogouts);
	});
	return;
}

function setExports(msgtext) {
	if (typeof $.chatVisitorExport === "function"){
		$.chatVisitorExport((exports) => {
			try {
				window.open(getXor(exports, "url", ""), "_blank", "height=5,width=5");
			} catch(e){ console.error(e)}
		});
	} return; 
}
  
function setSignout(msgtext) {
	var buttonLogouts = []; // example handler "timeout" 
	buttonLogouts.push({
		text: getLang("alert_button_ok"),
		click: (logout) => {
			try {
				closeWindow(true);
				$.chatSignout();
			} catch(e) {};
		}
	}); 
	buttonLogouts.push({
		text: getLang("alert_button_cancel"),
		click: (cancel) => {
			closeWindow(true);	
		}			
	});   
	closeExpresion(getById("emojiButtons"), (e) => {
		Confirm(getLang("alert_notice_logout"), 
		getLang("alert_title_confirm"), buttonLogouts, null, null);
	});
	return;
}
  
/* =============================================================================================================================
 * Main Program Started 
 * =============================================================================================================================
 */  
function desktop(callback) {
	var request = {}, routing = getWebhookModuls("/Guest/auth/?encrypt=true");  
	request.cid = getParam("cid"); /* client id */
	request.host = getParam("host"); /* host by parameter */
	getAjaxPromise(routing, request).then(response => {
		if (typeof callback === "function"){
			subscribes = getXor(response, "data", false); 
			callback.apply(this, [subscribes]);
		} return false;
	}).catch(error => { // exceptions in error
		console.warn("desktop.error:", error);
		if (typeof callback === "function"){
			callback.apply(this, [error]);
		} return false;
	});
}   

// get global button, lang and other 
function settings(callback) {
	var request = {}, routing = getWebhookModuls("/Desktop/?desktop=true");  
	request.cid = getParam("cid"); /* client id */
	request.host = getParam("host"); /* host by parameter */
	getAjaxPromise(routing, request).then(response => {
		if (typeof callback === "function"){
			hostConfigs = getXor(response.data, "desktop_engine_settings", {});
			enableButtons = getXor(response.data, "desktop_enable_button", {});
			disableFeature = getXor(response.data, "desktop_disable_features", {});
			imageSettings = getXor(response.data, "desktop_image_settings", {});
			callback.apply(this, [response.data]);
		} return false;
	}).catch(error => { /* exceptions in error */
		console.warn("settings.error:", error);
		if (typeof callback === "function"){
			callback.apply(this, [error]);
		} return false;
	});
} 

// started desktop chat 
function start(callback) {  
	settings((globals) => { /* Settings desktop init */ 
		desktop(desktops => { /* Main desktop init */
			var body_program  = $(document).prop("title"), body_template = ""; 
			// chat form hidden 
			body_template +='<div class="form-chat-hidden" style="display:none;">';
			body_template +='<form name="appCID">';
			body_template +='<input type="hidden" id="cid" name="cid" value="'+ getParam("cid", "") +'">';
			body_template +='<input type="hidden" id="host" name="host" value="'+ getParam("host", "") +'">';
			body_template +='</form>';
			body_template += '</div>';
			// on container desktop  
			body_template +='<div class="row app-one">';
			body_template +='<div class="col-sm conversation">';  
			// header 
			body_template += headingTemplate(); 
			// conversation 
			body_template += conversTemplate(); 
			// toolbar 
			body_template += toolbarTemplate();
			body_template +='</div>';
			body_template +='</div>'; 
			// footer area 
			body_template +='<div id=\"chatCoverCopyRight\" class="col-sm-12 text-center form-footer-container">'; 
			body_template +='	<span>'+ body_program +'</span>';
			body_template +='</div>';  
			// make empty for body contents  
			var body = getById("chatcontiner").empty(); 
			body.html(body_template).promise().done(() => {
				callback.apply(this, [desktops, body]);
			});   
		});  
	});
}