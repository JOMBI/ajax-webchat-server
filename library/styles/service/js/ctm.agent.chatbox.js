/**
 * Copyright (c) 2019 OMENS 
 * This script may be used for non-commercial purposes only. For any
 * commercial purposes, please contact the author at 
 * anant.garg@inscripts.com
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * Update 
 * - $.Revision 2021/12/13 02:49
 * - $.Revision 2024/07/22 00:00
 */
 var windowWhatsAppFocus = true;
 var usernameWhatsApp = false;
 var originalWhatsAppTitle = false; 
 var chatWhatsAppHeartbeatCount = 0;
 var minChatWhatsAppHeartbeat = 3000;
 var maxChatWhatsAppHeartbeat = 33000; 
 var chatWhatsAppHeartbeatTime = minChatWhatsAppHeartbeat; 
 var blinkWhatsAppOrder = 0;
 var chatWhatsAppBoxFocus = [];
 var newWhatsAppMessages = [];
 var newWhatsAppMessagesWin = [];
 var chatWhatsAppBoxes = [];
 var chatWhatsAppWidth = 300;
 var chatWhatsAppChannel = "C004"; 
 
 // var chatWhatsAppHost = "https//58.65.241.83:8886";
 // var chatWhatsAppHost = "https://smartchat.peranti.solutions";
 // var chatWhatsApiUrl = chatWhatsAppHost +"/index.php/Chat/index/";
 // var chatWhatsAppApiAuth = "Basic QzAwNDoxMjM0NTY3ODk=";
 // var chatWhatsAppApiKey = "67B27BD364AF1486E09F09B985FF5F6FA5B8816C9592C7394601B1FD67A5C147";
 
 var chatWhatsAppHost = "http://192.168.10.236";
 var chatWhatsApiUrl = chatWhatsAppHost +"/smartchat/index.php/Chat/index/";
 var chatWhatsAppApiAuth = "Basic QzAwNDoxMjM0NTY3ODk=";
 var chatWhatsAppApiKey = "6F0BD3BECD1F2E126AA50404994849221A99F034AF627234A299D514193BF1A2";
 
 var chatWhatsAppMinimize = "chat_web_minimize";
 var chatWhatsAppSession  = "chat_web_session";
 var chatWhatsAppVisitor  = "chat_web_visitor";
 var chatWhatsAppComplete = "chat_web_complete";
 var chatWhatsAppPostion  = 'bottom'; // default 
 var chatWhatsAppBottom   = '27px';
 var chatWhatsAppTimeout  = null; // that's 3 seconds of not typing
 var chatWhatsAppChatWith = 0; // that's 3 seconds of not typing
 var delay = 3000; // that's 3 seconds of not typing
 var ontype = 0; // that's 3 seconds of not typing
 var timer = null; // that's 3 seconds of not typing
  
 // add for interface webscoket 
 var chatWhatsAppCtm = {
	user: {channel: "", username: "", password: ""},
	sock: {url: "ws://192.168.10.236:17080/agent/"},
	// sock: {url: "wss://smartchat.peranti.solutions/agent/"},
	//sock: {url: "wss://smartchat.agent.peranti.solutions/"},
	call: {}
 };
  
/* @brief window.DBI
 * @details get detail description function
 * @return mixed return value
 */ 
 if (typeof window.DBI === "undefined")
{
	window.DBI = typeof window.localStorage === "object" ? window.localStorage : false;  
}

/* @brief window.DBS
 * @details get detail description function
 * @return mixed return value
 */ 
 if (typeof window.DBS === "undefined")
{
	window.DBI = typeof window.sessionStorage === "object" ? window.sessionStorage : false; 
}

/* Make prototype in storage */ 
 Storage.prototype.getJSON = function(item, defaults) 
{
	if (typeof defaults === "undefined"){
		defaults = false;
	} try {
		var outputstr = this.hasOwnProperty(item) ? this[item] : ""; 
		if (outputstr !== "" ){
			return JSON.parse(outputstr);
		} return defaults;
	} catch(e){
		return defaults;
	} return defaults;
} 

/* @brief chatWhatsAppPrint
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppPrint(format) 
{
	for (var i=1; i < arguments.length; i++) {
		format = format.replace(/%s/, arguments[i] );
	} return format;
};

/* @brief chatWhatsAppUpper
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppUpper(str)
{
	return str.toUpperCase();
}

/* @brief chatWhatsAppDownload
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppDownload(obj, url)
{
	window.open(url, "_blank", "height=10,width=10");
	return false;
}

/* @brief chatWhatsAppBytes
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppBytes(bytes, decimals)
{
	var lang = {
	"bytes" : "Bytes",	
	"kb" : "KB",
	"mb" : "MB",
	"gb" : "GB",
	"tb" : "TB",
	"pb" : "PB",
	"eb" : "EB",
	"zb" : "ZB",
	"yb" : "YB" };
	
	if (bytes === 0) return "0 "+ lang.bytes;
    var k = 1024;
    var dm = (decimals && decimals >= 0)? decimals : 2;
    var sizes = [lang.bytes, lang.kb, lang.mb, lang.gb, lang.tb, lang.pb, lang.eb, lang.zb, lang.yb];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}

/* @brief chatWhatsAppXor
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppXor(obj, value, defaults) 
{
	if (typeof defaults === "undefined"){
		defaults = false;
	} if (typeof obj !== "object"){
		return defaults;
	} if (obj.hasOwnProperty(value)){
		return obj[value]; 
	} return defaults;
};

/* @brief chatWhatsActiveGuested
 * @details get detail description function 
 * @return mixed return value
 */
 function chatWhatsActiveGuested()
{ 
	var userid = Ext.Session('UserId').getSession();
	var dataChat = {action: "chatGsActive"};
	var users = chatWhatsAppWithSession(true); 
	$.ajax ({
		url: chatWhatsAppAction(dataChat), 
		crossDomain: true,
		cache: false,
		data: {"userid": "userid", "guest": users},
		type: "POST",
		dataType: "json",
		success: (res) => {
			if (typeof res === "object" &&(parseInt(res.success) > 0)) {
				var visitor = res.hasOwnProperty("data") ? res.data : false; /* get guest active */
				if (typeof visitor === "object"){
					for (var i in visitor){
						var chatboxs = visitor[i], visitorname = false, visitorstate = 0, chatbox = false;
						visitorname = chatboxs.hasOwnProperty("user") ? chatboxs.user : false;
						if (!visitorname){
							continue;
						} // read state from this 
						visitorstate = parseInt(chatboxs.status) > 0 ? "Online" : "<span style='color:#faffff;'>Offline</span>";
						chatbox = $( "#chatwhatsappbox_"+ visitorname).find(".chatwhatsappboxtitle");
						if (chatbox.length>0){
							chatbox.html(window.chatWhatsAppPrint("%s - %s", visitorname, visitorstate));
						}	
					}
				}
			}	 
		}
	});
}

/* @brief chatWhatsAppAction
 * @details get detail description function 
 * @return mixed return value
 */
 function chatWhatsAppAction(chat)
{
	if (typeof chat !== "object"){
		return 0;
	} 
	var param = new Array(), ptr = 0; for(var key in chat ){
		param[ptr] = chatWhatsAppPrint('%s=%s', key, chat[key]);
		ptr++;
	} 
	var action = param.join('&').toString(),
		url = chatWhatsAppPrint('%s?%s', chatWhatsApiUrl, action); 
	return 	url.toString();
}

/* @brief chatWhatsAppRestructureBox
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppRestructureBox(s) 
{
	whatsAppAlign = 0;
	for (x in chatWhatsAppBoxes) {
		var chatwhatsappboxsession = chatWhatsAppBoxes[x]; 
		if ($("#chatwhatsappbox_"+ chatwhatsappboxsession).css('display') != 'none') {
			if (whatsAppAlign == 0) {
				$("#chatwhatsappbox_"+ chatwhatsappboxsession).css('right', '20px');
			} else {
				width = (whatsAppAlign)*(chatWhatsAppWidth+7) + 20;
				$("#chatwhatsappbox_"+ chatwhatsappboxsession).css('right', width+'px'); 
			} whatsAppAlign ++;
		}
	} if (typeof s === "function"){
		s.apply(this, [chatWhatsAppBoxes]);
	}
}

/* @brief chatWhatsAppDragable
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppDragable(chatbox, context)
{
	// console.log("chatWhatsAppDragable:", context);
	return; 
	if ($(chatbox).length > 0) {
		$(chatbox).draggable ({
			start:() => { 
				$(chatbox).css('cursor', 'move');
			},drag:() => { 
				$(chatbox).css('cursor', 'move');
			},stop:() => { 
				$(chatbox).css('cursor', 'default');
			}
		}); 
	}
}

/* @brief chatWhatsAppWith
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppWith(chatuser)
{
	chatWhatsAppCreateBox(chatuser); 
	$("#chatwhatsappbox_"+ chatuser +" .chatboxtextareawhatsapp").focus(); 
	var whatsappBox = chatWhatsAppPrint("#chatwhatsappbox_%s", chatuser);
	if (whatsappBox.toString().length > 0){
		chatWhatsAppDragable(whatsappBox, "chatWhatsAppWith");
	} return false;
}

/* @brief chatWhatsAppTypeFiles
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppTypeFiles(str, uppercase)
{
	var retstr = str.split(".");
	if (retstr.length <= 0){
		return false;
	} var ret = retstr[retstr.length - 1];
	if (uppercase) {
		ret = ret.toUpperCase();
	} return ret;
}

/* @brief chatWhatsAppIconFiles
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppIconFiles(str)
{
	// image icon
	var ret = '<i class="fa fa-file"></i>';
	if (str.toLowerCase().endsWith(".png")) {
		ret = '<i class="fa fa-file-image-o"></i>';
    } if (str.toLowerCase().endsWith(".jpg")) {
		ret =  '<i class="fa fa-file-image-o"></i>';
    } if (str.toLowerCase().endsWith(".jpeg")) {
		ret = '<i class="fa fa-file-image-o"></i>';
    } if (str.toLowerCase().endsWith(".bmp")){ 
		ret =  '<i class="fa fa-file-image-o"></i>';
    } if (str.toLowerCase().endsWith(".gif")) {
		ret =  '<i class="fa fa-file-image-o"></i>'; 
	} // video Icons
    if (str.toLowerCase().endsWith(".mov")){
		ret =  '<i class="fa fa-file-video-o"></i>';
	} if (str.toLowerCase().endsWith(".avi")) {
		ret =  '<i class="fa fa-file-video-o"></i>';
	} if (str.toLowerCase().endsWith(".mpeg")) {
		ret =  '<i class="fa fa-file-video-o"></i>';
	} if (str.toLowerCase().endsWith(".mp4")) {
		ret =  '<i class="fa fa-file-video-o"></i>';
	} if (str.toLowerCase().endsWith(".mvk")) {
		ret =  '<i class="fa fa-file-video-o"></i>';
	} if (str.toLowerCase().endsWith(".webm")) {
		ret =  '<i class="fa fa-file-video-o"></i>';
	} // Audio Icons
    if (str.toLowerCase().endsWith(".wav")){ 
		ret =  '<i class="fa fa-file-audio-o"></i>';
	} if (str.toLowerCase().endsWith(".mp3")) {
		ret =  '<i class="fa fa-file-audio-o"></i>';
	} if (str.toLowerCase().endsWith(".ogg")) {
		ret =  '<i class="fa fa-file-audio-o"></i>';
    } // Compressed Icons
    if (str.toLowerCase().endsWith(".zip")){
		ret =  '<i class="fa fa-file-archive-o"></i>';
	} if (str.toLowerCase().endsWith(".rar")){ 
		ret =  '<i class="fa fa-file-archive-o"></i>';
	} if (str.toLowerCase().endsWith(".tar.gz")) {
		ret =  '<i class="fa fa-file-archive-o"></i>';
    } // Pdf Icons
    if (str.toLowerCase().endsWith(".pdf")){ 
		ret =  '<i class="fa fa-file-pdf-o"></i>';
	} return ret; 
}

/* @brief chatWhatsAppOpenFiles
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppOpenFiles(buttons, chatwhatsappboxsession)
{ 
	$("#input-files-"+ chatwhatsappboxsession).trigger("click");
	$("#button-files-close-"+ chatwhatsappboxsession).show(0, function(){
		$("#button-files-open-"+ chatwhatsappboxsession).hide();
	}); 
}

/* @brief chatWhatsAppCloseFiles
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppCloseFiles(buttons, chatwhatsappboxsession)
{ 
	$("#button-files-open-"+ chatwhatsappboxsession).show(0, function(){
		$("#blob-files-"+ chatwhatsappboxsession)[0].value = "";
		$("#chatboxtoolbarwhatsapp-"+ chatwhatsappboxsession).hide();
		$("#button-files-close-"+ chatwhatsappboxsession).hide();
	}); 
}

/* @brief chatWhatsAppPreviewFiles
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppPreviewFiles(files, chatwhatsappboxsession, hook)
{ 
	var messageStr = "<table border=0 style=\"color:#8e9392;\">";
	messageStr += "<tr><td>"+ chatWhatsAppXor(files, "Icon", "") +"</td><td>"+ chatWhatsAppXor(files, "Name", "") +"</td></tr>";
	messageStr += "<tr><td colspan=2 style=\"color:#8e9392;\">";
	messageStr += "<span style=\"margin-right:2px;\">"+ chatWhatsAppUpper(chatWhatsAppXor(files, "Type", "")) +"</span>";
	messageStr += "<span style=\"font-weight:bold;\">.</span> "+ chatWhatsAppBytes(chatWhatsAppXor(files, "Size", "")) +"<span style=\"font-weight:bold;margin-left:5px;\">.</span>";
	messageStr += "<span id=\"fileProgress-"+ chatwhatsappboxsession+"\" style=\"margin-left:5px;\">Upload: 0 %</span></td></tr>";
	messageStr +='</table>';	
	if (typeof hook === "function"){
		hook.apply(this, [messageStr]);
	} return messageStr;
} 

/* @brief chatWhatsAppChangeFiles
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppChangeFiles(inputs, chatwhatsappboxsession)
{
	var fileArray = $(inputs).prop('files'), fileAttr = {}, fileObj = false,
	fileBlobs = $("#blob-files-"+ chatwhatsappboxsession), 
	fileToolbar = $("#chatboxtoolbarwhatsapp-"+ chatwhatsappboxsession),
	fileMarkups = $("#chatboxtoolbarbuttonfiles-"+ chatwhatsappboxsession);
	fileMarkups.empty();
	fileMarkups.html("<progress id=\"fileSliderProgress\" max=100 value=0 style=\"width:100%;\"></progress>");
	fileToolbar.show(0, function(){
		fileBlobs[0].value= "";	
	});
	if (fileArray.length <= 0){
		console.warn("no file to selection")
	} // get all objects 
	fileObj = fileArray[fileArray.length - 1];
	if (typeof fileObj !== "object"){
		return false;
	} // tmp process 
	fileAttr.fileName = fileObj.name; 
	fileAttr.fileMime = fileObj.type;
	fileAttr.fileSize = fileObj.size; 
	fileAttr.fileIcon = chatWhatsAppIconFiles(fileObj.name);
	fileAttr.fileType = chatWhatsAppTypeFiles(fileObj.name);
	fileAttr.fileModified = fileObj.lastModified; 
	
	// get to read of file 
	var onupdateprogress= false, documents = {}, 
	reader = new FileReader();
	reader.Icon = fileAttr.fileIcon; 
	reader.Name = fileAttr.fileName;
	reader.Size = fileAttr.fileSize; 
	reader.Mime = fileAttr.fileMime; 
	reader.Type = fileAttr.fileType;  
	/* event for update display */
	var onupdateprogress = (value) => {
		var fileSliderProgress = $("#fileSliderProgress");
		if (fileSliderProgress.length){
			fileSliderProgress[0].value = Math.round(value); 
		}
	} // onloadstart 
	reader.onloadstart = () => {
		onupdateprogress(0); 
	}, // onprogress 
	reader.onprogress = (event) => { 
		if (event.lengthComputable){
			var percent = ((event.loaded / event.total) * 100);  
			if (percent < 100){			
				onupdateprogress(percent); 
			} else onupdateprogress(percent);
		}
	}, // onloaded 
	reader.onloadend = (event) => {
		onupdateprogress(0);
	}, // abort 
	reader.onabort = (event) => { 
		window.alert('File read cancelled');
		onupdateprogress(0);
	}, // loaded 
	reader.onload = (event) => { 
		if (reader.result){
			reader.Data = reader.result;
			onupdateprogress(0);
		} // set to temp process 
		documents.name = reader.Name;
		documents.size = reader.Size; 
		documents.mime = reader.Mime;  
		documents.type = reader.Type; 
		documents.data = reader.Data;  
		documents.url = ""; 
		chatWhatsAppPreviewFiles(reader, chatwhatsappboxsession, (messageStr) => { 
			fileMarkups.html(messageStr);
			fileBlobs[0].value = JSON.stringify(documents); 
		});
	}, reader.readAsDataURL(fileObj); 
	return 0;  
}

/* @brief chatWhatsAppDeleteMessage
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppDeleteMessage(userid, session, msgid)
{	
	// prioroty will delete via socket 
	var body = { 
		routing:{
			"action": "deleteAppChat"
		},
		data: { 
			msgid: msgid, 
			userid: userid, 
			session: session
		}
	};  
	var ajaxSettings = {
		url: chatWhatsAppAction(body.routing), 
		crossDomain: true,
		async: true,
		cache: false,
		data: body.data,
		type: "POST",
		dataType: "json",
		success: (res) => {
			if (typeof res === "object" && res.success){
				var chatboxcontentwhatsapp = $("#chatboxcontentwhatsapp-"+ session);
				if (chatboxcontentwhatsapp){
					chatboxcontentwhatsapp.find("#chatboxmessagewhatsapp-"+ msgid).hide();
				} return false;
			} 
		}
	};
	// started delete message by 2 kondition 
	try { 
		window.ctmClient.Delete(body.data, (event) => {
			console.log("Delete Message: %s success", event.msgid);
			var chatboxcontentwhatsapp = $("#chatboxcontentwhatsapp-"+ event.session);
			if (chatboxcontentwhatsapp){
				chatboxcontentwhatsapp.find("#chatboxmessagewhatsapp-"+ event.msgid).hide();
			} 
		}); 
	} // catch error started 
	catch (e){
		console.warn("warning:", e);
		$.ajax(ajaxSettings);	 
	} return; 
}
 
/* @brief chatWhatsAppCreateVisitor
 * @details create coversation if not foun before
 * @param  "visitor" object visitor minim required 
 * @param "callback" if success ada new conversation 
 * @return mixed return value
 */
 function chatWhatsAppCreateVisitor(visitor, callback) 
{
	if (!chatWhatsAppXor(visitor, "session", 0)){
		console.warn("Object Visitor Failed");
		return 0;
	} // check on object storage 
	var chatBoxVisitor = DBI.getJSON(visitor.session, false); 
	if (chatBoxVisitor){
		if (typeof callback === "function"){
			callback.apply(this, chatBoxVisitor);
		} return chatBoxVisitor;
	} // next will create this object  
	var chatSessionVistor = DBI.getJSON(chatWhatsAppSession), chatTempVistor ={}, chatCounterVistor = 0;
	for (x in chatSessionVistor){
		chatCounterVistor = chatSessionVistor[x].id +1;
	} // make new constructor 
	if (!chatSessionVistor.hasOwnProperty(visitor.session)){
		chatSessionVistor[visitor.session] = {};
	}
	chatTempVistor.id = parseInt(chatCounterVistor);
	chatTempVistor.session = chatWhatsAppXor(visitor, "session", 0);
	chatTempVistor.name = chatWhatsAppXor(visitor, "username", "");
	chatTempVistor.channel = chatWhatsAppXor(visitor, "channel", "");
	chatTempVistor.email = chatWhatsAppXor(visitor, "email", "");
	chatTempVistor.status = chatWhatsAppXor(visitor, "status", 0); 
	if (chatWhatsAppXor(visitor, "time", false)){
		chatTempVistor.update = chatWhatsAppXor(visitor, "time", 0);
	} if (chatWhatsAppXor(visitor, "update", false)){
		chatTempVistor.update = chatWhatsAppXor(visitor, "update", 0);
	} 
	$.each (chatTempVistor, (key, value) => {
		chatSessionVistor[chatTempVistor.session][key] = value;
	});
	// Global chat 
	DBI.setItem(visitor.session, JSON.stringify(chatTempVistor));
	DBI.setItem(chatWhatsAppSession, JSON.stringify(chatSessionVistor));
	if (typeof callback === "function"){
		callback.apply(this, chatTempVistor);
	} return chatTempVistor; 
	return 0; 
}	

/* @brief chatWhatsAppCreateBox
 * @details get detail description function
 * @param  "chatwhatsappboxsession" session
 * @param "minimizeChatBox" box chat is close|open  
 * @return mixed return value
 */
 function chatWhatsAppCreateBox(chatwhatsappboxsession, minimizeChatBox, debugs) 
{
	var chatBoxVisitorAgentId = Ext.Session("UserId").getSession(), chatBoxVisitorMinimize = {}, chatBoxVisitorGrowth = false;
	chatBoxVisitorMinimize[chatBoxVisitorAgentId] = {}; 
	chatBoxVisitorGrowth = DBI.getJSON(chatWhatsAppMinimize, false);
	if (chatBoxVisitorGrowth){ 
		chatBoxVisitorMinimize[chatBoxVisitorAgentId] = chatBoxVisitorGrowth[chatBoxVisitorAgentId];
	} // read session visitor 
	var chatVisitorSession = DBI.getJSON(chatwhatsappboxsession, false), visitor = {};
	if (!chatVisitorSession){
		return 0;
	} // default for data visitor 
	visitor.chatVisitorId = chatVisitorSession.hasOwnProperty("name") ? chatVisitorSession.name : 0, 
	visitor.chatVisitorName = chatVisitorSession.hasOwnProperty("name") ? chatVisitorSession.name : "",
	visitor.chatVisitorTitle = chatVisitorSession.hasOwnProperty("name") ? chatVisitorSession.name : "",
	visitor.chatVisitorSession = chatVisitorSession.hasOwnProperty("session") ? chatVisitorSession.session: 0;
	// check chat box session 
	if ($("#chatwhatsappbox_"+ chatwhatsappboxsession).length > 0) {
		if ($("#chatwhatsappbox_"+ chatwhatsappboxsession).css("display") == 'none') {
			$("#chatwhatsappbox_"+ chatwhatsappboxsession).css("display", "block");
			chatWhatsAppRestructureBox();
		} 
		$("#chatwhatsappbox_"+ chatwhatsappboxsession +" .chatboxtextareawhatsapp").focus();
		return;
	}   
	var whatsappBox = chatWhatsAppPrint("chatwhatsappbox_%s", chatwhatsappboxsession), messageStr = "";
		messageStr +='<div id="chatboxheadwhatsapp-'+ chatwhatsappboxsession +'" class="chatboxheadwhatsapp">';
		messageStr +='<div id="chatwhatsappboxtitle-'+ chatwhatsappboxsession +'" class="chatwhatsappboxtitle">';
		messageStr +='<span style="font-size:18px;" ><i class="fa fa-user-circle"></i></span>&nbsp;'+ visitor.chatVisitorTitle +'&nbsp;<span class="chatwhatsapponstate"></span> &nbsp; <span class="chatwhatsappontyping"></span> </div>';
		messageStr +='<div id="chatwhatsappboxoptions-'+ chatwhatsappboxsession +'" class="chatwhatsappboxoptions">';
		messageStr +='<a href="javascript:void(0)" onclick="javascript:chatWhatsAppToggleGrowth(\''+ chatwhatsappboxsession +'\')"><span id="chat-whatapps-minimize"><i class="fa fa-minus-square"></i></span></a>&nbsp;'
		messageStr +='&nbsp;<a href="javascript:void(0)" onclick="javascript:chatWhatsAppCloseBox(\''+ chatwhatsappboxsession +'\')"><i class="fa fa-window-close"></i></a></div><br clear="all"/></div>';
		messageStr +='<div id="chatboxcontentwhatsapp-'+ chatwhatsappboxsession +'" class="chatboxcontentwhatsapp"></div>';
		
		// file document share 
		messageStr +='<div id="chatboxtoolbarwhatsapp-'+ chatwhatsappboxsession +'" class="chatboxtoolbarwhatsapp" style="display:none;">';
		messageStr +='<input type="file" id="input-files-'+ chatwhatsappboxsession +'" name="input-files-'+ chatwhatsappboxsession +'" onchange="chatWhatsAppChangeFiles(this, '+ chatwhatsappboxsession +');" style="display:none;"></input>'
		messageStr +='<textarea id="blob-files-'+ chatwhatsappboxsession +'" name="blob-files-'+ chatwhatsappboxsession +'" style="display:none;"></textarea>'
		messageStr +='<table border=0 id="chatboxtoolbarcontainer-'+ chatwhatsappboxsession +'" class="chatboxtoolbarcontainer">';
			messageStr +='<tr>'; 
			messageStr +='<td id="chatboxtoolbarbuttondocument-'+ chatwhatsappboxsession +'" class="chatboxtoolbarbuttondocument">';
			messageStr +='<div id="chatboxtoolbarbuttonfiles-'+ chatwhatsappboxsession +'" class="chatboxtoolbarbuttonfiles">';
			
			/*
			messageStr +='<table border=0>';
				messageStr +='<tr><td>Name</td></tr>';
				messageStr +='<tr><td>Size</td></tr>';
			messageStr +='</table>';
			*/
			
			messageStr +='</div>';
			messageStr +='</td>';
			// messageStr +='<td id="chatboxtoolbarbuttonclose-'+ chatwhatsappboxsession +'" class="chatboxtoolbarbuttonclose"><a href="javascript:void(0);" value="'+ chatwhatsappboxsession +'" onclick="chatWhatsAppCloseDocument($(this));"><i class="fa fa-close"></i></a></td>'; 
			messageStr +='</tr>';
		messageStr +='</table>';
		messageStr +='</div>';
		
		// '<span class="chatclearwhatsapp"><a href="javascript:void(0)" onClick="javascript:chatWhatsAppClearReason(\''+chatwhatsappboxsession+'\')"><i class="fa fa-pencil"></i>&nbsp;Clear</a></span></div>'+
		messageStr +='<div id="chatboxinputwhatsapp-'+ chatwhatsappboxsession +'" class="chatboxinputwhatsapp">';
		messageStr +='<table style="width:100%;" border="0">';
		messageStr +='<tr>';
		messageStr +='<td style="width:80%;">';
		messageStr +='<textarea id="chatboxtextareawhatsapp-'+ chatwhatsappboxsession +'" class="chatboxtextareawhatsapp"  visitorsession="'+ visitor.chatVisitorSession +'" visitorid="'+ visitor.chatVisitorId +'" visitorname="'+ visitor.chatVisitorName +'" oninput="chatWhatsAppIsTyping(this);" onkeydown="javascript:return chatWhatsAppCheckBoxInput(event, this, \''+ chatwhatsappboxsession +'\');"></textarea>';
		messageStr +='</td>';
		messageStr +='<td style="width:10%;text-align:center;">';
		messageStr +='<button id="button-files-open-'+ chatwhatsappboxsession +'" class="button-round" style="margin-left:5px;" onclick="chatWhatsAppOpenFiles(this, '+ chatwhatsappboxsession +');" title=\"Share\"><i class="fa fa-paperclip"></i></button>'
		messageStr +='<button id="button-files-close-'+ chatwhatsappboxsession +'" class="button-round" style="margin-left:5px;display:none;" onclick="chatWhatsAppCloseFiles(this, '+ chatwhatsappboxsession +');" title=\"Close\"><i class="fa fa-close"></i></button>'
		
		// messageStr +='<div class="button-send-whatsapp" onclick="javascript:return chatWhatsAppCheckBoxInput(event, \''+ chatwhatsappboxsession +'_chatboxtextareawhatsapp\',\''+ chatwhatsappboxsession +'\');" ></div>';
		messageStr +='</td>'; 
		messageStr +='<td style="width:10%;text-align:center;">';
		messageStr +='<button id="button-text-send-'+ chatwhatsappboxsession +'" class="button-round" onclick="chatWhatsAppCheckBoxInput(event,  $(\'#chatboxtextareawhatsapp-'+ chatwhatsappboxsession +'\')[0], \''+ chatwhatsappboxsession +'\');\" title="Send"><i class="fa fa-send"></i></button>';
		// messageStr +='<div class="button-send-whatsapp" onclick="javascript:return chatWhatsAppCheckBoxInput(event, \''+ chatwhatsappboxsession +'_chatboxtextareawhatsapp\',\''+ chatwhatsappboxsession +'\');" ><i class="fa fa-send"></i></div>';
		messageStr +='</td>'; 
		messageStr +='</tr>';
		messageStr +='</table>';
		messageStr +='</div>';
		messageStr +='</div>'; 
	
	$("<div/>").attr("id",whatsappBox).addClass('chatboxwhatsapp').html(messageStr).appendTo($( 'body' ));
	var chatBoxVisitorHeader = chatWhatsAppPrint('#%s', whatsappBox);
	if ($(chatBoxVisitorHeader).length > 0){
		$(chatBoxVisitorHeader).css('width', chatWhatsAppWidth);
	}  
	// set chat box of position 
	if (typeof chatWhatsAppPostion === "string" &&(chatWhatsAppPostion == 'bottom')){
		$(chatBoxVisitorHeader).css(chatWhatsAppPostion, chatWhatsAppBottom);
	} else if (typeof chatWhatsAppPostion === "string" &&(chatWhatsAppPostion == 'top')){
		$(chatBoxVisitorHeader).css(chatWhatsAppPostion, chatWhatsAppBottom);
	} else {
		$(chatBoxVisitorHeader).css('bottom', chatWhatsAppBottom);
	}   
	chatWhatsAppBoxeslength = 0;
	for (x in chatWhatsAppBoxes) {
		if ($("#chatwhatsappbox_"+ chatWhatsAppBoxes[x]).css('display') != 'none') {
			chatWhatsAppBoxeslength ++;
		}
	} if (chatWhatsAppBoxeslength == 0) {
		$( "#chatwhatsappbox_"+ chatwhatsappboxsession).css('right', '20px');
	} else {
		width = (chatWhatsAppBoxeslength)*(chatWhatsAppWidth+7)+20;
		$( "#chatwhatsappbox_" + chatwhatsappboxsession).css('right', width+'px');
	}   
	chatWhatsAppBoxes.push(chatwhatsappboxsession);
	if (minimizeChatBox == 1) {
		minimizedChatBoxes = []; 
		if (chatBoxVisitorMinimize.hasOwnProperty(chatBoxVisitorAgentId)){
			minimizedChatBoxes = Object.keys(chatBoxVisitorMinimize[chatBoxVisitorAgentId]);
		}
		minimize = 0;
		for (j=0;j<minimizedChatBoxes.length;j++) {
			if (minimizedChatBoxes[j] == chatwhatsappboxsession) {
				$("#chatwhatsappbox_"+ chatwhatsappboxsession).find("#chat-whatapps-minimize").html("<i class='fa fa-window-maximize'></i>");
				minimize = 1;
			}
		} 
		if (minimize == 1) {
			$('#chatwhatsappbox_'+chatwhatsappboxsession+' .chatboxcontentwhatsapp').css('display','none');
			$('#chatwhatsappbox_'+chatwhatsappboxsession+' .chatboxinputwhatsapp').css('display','none');
			$('#chatwhatsappbox_'+chatwhatsappboxsession+' .chatclearwhatsapp').css('display','none');
		}
	}  
	chatWhatsAppBoxFocus[chatwhatsappboxsession] = false;
	$('#chatwhatsappbox_'+chatwhatsappboxsession+' .chatboxtextareawhatsapp').blur(() => {
		chatWhatsAppBoxFocus[chatwhatsappboxsession] = false;
		$('#chatwhatsappbox_'+chatwhatsappboxsession+' .chatboxtextareawhatsapp').removeClass('chatboxtextareawhatsappselected'); 
		
	}).focus(() => {
		chatWhatsAppBoxFocus[chatwhatsappboxsession] = true;
		newWhatsAppMessages[chatwhatsappboxsession] = false;
		$('#chatwhatsappbox_'+chatwhatsappboxsession+' .chatboxheadwhatsapp').removeClass('chatwhatsappboxblink');
		$('#chatwhatsappbox_'+chatwhatsappboxsession+' .chatboxtextareawhatsapp').addClass('chatboxtextareawhatsappselected');
	}); 
	$('#chatwhatsappbox_'+chatwhatsappboxsession).click(() => {
		if ($('#chatwhatsappbox_'+chatwhatsappboxsession+' .chatboxcontentwhatsapp').css('display') != 'none') {
			$('#chatwhatsappbox_'+chatwhatsappboxsession+' .chatboxtextareawhatsapp').focus();
		}
	}); 
	$('#chatwhatsappbox_'+chatwhatsappboxsession).show();
}

/* @brief chatWhatsAppOnInitSession
 * @details started init chat is ready to login 
 * @return mixed return value
 */
 function chatWhatsAppOnInitSession()
{
	window.chatWhatsAppStartSession((s) => {
		$([window, document]).blur(()=> {
			windowWhatsAppFocus = false; 
		}).focus(() => {
			windowWhatsAppFocus = true;
			document.title = originalWhatsAppTitle;
		});	 
		if (chatWhatsAppOnAgentMonitor !== null){
			chatWhatsAppOnAgentMonitor();
		} return;
	}); 
}

/* @brief chatWhatsAppOnEventPong
 * @details agent monitoring on local state 
 * @return mixed return value
 */ 
 function chatWhatsAppOnAgentMonitor()
{
	var chatBoxVisitor = DBI.getJSON(chatWhatsAppSession, false); 
	if (window.ctmClient.helper.isObj(chatBoxVisitor)){
		$.each(chatBoxVisitor, (session, visitor) => {  
			var sessionVisitor = DBI.getJSON(session, {}), 
			personVisitor = {title: "", "fontSize": "11px", "fontColor": "#ffffff"};
			if (sessionVisitor.status){
				personVisitor.fontColor = "#ffffff";
				personVisitor.title = "";
			} else if (!sessionVisitor.status){
				personVisitor.fontColor = "#69ddd2";
				personVisitor.title = "off"; 
			} /* visitor get back online on the same session */
			if (sessionVisitor.status){
				$(sprintf("#%s_chatboxtextareawhatsapp", visitor.session)).attr("disabled", false);	
				$(sprintf("#chatwhatsappbox_%s", visitor.session)).find(".chatwhatsapponstate").html(personVisitor.title); 
			} /* Visitor is Offline */ 
			else if (!sessionVisitor.status){
				$(sprintf("#%s_chatboxtextareawhatsapp", visitor.session)).attr("disabled", true);
				$(sprintf("#chatwhatsappbox_%s", visitor.session)).find(".chatwhatsapponstate").css({"font-size": personVisitor.fontSize}).html(personVisitor.title);
			} $(sprintf("#chatwhatsappbox_%s", visitor.session)).find(".chatwhatsappboxtitle").css({"color": personVisitor.fontColor}); 
		});
	}  
}

/* @brief chatWhatsAppOnEventPong
 * @details get pong on tick time process from socket agent
 * @param "ctm" object socket client 
 * @param "client" object stream client sock
 * @return mixed return value
 */ 
 function chatWhatsAppOnEventPong(ctm, client)
{
	while (window.ctmClient.helper.isObj(client)){ 
		if (chatWhatsAppOnAgentMonitor !== null ){
			chatWhatsAppOnAgentMonitor();
		} break;
	} return;
}

/* @brief chatWhatsAppOnEventState
 * @details get detail description function
 * @param "ctm" object socket client 
 * @param "chat" object stream client sock
 * @param "states" state of agent 
 * @return mixed return value
 */ 
 function chatWhatsAppOnEventState(ctm, chat, states)
{
	if (typeof states === "undefined"){
		var states = 0;
	}  if (typeof chat !== "object"){
		return false;
	} if (!chat.hasOwnProperty("username")){
		return false;
	}  
	var chatVistor = DBI.getJSON(chatWhatsAppSession, false);  
	if (typeof chatVistor === "object" && chatVistor !== null){
		$.each(chatVistor, (x, s) => {
			var chatBoxVisitor = $("#chatwhatsappbox_"+ s.session); 
			if (chatBoxVisitor.length > 0){ 
				if (states > 0){
					chatBoxVisitor.find(".chatwhatsappontyping").html("");
					chatBoxVisitor.find(".chatboxtextareawhatsapp")
					.attr("disabled", false);	
				} else {
					chatBoxVisitor.find(".chatwhatsappontyping").html("( Disconnect )");
					chatBoxVisitor.find(".chatboxtextareawhatsapp")
					.attr("disabled", true);
				} 
			}
		});
	} 
	var logout = false, request = {}, routing = {"action": "stateAppChat"};
	request.userid = Ext.Session("UserId").getSession();
	request.states = states; 
	logout = {
		url: chatWhatsAppAction(routing), 
		cache: false,
		async: true,
		crossDomain: true,
		type: "POST",
		data: request,	
		dataType: "json",
		success: (ret, status) => {
			console.log("CTM.Agent >> Auto Changed State: [%s]", states);
		}
	}; 
	$.ajax(logout);
	return false; 
}
 
/* @brief chatWhatsAppOnVisitorMonitor
 * @details monitoring session visitor by server 
 * @param "ctm" object socket client 
 * @param "visitors" object all visitor connected with me 
 * @return mixed return value
 */
 function chatWhatsAppOnVisitorMonitor(ctm, visitors)
{
	if (!ctm.helper.isObj(visitors)){
		return 0;
	} if (visitors.size <= 0) {
		return 0;
	} $.each(visitors.data, (session, visitor) => { 
		var localVisitorBoxChat = DBI.getJSON(session, false); 
		if (typeof localVisitorBoxChat === "object" && localVisitorBoxChat !== null ){
			if (!localVisitorBoxChat.hasOwnProperty("status")){
				localVisitorBoxChat["status"] = visitor.status; 
			} localVisitorBoxChat["status"] = visitor.status;
			DBI.setItem(session, JSON.stringify(localVisitorBoxChat));	
		}  
	});  return visitors;
}

/* @brief chatWhatsAppOnCompleted
 * @details event completed message 
 * @param "ctm" object socket client 
 * @param "events" object stream from server
 * @return mixed return value
 */
 function chatWhatsAppOnCompleted(ctm, events)
{
	if (typeof events === "object" && (events.hasOwnProperty("data"))){
		window.ctmClient.Utils.loger("Chat Session: %s Completed", events.data.session);  
	} return 0;
}

/* @brief chatWhatsAppOnDeleted
 * @details got message on delete message 
 * @param "ctm" object socket client 
 * @param "events" object stream from server
 * @return mixed return value
 */
 function chatWhatsAppOnDeleted(ctm, events)
{
	var message = (typeof events === "object" && events.hasOwnProperty("data")) ? events.data : false;  
	if (message){
		$("#chatboxmessagewhatsapp-"+ message.msgid).hide();  
		window.ctmClient.Utils.loger("Chat ID: %s Deleted", message.msgid);  
	} return 0;
}

/* @brief chatWhatsAppOnDeleted
 * @details got message session start|end 
 * @param "ctm" object socket client 
 * @param "events" object stream from server
 * @return mixed return value
 */
 function chatWhatsAppOnSession(ctm, events)
{
	var visitor = false, transport = {}, chatMessageComplete = {};
	if (!ctm.helper.isObj(events)){
		return 0;
	}
	transport.success = true;
	transport.data = {};
	visitor = chatWhatsAppXor(events, "visitor", false);
	if (!visitor){
		return false;
	} if (!transport.data.hasOwnProperty(visitor.session)){
		transport.data[visitor.session] = {};
	}
	transport.data[visitor.session]["session"]  = chatWhatsAppXor(visitor, "session", "");   
	transport.data[visitor.session]["channel"]  = chatWhatsAppXor(visitor, "channel", "");   
	transport.data[visitor.session]["userid"]   = chatWhatsAppXor(visitor, "userid", ""); 	 
	transport.data[visitor.session]["email"]  	= chatWhatsAppXor(visitor, "email", ""); 	 
	transport.data[visitor.session]["username"] = chatWhatsAppXor(visitor, "username", "");  
	transport.data[visitor.session]["status"]   = chatWhatsAppXor(visitor, "state", ""); 
	/* get chat completed to drop from this session */
	chatMessageComplete = DBI.getJSON(chatWhatsAppComplete, {});
	if (chatMessageComplete.hasOwnProperty(visitor.session)){
		delete chatMessageComplete[visitor.session];
	} /* update chat message completed */
	DBI.setItem(chatWhatsAppComplete, JSON.stringify(chatMessageComplete));
	if (chatWhatsAppOnVisitorMonitor(ctm, transport)){
		window.ctmClient.Utils.loger("User With Session: %s Disconnect", chatWhatsAppXor(visitor, "session", 0));  
	} return 0;      
}

/* @brief chatWhatsAppOnHeartbeat
 * @details get detail description function
 * @param "ctm" object socket client 
 * @param "events" object stream from server
 * @return mixed return value
 */
 function chatWhatsAppOnHeartbeat(ctm, events)
{ 
	var itemsfound = 0; 
	if (windowWhatsAppFocus == false) {
		var blinkNumberWhatsApp = 0;
		var titleChangedWhatsApp = 0;
		for (x in newWhatsAppMessagesWin) {
			if (newWhatsAppMessagesWin[x] == true) {
				++ blinkNumberWhatsApp;
				var blinkVisitor = DBI.getItem(x);
				if (blinkVisitor === null) continue;
				else if (blinkVisitor !== null){
					blinkVisitor = JSON.parse(blinkVisitor);
				} if (blinkNumberWhatsApp>= blinkWhatsAppOrder) {
					document.title = blinkVisitor.name +' says...';
					titleChangedWhatsApp = 1;
					break;	
				}
			}
		} 
		if (titleChangedWhatsApp == 0) {
			document.title = originalWhatsAppTitle;
			blinkWhatsAppOrder = 0;
		} else {
			++blinkWhatsAppOrder;
		}

	} else {
		for (x in newWhatsAppMessagesWin) {
			newWhatsAppMessagesWin[x] = false;
		}
	} 
	for (x in newWhatsAppMessages) {
		if (newWhatsAppMessages[x] == true) {
			if (chatWhatsAppBoxFocus[x] == false) {
				// FIXME: add toggle all or none policy, otherwise it looks funny
				$('#chatwhatsappbox_'+x+' .chatboxheadwhatsapp').toggleClass('chatwhatsappboxblink');
			}
		}
	}
	// chatWhatsAppSetHeaderBeforeSend(s) 
	chatWhatsAppSetHeaderBeforeSend((header) => {
		var request = {}, routing = {"action": "heartbeatAppChat"};
		request.userid = Ext.Session("UserId").getSession(); 
		request.agents = Ext.Session("UserId").getSession();
		$.ajax ({
			url: chatWhatsAppAction(routing),
			crossDomain: true,
			cache: false, 
			async: true,
			type : "POST", 
			dataType: "json",
			data: request,
			success: (ret) => { 
				// then each statements 
				$.each(ret.items, (i,item) => {
					if (item) { 
						// fix strange ie bug 
						var localVisitorName = item.hasOwnProperty("f") ? item.f : false,
						localVisitorBoxSession = item.hasOwnProperty("i") ? item.i: false, 
						localVisitorBoxNew = false,
						localVisitorBoxChat = false; 
						// check and update session storage : 
						var localVisitorStorage = DBI.getItem(localVisitorBoxSession);
						if (localVisitorStorage !== null){
							localVisitorStorage = JSON.parse(localVisitorStorage);
						} else {
							chatWhatsAppChatWith ++;
							localVisitorBoxNew = true;
							var localVisitorCounter = DBI.getItem(chatWhatsAppVisitor);
							if (localVisitorCounter !== null){
								chatWhatsAppChatWith = parseInt(localVisitorCounter) + 1;
							}  
							localVisitorBoxChat = window.chatWhatsAppPrint("CHAT_%s", chatWhatsAppChatWith);
							localVisitorStorage = { 
								id: chatWhatsAppChatWith, 
								chat: localVisitorBoxChat,
								name: localVisitorName,
								session: localVisitorBoxSession
							};
						}  
						// updated session only 
						if (!localVisitorBoxNew){
							if (localVisitorBoxSession !== localVisitorStorage.session){
								localVisitorStorage.session = localVisitorBoxSession;
							}
						} 
						var localVisitorSession = DBI.getItem(chatWhatsAppSession);
						if (localVisitorSession !== null){
							localVisitorSession = JSON.parse(localVisitorSession);
							localVisitorSession[localVisitorBoxSession] = {id: chatWhatsAppChatWith, session: localVisitorBoxSession, name: item.f};  
						} else {
							localVisitorSession = {};
							localVisitorSession[localVisitorBoxSession] = {id: chatWhatsAppChatWith, session: localVisitorBoxSession, name: item.f};
						} 
						
						DBI.setItem(chatWhatsAppVisitor, chatWhatsAppChatWith);
						DBI.setItem(chatWhatsAppSession, JSON.stringify(localVisitorSession));
						DBI.setItem(localVisitorBoxSession, JSON.stringify(localVisitorStorage));
						
						// start write chatbox session from visitor:  
						if ($("#chatwhatsappbox_" + localVisitorBoxSession).length <= 0) {
							chatWhatsAppCreateBox(localVisitorBoxSession);
						} if ($("#chatwhatsappbox_"+ localVisitorBoxSession).css('display') == 'none') {
							$("#chatwhatsappbox_"+ localVisitorBoxSession).css('display','block');
							chatWhatsAppRestructureBox();
						} if (item.s == 1) {
							item.f = request.agents;
						} if (item.s == 2) {
							$("#chatwhatsappbox_"+ localVisitorBoxSession +" .chatboxcontentwhatsapp")
								.append('<div class="chatboxmessagewhatsapp"><span class="chatboxwhatsappinfo">'+ item.m +'</span></div>');
						} else {
							// chatbox position 
							var displayChat = "chat-left-form";
							if (item.f != request.agents){
								displayChat = "chat-right-form";  
							} 
							newWhatsAppMessages[localVisitorBoxSession] = true;
							newWhatsAppMessagesWin[localVisitorBoxSession] = true;
							chatWhatsAppBoxConversation("receive", item, (messageStr) => {
								$("#chatwhatsappbox_"+ localVisitorBoxSession +" .chatboxcontentwhatsapp").append(messageStr); 
							}); 
						}
						$("#chatwhatsappbox_"+ localVisitorBoxSession +' .chatboxcontentwhatsapp')
						.scrollTop($("#chatwhatsappbox_"+ localVisitorBoxSession +" .chatboxcontentwhatsapp")[0].scrollHeight);
						itemsfound += 1;
					}
				});

				chatWhatsAppHeartbeatCount++; 
				if (itemsfound > 0) {
					chatWhatsAppHeartbeatTime = minChatWhatsAppHeartbeat;
					chatWhatsAppHeartbeatCount = 1;
				} else if (chatWhatsAppHeartbeatCount >= 10) {
					chatWhatsAppHeartbeatTime *= 2;
					chatWhatsAppHeartbeatCount = 1;
					if (chatWhatsAppHeartbeatTime > maxChatWhatsAppHeartbeat) {
						chatWhatsAppHeartbeatTime = maxChatWhatsAppHeartbeat;
					} 
				}  
			}, 
			// get error message 
			error: (request, error) => {
				console.warn("request: ", request, " error: ", error); 
			}
		}); 
	}); 	 
}

/* @brief chatWhatsAppCloseMinimize
 * @details get detail description function
 * @param "chatwhatsappboxsession" chatbox session 
 * @param "doCallback" callback function if success  
 * @return mixed return value
 */
 function chatWhatsAppCloseMinimize(chatwhatsappboxsession, doCallback) 
{
	var chatAgentId = Ext.Session("UserId").getSession(), chatBoxVisitorGrowth = {};
	if (DBI.getItem(chatWhatsAppMinimize) !== null){
		chatBoxVisitorGrowth = JSON.parse(DBI.getItem(chatWhatsAppMinimize)); 
	} if (chatBoxVisitorGrowth.hasOwnProperty(chatAgentId)){
		if (chatBoxVisitorGrowth[chatAgentId].hasOwnProperty(chatwhatsappboxsession)){
			delete chatBoxVisitorGrowth[chatAgentId][chatwhatsappboxsession];
			DBI.setItem(chatWhatsAppMinimize, JSON.stringify(chatBoxVisitorGrowth));
		}
	} if (typeof doCallback === "function"){
		doCallback.apply(this, [chatwhatsappboxsession]);
	} return;
}

/* @brief chatWhatsAppCloseBox
 * @details close chat box by agent session 
 * @param "chatwhatsappboxsession" session of cghatbox 
 * @return mixed return value
 */
 function chatWhatsAppCloseBox(chatwhatsappboxsession) 
{
	$("#chatwhatsappbox_"+ chatwhatsappboxsession).css("display", "none");  
	var visitorMessageComplete = DBI.getJSON(chatWhatsAppComplete, {});
	if (!visitorMessageComplete.hasOwnProperty(chatwhatsappboxsession)){
		visitorMessageComplete[chatwhatsappboxsession] = chatwhatsappboxsession;
	} // push to local storage
	DBI.setItem(chatWhatsAppComplete, JSON.stringify(visitorMessageComplete));
	if (typeof window.ctmClient === "object"){
		try { /* sent to message of completed message */
			window.ctmClient.Complete(chatwhatsappboxsession);
		} catch (e) {console.warn("error:", e)}
	} /* collab with ajax session */
	chatWhatsAppRestructureBox((data) => { 
		var request = {}, routing = {"action": "closeAppChat"};
		request.chatbox = chatwhatsappboxsession;
		request.userid = Ext.Session("UserId").getSession();
		// post data to retructure process 
		$.ajax ({
			url: chatWhatsAppAction(routing),
			type: "POST", 
			dataType: "json",
			crossDomain: true,
			cache: false, 
			async: true, 
			data: request,
			success: (res) => { 
				// get all response process 
				if (typeof res === "object" &&(parseInt(res.success) > 0)){ 
					var localVisitorSession = DBI.getItem(chatWhatsAppSession),
					localVisitorCounter = DBI.getItem(chatWhatsAppVisitor);
					if (localVisitorSession !== null){
						localVisitorSession = JSON.parse(localVisitorSession);
						$.each(localVisitorSession, (chatVisitorSession, visitor) => { 
							if (chatVisitorSession == chatwhatsappboxsession){
								chatWhatsAppCloseMinimize(chatVisitorSession, (e) => { 
									delete localVisitorSession[chatVisitorSession];
								});
								DBI.removeItem(chatVisitorSession);	
								if (localVisitorCounter !== null){
									localVisitorCounter = parseInt(localVisitorCounter);
									if (localVisitorCounter > 0){
										localVisitorCounter --;
									}
								}
							} 
						});
						DBI.setItem(chatWhatsAppVisitor, localVisitorCounter);
						DBI.setItem(chatWhatsAppSession, JSON.stringify(localVisitorSession));
					} 
				}
			}, // catch on error
			error: (err) => {
				console.warn("error:", err);
			}	
		}); 
	});
}

/* @brief chatWhatsAppToggleGrowth
 * @details get detail description function
 * @param "chatwhatsappboxsession" session of cghatbox 
 * @return mixed return value
 */
 function chatWhatsAppToggleGrowth(chatwhatsappboxsession) 
{
	var chatBoxVisitorAgentId = Ext.Session("UserId").getSession(), chatBoxVisitorMinimize = {}, chatBoxVisitorGrowth = false;
	chatBoxVisitorMinimize[chatBoxVisitorAgentId] = {}; 
	chatBoxVisitorGrowth = DBI.getItem(chatWhatsAppMinimize);
	if (chatBoxVisitorGrowth !== null){
		chatBoxVisitorGrowth = JSON.parse(chatBoxVisitorGrowth);
		chatBoxVisitorMinimize[chatBoxVisitorAgentId] = chatBoxVisitorGrowth[chatBoxVisitorAgentId];
	}
	// handler grow window section 
	while (chatwhatsappboxsession !== ""){
		// show up chatbox 
		if ($("#chatwhatsappbox_"+ chatwhatsappboxsession +" .chatboxcontentwhatsapp").css("display") == 'none') { 
			$('#chatwhatsappbox_'+ chatwhatsappboxsession +' .chatboxcontentwhatsapp').css("display","block");
			$('#chatwhatsappbox_'+ chatwhatsappboxsession +' .chatboxinputwhatsapp').css("display","block");
			$('#chatwhatsappbox_'+ chatwhatsappboxsession +' .chatclearwhatsapp').css("display","block");
			$('#chatwhatsappbox_'+chatwhatsappboxsession+' .chatboxcontentwhatsapp').scrollTop($('#chatwhatsappbox_'+ chatwhatsappboxsession +' .chatboxcontentwhatsapp')[0].scrollHeight);
			
			$("#chatwhatsappbox_"+ chatwhatsappboxsession).find("#chat-whatapps-minimize").html("<i class='fa fa-minus-square'></i>");
			if (chatBoxVisitorMinimize[chatBoxVisitorAgentId].hasOwnProperty(chatwhatsappboxsession)){
				delete chatBoxVisitorMinimize[chatBoxVisitorAgentId][chatwhatsappboxsession];
			} break;
		}  
		// close content
		
		// $("#chatwhatsappbox_"+ chatwhatsappboxsession).css("bottom", chatWhatsAppBottom);
		$("#chatwhatsappbox_"+ chatwhatsappboxsession +" .chatboxcontentwhatsapp").css("display","none");
		$("#chatwhatsappbox_"+ chatwhatsappboxsession +" .chatboxinputwhatsapp").css("display", "none");
		$("#chatwhatsappbox_"+ chatwhatsappboxsession +" .chatclearwhatsapp").css("display", "none");
		$("#chatwhatsappbox_"+ chatwhatsappboxsession).find("#chat-whatapps-minimize").html("<i class='fa fa-window-maximize'></i>"); 
		if (!chatBoxVisitorMinimize[chatBoxVisitorAgentId].hasOwnProperty(chatwhatsappboxsession)){
			chatBoxVisitorMinimize[chatBoxVisitorAgentId][chatwhatsappboxsession] = chatwhatsappboxsession;
		} break;
	} 
	/* update storage */
	DBI.setItem(chatWhatsAppMinimize, JSON.stringify(chatBoxVisitorMinimize)); 
}

/* @brief chatWhatsAppBoxConversation
 * @details get detail description function
 * @param "messageStr" session of chatbox 
 * @return mixed return value
 */
 function chatWhatsAppBoxRegexString(messageStr) 
{
	messageStr = messageStr.replace(/\"/g,"&quot;");
	messageStr = messageStr.replace(/\\n/g, "<br>"); 
	return messageStr;
}

/* @brief chatWhatsAppBoxConversation
 * @details get detail description function
 * @param "type" session of chatbox 
 * @param "msg" session of chatbox 
 * @param "hook" session of chatbox 
 * @return mixed return value
 */
 function chatWhatsAppBoxConversation(type, msg, hook) 
{
	var messageStr = "";
	var documentStr = "";
	var username = Ext.Session("UserId").getSession();
	var session = chatWhatsAppXor(msg, "i", "");
	var message = chatWhatsAppXor(msg, "m", ""); 
	var msgid = chatWhatsAppXor(msg, "n", 0);
	var files = chatWhatsAppXor(msg, "x", 0);
	var from = chatWhatsAppXor(msg, "f", "");
	var sent = chatWhatsAppXor(msg, "t", 0); 
	if (username === from){
		from += " ( Me )";
	} 
	
	// get contains files  
	message = chatWhatsAppBoxRegexString(message);
	if (files){
		var filename = chatWhatsAppXor(files, "name", "");
		var filetype = chatWhatsAppUpper(chatWhatsAppXor(files, "type", ""));
		var filesize = chatWhatsAppBytes(chatWhatsAppXor(files, "size", 0));
		var fileurls = chatWhatsAppXor(files, "url", "");
		
		documentStr = "<table class=\"chatboxmessagewhatsappdoccover\">";
		documentStr += "<tr>";
		documentStr += "<td class=\"chatboxmessagewhatsappdoctype\"><i class=\"fa fa-paperclip\"></i></td>";
		documentStr += "<td class=\"chatboxmessagewhatsappdocname\">"+ filename +"</td>";
		documentStr += "</tr>";
		documentStr += "<tr>";
		documentStr += "<td colspan=\"2\" class=\"chatboxmessagewhatsappdocsize\"><span>"+ filetype +"</span>"; 
		documentStr += "<span class=\"chatboxmessagewhatsappdocdotted\">.</span><span>"+ filesize +"</span>";
		documentStr += "<span class=\"chatboxmessagewhatsappdocsparator\">|</span>";
		documentStr += "<span class=\"chatboxmessagewhatsappdocdownload\"><a href=\"javascript:void(0);\" onclick=\"chatWhatsAppDownload(this, '"+ fileurls +"');\">Donwload</span>";
		documentStr +="</td>";
		documentStr += "</tr>";
		documentStr += "</table>";
	}   
	// on send message  
	if (type === "sender"){
		messageStr  = "<div id=\"chatboxmessagewhatsapp-"+ msgid +"\" class=\"chatboxmessagewhatsapp ourMessage\">";
		messageStr += "<table border=0 cellpadding=0 cellspacing=0 style=\"width:99%;\">";
		messageStr += "<tr>";
		messageStr += "<td class=\"chatboxmessagewhatsappicon\">";
		messageStr += "<span class=\"chat-icon-me\"><i class='fa fa-user-circle'></i></span>";		
		messageStr += "</td>";
		messageStr += "<td class=\"chatboxmessagewhatsappfrom\"><b>"+ from +"</b></td>";
		messageStr += "</tr>";
		messageStr += "<tr>";
		messageStr += "<td colspan='2' class=\"chatboxmessagewhatsapptext\">";
		messageStr += "<div class=\"chatboxmessagewhatsappcover\">";
		if (files){
			messageStr += "<div class=\"chatboxmessagewhatsappdoc\">";
			messageStr += documentStr;
			messageStr += "</div>"; 
		} 
		messageStr += message;
		messageStr += "</div>";
		messageStr += "</tr>";
		
		messageStr +="<tr>";
		messageStr +="<td colspan=2 class=\"chatboxmessagewhatsappsent\">";
		messageStr +="<span class=\"chatboxmessagewhatsappspan\">"+ sent +"</span>&nbsp;<span>|</span>&nbsp;";
		messageStr +="<span><a href=\"javascript:void(0);\" onclick=\"chatWhatsAppDeleteMessage('"+ username +"', '"+ session +"', '"+ msgid +"');\">Delete</a></span></td>";
		messageStr +="</tr>";
		messageStr += "</table>";
		messageStr += "</div>";
	} 
	// on receive message
	else if (type === "receive"){ 
		messageStr  ="<div id=\"chatboxmessagewhatsapp-"+ msgid +"\"  class=\"chatboxmessagewhatsapp theirMessage\">";
		messageStr +="<table border=0 cellpadding=0 cellspacing=0 style=\"width:99%;\">";
		messageStr +="<tr>";
		messageStr +="<td class=\"chatboxmessagewhatsappfrom\">"+ from +"</td>";
		messageStr +="<td class=\"chatboxmessagewhatsappicon\">";
		messageStr +="<span class=\"chat-icon-you\"><i class=\"fa fa-user-circle\"></i></span>";
		messageStr +="</td>";
		messageStr +="</tr>";
		messageStr +="<tr>";
		messageStr +="<td colspan= 2 class=\"chatboxmessagewhatsapptext\">";
		messageStr +="<div class=\"chatboxmessagewhatsappcover\">"; 
		if (files){
			messageStr +="<div class=\"chatboxmessagewhatsappdoc\">"+ documentStr +"</div>"; 
		}  
		messageStr += message;
		messageStr +="</div>"
		messageStr +="</td>"; 
		messageStr +="</tr>";
		messageStr +="<tr>";
		messageStr +="<td colspan=2 class=\"chatboxmessagewhatsappsent\">";
		messageStr +="<span><a href=\"javascript:void(0);\" onclick=\"chatWhatsAppDeleteMessage('"+ username +"', '"+ session +"', '"+ msgid +"');\">Delete</a></span>";
		messageStr +="&nbsp;<span>|</span>&nbsp;<span class=\"chatboxmessagewhatsappspan\">"+ sent +"</span></td>";
		messageStr +="</tr>";
		messageStr +="</table>";
		messageStr +="</div>";  
	} 
	// After sent will call the hook 
	if (typeof hook === "function"){
		hook.apply(this, [messageStr]);
	} return messageStr;
}

/* @brief chatWhatsAppCheckBoxInput
 * @details get detail description function
 * @param "event" session of cghatbox 
 * @param "chatboxtextarea" session of cghatbox 
 * @param "chatwhatsappboxsession" session of chatbox 
 * @return mixed return value
 */
 function chatWhatsAppCheckBoxInput(event, chatboxtextarea, chatwhatsappboxsession) 
{
	// chatwhatsappboxtitle : sessionid 
	var chatBlobFiles = $("#blob-files-"+ chatwhatsappboxsession).val();
	var chatVisitorSession = DBI.getItem(chatwhatsappboxsession);
	if (chatVisitorSession !== null){
		chatVisitorSession = JSON.parse(chatVisitorSession);
	} // local parameter 
	var chatVisitorId = chatWhatsAppXor(chatVisitorSession, "name", false);
	var chatAgentId = Ext.Session('UserId').getSession(); 
	// before send check validation of visitor 
	if (!chatVisitorId){
		return false;
	} 
	// set for started process post  
	var request = {}, routing = {"action": "sendAppChat"};
	request.message = "";   
	request.to = chatVisitorId;
	request.from = chatAgentId; 
	request.files = chatBlobFiles; 
	request.message = $(chatboxtextarea).val();
	request.message = request.message.replace(/^\s+|\s+$/g,"");  
	// next to process send message 
	if (event.type === "click") {   
		if (request.files === "" && request.message === ""){
			$(chatboxtextarea).focus().css("height", "44px"); 	
			return false;
		} 
		// post ajax request 
		$.ajax ({
			url: chatWhatsAppAction(routing),
			type: "POST", 
			dataType: "json",
			crossDomain: true,
			cache: false, 
			data: request,
			xhr: function(events) {
				$(chatboxtextarea).attr("disabled", true);
				var myXhr = $.ajaxSettings.xhr();
				if (myXhr.upload){
					myXhr.upload.addEventListener("progress", function(event){
						var percent = (event.loaded / event.total) * 100;
						$("#fileProgress-"+ chatwhatsappboxsession).html("Upload: "+ Math.round(percent) +" %");
					}, false);
				} return myXhr;  
			}, 
			success:(res) => {
				$(chatboxtextarea).attr("disabled", false);
				$(chatboxtextarea).val("").focus().css("height", "44px");  
				if (typeof res !== "object"){
					return false;
				} if (typeof chatWhatsAppXor(res, "msg", false) !== "object"){
					return false;
				}  
				chatWhatsAppCloseFiles(this, chatwhatsappboxsession);
				chatWhatsAppBoxConversation( "sender", chatWhatsAppXor(res, "msg", {}), (messageStr) => {
					$("#chatwhatsappbox_"+ chatwhatsappboxsession +" .chatboxcontentwhatsapp").append(messageStr);
					$("#chatwhatsappbox_"+ chatwhatsappboxsession +" .chatboxcontentwhatsapp").scrollTop($('#chatwhatsappbox_'+ chatwhatsappboxsession +' .chatboxcontentwhatsapp')[0].scrollHeight);
				});    
			},
			error:(err) => {
				console.warn("error:", err);
				window.alert("Sorry, the uploaded file may be too large, try another file"); 
				$(chatboxtextarea).attr("disabled", false);
				chatWhatsAppCloseFiles(this, chatwhatsappboxsession); 
			}
		});   
		chatWhatsAppHeartbeatTime = minChatWhatsAppHeartbeat;
		chatWhatsAppHeartbeatCount = 1;
		window.clearTimeout(window.chatWhatsAppTimeout); 
		return false;
	}	 
	// on keydown 
	if (event.type === "keydown" &&(event.keyCode == 13 && event.shiftKey == 0)){  
		// if file or text is empty
		if (request.files === "" && request.message === ""){
			$(chatboxtextarea).focus().css("height", "44px"); 	
			return false;
		} // post ajax request 
		$.ajax ({
			url: chatWhatsAppAction(routing),
			type: "POST", 
			dataType: "json",
			crossDomain: true,
			cache: false,  
			data: request,
			xhr: function(events) {
				$(chatboxtextarea).attr("disabled", true);
				var myXhr = $.ajaxSettings.xhr();
				if (myXhr.upload){
					myXhr.upload.addEventListener("progress", function(event){
						var percent = (event.loaded / event.total) * 100;
						$("#fileProgress-"+ chatwhatsappboxsession).html("Upload: "+ Math.round(percent) +" %");
					}, false);
				} return myXhr;  
			}, 
			success:(res) => {
				$(chatboxtextarea).attr("disabled", false);
				$(chatboxtextarea).val("").focus().css("height", "44px");  
				if (typeof res !== "object"){
					return false;
				} if (typeof chatWhatsAppXor(res, "msg", false) !== "object"){
					return false;
				}  
				chatWhatsAppCloseFiles(this, chatwhatsappboxsession);
				chatWhatsAppBoxConversation( "sender", chatWhatsAppXor(res, "msg", {}), (messageStr) => {
					$("#chatwhatsappbox_"+ chatwhatsappboxsession +" .chatboxcontentwhatsapp").append(messageStr);
					$("#chatwhatsappbox_"+ chatwhatsappboxsession +" .chatboxcontentwhatsapp").scrollTop($('#chatwhatsappbox_'+ chatwhatsappboxsession +' .chatboxcontentwhatsapp')[0].scrollHeight);
				});    
			},
			error:(err) => {
				console.warn("error:", err);
				window.alert("Sorry, the uploaded file may be too large, try another file"); 
				$(chatboxtextarea).attr("disabled", false);
				chatWhatsAppCloseFiles(this, chatwhatsappboxsession); 
			}
		});   
		chatWhatsAppHeartbeatTime = minChatWhatsAppHeartbeat;
		chatWhatsAppHeartbeatCount = 1; 
		window.clearTimeout(window.chatWhatsAppTimeout); 
		return false; 
	}
	var adjustedHeight = chatboxtextarea.clientHeight, maxHeight = 94;
	if (maxHeight > adjustedHeight) {
		adjustedHeight = Math.max(chatboxtextarea.scrollHeight, adjustedHeight);
		if (maxHeight)
			adjustedHeight = Math.min(maxHeight, adjustedHeight);
		if (adjustedHeight > chatboxtextarea.clientHeight)
			$(chatboxtextarea).css('height',adjustedHeight+8 +'px');
	} else {
		$(chatboxtextarea).css('overflow','auto');
	} 
}
 
/* @brief chatWhatsAppClearReason
 * @details get detail description function
 * @param "chatwhatsappboxsession" session of chatbox 
 * @return mixed return value
 */
 function chatWhatsAppClearReason(chatwhatsappboxsession)
{
	var userid = Ext.Session("UserId").getSession();
	$("#chatwhatsappbox_"+ chatwhatsappboxsession +" .chatboxcontentwhatsapp").html("");
	$.post(chatWhatsAppAction({ "action": "closeAppChat", "userid": userid }), {"chatbox": chatwhatsappboxsession}, (res) => {	
		console.log(res);
	});
}

/* @brief chatWhatsAppPlugin
 * @details get detail description function
 * @param "contextActivity" session of chatbox 
 * @return mixed return value
 */
 function chatWhatsAppPlugin(contextActivity)
{
	var whatsAppNumber = $(chatWhatsAppPrint('#%s', contextActivity));
	if (whatsAppNumber.length >0 &&(whatsAppNumber.val() == '')){
		return 0;
	} else if (whatsAppNumber.length >0 &&(whatsAppNumber.val() != '')){
		window.chatWhatsAppWith(whatsAppNumber.val());
		return 0;
	} return 0;
}

/* @brief chatWhatsAppLoginSession
 * @details agent set login fron browser 
 * @param "s" object session of agents 
 * @return mixed return value
 */ 
 function chatWhatsAppLoginSession(s)
{ 
	var userid = Ext.Session("UserId").getSession();
	var routing = {"action": "loginAppChat",  "userid": userid}; 
	$.ajax ({
		url: chatWhatsAppAction(routing), 
		cache: false,
		type: "POST",
		dataType: "json",
		success: (ret, status) => {
			if (typeof ret !== "object"){
				ret = JSON.parse(ret);
			} if (typeof s === "function"){
				s.apply(this, [ret]);
			}
		}
	});  
}

/* @brief chatWhatsAppReadySession
 * @details agent set ready fron browser 
 * @param "s" object session of agents 
 * @return mixed return value
 */ 
 function chatWhatsAppReadySession(s)
{  
	// sent message via socket 
	if (typeof window.ctmClient === "object"){
		if (window.ctmClient.helper.isFunc(window.ctmClient.Ready)){
			window.ctmClient.Ready(s);
			return;
		}
	} // via ajax 
	var userid = Ext.Session('UserId').getSession()
	var routing = {"action": "readyAppChat", "userid": userid }; 
	$.ajax ({
		url: chatWhatsAppAction(routing), 
		crossDomain: true,
		type: "POST",
		cache: false,
		dataType: "json",
		success: (ret, status) => {
			if (typeof s === "function"){
				s.apply(this, [ret]);
			}
		}
	}); 
}

/* @brief chatWhatsAppNotReadySession
 * @details  agent set "aux" fron browser 
 * @param "s" object session of agents 
 * @return mixed return value
 */ 
 function chatWhatsAppNotReadySession(s)
{  
	/* sent message via socket */
	if (typeof window.ctmClient === "object"){
		if (window.ctmClient.helper.isFunc(window.ctmClient.Aux)){
			window.ctmClient.Aux(s);
			return;
		}
	} /* Old session process via ajax */
	var userid = Ext.Session('UserId').getSession();
	var routing = {"action": "notreadyAppChat", "userid": userid };
	$.ajax ({
		url: chatWhatsAppAction(routing), 
		cache: false,
		dataType: "json",
		type: "POST",
		success: (ret, status) => {
			if (typeof s === "function"){
				s.apply(this, [ret]);
			}
		}
	}); 
}
  
/* @brief chatWhatsAppLogoutSession
 * @param object $to detail process 
 * @param "s" object session of agents 
 * @return mixed return value
 */ 
 function chatWhatsAppLogoutSession(s)
{  
	var routing = {"action": "logoutAppChat"}, request = {}; 
	request.userid = Ext.Session('UserId').getSession();
	var logout = {
		url: chatWhatsAppAction(routing), 
		async: true,
		cache: false,
		crossDomain: true,
		type: "POST",
		data: request,	
		dataType: "json",
		success: (ret, status) => {
			if (typeof s === "function" ){
				s.apply(this, [ret]);
			}
		}
	}; 
	// if modul ctm is loaded 
	if (typeof window.ctmClient === "object"){
		window.ctmClient.Logout((h) => {  
			$.ajax(logout); 
		}); 
		return false;
	} // then sent logout via webajax 
	$.ajax(logout); 
}

/* @brief chatWhatsAppSentTypingSession
 * @param object $to detail process 
 * @param "chatbox" session of chatbox 
 * @param "typing" object typing info 
 * @return mixed return value
 */ 
 function chatWhatsAppSentTypingSession(chatbox, typing)
{  
	// get session local Agents 
	var userid = Ext.Session('UserId').getSession();
	if (typeof chatbox === "object") {
		chatbox.user = "agent";
		chatbox.from = userid;
		chatbox.type = typing;
	} // custom for check data if moduls exist will sent via webscoket 
	if (typeof window.ctmClient === "object"){
		window.ctmClient.SentTyping(chatbox);
		return ;
	} 
	var routing = chatWhatsAppAction({"action": "sentTypingAppChat"});
	$.post(routing, chatbox, (data, status) => {
		// console.log("chatWhatsAppSentTypingSession:", data); 
	});
	return 0;
}

/* @brief onTypingWhatsAppSession
 * @details got message typing from visitor 
 * @param object "ctm" detail process not usage
 * @param object "visitor" detail process not usage
 * @return mixed return value
 */ 
 function chatWhatsAppOnTypingSession(ctm, visitor)
{  
	if (typeof window.ctmClient === "object" && (typeof visitor === "object")){
		if (!visitor.hasOwnProperty("session")){
			return 0;
		}  // chat hanya boleh yang tidak ada pada session completed 
		var visitorMessageComplete = DBI.getJSON(chatWhatsAppComplete, {});
		if (visitorMessageComplete.hasOwnProperty(visitor.session)){
			return 0;
		} // new handler on typing 	
		if (visitor.typing){ // get on typing base only 
			visitor.status = 1;
			chatWhatsAppCreateVisitor(visitor, (events) => {
				chatWhatsAppCreateBox(visitor.session, 1);
			}); 
			$(sprintf("#chatwhatsappbox_%s", visitor.session)).find(".chatwhatsappontyping").css("font-size", "9px;").html("( typing ... )"); 
		} else if (!visitor.typing){
			$ (sprintf("#chatwhatsappbox_%s", visitor.session)).find(".chatwhatsappontyping").html(""); 
		} return;
	}
}

/* @brief chatWhatsAppWithSession
 * @param object $to detail process 
 * @return mixed return value
 */
 function chatWhatsAppWithSession(options)
{  
	var localVisitorSessionRetval = {}; 
	var localVisitorSession = DBI.getItem(chatWhatsAppSession);
	if (localVisitorSession !== null){ 
		localVisitorSession = JSON.parse(localVisitorSession);
	} else {
		return localVisitorSessionRetval;
	}  
	for (var i in localVisitorSession){
		var localVisitorName = localVisitorSession[i];
		if (localVisitorName !== "" ){
			localVisitorSessionRetval[localVisitorName.name] = localVisitorName.name; 
		}
	} 
	if (typeof options === "undefined"){
		return localVisitorSessionRetval; 
	} 
	if (options){
		localVisitorSessionRetval = Object.keys(localVisitorSessionRetval).join(",");
		localVisitorSessionRetval.toString();
	} return localVisitorSessionRetval;
	
}

/* @brief chatWhatsAppHasWhiteSpace
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppHasWhiteSpace(s)
{
  return /\s/g.test(s);
}

/* @brief chatWhatsAppStartConversation
 * @details get active last conversation ti bind local session
 * @param "doCallback" callback function if success 
 * @return mixed return value
 */
 function chatWhatsAppStartConversation(doCallback)
{
	var request = {}, routing = {"action": "conversationAppChat"};  
	request.userid = Ext.Session("UserId").getSession(); 
	$.ajax ({
		url: chatWhatsAppAction(routing), 
		type: "POST",
		dataType: "json",
		crossDomain: true,
		async: true,
		cache: false,
		data: request,
		success: (session) => { 
			var localVisitorSession = {};
			if (!session.error){
				localVisitorSession = DBI.getJSON(chatWhatsAppSession, false); 
				if (!localVisitorSession){
					localVisitorSession = {};
				} 
				var chatVisitorVisibility = {}, chatStorageVisibility = false;
				$.each(session.data, (localVisitorSessionId, visitor) => {
					var localVisitorCounter = DBI.getItem(chatWhatsAppVisitor), 
					localVisitorBoxChat = {}, localVisitorId = 0;  
					if (localVisitorCounter !== null){
						localVisitorId = parseInt(localVisitorCounter);
					} if (localVisitorSession.hasOwnProperty(localVisitorSessionId)){ 
						localVisitorSession[localVisitorSessionId]["channel"] = visitor.channel;
						localVisitorSession[localVisitorSessionId]["email"] = visitor.email;
						localVisitorSession[localVisitorSessionId]["update"] = visitor.update;  
						localVisitorBoxChat = DBI.getItem(localVisitorSessionId);
						if (localVisitorBoxChat !== null){
							localVisitorBoxChat = JSON.parse(localVisitorBoxChat);
						} if (typeof localVisitorBoxChat === "object"){ 
							if (localVisitorBoxChat.hasOwnProperty("chat")){
								delete localVisitorBoxChat["chat"];
							} if (!localVisitorBoxChat.hasOwnProperty("channel")){
								localVisitorBoxChat.channel = visitor.channel;
							} if (!localVisitorBoxChat.hasOwnProperty("email")){
								localVisitorBoxChat.email = visitor.email;
							} if (!localVisitorBoxChat.hasOwnProperty("update")){
								localVisitorBoxChat.update = visitor.update;
							} DBI.setItem(localVisitorSessionId, JSON.stringify(localVisitorBoxChat)); 
						}  
					} else if (!localVisitorSession.hasOwnProperty(localVisitorSessionId)){
						localVisitorId ++;	  
						localVisitorBoxChat.id = localVisitorId;
						localVisitorBoxChat.channel = visitor.channel;
						localVisitorBoxChat.status = visitor.channel; 
						localVisitorBoxChat.session = visitor.session; 
						localVisitorBoxChat.name = visitor.name;
						localVisitorBoxChat.email = visitor.email; 
						localVisitorBoxChat.update = visitor.update;
						DBI.setItem(chatWhatsAppVisitor, localVisitorId); 
						DBI.setItem(localVisitorSessionId, JSON.stringify(localVisitorBoxChat)); 
						localVisitorSession[localVisitorSessionId] = localVisitorBoxChat;
					} 
					chatStorageVisibility = DBI.getJSON(chatWhatsAppMinimize, {});
					if (chatStorageVisibility[request.userid] !== null ){
						chatStorageVisibility = chatStorageVisibility[request.userid];
					}  // make new Array for visibility
					chatVisitorVisibility[localVisitorSessionId] = {}; 
					chatVisitorVisibility[localVisitorSessionId]["state"] = 0;
					if ($.inArray(localVisitorSessionId, Object.keys(chatStorageVisibility)) >= 0 ){
						chatVisitorVisibility[localVisitorSessionId]["state"] = 1; 
					} 
				}); 
				/* visibility: catch then make new growth session ([session, [state ]]) */
				$.each(chatVisitorVisibility, (x, visibility) => { 
					chatWhatsAppCreateBox(x, visibility.state)
				}); 
				DBI.setItem(chatWhatsAppSession, JSON.stringify(localVisitorSession));
			} if (typeof doCallback === "function"){
				doCallback.apply(this, [localVisitorSession]);
			};
		} 
	});
}

/* @brief startWhatsAppSession
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppStartSession(doCallback)
{  
	chatWhatsAppStartConversation((localVisitorSession) => { 
		var request = {}, routing= {"action": "startAppChat"};
		request.userid = Ext.Session("UserId").getSession();
		request.guest = chatWhatsAppWithSession(true);
		request.agents = Ext.Session("UserId").getSession();
		request.visitor = chatWhatsAppWithSession(true); 
		$.ajax ({
			url: chatWhatsAppAction(routing), 
			crossDomain: true,
			async: true,
			cache: false,
			data: request,
			type: "POST",
			dataType: "json",
			success: (res) => { 
				// fixed: [20240720] this get bugs fo check again 
				usernameWhatsApp = Ext.Session("UserId").getSession();
				if (!usernameWhatsApp){
					console.warn("Please check user agent!");
					return false;
				} 
				$.each(res.items, (i, item) => {
					if (item) {  
						chatwhatsappboxtitle = item.i;  
						if ($("#chatwhatsappbox_"+ chatwhatsappboxtitle).length <= 0) {
							if (item.f != usernameWhatsApp){ 
								chatWhatsAppCreateBox(chatwhatsappboxtitle, 1);
							}
						} if (item.s == 1) {
							item.f = usernameWhatsApp;
						} if (item.s == 2) {
							$('#chatwhatsappbox_'+chatwhatsappboxtitle+' .chatboxcontentwhatsapp')
							.append('<div class="chatboxmessagewhatsapp"><p class="chatboxwhatsappinfo">'+ item.m +'</p></div>');
						} else {
							// kondision and  position 
							var displayChat = "chat-left-form";
							if (item.f != request.agents){
								displayChat = "chat-right-form";   
								chatWhatsAppBoxConversation("receive", item, (messageStr) => {
									$("#chatwhatsappbox_"+ chatwhatsappboxtitle +" .chatboxcontentwhatsapp").append(messageStr); 
								}); 
							} else if (item.f == request.agents){
								chatWhatsAppBoxConversation("sender", item, (messageStr) => {
									$( "#chatwhatsappbox_"+ chatwhatsappboxtitle +" .chatboxcontentwhatsapp").append(messageStr); 
								});  
							}   
						}
					}
				}); 
				
				for (i=0; i<chatWhatsAppBoxes.length; i++) {
					chatwhatsappboxtitle = chatWhatsAppBoxes[i];
					if (chatWhatsAppHasWhiteSpace(chatwhatsappboxtitle)){
						continue;
					} 
					$('#chatwhatsappbox_'+ chatwhatsappboxtitle +' .chatboxcontentwhatsapp').scrollTop($('#chatwhatsappbox_'+ chatwhatsappboxtitle +' .chatboxcontentwhatsapp')[0].scrollHeight );
					window.setTimeout(() => {
						$('#chatwhatsappbox_'+ chatwhatsappboxtitle +'.chatboxcontentwhatsapp').scrollTop($('#chatwhatsappbox_'+ chatwhatsappboxtitle +' .chatboxcontentwhatsapp')[0].scrollHeight );
						var whatsappBox = chatWhatsAppPrint('#chatwhatsappbox_%s', chatwhatsappboxtitle);
						if (whatsappBox.toString().length > 0){
							chatWhatsAppDragable(whatsappBox, "chatWhatsAppStartSession");
						}
					},100);
				} 
				if ($('#main_content').length > 0){
					// window.setTimeout('chatWhatsAppHeartbeat();',chatWhatsAppHeartbeatTime);
				} // handleEvent for callback session  
				if (typeof doCallback === "function"){
					doCallback.apply(this, [res]);
				} 
			}
		}); 
	});
}

/* @brief chatWhatsAppSetHeaderBeforeSend
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppSetHeaderBeforeSend(s)
{
	$.ajaxSetup({
		beforeSend:(xhr) => { 
			DBI.setItem( "Authorization", chatWhatsAppApiAuth);
			DBI.setItem( "ApiKey", chatWhatsAppApiKey); 
			if (typeof window.DBI === "object"){ 
				xhr.setRequestHeader( "Authorization", DBI.getItem( "Authorization" ));
				xhr.setRequestHeader( "ApiKey", DBI.getItem( "ApiKey" ));
			}
		}
	}); 
	if (typeof s === "function"){
		s.apply(this, [$]);
		return false;
	} 
}	

/* @brief chatWhatsAppWebsocket
 * @details get detail description function
 * @return mixed return value
 */
 function chatWhatsAppWebsocket(agents)
{
	// chck CTM agents moduls 
	if (typeof window.ctmClient !== "object"){
		return false;
	} // check owner property ctm moduls 
	if (agents.hasOwnProperty("username")){ 
		chatWhatsAppCtm.user.channel  = chatWhatsAppXor(agents, "client"  , ""); // agents.hasOwnProperty("client") ? agents.client : "";
		chatWhatsAppCtm.user.username = chatWhatsAppXor(agents, "username", ""); // agents.hasOwnProperty("username") ? agents.username : ""; 
		chatWhatsAppCtm.user.password = chatWhatsAppXor(agents, "password", "1234"); // agents.hasOwnProperty("password") ? agents.password : "1234"; 
		// validation of session */
		if (chatWhatsAppCtm.user.channel !== ""){
			window.sessionStorage.setItem("ctm_agent_session", JSON.stringify(chatWhatsAppCtm));  
		} // updated: Collab websocket with webajax 
		window.ctmClient.Start((ctm) => {  
			window.ctmClient.Init((ctm) => { 
				var register_fuctions = {
					onpong: "chatWhatsAppOnEventPong",
					onopen: "chatWhatsAppOnEventState",
					onerror : "chatWhatsAppOnEventState",
					onready : "chatWhatsAppOnInitSession",
					ontyping : "chatWhatsAppOnTypingSession",
					onvisitor : "chatWhatsAppOnVisitorMonitor",
					onsession : "chatWhatsAppOnSession",
					ondeleted : "chatWhatsAppOnDeleted",
					oncompleted : "chatWhatsAppOnCompleted",
					onheartbeat : "chatWhatsAppOnHeartbeat"
				};  
				ctm.Utils.loger("Connection initialize on %s", ctm.cfg.sock.url);  
				$.each(register_fuctions, (events, hook) => {  
					ctm.Utils.loger("Register function: %s", hook);
					ctm.thread[events] = hook;	
				}); 
				ctm.Connect((ctm, o) => {
					ctm.Utils.loger("Wait Response Connection from %s ...", ctm.cfg.sock.url);
				}); 
			}); 
		});		
	}
}

/* @brief chatWhatsAppSetHeaderBeforeSend
 * @details get detail description function
 * @return mixed return value
 */
$(document).ready(function(){ 
	try {
		var chatAgentId = window.parseInt(Ext.Session("UserWebChat").getSession()),
		chatAgentName = Ext.Session('UserId').getSession(); 
		console.log("CTM.Agent >> Got info of userid[%s]", chatAgentId);
		console.log("CTM.Agent >> Got info of username[%s]", chatAgentName);  
		if (chatAgentId < 1) {
			return 0;
		} if (typeof chatAgentName === "undefined") {
			return 0;
		} // set default of title 
		originalWhatsAppTitle = document.title;
		if ($('#main_content').length > 0) {  
			window.chatWhatsAppSetHeaderBeforeSend(() => { 
				window.chatWhatsAppLoginSession((res) => { 
					if (typeof res !== "object"){
						return false;
					} if (!res.success){
						return false;
					} // convert data to object here 
					var agents = res.hasOwnProperty("data") ? res.data : {};
					if (typeof window.chatWhatsAppWebsocket === "function"){
						window.chatWhatsAppWebsocket(agents);
					} 
				}); 
			});
		} 
	} catch(e){
		console.warn('WARNING: Terjadi kesalahan ketika object tidak terdefinisi.');
		return false;
	}	  
});

/* @brief chatWhatsAppNotTyping
 * @details Detect User Untyping from textarea
 * @return mixed return value
 */
 function chatWhatsAppNotTyping(chatbox) 
{
	ontype = 0;
	if (!ontype){
		chatWhatsAppSentTypingSession(chatbox, 0);	
	} return 0;
}

/* @brief chatWhatsAppNotTyping
 * @details Detect User Untyping from textarea
 * @return mixed return value
 */ 
 function chatWhatsAppIsTyping(input) 
{
	var chatbox = {};
	chatbox.visitor = {};
	chatbox.value = $(input).val();
	chatbox.visitor.id = $(input).attr("visitorid");
	chatbox.visitor.name = $(input).attr("visitorname");
	chatbox.visitor.session = $(input).attr("visitorsession");
	window.clearTimeout(timer);
	if (chatbox.value !== "") {
		ontype ++; 
		if (ontype == 1) {
			chatWhatsAppSentTypingSession(chatbox, 1);
		} 
		timer = window.setTimeout(() => {chatWhatsAppNotTyping(chatbox)}, delay);
		return 0; 
	}  
	else {
		chatWhatsAppNotTyping(chatbox);
	} return 0;
} 

/* Cookie plugin 
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 */ 
if (typeof jQuery === "object"){
	// jQuery.support.cors = true; 
}
jQuery.cookie = function(name, value, options){
	/*! FIXED: name and value given, set cookie */
    if (typeof value != 'undefined') { 
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // CAUTION: Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};