/*!
 * Kawani CTM -- Chat Text Message Agent 
 *
 * Copyright (C) 2019 - 2022, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the CTM project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 * + Update 
 * -- $.Revision 2021/12/13 02:49
 * -- $.Revision 2024/07/23 00:00
 */
 
/**!
 * 0 : CTM_GLOBAL.SOCKET.CONNECTING	Socket has been created. The connection is not yet open.
 * 1 : CTM_GLOBAL.SOCKET.OPEN		The connection is open and ready to communicate.
 * 2 : CTM_GLOBAL.SOCKET.CLOSING	The connection is in the process of closing.
 * 3 : CTM_GLOBAL.SOCKET.CLOSED		The connection is closed or couldn't be opened.
 */
 
/* @brief CTM global definition for share objects "CTM_GLOBAL.SESSION" "CTM_GLOBAL.RETVAL.ZERO" */
window.CTM_GLOBAL  = {
	STORAGE : "ctm_agent_storage",
	SESSION : "ctm_agent_session", 
	TIMEOUT : 5000,
	WAITALL : 1000,
	RETVAL	: {
		ZERO   : 0,
		EMPTY  : "",
		SUCCESS: 1,
		FAILED : 0
	},
	SOCKET : {
		OPEN 	  : 1,
		CLOSING   : 2,
		CLOSED 	  : 3,
		CONNECTING: 0
	}, 
	STATUS: {
		RESERVED: 6,
		REGISTER: 10,
		LOGOUT 	: 0,
		LOGIN	: 3,
		READY 	: 1,
		ONLINE	: 7,
		BUSY	: 4,
		AUX 	: 2,
		ACW 	: 5	
	}
};

/* @brief new Kawani "Service Agent Router" */
 function ctmAgent()
{
	this.cfg = {sock: {}, user:{}, call:{}};
	this.sock = false; 
	this.label = {status: false};
	
	/* handler on client session */
	this.thread = {
		onheartbeat: "", 
		onvisitor  : "",
		ontyping   : "", 
		onready	   : "", 
		onopen	   : "",
		onclose	   : "",
		onerror	   : "",
		onaux	   : "", 
		onlogout   : ""
	};
	/* SOCKET */
	if (typeof this.cfg.sock === "object" &&(!this.cfg.sock.hasOwnProperty('type'))){ 
		this.cfg.sock["type"] = "ws";
	}  
	if (typeof this.cfg.sock === "object" &&(!this.cfg.sock.hasOwnProperty('url'))){ 
		this.cfg.sock["url"] = "ws://"+ window.location.hostname +":17080/agent";
	} 
	if (typeof this.cfg.sock === "object" &&(!this.cfg.sock.hasOwnProperty('state'))){ 
		this.cfg.sock["state"] = window.CTM_GLOBAL.RETVAL.ZERO;
	}
	if (typeof this.cfg.sock === "object" &&(!this.cfg.sock.hasOwnProperty('closed'))){ 
		this.cfg.sock["closed"] = window.CTM_GLOBAL.RETVAL.ZERO;
	} 
	if (typeof this.cfg.sock === "object" &&(!this.cfg.sock.hasOwnProperty('timeout'))){ 
		this.cfg.sock["timeout"] = window.CTM_GLOBAL.TIMEOUT;
	}
	if (typeof this.cfg.sock === "object" &&(!this.cfg.sock.hasOwnProperty('connected'))){ 
		this.cfg.sock["connected"] = window.CTM_GLOBAL.RETVAL.ZERO;
	}  
	/* USER */
	if (typeof this.cfg.user === "object" &&(!this.cfg.user.hasOwnProperty("domain"))){
		this.cfg.user["domain"] = CTM_GLOBAL.RETVAL.EMPTY;
	}
	if (typeof this.cfg.user === "object" &&(!this.cfg.user.hasOwnProperty("session"))) {
		this.cfg.user["session"] = CTM_GLOBAL.RETVAL.EMPTY;
	}
	if (typeof this.cfg.user === "object" &&(!this.cfg.user.hasOwnProperty("channel"))) {
		this.cfg.user["channel"] = CTM_GLOBAL.RETVAL.EMPTY;
	}
	if (typeof this.cfg.user === "object" &&(!this.cfg.user.hasOwnProperty("userid"))) {
		this.cfg.user["userid"] = CTM_GLOBAL.RETVAL.ZERO;
	}
	if (typeof this.cfg.user === "object" &&(!this.cfg.user.hasOwnProperty("username"))) {
		this.cfg.user["username"] = CTM_GLOBAL.RETVAL.EMPTY;
	} 
	if (typeof this.cfg.user === "object" &&(!this.cfg.user.hasOwnProperty("password"))) {
		this.cfg.user["password"]= CTM_GLOBAL.RETVAL.EMPTY;
	} 
	if (typeof this.cfg.user === "object" &&(!this.cfg.user.hasOwnProperty("status"))) {
		this.cfg.user["status"] = CTM_GLOBAL.RETVAL.ZERO; 
	}  
	/* CALLER */
	if (typeof this.cfg.call === "object"){
		this.cfg.call = {session: "", callerid: "", status: "", direction: "", data : ""};
	} return this;
}; 
 
/* @brief window.Sar.prototype.loger()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
ctmAgent.prototype.Global = window.detectBrowser ;
   
/* @brief window.ctmAgent.prototype.loger()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
ctmAgent.prototype.Browser = function()
{ 
    if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) {
        return window.WebSocketBrowser.opera;
    } else if (navigator.userAgent.indexOf("Chrome") != -1 ) {
        return window.WebSocketBrowser.chrome;
    } else if (navigator.userAgent.indexOf("Safari") != -1) {
        return window.WebSocketBrowser.safari; 
    } else if (navigator.userAgent.indexOf("Firefox") != -1 ){
        return window.WebSocketBrowser.firefox; 
    } else if ((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) {
        return window.WebSocketBrowser.explorer;
    } else {
        return window.WebSocketBrowser.default;
    } return "";
}    

/* @brief window.ctmAgent.prototype.loger()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
ctmAgent.prototype.Utils = {
	log: function(value){
		console.log("CTM.Agent >> %s", value);
		return value.toString().length;
	}, 
	strcmp: function(key, val) {
		if (key!== null && key.localeCompare(val) == 0 ){
			return true;
		} return false;
	},
	loger: function(format){
		for (var i=1; i < arguments.length; i++) {
			format = format.replace(/%s/, arguments[i] );
		} this.log(format);
	}
} 

/* @brief window.ctmAgent.prototype.helper()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
ctmAgent.prototype.helper = {
	isFunc: function(value){
		if (typeof value === "function"){
			return CTM_GLOBAL.RETVAL.SUCCESS;
		} return CTM_GLOBAL.RETVAL.FAILED;
	},
	isObj: function(value){
		if (typeof value === "object"){
			return CTM_GLOBAL.RETVAL.SUCCESS;
		} return CTM_GLOBAL.RETVAL.FAILED;
	},
	isString: function(value){
		if (typeof value === "string"){
			return CTM_GLOBAL.RETVAL.SUCCESS;
		} return CTM_GLOBAL.RETVAL.FAILED;
	},
	toString: function(value){
		if (!window.ctmClient.helper.isObj(value)){
			return value.toString();
		} return "";
	},
	toInteger: function(value){
		if (!window.ctmClient.helper.isObj(value)){
			return parseInt(value);
		} return 0;
	}
}  

/* @brief window.ctmAgent.prototype.objects()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
ctmAgent.prototype.objects = { 
	set: function(s, key, value, doCallback){ 
		var Constructor = {};
		if (window.ctmClient.helper.isObj(s)){
			s[key] = value; 
		} /* return is function */
		if (window.ctmClient.helper.isFunc(doCallback)){
			doCallback.apply(this, [window.ctmClient]);
		} return this;
	}, 
	get: function(s, key){ 
		if (window.ctmClient.helper.isObj(s) &&(s.hasOwnProperty(key))){
			return s[key];
		} return 0;
	} 
}  

/* @brief window.ctmAgent.prototype.events()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
ctmAgent.prototype.events = {
	/* open socket */
	onOpen: function(sock){
		if (sock.target.readyState){
			window.ctmClient.cfg.sock.state = sock.target.readyState;
			window.ctmClient.cfg.sock.connected = sock.target.readyState;
			if (window.ctmClient.thread.onopen !== ""){
				if (window.ctmClient.helper.isFunc(window[window.ctmClient.thread.onopen])){
					window[window.ctmClient.thread.onopen].apply(this, [window.ctmClient, ctmClient.cfg.user, sock.target.readyState]);
				}
			} if (window.ctmClient.helper.isFunc(window.ctmClient.Register)){
				window.setTimeout(() => {
					window.ctmClient.Register.apply(this, [window.ctmClient]);
				}, 100);
			}
		} return 0;
	},
	/* onClose */
	onClose: function(sock){ 
		 /* try to reconnecting if socket is close */
		if (sock.target.readyState){
			window.ctmClient.objects.set(window.ctmClient.cfg.sock, "state", sock.target.readyState);
		} window.ctmClient.objects.set(window.ctmClient.cfg.sock, "connected", CTM_GLOBAL.RETVAL.FAILED, (e) => { e.UpdateStorage() });
		if (window.ctmClient.thread.onclose !== ""){
			if (window.ctmClient.helper.isFunc(window[window.ctmClient.thread.onclose])){
				window[window.ctmClient.thread.onclose].apply(this, [window.ctmClient, ctmClient.cfg.user, 0]);
			}
		} if (window.ctmClient.helper.isFunc(ctmClient.Reconnect)){
			window.setTimeout(() => {
				window.ctmClient.Reconnect.apply(this, [window.ctmClient.cfg]);
			}, window.CTM_GLOBAL.WAITALL);
		} return 0;
	},
	/* onError */
	onError: function(sock){
		// console.log("onError:", sock);
		if (sock.target.readyState){
			window.ctmClient.objects.set(window.ctmClient.cfg.sock, "state", sock.target.readyState);
		} if (window.ctmClient.thread.onerror !== ""){
			if (window.ctmClient.helper.isFunc(window[window.ctmClient.thread.onerror])){
				window[window.ctmClient.thread.onerror].apply(this, [window.ctmClient, ctmClient.cfg.user, 0]);
			}
		} window.ctmClient.objects.set(window.ctmClient.cfg.sock, "connected", CTM_GLOBAL.RETVAL.FAILED, (e) => { e.UpdateStorage() });
		return 0;
	},
	/* onSighup */
	onSighup: function(sighup){ 
		/* dont reconnected socket if sighup exist */
		if (sighup.hasOwnProperty("pid")){
			window.ctmClient.Utils.loger("Socket Hangup Reconnecting");
			window.ctmClient.objects.set(window.ctmClient.cfg.sock, "closed", sighup.pid);
		} return 0; 
	},
	/* onPong */
	onPong: function(pong){
		window.clearTimeout(window.timeoutPong);
		window.ctmClient.Utils.loger("Got Pong Pid: %s host: %s", pong.pid, pong.host); 
		if (typeof pong === "object"){
			window.ctmClient.objects.set(window.ctmClient.cfg.user, "host", pong.host, (e) => { e.UpdateStorage() });
		} // sent on PONG
		if (window.ctmClient.thread.onpong !== ""){
			if (window.ctmClient.helper.isFunc(window[window.ctmClient.thread.onpong])){
				window[window.ctmClient.thread.onpong].apply(this, [window.ctmClient, pong]);
			}
		} return 0;
	},
	/* onTyping */
	onTyping: function(typing){
		if (window.ctmClient.thread.ontyping !== ""){
			if (window.ctmClient.helper.isFunc(window[window.ctmClient.thread.ontyping])){
				window[window.ctmClient.thread.ontyping].apply(this, [window.ctmClient, typing.visitor]);
			}
		} return 0;
	},
	/* onPopup */
	onPopup: function(popup) {
		window.ctmClient.Utils.loger("Call from: %s", popup.callerid);
		window.setTimeout(() => {
			for (var s in popup){
				window.ctmClient.objects.set(ctmClient.cfg.call, s, popup[s]); 
			} window.ctmClient.UpdateStorage((ctm) => {
				ctm.Answer(popup);	
			});
		}, 2000);
		return 0;
	},
	/* onHeartbeat */
	onHeartbeat: function(heartbeat){
		if (window.ctmClient.thread.onheartbeat !== ""){
			if (window.ctmClient.helper.isFunc(window[window.ctmClient.thread.onheartbeat])){
				window[window.ctmClient.thread.onheartbeat].apply(this, [window.ctmClient, heartbeat]);
			}
		} return 0;
	},
	/* onDeleted */
	onDeleted: function(deleted){ 
		if (window.ctmClient.thread.ondeleted !== ""){
			if (window.ctmClient.helper.isFunc(window[window.ctmClient.thread.ondeleted])){
				window[window.ctmClient.thread.ondeleted].apply(this, [window.ctmClient, deleted]);
			}
		} return 0;
	},
	/* onCompleted */
	onCompleted: function(completed){
		if (window.ctmClient.thread.oncompleted !== ""){
			if (window.ctmClient.helper.isFunc(window[window.ctmClient.thread.oncompleted])){
				window[window.ctmClient.thread.oncompleted].apply(this, [window.ctmClient, completed]);
			}
		} return 0;
	},
	onSession : function(session){
		if (window.ctmClient.thread.onsession !== ""){
			if (window.ctmClient.helper.isFunc(window[window.ctmClient.thread.onsession])){
				window[window.ctmClient.thread.onsession].apply(this, [window.ctmClient, session]);
			}
		} return 0;
	},
	/* onBroadcast */
	onBroadcast: function(broadcast) {
		window.alert(`Broadcast message: ${broadcast.message}`); 
		return 0;
	},
	/* onVisitor */
	onVisitor: function(visitor){
		// console.log("onVisitor:", visitor);
		if (window.ctmClient.thread.onvisitor !== ""){
			if (window.ctmClient.helper.isFunc(window[window.ctmClient.thread.onvisitor])){
				window[window.ctmClient.thread.onvisitor].apply(this, [window.ctmClient, visitor]);
			}
		} return 0;
	},
	
	/* onState */ 
	onState: function(events) {
		if (!events.hasOwnProperty("status")){
			return 0;
		} /* events: Logout */
		if (events.status == CTM_GLOBAL.STATUS.LOGOUT){ 
			window.ctmClient.Utils.loger("Signal Events: Logout"); 
			window.ctmClient.objects.set(ctmClient.cfg.user, "state"	, CTM_GLOBAL.RETVAL.ZERO);
			window.ctmClient.objects.set(ctmClient.cfg.user, "status"	, events.status);  
			window.ctmClient.objects.set(ctmClient.cfg.user, "session"	, CTM_GLOBAL.RETVAL.EMPTY);
			window.ctmClient.objects.set(ctmClient.cfg.sock, "connected", CTM_GLOBAL.RETVAL.ZERO); 
			window.ctmClient.UpdateStorage();
			
			/* keep logging out regardless of the response*/
			window.ctmClient.label.status.html("Signal Events: Logout");  
			if (typeof logoutHandler !== "function"){
				window.ctmClient.Disconnect();  
			} else if (typeof logoutHandler === "function"){ 
				logoutHandler.apply(this, [window.ctmClient]);
			} return; 
			
		} /* events: Login */ 
		else if (events.status == CTM_GLOBAL.STATUS.LOGIN){
			window.ctmClient.Utils.loger("Signal Events.State: Login"); 
			window.ctmClient.label.status.html("Signal Events.State: Login");  
			window.ctmClient.objects.set(ctmClient.cfg.user, "status", events.status);  
			window.ctmClient.UpdateStorage(); 
			if (events.response){
				window.ctmClient.Online.apply(this, [window.ctmClient]);
			} 
		} 
		/* events: Online */ 
		else if (events.status == CTM_GLOBAL.STATUS.ONLINE){
			window.ctmClient.Utils.loger("Signal Events.State: Online"); 
			window.ctmClient.objects.set(ctmClient.cfg.user, "status", events.status); 
			if (window.ctmClient.thread.onready !== ""){
				if (window.ctmClient.helper.isFunc(window[window.ctmClient.thread.onready])){
					window[window.ctmClient.thread.onready].apply(this, [window.ctmClient]);
				}
			} window.ctmClient.UpdateStorage(); 
			
		} /* events: Ready */ 
		else if (events.status == CTM_GLOBAL.STATUS.READY){
			window.ctmClient.Utils.loger("Signal Events.State: Ready"); 
			window.ctmClient.objects.set(ctmClient.cfg.user, "status", events.status); 
			window.ctmClient.UpdateStorage(); 
			
		} /* events: Aux */ 
		else if (events.status == CTM_GLOBAL.STATUS.AUX){
			window.ctmClient.Utils.loger("Signal Events.State: Aux");
			window.ctmClient.objects.set(ctmClient.cfg.user, "status", events.status); 
			window.ctmClient.UpdateStorage(); 
			
		} /* Events: Busy */
		else if (events.status == CTM_GLOBAL.STATUS.BUSY){
			window.ctmClient.Utils.loger("Signal Events.State: Busy");	 
			window.ctmClient.objects.set(ctmClient.cfg.user, "status", events.status); 
			window.ctmClient.UpdateStorage(); 
			
		} /* Events: Acw */
		else if (events.status == CTM_GLOBAL.STATUS.ACW){
			window.ctmClient.Utils.loger("Signal Events.State: Acw"); 
			window.ctmClient.objects.set(ctmClient.cfg.user, "status", events.status); 
			window.ctmClient.UpdateStorage(); 
			
		} /* Events: Reserved / Reservasi */
		else if (events.status == CTM_GLOBAL.STATUS.RESERVED){
			window.ctmClient.Utils.loger("Signal Events.State: Reserved"); 
			window.ctmClient.objects.set(ctmClient.cfg.user, "state", events.status); 
			window.ctmClient.UpdateStorage();
			 
		} /* Events: Register */
		else if (events.status == CTM_GLOBAL.STATUS.REGISTER) {
			window.ctmClient.Utils.loger("Signal Events.State: Register");
			window.ctmClient.objects.set(ctmClient.cfg.user, "status", events.status);  
			window.ctmClient.UpdateStorage();
			/* failed register */
			if (!events.response){
				window.ctmClient.label.status.html("Signal Events.State: Rejected"); 
				return 0;
			} /* if success register */
			if (events.hasOwnProperty("userid")){
				window.ctmClient.objects.set(ctmClient.cfg.user, "userid", events.userid); 
			} if (events.hasOwnProperty("domain")){
				window.ctmClient.objects.set(ctmClient.cfg.user, "domain", events.domain); 
			} if (events.hasOwnProperty("sessionid")){
				window.ctmClient.objects.set(ctmClient.cfg.user, "session", events.sessionid); 
			} if (events.response &&(events.sessionid)){
				window.ctmClient.Login.apply(this, [window.ctmClient]);
			}
		} /* return of state: null */
		return 0;
	},
	/* onMessage */
	onMessage: function(bytes) {	
		var signal = false; /* set default false */
		if (bytes.data == ""){
			return 0;
		} /* Got Signal to Process */
		// window.ctmClient.Utils.loger( "Got signal from server" );
		try{
			signal = JSON.parse(bytes.data); 
			// console.log( "Signal:", signal ); 
			if (!signal){
				return 0;
			} /* Got Message event "Register" */
			if (signal.state){
				/* drive to ansycron process */
				window.ctmClient.Utils.loger( "Got Event: State" );
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onState)){
					window.ctmClient.events.onState.apply(this, [signal.state]); 
				} return 0; 
			} /* Got Message event "Popup" */
			else if (signal.popup){
				window.ctmClient.Utils.loger( "Got Event: Popup" );
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onPopup)){
					window.ctmClient.events.onPopup.apply(this, [signal.popup]);
				} return 0;
			} /* Got Message event "Broadcast" */
			else if (signal.broadcast){
				window.ctmClient.Utils.loger( "Got Event: Broadcast" );
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onBroadcast)){
					window.ctmClient.events.onBroadcast.apply(this, [signal.broadcast]);
				} return 0;
			} /* Got Message event "Pong" */
			else if (signal.pong){
				window.ctmClient.Utils.loger( "Got Event: Pong" ); 
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onPong)){
					window.ctmClient.events.onPong.apply(this, [signal.pong]);
				} return 0;
			} /* Got Message event "onHeartbeat" */
			else if (signal.heartbeat){
				window.ctmClient.Utils.loger( "Got Event: Heartbeat" ); 
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onHeartbeat)){
					window.ctmClient.events.onHeartbeat.apply(this, [signal.heartbeat]);
				} return 0;
			} /* Got Message event "Typing" */
			else if (signal.typing){
				window.ctmClient.Utils.loger( "Got Event: Typing" ); 
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onTyping)){
					window.ctmClient.events.onTyping.apply(this, [signal.typing]);
				} return 0; 
			} /* Got Message event "onVisitor" */
			else if (signal.visitor){
				window.ctmClient.Utils.loger( "Got Event: Visitor" ); 
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onVisitor)){
					window.ctmClient.events.onVisitor.apply(this, [signal.visitor]);
				} return 0;  
			} /* Got Message event "onCompleted" */
			else if (signal.completed){
				window.ctmClient.Utils.loger( "Got Event: Completed" ); 
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onCompleted)){
					window.ctmClient.events.onCompleted.apply(this, [signal.completed]);
				} return 0;  
			} /* Got Message event "onDeleted" */
			else if (signal.deleted){
				window.ctmClient.Utils.loger( "Got Event: Deleted" ); 
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onDeleted)){
					window.ctmClient.events.onDeleted.apply(this, [signal.deleted]);
				} return 0;  
			} 
			/* Got Message event "onSession" */
			else if (signal.session){
				window.ctmClient.Utils.loger( "Got Event: Session" );
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onSession)){
					window.ctmClient.events.onSession.apply(this, [signal.session]);
				} return 0;  
			}
			/* Got Message event "close" */
			else if (signal.sighup){
				window.ctmClient.Utils.loger( "Got Event: Sighup"); 
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onSighup)){
					window.ctmClient.events.onSighup.apply(this, [signal.sighup]);
				} return 0;  
			} /* Got Message event "Message" */
			else if (signal.message){
				window.ctmClient.Utils.loger( "Got Event: Message" );  
			}   
		} catch (signal_error){
			console.log(signal_error);
			return 0;
		} return 0;
	} 
}
 
/* @brief window.ctmAgent.prototype.onOpen()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Start = function(doCallback) 
{
	if (window.ctmClient.helper.isFunc(doCallback)){
		doCallback.apply(this, [window]);
	} return 0;
}
  
/* @brief window.ctmAgent.prototype.Init
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Init = function(s)
{
	/* get config from localStorage */
	window.ctmClient.Utils.loger("Start Init Configration Session");
	const cfgSock = window.ctmClient.Storage().get("sock"), cfgUser = window.ctmClient.Storage().get("user");
	if (window.ctmClient.helper.isObj(cfgSock) &&(cfgSock.hasOwnProperty("url"))){
		window.ctmClient.objects.set(window.ctmClient.cfg.sock, "url", cfgSock.url);
	}  
	/* config for user.channel */ 
	if (window.ctmClient.helper.isObj(cfgUser) &&(cfgUser.hasOwnProperty("channel"))){
		window.ctmClient.objects.set(window.ctmClient.cfg.user, "channel", cfgUser.channel);  
	} 
	/* config for user.username */ 
	if (window.ctmClient.helper.isObj(cfgUser) &&(cfgUser.hasOwnProperty("username"))){ 
		window.ctmClient.objects.set(window.ctmClient.cfg.user, "username", cfgUser.username);  
	} 
	/* config for user.password */ 
	if (window.ctmClient.helper.isObj(cfgUser) &&(cfgUser.hasOwnProperty("password"))){  
		window.ctmClient.objects.set(window.ctmClient.cfg.user, "password", cfgUser.password);  
	} 
	/* after sucess will check of loaded */
	window.ctmClient.label = {status: $("#ctm_agent_status")};
	if (window.ctmClient.helper.isFunc(s)){
		s.apply(this, [window.ctmClient]);
	} return this;
} 	

/* @brief window.ctmAgent.Action.Write()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Write = function(value, doCallback) 
{	
	var write = window.ctmClient.helper.toString(value), 
	connected = CTM_GLOBAL.RETVAL.SUCCESS, sent = CTM_GLOBAL.RETVAL.ZERO; 
	while (connected){ 
		/* CTM_GLOBAL.SOCKET.CLOSING */
		if (window.ctmClient.sock.readyState == CTM_GLOBAL.SOCKET.CLOSING){
			connected = CTM_GLOBAL.RETVAL.FAILED;
			break;
		} /* CTM_GLOBAL.SOCKET.CLOSED */ 
		if (window.ctmClient.sock.readyState == CTM_GLOBAL.SOCKET.CLOSED){
			connected = CTM_GLOBAL.RETVAL.FAILED;
		} break;
	} 	
	try {
		if (write &&(connected)){
			window.ctmClient.sock.send(write);
			sent = write.toString().length;
		} if (window.ctmClient.helper.isFunc(doCallback)){
			doCallback.apply(this, [window.ctmClient, write, sent]);
		} return 0;
		
	} catch(e){
		console.log(e);
		return 0;
	}
} 	 
 
/* @brief window.ctmAgent.Action.Ping()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Ping = function(s)
{
	window.ctmClient.Utils.loger("Client Response Pong");  
	var message = { "action" : "ping", 
		"data" : {
			"channel": window.ctmClient.getUser("channel"),
			"session": window.ctmClient.getUser("session"),
			"username": window.ctmClient.getUser("username")
		}
	}   
	var buf = JSON.stringify(message); 
	window.ctmClient.Write(buf, (ctm, buf, sent) => { 
		ctm.Utils.loger("Client Send.Ping => %s", sent); 
	});
	return 0;
}

/* @brief window.ctmAgent.Action.Ready
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Ready = function(doCallback)
{
	window.ctmClient.Utils.loger("Client Register to make session"); 
	var message = {"action": "ready", 
		"data": {
			"userid" : window.ctmClient.getUser("userid"),	
			"channel": window.ctmClient.getUser("channel"),
			"session": window.ctmClient.getUser("session"),	
			"username": window.ctmClient.getUser("username")
		}
	} 
	var buf = JSON.stringify(message); 
	window.ctmClient.Write(buf, (ctm, buf,sent) => { 
		ctm.Utils.loger("Client Send.Ready => %s", sent);  
		if (ctm.helper.isFunc(doCallback)){
			doCallback.apply(this, [ctm]);
		} return false; 
	});
	return 0; 
} 	

/* @brief window.ctmAgent.Action.Online
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Online = function(doCallback)
{
	window.ctmClient.Utils.loger("Client Register to make session"); 
	var message = {"action": "online", 
		"data": {
			"userid" : window.ctmClient.getUser("userid"),	
			"channel": window.ctmClient.getUser("channel"),
			"session": window.ctmClient.getUser("session"),	
			"username": window.ctmClient.getUser("username")
		}
	} 
	var buf = JSON.stringify(message); 
	window.ctmClient.Write(buf, (ctm, buf,sent) => { 
		ctm.Utils.loger("Client Send.Online => %s", sent); 
		if (ctm.helper.isFunc(doCallback)){
			doCallback.apply(this, [ctm]);
		} return false; 
	});
	return 0; 
} 	

/* @brief window.ctmAgent.Action.Ready
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Aux = function(doCallback)
{
	/* Prepare before send register */
	window.ctmClient.Utils.loger("Client Register to make session"); 
	var message = {"action": "aux", 
		"data" : {
			"userid": window.ctmClient.getUser("userid"),	
			"session": window.ctmClient.getUser("session"),	
			"channel": window.ctmClient.getUser("channel"),
			"username": window.ctmClient.getUser("username")
		}
	} /* convert object to String */
	var buf = JSON.stringify(message); 
	window.ctmClient.Write(buf, (ctm, buf, sent) => { 
		ctm.Utils.loger("Client Send.Aux => %s", sent);  
		if (ctm.helper.isFunc(doCallback)){
			doCallback.apply(this, [ctm]);
		} return false; 
	}); 
} 	
 
/* @brief window.ctmAgent.Action.Login()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Login = function()
{
	/* Prepare before send register */
	window.ctmClient.Utils.loger("Client Register to make session"); 
	var message = {"action": "login", 
		"data": {
			"userid" : window.ctmClient.getUser("userid"),	
			"session": window.ctmClient.getUser("session"),	
			"channel": window.ctmClient.getUser("channel"),
			"username": window.ctmClient.getUser("username"),
			"password": window.ctmClient.getUser("password")
		}
	} /* convert object to string */
	var buf = JSON.stringify(message); 
	window.ctmClient.Write(buf, (ctm, buf,sent) => { 
		ctm.Utils.loger("Client Send.Login => %s", sent); 
	}); return 0; 
	
} 

/* @brief window.ctmAgent.Action.Logout()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Logout = function(doCallback)
{
	/* Prepare before send register */
	window.ctmClient.Utils.loger("Client Logout from session"); 
	var message = {"action": "logout", 
		"data": {
			"userid"  : window.ctmClient.getUser("userid"),		
			"session" : window.ctmClient.getUser("session"),	
			"channel" : window.ctmClient.getUser("channel"),
			"username": window.ctmClient.getUser("username")
		}
	} /* convert object to String */
	var buf = JSON.stringify(message); 
	window.ctmClient.Write(buf, (ctm, buf,sent) => { 
		ctm.Utils.loger("Client Send.Logout => %s", sent); 
		if (ctm.helper.isFunc(doCallback)){
			doCallback.apply(this, [window.ctmClient]);
		}
	}); 
	return 0;  
} 	
 
/* @brief window.ctmAgent.Action.Disconnect()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Disconnect = function(doCallbak)
{
	window.ctmClient.Close();
	if (window.sessionStorage.getItem(window.CTM_GLOBAL.SESSION) !== null){
		window.sessionStorage.removeItem(window.CTM_GLOBAL.SESSION);
	} if (window.ctmClient.helper.isFunc(doCallbak)){
		doCallbak.apply(this,[window.ctmClient]);
	} return 0;
}

/* @brief window.ctmAgent.Action.Connect()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Connect = function(doCallbak)
{  
	/* STARTED: process on connect */ 
	this.Utils.loger("Make Connection to %s Started ...", this.cfg.sock.url);
	try {
		if (typeof MozWebSocket === "function") this.sock = new MozWebSocket(this.cfg.sock.url);
		else {
			this.sock = new WebSocket(this.cfg.sock.url, "agents");
		} if (this.sock === null){
			this.loger("Create new Connection to %s fail.", this.cfg.sock.url);
			return 0;
		} 
		
	    /* 0 : CONNECTING Socket has been created. The connection is not yet open.
		 * 1 : OPEN		  The connection is open and ready to communicate.
		 * 2 : CLOSING	  The connection is in the process of closing.
		 * 3 : CLOSED	  The connection is closed or couldn't be opened.
		 */
		 
		/* event: onopen from websocket */ 
		this.sock.onopen = function(event){ 
			window.ctmClient.Utils.loger("Connection Open");
			window.ctmClient.label.status.html("Connected"); 
			if (window.ctmClient.helper.isFunc(window.ctmClient.events.onOpen)){
				window.ctmClient.objects.set(window.ctmClient.cfg.sock, "connected", CTM_GLOBAL.RETVAL.SUCCESS);
				window.ctmClient.objects.set(window.ctmClient.cfg.sock, "state", CTM_GLOBAL.SOCKET.OPEN);
				window.ctmClient.events.onOpen.apply(this, [event]); 
			} 
		},
		/* event: onclose from websocket */
		this.sock.onclose = function(event){
			window.ctmClient.Utils.loger("Connection Close"); 
			window.ctmClient.label.status.html("Disconnected"); 
			if (window.ctmClient.helper.isFunc(window.ctmClient.events.onClose)){
				window.ctmClient.objects.set(window.ctmClient.cfg.sock, "connected", CTM_GLOBAL.RETVAL.FAILED);
				window.ctmClient.objects.set(window.ctmClient.cfg.sock, "state", CTM_GLOBAL.SOCKET.CLOSED);
				window.ctmClient.events.onClose.apply(this, [event]);
			} 
		}, 
		/* event: onmessage from websocket */
		this.sock.onmessage = function(event){
			if (window.ctmClient.helper.isFunc(window.ctmClient.events.onMessage)){
				window.ctmClient.objects.set(window.ctmClient.cfg.sock, "state", CTM_GLOBAL.SOCKET.CONNECTING);
				window.ctmClient.events.onMessage.apply(this, [event]);
			}
		}, /* event: onerror from websocket */
		this.sock.onerror = function(event){
			// console.log(event);
			window.ctmClient.Utils.loger("Connection Error");
			window.ctmClient.label.status.html("Connection Error"); 
			if (window.ctmClient.helper.isFunc(window.ctmClient.events.onError)){
				window.ctmClient.objects.set(window.ctmClient.cfg.sock, "connected", CTM_GLOBAL.RETVAL.FAILED); 
				window.ctmClient.objects.set(window.ctmClient.cfg.sock, "state", CTM_GLOBAL.SOCKET.CLOSED);
				window.ctmClient.events.onError.apply(this, [event]);
			} 
		}; 
	} 
	/* if have try error with nt found */
	catch(error){
		console.log(error); 
	} 
	/* NEXT: get callback after success */ 
	if (window.ctmClient.helper.isFunc(doCallbak)){ 
		doCallbak.apply(this, [window.ctmClient, window.ctmClient.cfg]);
	} return 0;
} 	
/* @brief window.ctmAgent.Action.Reconnect()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Reconnect = function(cfg)
{
	if (window.ctmClient.cfg.sock.closed){
		return 0;
	} window.ctmClient.Utils.loger("Reconnecting to %s ...", cfg.sock.url);
	window.ctmClient.label.status.html("Try Reconnecting ...");
	window.setTimeout(() => {
		window.ctmClient.Connect((ctm, o) => {
			ctm.Utils.loger("Wait Response Connection from %s ...", cfg.sock.url); 
			window.ctmClient.label.status.html("Wait Response Connection ...");			
		}); 
	}, window.CTM_GLOBAL.TIMEOUT);
}

/* @brief window.ctmAgent.Action.Register()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Register = function(ctm) 
{  
	window.ctmClient.Utils.loger("Client Register to make session"); 
	var message = {"action" : "register", 
		"data" : {
			"channel": ctm.getUser("channel"),
			"username": ctm.getUser("username"),
			"password": ctm.getUser("password")
		}
	} /* convert object to String */
	var buf = JSON.stringify(message); 
	window.ctmClient.Write(buf, (ctm, buf, sent) => { 
		ctm.Utils.loger("Client Send.Register => %s", sent);  
	}); 
	/* finally return is value integer */
	return 0;
}

/* @brief window.ctmAgent.Action.SentTyping()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.SentTyping = function(chatbox)
{
	window.ctmClient.Utils.loger("Client Sent Typing"); 
	var message = { "action" : "typing", 
		"data" : {
			"channel" : window.ctmClient.getUser("channel"),
			"userid"  : window.ctmClient.getUser("userid"),
			"username": window.ctmClient.getUser("username"),
			"typing"  : chatbox.type,
			"visitor" : {
				"id": chatbox.visitor.id, 
				"session": chatbox.visitor.session
			}	 
		}
	} /* convert object to String */
	var buf = JSON.stringify(message); 
	window.ctmClient.Write(buf, (ctm, buf, sent) => {
		ctm.Utils.loger("Client Send.Typing => %s", sent);  
	}); 
	return 0;
}


/* @brief window.ctmAgent.Action.Answer()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Answer = function(s)
{	
	window.ctmClient.objects.set(window.ctmClient.cfg.caller, "status", "answer"); 
	window.ctmClient.UpdateStorage((ctm) => {
		var answer = ctm.Storage().get("caller");
		ctm.Utils.loger("Client Answers %s", s.sessionid);   
		if (ctm.Utils.strcmp(answer.session, s.session)){ 
			var message = {"action": "call",
				"data":{
					"type": answer.type,
					"status": answer.status,
					"userid": answer.userid, 
					"session": answer.session,
					"channel": answer.channel,
					"callerid": answer.callerid
				}
			} /* convert object to String */
			var buf = JSON.stringify(message); 
			ctm.Write(buf, (ctm, buf, sent) => { 
				ctm.Utils.loger("Client Send.Answer => %s", sent);   
			});  
		} 
	}); 
	return 0;
} 

/* @brief window.ctmAgent.Action.Complete()
 * @details client sent message to completed chat 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Complete  = function(sessionid)
{
	window.ctmClient.Utils.loger("Client Sent Complete Message"); 
	var requestor = { "action" : "complete", 
		"data" : {
			"channel" : window.ctmClient.getUser("channel"),
			"userid"  : window.ctmClient.getUser("userid"),
			"username": window.ctmClient.getUser("username"),
			"message" : {
				"session": sessionid,
			}	 
		}
	} /* convert object to String */
	var buf = JSON.stringify(requestor); 
	window.ctmClient.Write(buf, (ctm, buf, sent) => { 
		ctm.Utils.loger("Client Send.Complete => %s", sent);  
	}); 
	return 0;
}

/* @brief window.ctmAgent.Action.Delete()
 * @details client sent deleted message 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Delete= function(chatbox, callback)
{
	window.ctmClient.Utils.loger("Client Sent Delete Message"); 
	var requestor = { "action" : "delete", 
		"data" : {
			"channel" : window.ctmClient.getUser("channel"),
			"userid"  : window.ctmClient.getUser("userid"),
			"username": window.ctmClient.getUser("username"),
			"message" : {
				"session": chatbox.session,
				"msgid": chatbox.msgid
			} 
		}
	} /* convert object to String */
	var buf = JSON.stringify(requestor); 
	window.ctmClient.Write(buf, (ctm, buf, sent) => { 
		ctm.Utils.loger("Client Send.Delete => %s", sent);   
		if (typeof callback === "function"){
			callback.apply(this, [requestor.data.message]);
		}
	}); 
	return 0;
} 

/* @brief window.ctmAgent.Action.Hangup()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Hangup  = function()
{
	return 0;
}

/* @brief window.ctmAgent.Action.Close()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Close = function()
{
	try {
		window.ctmClient.objects.set(window.ctmClient.cfg.sock, "state" , CTM_GLOBAL.RETVAL.FAILED);
		window.ctmClient.objects.set(window.ctmClient.cfg.sock, "closed", CTM_GLOBAL.RETVAL.SUCCESS);
		if (window.ctmClient.cfg.sock.closed) {	
			window.ctmClient.sock.close();
		} return 0; 
	} catch(e){
		console.log(e);
		return 0;
	} 
}  

/* @brief window.ctmAgent.Action.getUser()
 * @details get value from objects window
 * @retval mixed return is value 
 */
 ctmAgent.prototype.getUser = function(key)
{
	return window.ctmClient.objects.get(window.ctmClient.cfg.user, key);
} 

/* @brief window.ctmAgent.Action.Clear()
 * @details remove reff objects window 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Clear = function(s, key)
{
	if (window.ctmClient.helper.isObj(s) &&(s.hasOwnProperty(key))){
		delete s[key]; 
	} return 0;
} 

/* @brief window.ctmAgent.Action.UpdateStorage()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.UpdateStorage = function(doCallbacks)
{
	/* socket ============================ */ 
	if (!window.ctmClient.cfg.storage.sock.hasOwnProperty('closed')){
		window.ctmClient.cfg.storage.sock.closed = window.ctmClient.cfg.sock.closed;
	} else {
		window.ctmClient.cfg.storage.sock.closed = window.ctmClient.cfg.sock.closed;
	} 
	
	if (!window.ctmClient.cfg.storage.sock.hasOwnProperty('connected')){
		window.ctmClient.cfg.storage.sock.connected = window.ctmClient.cfg.sock.connected;
	} else {
		window.ctmClient.cfg.storage.sock.connected = window.ctmClient.cfg.sock.connected;
	}  
	
	if (!window.ctmClient.cfg.storage.sock.hasOwnProperty('timeout')){
		window.ctmClient.cfg.storage.sock.timeout = window.ctmClient.cfg.sock.timeout;
	} else {
		window.ctmClient.cfg.storage.sock.timeout = window.ctmClient.cfg.sock.timeout;
	}  
	
	if (!window.ctmClient.cfg.storage.sock.hasOwnProperty('state')){
		window.ctmClient.cfg.storage.sock.state = window.ctmClient.cfg.sock.state;
	} else {
		window.ctmClient.cfg.storage.sock.state = window.ctmClient.cfg.sock.state;
	}  
	
	// user ============================ 
	if (!window.ctmClient.cfg.storage.user.hasOwnProperty('userid')){
		window.ctmClient.cfg.storage.user.userid = window.ctmClient.cfg.user.userid;
	} else {
		window.ctmClient.cfg.storage.user.userid = window.ctmClient.cfg.user.userid;
	} 
	if (!window.ctmClient.cfg.storage.user.hasOwnProperty('session')){
		window.ctmClient.cfg.storage.user.session = window.ctmClient.cfg.user.session;
	} else {
		window.ctmClient.cfg.storage.user.session = window.ctmClient.cfg.user.session;
	} 
	if (!window.ctmClient.cfg.storage.user.hasOwnProperty('domain')){
		window.ctmClient.cfg.storage.user.domain = window.ctmClient.cfg.user.domain;
	} else {
		window.ctmClient.cfg.storage.user.domain = window.ctmClient.cfg.user.domain;
	} 	
	if (!window.ctmClient.cfg.storage.user.hasOwnProperty('status')){
		window.ctmClient.cfg.storage.user.status = window.ctmClient.cfg.user.status;
	} else {
		window.ctmClient.cfg.storage.user.status = window.ctmClient.cfg.user.status;
	}  
	if (!window.ctmClient.cfg.storage.user.hasOwnProperty('host')){
		window.ctmClient.cfg.storage.user.host = window.ctmClient.cfg.user.host;
	} else {
		window.ctmClient.cfg.storage.user.host = window.ctmClient.cfg.user.host;
	}   
	
	// call ============================ 
	if (!ctmClient.cfg.storage.hasOwnProperty('call')){
		window.ctmClient.cfg.storage.call = window.ctmClient.cfg.call;
	} else {
		window.ctmClient.cfg.storage.call = window.ctmClient.cfg.call;
	}    
	window.sessionStorage.setItem(CTM_GLOBAL.SESSION, JSON.stringify(window.ctmClient.cfg.storage));  
	if (window.ctmClient.helper.isFunc(doCallbacks)){
		doCallbacks.apply(this, [window.ctmClient]);
	} return 0;
} 
/* @brief window.ctmAgent.Action.Storage()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmAgent.prototype.Storage = function()
{ 
	var storage_values = false;  
	if (!window.ctmClient.helper.isObj(this.cfg.storage_values)){
		window.ctmClient.cfg.storage = false; 
	} if ((storage = window.sessionStorage.getItem(CTM_GLOBAL.SESSION)) === null ){
		window.ctmClient.cfg.storage = false;   
	} else {
		window.ctmClient.cfg.storage = JSON.parse(storage);
	}  
	var local_storage = window.ctmClient.cfg.storage,
	Constructor = {
		set: (ca) => {},
		get: (ca) => {
			if (local_storage.hasOwnProperty(ca)){
				return local_storage[ca];
			} return "";
		}, 
		fetch: () => {
			return window.ctmClient.cfg.storage;
		}
	}; /* return object values */
	return Constructor;
} 
 
 /* starting to process */
 if (typeof window.ctmClient !== "object")
{
	window.ctmClient = new ctmAgent(); /* new struct */ 
}