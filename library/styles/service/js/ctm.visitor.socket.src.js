/*!
 * Kawani CTM -- Chat Text Message Visitor 
 *
 * Copyright (C) 2019 - 2022, Peranti Digital Solusindo, Inc.
 *
 * Jombi Par <jombi.php@gmail.com>
 *
 * See http://www.perantidigital.co,id for more information about
 * the SAR project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * <BEGIN>
 * @version 4.5
 * + $.revision 2023/12/11 00:00
 * + $.revision 2024/07/23 00:00
 * + $.revision 2024/07/24 00:00
 * </BEGIN>
 */
 
/**!
 * 0 : CTM_GLOBAL.SOCKET.CONNECTING	Socket has been created. The connection is not yet open.
 * 1 : CTM_GLOBAL.SOCKET.OPEN		The connection is open and ready to communicate.
 * 2 : CTM_GLOBAL.SOCKET.CLOSING	The connection is in the process of closing.
 * 3 : CTM_GLOBAL.SOCKET.CLOSED		The connection is closed or couldn't be opened.
 */
/*! \brief CTM global definition for share objects "CTM_GLOBAL.SESSION" "CTM_GLOBAL.RETVAL.ZERO" */
// 
window.CTM_GLOBAL  = {
	STORAGE : "ctm_visitor_storage",
	SESSION : "ctm_visitor_session", 
	TIMEOUT : 10000,
	WAITALL : 1000,
	RETVAL	: {
		ZERO : 0,
		EMPTY : "",
		SUCCESS : 1,
		FAILED : 0
	},
	SOCKET : {
		OPEN : 1,
		CLOSING : 2,
		CLOSED : 3,
		CONNECTING : 0
	}, 
	SIGNAL :{
		START_SESSION: "StartSession",
		START_SESSION: "EndSession" 
	},
	STATUS: {
		RESERVED : 6,
		REGISTER : 10,
		LOGOUT : 0,
		LOGIN : 3,
		READY : 1,
		BUSY: 4,
		AUX : 2,
		ACW : 5	
	},
	LABEL : {
		BUSY : "Busy",
		BREAK : "Break",	
		CLOSE : "Close",
		TYPING : "Typing ...",
		ONLINE : "Online",
		OFFLINE : "Offline",
		ONSIGHUP: "Hangup Connection",
		DISCONNECT : "Disconnected",
		RECONNECT : "Reconnecting ..."
	}
};

/* \brief new Kawani "Chat Text Message" */
 function ctmVisitor()
{
	this.sock = false; 
	this.storage = {}; 
	this.agent = {userid:"", channel:"", username:"", status: 0, time: 0};
	this.label = {socket: false, state: false, typing: false, comment: false}; 
	this.func = {heartbeat: false };
	this.cfg = {sock: {}, user:{}, call:{}}; 
	this.cfg.sock.type 		= "ws";
	this.cfg.sock.url 		= this.cfg.sock.type +"://"+ window.location.hostname +":17080/ctm/";
	this.cfg.sock.state 	= window.CTM_GLOBAL.RETVAL.ZERO;
	this.cfg.sock.closed	= window.CTM_GLOBAL.RETVAL.ZERO;
	this.cfg.sock.timeout 	= window.CTM_GLOBAL.TIMEOUT;
	this.cfg.sock.connected = window.CTM_GLOBAL.RETVAL.ZERO;  
	this.cfg.user.userid	= CTM_GLOBAL.RETVAL.ZERO;
	this.cfg.user.username 	= CTM_GLOBAL.RETVAL.EMPTY;
	this.cfg.user.channel  	= CTM_GLOBAL.RETVAL.EMPTY;
	this.cfg.user.email 	= CTM_GLOBAL.RETVAL.EMPTY;
	this.cfg.user.session 	= CTM_GLOBAL.RETVAL.EMPTY;
	this.cfg.user.domain 	= CTM_GLOBAL.RETVAL.EMPTY;
	this.cfg.user.status 	= CTM_GLOBAL.RETVAL.ZERO; 
	this.cfg.call.data 		= CTM_GLOBAL.RETVAL.EMPTY;
	this.cfg.call.status 	= CTM_GLOBAL.RETVAL.ZERO;
	this.cfg.call.session 	= CTM_GLOBAL.RETVAL.EMPTY;
	this.cfg.call.callerid 	= CTM_GLOBAL.RETVAL.EMPTY;
	this.cfg.call.direction = CTM_GLOBAL.RETVAL.ZERO;
	return this;
}; 

/* @brief window.ctmVisitor.prototype.loger()
 * @details Client Global parameter 
 * @retval mixed return is value 
 */
ctmVisitor.prototype.Global = window.CTM_GLOBAL ;
  
/* @brief window.ctmVisitor.prototype.loger()
 * @details Client Utils Properties  
 * @retval mixed return is value 
 */
ctmVisitor.prototype.Utils = {
	log : function(strings){
		console.log("CTM.Visitor >> %s", strings);
		return strings.toString().length;
	}, 
	loger: function(strings){
		for (var i=1; i < arguments.length; i++) {
			strings = strings.replace(/%s/, arguments[i] );
		} this.log(strings);
	},
	text: function(value, before){
		if (window.ctmClient.helper.isFunc(before)){
			before.apply(this, [window.ctmClient]);
		} return window.ctmClient.label.socket.html(value);
	},
	state: function(value){
		return window.ctmClient.label.state.html(value);
	},
	typing: function(value){
		return window.ctmClient.label.typing.html(value);
	},
	strcmp : function(key, val) {
		if (key!== null && key.localeCompare(val) == 0 ){
			return true;
		} return false;
	},
	verbose: function(value){
		console.log("CTM.Visitor >> verbose: ", value); 
	},
};

/* @brief window.ctmVisitor.prototype.helper()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
ctmVisitor.prototype.helper = {
	isObj: function(value){
		if (typeof value === "object"){
			return CTM_GLOBAL.RETVAL.SUCCESS;
		} return CTM_GLOBAL.RETVAL.FAILED;
	},
	isFunc: function(value){ 
		if (typeof value === "function"){
		return CTM_GLOBAL.RETVAL.SUCCESS;
		} return CTM_GLOBAL.RETVAL.FAILED;
	},
	isString: function(value){ 
		if (typeof value === "string"){
		return CTM_GLOBAL.RETVAL.SUCCESS;
		} return CTM_GLOBAL.RETVAL.FAILED;
	},
	toString: function(value){ 
		if (!this.isObj(value)){
		return value.toString();
		} return "";		
	},
	toInteger: function(value){ 
		if (!this.isObj(value)){
		return parseInt(value);
		} return 0;
		
	}
};
 
/* @brief window.ctmVisitor.prototype.objects()
 * @details Client Object Properties
 * @retval mixed return is value 
 */
ctmVisitor.prototype.objects = {
	set: function(objects, key, value, doCallback){
		// console.log(">>> ctmVisitor.objects.set: \"%s\" key: \"%s\" value: \"%s\"", JSON.stringify(objects), key, value);
		if (window.ctmClient.helper.isObj(objects)){
			objects[key] = value;   
		} if (window.ctmClient.helper.isFunc(doCallback)){
			doCallback.apply(this, [window.ctmClient]);
		} return 0;
	},
	get: function(objects,key){
		if (window.ctmClient.helper.isObj(objects) &&(objects.hasOwnProperty(key))){
			return objects[key];
		} return 0;
	}		
};

/* @brief window.ctmVisitor.prototype.labels()
 * @details Client Label Properties
 * @retval mixed return is value 
 */
ctmVisitor.prototype.labels = {
	onState: function(state){
		if (state == CTM_GLOBAL.STATUS.LOGOUT){
			window.ctmClient.Utils.state(CTM_GLOBAL.LABEL.OFFLINE).css("color", "#dddddd"); 
		} else if (state == CTM_GLOBAL.STATUS.READY){
			window.ctmClient.Utils.state(CTM_GLOBAL.LABEL.ONLINE).css("color", "#5bc3cb"); 
		} else if (state == CTM_GLOBAL.STATUS.AUX){
			window.ctmClient.Utils.state(CTM_GLOBAL.LABEL.BREAK).css("color", "#f49090");
		} else if (state == CTM_GLOBAL.STATUS.BUSY){
			window.ctmClient.Utils.state(CTM_GLOBAL.LABEL.ONLINE).css("color", "#5bc3cb");
		} 
	} 
}
/* @brief window.ctmVisitor.prototype.callbacks()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
ctmVisitor.prototype.callbacks = {
	onHeartbeat: function(message){
		if (window.ctmClient.func.heartbeat !== "" ){
			window.ctmClient.func.heartbeat.apply(this, [window.ctmClient, message]);  
		}
	},
	onComment: function(message){
		if (window.ctmClient.func.comment !== ""){
			window.ctmClient.func.comment.apply(this, [window.ctmClient, message]); 
		} return 0; 
	},
	onCompleted: function(message){
		if (window.ctmClient.func.completed !== ""){ 
			window.ctmClient.func.completed.apply(this, [window.ctmClient, message]); 
		} return 0; 
	},
	onDeleted: function(message){
		if (window.ctmClient.func.deleted !== ""){
			window.ctmClient.func.deleted.apply(this, [window.ctmClient, message]); 
		} return 0; 
	}, 
	onTimeout: function(message){
		if (window.ctmClient.func.timeout !== ""){
			window.ctmClient.func.timeout.apply(this, [window.ctmClient, message]); 
		} return 0; 
	}
} 

/* @brief window.ctmVisitor.prototype.events()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
ctmVisitor.prototype.events = {
	onOpen:function(sock){  
		// console.log("=========================== OPEN =========================")
		if (sock.target.readyState){
			window.ctmClient.callbacks.onComment(1);
		} if (sock.target.readyState){
			window.ctmClient.cfg.sock.state = sock.target.readyState;
			window.ctmClient.cfg.sock.connected = sock.target.readyState;
			if (window.ctmClient.helper.isFunc(window.ctmClient.StartSession)){
				window.ctmClient.StartSession.apply(this, [window.ctmClient]);
			}
		} 
		window.setTimeout(() => {
			window.ctmClient.Utils.text(CTM_GLOBAL.LABEL.ONLINE).css("color", "#5bc3cb");
		}, 500);
		return 0;
	}, 
	onClose:function(sock){
		if (sock.target.readyState){
			window.ctmClient.callbacks.onComment(0);
		} if (sock.target.readyState){
			window.ctmClient.objects.set(window.ctmClient.cfg.sock, "state", sock.target.readyState);
		} 
		window.ctmClient.Utils.text(CTM_GLOBAL.LABEL.DISCONNECT,(s) => {s.labels.onState(CTM_GLOBAL.STATUS.LOGOUT)});
		window.ctmClient.objects.set(window.ctmClient.cfg.sock,"connected",CTM_GLOBAL.RETVAL.FAILED,(e) => {e.setUpdateStorage()});
		window.setTimeout((e) => {
			if (window.ctmClient.helper.isFunc(window.ctmClient.Reconnect)){
				window.ctmClient.Reconnect.apply(this, [window.ctmClient.cfg]);
			}  
		}, 5000); 
		return 0;
	},
	onError:function(sock){
		if (sock.target.readyState){
			window.ctmClient.callbacks.onComment(0);
		} if (sock.target.readyState){
			window.ctmClient.objects.set(window.ctmClient.cfg.sock, "state", sock.target.readyState);
		} window.ctmClient.objects.set(window.ctmClient.cfg.sock, "connected", CTM_GLOBAL.RETVAL.FAILED,(e) => {e.setUpdateStorage()});
		return 0;
	}, 
	onHeartbeat:function(heartbeat){
		if (window.ctmClient.helper.isObj(heartbeat)){
			window.ctmClient.callbacks.onHeartbeat();
		} return 0;
	},  
	onCompleted: function(message){
		if (window.ctmClient.helper.isObj(message)){
			window.ctmClient.callbacks.onCompleted(message);
		} return 0; 
	},
	onDeleted: function(message){
		if (window.ctmClient.helper.isObj(message)){
			window.ctmClient.callbacks.onDeleted(message);
		} return 0;  
	}, 
	onState:function(state){  
		// console.log("state:", state);
		if (window.ctmClient.helper.isObj(state)){
			if (window.ctmClient.helper.isFunc(window.ctmClient.labels.onState)){
				window.ctmClient.labels.onState(state.agent.status);
			} /* update all session from agents */
			window.ctmClient.objects.set(window.ctmClient.agent,"userid", state.agent.userid);
			window.ctmClient.objects.set(window.ctmClient.agent,"channel", state.agent.channel);
			window.ctmClient.objects.set(window.ctmClient.agent,"username", state.agent.username);
			window.ctmClient.objects.set(window.ctmClient.agent,"status", state.agent.status);
			window.ctmClient.objects.set(window.ctmClient.agent,"time", state.agent.time);
		} /* update session storage */ 
		window.ctmClient.setUpdateStorage((e) => {
			window.ctmClient.Utils.loger( "Updated Session success" );
		});
		return 0; 
	},
	onTyping:function(typing){ 
		if (window.ctmClient.helper.isObj(typing)){
			if (typing.agent.typing == 1){
				window.ctmClient.Utils.typing(CTM_GLOBAL.LABEL.TYPING); 
			} else window.ctmClient.Utils.typing(""); 
		} return 0;
	},
	onTimeout: function(timeout){ 
		if (window.ctmClient.helper.isObj(timeout)){
			window.ctmClient.callbacks.onTimeout(timeout);
		} return 0;  
	},
	onPong:function(pong){ 
		window.clearTimeout(window.timeoutPong);
		window.ctmClient.Utils.loger("Got Pong Pid: %s host: %s:%s", pong.pid, pong.host, pong.port); 
		if (window.ctmClient.helper.isObj(pong)){
			window.ctmClient.objects.set(window.ctmClient.cfg.user, "host", pong.host, (e) => {e.setUpdateStorage()});
		} return 0;
	},
	onPopup:function(popup){ 
		window.ctmClient.Utils.loger("Call from: %s", call.callerid);
		window.setTimeout(() => {
			for (var s in popup){
				window.ctmClient.objects.set(window.ctmClient.cfg.caller, s, popup[s]); 
			} window.ctmClient.setUpdateStorage((ctm) => {
				ctm.Answer(popup);	
			});
		}, 2000);
		return 0;
	},  
	// hangup close socket iterable 
	onSighup: function(sighup){ 
		window.ctmClient.objects.set(window.ctmClient.cfg.sock, "closed", true);
		window.ctmClient.Utils.text(CTM_GLOBAL.LABEL.ONSIGHUP);
		return;
	},
	onBroadcast:function(broadcast){
		window.alert(`Broadcast message: ${broadcast.message}`);
		return 0;
	}, 
	onSession: function(session){ 
		// console.log("onSession:", session); 
		if (session.events === "startsession") { 	
			/* updated for visitor: session */
			if (window.ctmClient.helper.isObj(session.visitor)){
				window.ctmClient.objects.set(window.ctmClient.cfg.user, "userid" , session.visitor.userid);
				window.ctmClient.objects.set(window.ctmClient.cfg.user, "session", session.visitor.session); 
				window.ctmClient.objects.set(window.ctmClient.cfg.user, "username", session.visitor.username);
				
			} /* updated for agent: session */
			if (window.ctmClient.helper.isObj(session.agent)){
				window.ctmClient.objects.set(window.ctmClient.agent, "userid"  , session.agent.userid);
				window.ctmClient.objects.set(window.ctmClient.agent, "session" , session.agent.session); 
				window.ctmClient.objects.set(window.ctmClient.agent, "channel" , session.agent.channel); 
				window.ctmClient.objects.set(window.ctmClient.agent, "username", session.agent.username);
				window.ctmClient.objects.set(window.ctmClient.agent, "status"  , session.agent.status);
				
			} /* check status default from DBI */
			if (window.ctmClient.helper.isFunc(window.ctmClient.labels.onState)){
				window.ctmClient.labels.onState(session.agent.status);
			} /* update session storage */ 
			window.ctmClient.setUpdateStorage((e) => { 
				window.ctmClient.Utils.loger("Updated Session success");
			});
		} 
		// window.ctmClient.Utils.text(""); 
		return false;
	},  
	onMessage :function(events){
		var signal = false; /* set default false */
		if (events.data == ""){
			return 0;
		} /* Got Signal to Process */
		// window.ctmClient.Utils.loger("Got signal from server");
		try {
			signal = JSON.parse(events.data); 
			// console.log("Signal:", signal);
			if (!signal){
				return 0;
			} /* Got Message event "Register" */
			if (signal.session){ 
				/* drive to ansycron process */
				window.ctmClient.Utils.loger("Got Signal: Session");
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onSession)){
					window.ctmClient.events.onSession.apply(this, [signal.session]); 
				} return 0; 
			} 
			/* Got Message event "Popup" */
			else if (signal.popup){ 
				window.ctmClient.Utils.loger("Got Signal: Popup");
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onPopup)){
					window.ctmClient.events.onPopup.apply(this, [signal.popup]);
				} return 0;
			} 
			/* Got Message event "Broadcast" */
			else if (signal.broadcast){ 
				window.ctmClient.Utils.loger("Got Signal: Broadcast");
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onBroadcast)){
					window.ctmClient.events.onBroadcast.apply(this, [signal.broadcast]);
				} return 0;
			} 
			/* Got Message event "State" */
			else if (signal.state){ 
				window.ctmClient.Utils.loger("Got Signal: State"); 
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onState)){
					window.ctmClient.events.onState.apply(this, [signal.state]);
				} return 0;
			}
			/* Got Message event "Heartbeat" */
			else if (signal.heartbeat){ 
				window.ctmClient.Utils.loger("Got Signal: Heartbeat"); 
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onHeartbeat)){
					window.ctmClient.events.onHeartbeat.apply(this, [signal.heartbeat]);
				} return 0;
			} 
			/* Got Message event "Pong" */
			else if (signal.pong){ 
				window.ctmClient.Utils.loger("Got Signal: Pong"); 
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onPong)){
					window.ctmClient.events.onPong.apply(this, [signal.pong]);
				} return 0;
			} 
			/* Got Message event "Typing" */
			else if (signal.typing){ 
				window.ctmClient.Utils.loger("Got Signal: Typing"); 
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onTyping)){
					window.ctmClient.events.onTyping.apply(this, [signal.typing]);
				} return 0;	
			} 
			/* Got Message event "Completed" */
			else if (signal.completed){ 
				window.ctmClient.Utils.loger("Got Signal: Completed"); 
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onCompleted)){
					window.ctmClient.events.onCompleted.apply(this, [signal.completed]);
				} return 0;	
			} 
			/* Got Message event "Delete" */
			else if (signal.deleted){ 
				window.ctmClient.Utils.loger("Got Signal: Deleted"); 
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onDeleted)){
					window.ctmClient.events.onDeleted.apply(this, [signal.deleted]);
				} return 0;	
			} 
			/* Got Message event "Timeout" */
			else if (signal.timeout){ 
				window.ctmClient.Utils.loger("Got Signal: Timeout"); 
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onTimeout)){
					window.ctmClient.events.onTimeout.apply(this, [signal.timeout]);
				} return 0;	
			}
			/* Got Message event "Sighup" */
			else if (signal.sighup){ 
				window.ctmClient.Utils.loger("Got Signal: Sighup"); 
				if (window.ctmClient.helper.isFunc(window.ctmClient.events.onSighup)){
					window.ctmClient.events.onSighup.apply(this, [signal.sighup]);
				} return 0;	
			}
			/* Got Message event "Message" */
			else if (signal.message){ 
				window.ctmClient.Utils.loger("Got Signal: Message");  
			}   
		} 
		catch (signal_error){
			window.ctmClient.Utils.verbose(signal_error); 
		} return 0;
	} 
};
 
/* @brief window.ctmVisitor.prototype.Start()
 * @details Start and running of socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.Start = function(doCallback)
{
	if (this.helper.isFunc(doCallback)){
		doCallback.apply(this, [window]);
	} return 0;
} 
  
/* @brief window.ctmVisitor.prototype.Init
 * @details init for settup configuration socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.Init = function(doCallback)
{
	/* get config from localStorage */
	this.Utils.loger("Start Init Configration Session");
	var cfgSock = this.Storage().get("sock"), cfgUser = this.Storage().get("user"); 
	if (this.helper.isObj(cfgSock) &&(cfgSock.hasOwnProperty("url"))){
		this.objects.set(window.ctmClient.cfg.sock, "url", cfgSock.url);
	} /* config for user.channel */ 
	if (this.helper.isObj(cfgUser) &&(cfgUser.hasOwnProperty("channel"))){
		this.objects.set(window.ctmClient.cfg.user, "channel", cfgUser.channel);  
	} /* config for user.username */ 
	if (this.helper.isObj(cfgUser) &&(cfgUser.hasOwnProperty("username"))){ 
		this.objects.set(window.ctmClient.cfg.user, "username", cfgUser.username);  
	} /* config for user.email */ 
	if (this.helper.isObj(cfgUser) &&(cfgUser.hasOwnProperty("email"))){   
		this.objects.set(window.ctmClient.cfg.user, "email", cfgUser.email);  
	} /* created label */
	this.setUpdateStorage(doCallback); 
	return this;
} 

/* @brief window.ctmVisitor.Action.Write()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.Write = function(value, doCallback) 
{	
	var write = this.helper.toString(value), 
	connected = CTM_GLOBAL.RETVAL.SUCCESS, sent = CTM_GLOBAL.RETVAL.ZERO; 
	while (connected){ 
		/* CTM_GLOBAL.SOCKET.CLOSING */
		if (window.ctmClient.sock.readyState == CTM_GLOBAL.SOCKET.CLOSING){
			connected = CTM_GLOBAL.RETVAL.FAILED;
			break;
		} /* CTM_GLOBAL.SOCKET.CLOSED */ 
		if (window.ctmClient.sock.readyState == CTM_GLOBAL.SOCKET.CLOSED){
			connected = CTM_GLOBAL.RETVAL.FAILED;
		} break;
	} 	
	if (write &&(connected)){
		window.ctmClient.sock.send(write);
		sent = write.toString().length;
	} if (this.helper.isFunc(doCallback)){
		doCallback.apply(this, [window.ctmClient, write, sent]);
	} return 0;
} 	 

/* @brief window.ctmVisitor.Action.doConnect()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.Connect = function(doCallbak)
{  
	// console.log("======================CONNECT ==============================")
	/* STARTED: process on connect */ 
	window.ctmClient.Utils.loger(`Make Connection to ${this.cfg.sock.url} Started ...`);
	try {
		if (typeof MozWebSocket === "function") this.sock = new MozWebSocket(this.cfg.sock.url);
		else {
			this.sock = new WebSocket(this.cfg.sock.url, "visitor");
		} if (this.sock === null){
			this.loger(`Create new Connection to ${this.cfg.sock.url} fail.`);
			return 0;
		} 
		
	    /* 0 : CONNECTING Socket has been created. The connection is not yet open.
		 * 1 : OPEN		  The connection is open and ready to communicate.
		 * 2 : CLOSING	  The connection is in the process of closing.
		 * 3 : CLOSED	  The connection is closed or couldn't be opened.
		 */
		 
		/* EVENT: onopen from websocket */ 
		this.sock.onopen = function(event){
			window.ctmClient.Utils.loger(`Response Connection from ${window.ctmClient.cfg.sock.url} Open`);
			if (window.ctmClient.helper.isFunc(window.ctmClient.events.onOpen)){
				window.ctmClient.objects.set(window.ctmClient.cfg.sock, "connected", CTM_GLOBAL.RETVAL.SUCCESS);
				window.ctmClient.objects.set(window.ctmClient.cfg.sock, "state", CTM_GLOBAL.SOCKET.OPEN);
				// console.log("start: ===================== OPEN(1)======================= ");
				window.ctmClient.events.onOpen.apply(this, [event]); 
			} 
		},
		/* EVENT: onclose from websocket */
		this.sock.onclose = function(event){
			window.ctmClient.Utils.loger(`Response Connection from ${window.ctmClient.cfg.sock.url} Close`); 
			if (window.ctmClient.helper.isFunc(window.ctmClient.events.onClose)){
				window.ctmClient.objects.set(window.ctmClient.cfg.sock, "connected", CTM_GLOBAL.RETVAL.FAILED);
				window.ctmClient.objects.set(window.ctmClient.cfg.sock, "state", CTM_GLOBAL.SOCKET.CLOSED);
				window.ctmClient.events.onClose.apply(this, [event]);
			} 
		}, 
		/* EVENT: onmessage from websocket */
		this.sock.onmessage = function(event){
			if (window.ctmClient.helper.isFunc(window.ctmClient.events.onMessage)){
				window.ctmClient.objects.set(window.ctmClient.cfg.sock, "state", CTM_GLOBAL.SOCKET.CONNECTING);
				window.ctmClient.events.onMessage.apply(this, [event]);
			}
		}, /* EVENT: onerror from websocket */
		this.sock.onerror = function(event){
			window.ctmClient.Utils.loger(`Response Connection from ${window.ctmClient.cfg.sock.url} Error`);
			if (window.ctmClient.helper.isFunc(window.ctmClient.events.onError)){
				window.ctmClient.objects.set(window.ctmClient.cfg.sock, "connected", CTM_GLOBAL.RETVAL.FAILED); 
				window.ctmClient.objects.set(window.ctmClient.cfg.sock, "state", CTM_GLOBAL.SOCKET.CLOSED);
				window.ctmClient.events.onError.apply(this, [event]);
			} 
		}; 
	} 
	/* if have try error with nt found */
	catch(error){
		console.log(error); 
	} 
	/* NEXT: get callback after success */ 
	if (window.ctmClient.helper.isFunc(doCallbak)){ 
		doCallbak.apply(this, [window.ctmClient, window.ctmClient.cfg]);
	} return 0;
} 	

/* @brief window.ctmVisitor.Action.doReconnect()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.Reconnect = function(cfg)
{
	if (window.ctmClient.cfg.sock.closed){
		return 0;
	} 
	window.ctmClient.Utils.text(CTM_GLOBAL.LABEL.RECONNECT);
	window.ctmClient.Utils.loger(`Reconnnecting to ${cfg.sock.url} ...`);
	window.setTimeout(() => {
		window.ctmClient.Connect((ctm, o) => {
			ctm.Utils.loger(`Wait Response Connection from ${cfg.sock.url} ...`);
		}); 
	}, window.CTM_GLOBAL.TIMEOUT);
}

/* @brief window.ctmVisitor.Action.Storage()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.Storage = function()
{ 
	window.ctmClient.storage = JSON.parse(window.sessionStorage.getItem(CTM_GLOBAL.SESSION)); 
	var local_storage = window.ctmClient.storage,
	Constructor = {
		set: (ca) => {},
		get: (ca) => {
			if (local_storage.hasOwnProperty(ca)){
				return local_storage[ca];
			} return "";
		}, fetch : () => {
			return window.ctmClient.cfg.storage;
		}
	}; /* return object values */
	return Constructor;
} 

/* @brief window.ctmVisitor.Action.Ping()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.Ping = function()
{
	window.ctmClient.Utils.loger("Client Response Pong");  
	var message = { "action" : "ping", 
		"data": {
			"session": window.ctmClient.getUser("session"),
			"channel": window.ctmClient.getUser("channel"),
			"email"	 : window.ctmClient.getUser("email") 
		}
	}   
	var buf = JSON.stringify(message); 
	window.ctmClient.Write(buf, (ctm, buf, sent) => { 
		ctm.Utils.loger(`Client Send.Ping => ${sent}`); 
	});
	return 0;
} 

/* @brief window.ctmVisitor.Action.Logout()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.Logout = function()
{
	window.ctmClient.Utils.loger("Client Register to make session"); 
	var message = { "action": "logout", 
		"data": {
			"session": window.ctmClient.getUser("session"),	
			"channel": window.ctmClient.getUser("channel"),
			"email"	 : window.ctmClient.getUser("email")
		}
	} /* convert object to String */
	var buf = JSON.stringify(message); 
	window.ctmClient.Write(buf, (ctm, buf,sent) => { 
		ctm.Utils.loger(`Client Send => ${buf} => ${sent}`); 
		window.sessionStorage.removeItem(CTM_GLOBAL.SESSION);
		if (typeof logoutHandler === "function"){
			logoutHandler.apply(this, [window.ctmClient]);
			return 0;
		}
	}); 
	return 0;  
} 	

/* @brief window.ctmVisitor.Action.StartSession()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.StartSession = function(s) 
{  
	window.ctmClient.Utils.loger("Client Register to make session"); 
	var message = {
		"action": "startsession", 
		"data": {
			"session" : s.getUser("session"),
			"channel" : s.getUser("channel"),
			"username": s.getUser("username"),
			"email"   : s.getUser("email")
		}
	} /* convert object to String */
	var buf = JSON.stringify(message); 
	window.ctmClient.Write(buf, (ctm, buf, sent) => { 
		ctm.Utils.loger(`Client Send.StartSession => ${sent}`); 
	}); 
	return 0;
}

/* @brief window.ctmVisitor.Action.EndSession()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.EndSession = function(doCallback) 
{  
	window.ctmClient.Utils.text(CTM_GLOBAL.LABEL.CLOSE); 
	window.ctmClient.Utils.loger("Client End Session"); 
	var message = {
		"action": "endsession", 
		"data":{
			"session": window.ctmClient.getUser("session"),
			"channel": window.ctmClient.getUser("channel"), 
			"email"	 : window.ctmClient.getUser("email")
		}
	} /* convert object to String */
	var buf = JSON.stringify(message); 
	window.ctmClient.Write(buf, (ctm, buf, sent) => { 
		window.ctmClient.Utils.loger(`Client Send.EndSession => ${sent}`); 
		if (window.ctmClient.helper.isFunc(doCallback)){
			doCallback.apply(this, [window.ctmClient]); 
		}
	}); 
	return 0;
}

/* @brief window.ctmVisitor.Action.EndSession()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.SendTyping = function(typing) 
{  
	window.ctmClient.Utils.loger("Client Sent Typing"); 
	var message = {
		"action": "ontyping", 
		"data":{
			"session" : window.ctmClient.getUser("session"),
			"channel" : window.ctmClient.getUser("channel"),
			"email"	  : window.ctmClient.getUser("email"),
			"typing"  : typing	
		}
	} /* convert object to String */
	var buf = JSON.stringify(message); 
	window.ctmClient.Write(buf, (ctm, buf, sent) => { 
		ctm.Utils.loger(`Client Send.SendTyping => ${sent}`); 
	}); 
	return 0;
}
/* @brief window.ctmVisitor.Action.Delete(msgid)
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.Delete = function(msgid, callback) 
{  
	window.ctmClient.Utils.loger("Client Sent Typing"); 
	var message = {
		"action": "delete", 
		"data":{
			"msgid"	  : msgid,	
			"channel" : window.ctmClient.getUser("channel"),
			"session" : window.ctmClient.getUser("session"),
			"userid"  : window.ctmClient.getUser("userid"),
			"username": window.ctmClient.getUser("username"),
			"email"	  : window.ctmClient.getUser("email")
		}
	} /* convert object to String */
	var buf = JSON.stringify(message); 
	window.ctmClient.Write(buf, (ctm, buf, sent) => {  
		if (typeof callback === "function"){
			callback.apply(this, [message.data]);
		} ctm.Utils.loger(`Client Send.delete => ${sent}`); 
	}); 
	return 0;
}
 

/* @brief window.ctmVisitor.Action.Close()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.Close = function()
{
	try {
		window.ctmClient.objects.set(window.ctmClient.cfg.sock, "state" , CTM_GLOBAL.RETVAL.FAILED);
		window.ctmClient.objects.set(window.ctmClient.cfg.sock, "closed", CTM_GLOBAL.RETVAL.SUCCESS);
		if (window.ctmClient.cfg.sock.closed) {	
			window.ctmClient.sock.close();
		} return 0; 
	} catch(e){
		console.log(e);
		return 0;
	} 
} 
 
/* @brief window.ctmVisitor.Action.Disconnect()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.Disconnect = function(doCallback)
{
	window.ctmClient.Close();
	window.setTimeout(() => {
		window.sessionStorage.removeItem(CTM_GLOBAL.SESSION);
		if (window.ctmClient.helper.isFunc(doCallback)){
			doCallback.apply(this, [window.ctmClient]);
		} 
	}, 500);
	return false;
} 
 

/* @brief window.ctmVisitor.Action.Clear()
 * @details remove reff objects window 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.Clear = function(s, key)
{
	if (window.ctmClient.helper.isObj(s) &&(s.hasOwnProperty(key))){
		delete s[key]; 
	} return 0;
} 
  
/* @brief window.ctmVisitor.Action.getCfgUser()
 * @details get value from objects window
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.getUser = function(key)
{
	return window.ctmClient.objects.get(window.ctmClient.cfg.user, key);
} 

/* @brief window.ctmVisitor.Action.setCallStorage()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.setCallStorage = function(key, value)
{
	if (!window.ctmClient.storage.hasOwnProperty("call")){
		window.ctmClient.storage["call"] = {};
	} if (!window.ctmClient.storage.call.hasOwnProperty(key)){
		window.ctmClient.storage.call[key] = value;
	} else window.ctmClient.storage.call[key] = value;
	return 0; 
}

/* @brief window.ctmVisitor.Action.setBoxsStorage()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.setBoxsStorage = function(key, value)
{
	if (!window.ctmClient.storage.hasOwnProperty("agent")){
		window.ctmClient.storage["agent"] = {};
	} if (!window.ctmClient.storage.agent.hasOwnProperty(key)){
		window.ctmClient.storage.agent[key] = value;
	} else window.ctmClient.storage.agent[key] = value;
	return 0; 
}

/* @brief window.ctmVisitor.Action.setStorage()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.setUserStorage = function(key, value)
{
	if (!window.ctmClient.storage.user.hasOwnProperty(key)){
		window.ctmClient.storage.user[key] = value;
	} else window.ctmClient.storage.user[key] = value;
	return 0;
}

/* @brief window.ctmVisitor.Action.setSockStorage()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.setSockStorage = function(key, value)
{
	if (!window.ctmClient.storage.sock.hasOwnProperty(key)){
		window.ctmClient.storage.sock[key] = value;
	} else window.ctmClient.storage.sock[key] = value;
	return 0;
}

/* @brief window.ctmVisitor.Action.setUpdateStorage()
 * @details Client Close connection from stream socket 
 * @retval mixed return is value 
 */
 ctmVisitor.prototype.setUpdateStorage = function(doCallbacks)
{
	this.setSockStorage( "state"	, window.ctmClient.cfg.sock.state);
	this.setSockStorage( "closed"	, window.ctmClient.cfg.sock.closed);
	this.setSockStorage( "timeout"	, window.ctmClient.cfg.sock.timeout);
	this.setSockStorage( "connected", window.ctmClient.cfg.sock.connected);
	this.setUserStorage( "host"		, window.ctmClient.cfg.user.host);
	this.setUserStorage( "email"	, window.ctmClient.cfg.user.email); 
	this.setUserStorage( "status"	, window.ctmClient.cfg.user.status); 
	this.setUserStorage( "domain"	, window.ctmClient.cfg.user.domain);
	this.setUserStorage( "userid"	, window.ctmClient.cfg.user.userid);
	this.setUserStorage( "channel"	, window.ctmClient.cfg.user.channel);
	this.setUserStorage( "session"	, window.ctmClient.cfg.user.session);
	this.setUserStorage( "username"	, window.ctmClient.cfg.user.username);  
	this.setCallStorage( "session"	, window.ctmClient.cfg.call.session);
	this.setCallStorage( "callerid" , window.ctmClient.cfg.call.callerid); 
	this.setCallStorage( "direction", window.ctmClient.cfg.call.direction); 
	this.setBoxsStorage( "userid"	, window.ctmClient.agent.userid);
	this.setBoxsStorage( "channel"	, window.ctmClient.agent.channel);
	this.setBoxsStorage( "username"	, window.ctmClient.agent.username);
	this.setBoxsStorage( "status"	, window.ctmClient.agent.status);
	this.setBoxsStorage( "time"		, window.ctmClient.agent.time);
	window.sessionStorage.setItem(CTM_GLOBAL.SESSION, JSON.stringify(window.ctmClient.storage));  
	if (window.ctmClient.helper.isFunc(doCallbacks)){
		doCallbacks.apply(this, [window.ctmClient]);
	} return 0;
} 

 // starting to process 
 if (typeof window.ctmClient !== "object")
{
	window.ctmClient = new ctmVisitor(); 
}