<?php 
// ------------------------------------------------------------------------------------------------
/*
 * Enigma User Interface 
 *
 * ------------------------------------------------------------------------------------------------
 * Modul 			lib class version / web attribute 
 *
 * @package			Enigma User Interface 
 * @version			1.2.0
 * @param			version 			
 * @title			title  
 * @install 		install date 	
 */
 
class EUI_Version  
{

 var $arr_conf_version = array();
 var $arr_base_config  = array();
 
// -------------------------------------------------------------------------------

/*
 * @ Instance			null 	
 */ 
 
 private static $Instance = null;
 

// --------------------------------------------------------------------------------------------------

/*
 * Modul 			@instance 
 * 
 * @param			@self 
 */ 
 
 public static function &Instance()
{
	if( is_null(self::$Instance) )
	{
		self::$Instance = new self();
	}
	return self::$Instance;
}
 
// --------------------------------------------------------------------------------------------------

/*
 * Modul 			@instance 
 * 
 * @param			@self 
 */ 

 public function EUI_Version()
{
	$this->arr_base_config = array ( 'path'=> BASEPATH .'keys' );
	if( count( $this->arr_conf_version) == 0 )
	{
		$ar_key =& KeyInstall();
		if( !is_object($ar_key) ) {
			return FALSE;
		}				
	// ------------------------------------------------------------------------------------------------------
	
		$ar_conf = $ar_key->getKeyInstall( $this->arr_base_config['path']);
		if( is_array( $ar_conf ) )
			foreach( $ar_conf as $key => $val ) 
		{
			
			$this->arr_conf_version[$key] = $val;
		}
	}
} 

// --------------------------------------------------------------------------------------------------

/*
 * Modul 			@instance 
 * 
 * @param			@self 
 */ 

 public function isvalid()
{
	$dsInstant =& KeyInstall();
	
	if(!isset( $this->arr_conf_version['install.keys'] ) ) 
	{
		return FALSE;
	}
	
	$arr_key_conf = $this->arr_conf_version['install.keys'] .".conf";
	if( strcmp($arr_key_conf, $dsInstant->_get_eui_keys()) ==0 )
	{
		return TRUE;
	} 
	else 
	{
		return FALSE;
	}
	
	return FALSE;
}
 
// --------------------------------------------------------------------------------------------------

/*
 * Modul 			@instance 
 * 
 * @param			@self 
 */ 
 
 public function version()
{
  $ar_vers = array();
  if(is_array($this->arr_conf_version) ) 
	foreach( $this->arr_conf_version as $key => $val ) 
 {
	if( preg_match("/(version)/", $key) ){
		$ar_vers[] = trim($val);
	}	
 }
 
 if( count($ar_vers)==0 ){
	return NULL;
 }
 
 return implode(".", $ar_vers);
}

// --------------------------------------------------------------------------------------------------

/*
 * Modul 			@instance 
 * 
 * @param			@self 
 */ 
 
 public function title()
{
	if(!is_array($this->arr_conf_version) ) 
  {
	return NULL;	
  }
   
   return (string)$this->arr_conf_version['system.title'];
}

// --------------------------------------------------------------------------------------------------

/*
 * Modul 			@instance 
 * 
 * @param			@self 
 */ 
 
 public function dsinstall()
{
  if(!is_array($this->arr_conf_version) ) 
  {
	return NULL;	
  }
   
   return (string)$this->arr_conf_version['install.data'];
}
  
// --------------------------------------------------------------------------------------------------

/*
 * Modul 			@instance 
 * 
 * @param			@self 
 */ 
 
 public function author()
{
	if(!is_array($this->arr_conf_version) ) 
  {
	return NULL;	
  }
   
   return (string)$this->arr_conf_version['system.auth'];
}

  
// --------------------------------------------------------------------------------------------------

/*
 * Modul 			@instance 
 * 
 * @param			@self 
 */ 
 
 public function description()
{
	if(!is_array($this->arr_conf_version) ) 
  {
	return NULL;	
  }
   
   return (string)$this->arr_conf_version['system.description'];
}

// END CLASS 
 
 }
 
 ?>