<?php
/*
 * @ pack : dila cisco EUI_Cisco 
 * ----------------------------------------------------------------------
 */
 
class EUI_Cisco  {


/*
 *  @pack : Cisco dial  
 * -------------------------------------------------------------------
 */ 
 
 private static $Instance = null;
 
/*
 *  @pack : set value on (array)_values 
 * -------------------------------------------------------------------
 */ 
 
 protected $_values = array();

/*
 *  @pack : set value on (array)_values 
 * -------------------------------------------------------------------
 */ 
 
 protected $final_output = array();
 
 /* 
  * @pack : Cisco dial  
  */
  
 private $crlf = "\r\n";
 private $enter = "\r\n\r\n";
 
/*
 *  @pack : Cisco dial  
 * -------------------------------------------------------------------
 */ 
 
  public static function &Instance()
{
	if( is_null(self::$Instance) ){
		self::$Instance = new self();
	}
	
	return self::$Instance;
 }
/*
 *  @pack : Cisco dial  
 * -------------------------------------------------------------------
 */ 
 
  public function EUI_Cisco()
{
	$this->UI =& get_instance();
	
	 if( is_object($this->UI)) 
	{
		$this->IP_Address = $this->UI->EUI_Session->_get_session('LoginIP');
		$this->IP_Extension = $this->UI->EUI_Session->_get_session('Username');
		$this->IP_Port = 80;
	} 
	
/* 
 * @ pack : set default parameter if not set its 
 */	
	$this->set_value('IP_Address', $this->IP_Address);
	$this->set_value('IP_Port',$this->IP_Port);
	$this->set_value('Extension',$this->IP_Extension);
	$this->set_value('Password','123');
 } 
 
 // EUI_Cisco:: aksesor =============================> 
 
 
 
/*
 *  @pack : Cisco dial  
 * -------------------------------------------------------------------
 */ 
 
 public function set_value( $key =null, $value = null )
{
	$this->_values[$key] = $value;
} 
// set_value =========================>


/*
 *  @pack : get value   
 * -------------------------------------------------------------------
 */ 
 
 protected function get_value( $field = null  )
{
	if( isset( $this->_values[$field] ) ) {
		return (string)$this->_values[$field];
	} else {
		return NULL;
	}
} 

/*
 *  @pack : Cisco dial 
 * -------------------------------------------------------------------
 */ 
 
 protected function get_cisco_auth()
{
	$UserId = $this->get_value('Extension');
	$Password = $this->get_value('Password');
    $this->cisco_auth = base64_encode( trim($UserId). ":" . trim($Password) );
	if( $this->cisco_auth )
	{
		return $this->cisco_auth;
	} else {
		return NULL;
	}	 
} 


/*
 *  @pack : Cisco dial 
 * -------------------------------------------------------------------
 */ 
 
 protected function get_dial_event( $string = null )
{
	$DialPhone = trim($this->get_value('Phone'));
	
	$URI = null;
	if($DialPhone)
		$URI ="Dial:$DialPhone#:1:Cisco/Dialer";
		
// @ pack : compiler its =============================> 

	if ( is_null($string) )  {
		$this->_xml = "XML=<CiscoIPPhoneExecute>".
						"<ExecuteItem URL=\"{$URI}\"/>".
					  "</CiscoIPPhoneExecute>";
	} else {
		$this->_xml = "XML=$string";
	}
	
	return urlencode($this->_xml);
} 

/*
 *  @pack : Cisco dial 
 * -------------------------------------------------------------------
 */ 
 
 protected function get_hangup_event()
{
  $URI = "Key:Speaker";
  $Event = trim($this->get_value('Event'));
  
// @ pack : is null 
 
  if(!is_null($Event) ){
	$URI = $Event;
  }	
		
// @ pack : compiler its =============================> 

  if(!is_null($URI)) 
  {
	$this->_xml = "XML=<CiscoIPPhoneExecute>".
			"<ExecuteItem Priority=\"0\" URL=\"{$URI}\"/>".
		"</CiscoIPPhoneExecute>";
   } 
   
  return urlencode($this->_xml);
} 

/*
 *  @pack : Cisco dial 
 * ------------------------------------------------------------------
 */ 
 
 protected function set_final_output( $output =null )
{
	$this->final_output[] = $output;
}

/*
 * default port && IP 
 */

 public function client_sock_open()
{	
  if( !function_exists('fsockopen') )
	exit( "function fsockopen not found " );
	
  
  $fp = fsockopen ( $this->get_value('IP_Address'),  
					$this->get_value('IP_Port'), 
					$errno, $errstr, 30);
	if( !$fp ){
		die( "sockopen error $errno : $errstr ");
		return FALSE;
	}
	return $fp;
}
 
/*
 *  @pack : Cisco dial 
 * ------------------------------------------------------------------
 */ 
 
 public function &Dial() 
{

// @ pack : final output generate =================>

 $_xml_data = $this->get_dial_event();
 $_ip_addrs = $this->get_value('IP_Address');
 $_cis_auth = $this->get_cisco_auth();
 $_out_data = "POST /CGI/Execute HTTP/1.0" . $this->crlf .
			  "Host: $_ip_addrs" . $this->crlf .
			  "Authorization: Basic $_cis_auth". $this->crlf .
			  "Connection: close". $this->crlf .
			  "Content-Type: application/x-www-form-urlencoded". $this->crlf .
			  "Content-Length: ". strlen($_xml_data) . $this->enter;
	
// @ pack : cek string if not empty =============> 
  $final_output = null;
  
  if( !is_null($_xml_data) ){
	$final_output = $_out_data . $_xml_data;	
  }
  
  if( strlen($final_output) > 0 ) {
	$this-> set_final_output($final_output);
  }	
  
// @ put process to client aksess socket ==========> 
  $fsockopen = $this->client_sock_open();
  if($fsockopen)  
  {
	 fputs($fsockopen, $final_output);
	 flush();
	 while (!feof($fsockopen)) {
		$response .= fgets($fsockopen, 128);
		flush();
	 }	
  }
  
 // @ pack : reset string  =========================> 
  $final_output = null;
  $this->last_response = $response;
 
// @ get last response header HTTP OK  ===============>

  if( preg_match("/OK/i", $response ) ) {	
	 return $this; 
  } else {
	 return FALSE;
  }
  
} 
// END :// Dial ====================================>



/*
 * @ pack : get last response on execute command socket 
 *  --------------------------------------------------------------- 
 */
 
 public function get_last_response()
{

  if( is_string($this->last_response) 
	AND !is_null($this->last_response) )
  {
	return $this->last_response;
  } else {
	return FALSE;
  }
  
} 
// get_last_response ========================> 


/*
 *  @pack : Cisco Hangup 
 *  --------------------------------------------------------------- 
 */
 
 public function Hangup()
{

// @ pack : final output generate =================>

 $_xml_data = $this->get_hangup_event();
 $_ip_addrs = $this->get_value('IP_Address');
 $_cis_auth = $this->get_cisco_auth();
 $_out_data = "POST /CGI/Execute HTTP/1.0" . $this->crlf .
			  "Host: $_ip_addrs" . $this->crlf .
			  "Authorization: Basic $_cis_auth". $this->crlf .
			  "Connection: close". $this->crlf .
			  "Content-Type: application/x-www-form-urlencoded". $this->crlf .
			  "Content-Length: ". strlen($_xml_data) . $this->enter;
	
// @ pack : cek string if not empty =============> 
  $final_output = null;
  
  if( !is_null($_xml_data) ){
	$final_output = $_out_data . $_xml_data;	
  }
  
  if( strlen($final_output) > 0 ) {
	$this-> set_final_output($final_output);
  }	
  
// @ put process to client aksess socket ==========> 
  $fsockopen = $this->client_sock_open();
  if($fsockopen)  
  {
	 fputs($fsockopen, $final_output);
	 flush();
	 while (!feof($fsockopen)) {
		$response .= fgets($fsockopen, 128);
		flush();
	 }	
  }
  
 // @ pack : reset string  =========================> 
  $final_output = null;
  $this->last_response = $response;
 
// @ get last response header HTTP OK  ===============>

  if( preg_match("/OK/i", $response ) ) {	
	 return $this; 
  } else {
	 return FALSE;
  }

}
// END HANGUP ==============================> 

// END CLASS 
}
// END FILE .

?>