<?php
/*
 * @ pack : list EUI_ListViewTable Of page 
 */
 
class EUI_ListViewTable 
{

/*
 * @ pack : instance 
 */
 
 var $_hader_table = array();
 var $_rows_header = array();
 
/*
 * @ pack : instance 
 */
 var $_view_show_table = null;
 
 /*
 * @ pack : instance 
 */
 
 private static $Instance = null;

/*
 * @ pack : instance 
 */
 
 public static function &Instance()
{
  if( is_null(self::$Instance) )
  {
	 self::$Instance = new self(); 
 }
	
 return self::$Instance;
	
} 
// @ pack set cellpading

public function set_header( $key, $value )
{
	$this->_hader_table[$key] = $value;
}
// @ pack set cellpading

public function set_rows_header( $key, $value )
{
	$this->_hader_rows[$key] = $value;
}

// @ pack set cellpading

public function set_tr_header( $key, $value )
{
	$this->_rows_tr_header[$key] = $value;
}


// @ pack set cellpading

public function set_th_count( $td = 0 )
{
	if( is_array( $n ) ){
		$this->_td_count = count($n);	
		
	} else { 
		$this->_td_count = $n;
	}
	
}


// @ pack set cellpading

public function set_td_first( $key, $value )
{
	$this->_td_first[$key] = $value;
}

// @ pack set cellpading

public function set_td_middle( $key, $value )
{
	$this->_td_middle[$key] = $value;
}

// @ pack set cellpading

public function set_td_last( $key, $value )
{
	$this->_td_lasted[$key] = $value;
}


// @ pack _swow 

 public function _show_header()
{
	$list_string = null;
	
	$header = $this->_hader_table;
	$list_string.= "<table ";
	
	if( isset( $header['width']) ) 
		$list_string .=" width=\"{$header['width']}\"";
		
	if( isset( $header['class']) ) 
		$list_string .=" class=\"{$header['class']}\"";
	
	if( isset( $header['cellspacing']) ) 
		$list_string .=" width=\"{$header['cellspacing']}\"";
	
	if( isset( $header['cellpadding']) ) 
		$list_string .=" width=\"{$header['cellpadding']}\"";
		
	$list_string .= "/> ";
	$list_string .= " <thead> ";
	
// @ pack : tr --- 
	
	$list_string .= "<tr ";
	if( is_array($this->_rows_tr_header) ) 
		foreach( $this->_rows_tr_header as $key => $value ) {
		$list_string .=" $key=\"$value\"";	
	}
	$list_string .= " />";
	
echo $list_string;
 
	
}

// END OF CLASS 
 
}


?>