<?php 

/*
 * @ package : library 
 * 
 * @ generate table ( html/excel ) ;
 */
 
class EUI_Grider 
{
	
 var $arr_thead  = array();
 var $arr_rhead  = array();
 var $arr_rindex = array();
 var $arr_tbody  = array(); 
 var $arr_tfoot  = array(); 
 var $arr_rbody  = array();
 var $arr_grider = array();
 var $arr_xls_header = array();
 var $arr_xls_row = array();

// ---------------------------------

 var $arr_class_grid = "report";
 
// ---------------------------------------------------------------------
// --------------- get on xls_header  ----------------------------------
 
 private static $Instance = null;


// ---------------------------------------------------------------------
// --------------- get on xls_header  ----------------------------------

public function set_class( $class = '' ) {
  $this->arr_class_grid = $class;
}


// ---------------------------------------------------------------------
// --------------- get on xls_header  ----------------------------------

public static function &Instance()
{
 if( is_null(self::$Instance) ) { 
	self::$Instance = new self();
 }
 return self::$Instance;
 
}

// ---------------------------------------------------------------------
// --------------- get on xls_header  ----------------------------------


 function __construct()
{ 
	$this->arr_thead[] = "<div>";
	$this->arr_tfoot[] = "</table></div>";
}

// ---------------------------------------------------------------------
// --------------- get on xls_header  ----------------------------------

function set_header( $key = null, $val = '' )
{
  if( !is_array($key) ){
	 $key = array($key => $val );
  }
  
  foreach( $key as $key => $value ){
	$this->arr_rindex[$key] = $value; 
	
  }
  
  $this->arr_rhead[] = "<thead>";
  $this->arr_rhead[] = "<tr height='35'>";
  
   foreach( $this->arr_rindex as $k => $label )
  {
	$this->arr_rindex[$k] = $label;  
	$this->arr_xls_header[$k] = $label;
	
	if( !is_array($label) ){
		$this->arr_rhead[] = "<th>". $label ."</th>";
	} else {
		$this->arr_rhead[] = "<th align=\"{$label['align']}\" >{$label['label']}</th>";
	}	
  } 
  
  $this->arr_rhead[] = "</tr>";
  $this->arr_rhead[] = "</thead>";
  
  
} 

// --------------- get on xls_header  --------------------------------

 function set_content( $row  = null ) 
{
  $this->arr_xls_row = $row;// set to re index   
  $this->arr_rbody[] = "<tbody>";
  foreach( $row as $rows )
 {
	$this->arr_rbody[] = "<tr>";
	foreach( $this->arr_rindex as $k => $value )
	{
		
		
		if( !is_array($value) ){
			$this->arr_rbody[] = "<td>{$rows[$k]}</td>";
		} else {
			$this->arr_rbody[] = "<td align=\"{$value['align']}\">{$rows[$k]}</td>";
		}
	}	
	
	$this->arr_rbody[] = "</tr>";
 } 
 $this->arr_rbody[] = "</tbody>";

  
}

// --------------- get on xls_header  --------------------------------

 function set_footer( $row  = null ) 
{
 foreach( $row as $rows ){
	$this->arr_rbody[] = "<tr>";
	foreach( $this->arr_header as $k => $value ){
		$this->arr_rbody[] = "<td>". $rows[$k]."</td>";
	}	
	$this->arr_rbody[] = "</tr>";
 } 
} 
 
// --------------- get on xls_header  --------------------------------

 function get_grid() 
{
 $this->arr_thead[]  = "<table class='{$this->arr_class_grid}' cellspacing=0 width='100%'>";
 
// -------------- page table ------------------	
   foreach( $this->arr_thead as $k => $value )
  {
	 $this->arr_grider[] = $value;
  }	 

// -------------- page table heaed ------------------  
   foreach( $this->arr_rhead as $k => $value )
  {
	 $this->arr_grider[] = $value;
   }	

  foreach( $this->arr_rbody as $k => $value )
 {
	$this->arr_grider[] = $value;
 }		  
  
  foreach( $this->arr_tfoot as $k => $value )
 {
	$this->arr_grider[] = $value;
  }		  
  
 $this->compile = "";
 foreach( $this->arr_grider as $k => $val ){
	$this->compile .= $val;
 }
 
 return $this->compile;
 
} 

// --------------- get on xls_header  --------------------------------

 public function get_xls_header()
{
	 $arr_xls_header = array();
	if( is_array( $this->arr_xls_header ) ) 
	{
		foreach( $this->arr_xls_header as $key => $row )
	   {
			$arr_xls_header[$key] = $row['label'];
		}
	}
	
	return $arr_xls_header;
} 

// --------------- get on xls_header  --------------------------------

 public function get_xls_row()
{
  $arr_rows = array();
  if( is_array( $this->arr_xls_row ) ) 
 {
    $arr_limit = (count($this->arr_xls_row)-1);
	$arr_rows = array_slice($this->arr_xls_row,0, $arr_limit);
 }
  return $arr_rows;

}

// --------------- get on xls_header  --------------------------------

 public function get_xls_all_row()
{
  $arr_rows = array();
  if( is_array( $this->arr_xls_row ) ) 
 {
    return $this->arr_xls_row;
 }
 
}

// --------------- get on xls_header  --------------------------------

 public function get_xls_bottom()
{
  $arr_rows = array();
  if( is_array( $this->arr_xls_row ) ) 
 {
    $arr_limit = (count($this->arr_xls_row)-1);
	$arr_rows = array_slice($this->arr_xls_row, $arr_limit, 1);
	foreach( $arr_rows as $k => $val ) {
		$arr_rows[$k] = preg_replace("/(<)|(>)|(\/)|(b)/i", "", $val);
	}
 }
  return $arr_rows;

}



// END CLASS  

}

?>