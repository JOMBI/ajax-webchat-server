<?php

/*
 * @ pack : EUI_CurlService 
 */
 
class EUI_CurlService 
{

var $argvs_string = array();
var $argc_string = null;
var $argc_webs = null;
var $argc_host = null;

/*
 * @ pack : EUI_CurlService 
 */
 
private static $Instance = NULL;

/*
 * @ pack : EUI_CurlService 
 */
 
 public function __construct()
{
 $this->argc_webs = "smsserver";
 $this->argc_host = "localhost";
} 
/*
 * @ pack : EUI_CurlService 
 */

 public static function &Instance()
{
  if( is_null(self::$Instance) )
 {
		self::$Instance = new self();
	}
	
 return self::$Instance;
 
}

// --------------------------------------------------------------

public function get_curl_project() {
	return $this->get_value('project');	
}


// --------------------------------------------------------------
/*
 * @ pack : EUI_CurlService 
 */
 
 public function get_curl_server( )
{
 $UI =& get_instance();
 $UI->db->reset_select();
 $UI->db->select("RegServer, RegModem", false);
 $UI->db->from("sgs_sms_setting");
 $UI->db->where("RegStatus", 1);
 $UI->db->where("RegProject", $this->get_value('project'));
 
 $qry = $UI->db->get();
 
 if( $qry->num_rows()>0 ) {
	if( $rows = $qry->result_first_assoc() ) 
	{
		$this->argc_host = $rows['RegServer'];
		
	// ------- server setup -------------
		
		$this->set_curl('serveraddess', $rows['RegServer']);
		$this->set_curl('modem', $rows['RegModem']);
	}
 }
 
 return $this->argc_host;
		
 }
 
/*
 * @ pack : reset curl 
 */
  
 public function reset_curl()
{
 if( is_array($this->argvs_string) )
 {
	$this->argvs_string = array();
 }
} 

// --------------------------------------------------------------
/*
 * @ pack : EUI_CurlService 
 */
 
 public function get_value( $keys = null )
{	
   if( isset( $this->argvs_string[$keys] ) )
  {
	return $this->argvs_string[$keys];	
  }
  return null;
} 

// --------------------------------------------------------------
/*
 * @ pack : EUI_CurlService 
 */
 
 public function set_curl( $keys =null, $values )
{
 if( !is_null($keys) )
 {
	$this->argvs_string[$keys] = $values;
 }
 
} 


/*
 * @ pack : EUI_CurlService 
 */
 public function get_server() {
	return $this->argc_webs;
} 

/*
 * @ pack : EUI_CurlService 
 */
 
 public function get_curl()
{
 $this->argc_string = array();
  if( is_array($this->argvs_string) )
 {
	foreach( $this->argvs_string 
		as $key => $value ) 
	{
		$this->argc_string[] = sprintf("%s=%s", $key, rawurlencode($value) );
	}	
 }
 
 // ------- then set to str ---  
 if( is_array( $this->argc_string ) ){
	 return join("&", $this->argc_string);
 }
 
 return $this->argc_string;
 
} 

/*
 * @ pack : compile_socket sen via socket ()
 */
 
 public function compile_socket()
{
 $UI =& get_instance();
 
 $UI->load->helper('EUI_Socket');
// @ pack : set command return  .... 
 
 $conds = 0;
 
// @ pack :  get server sms .... 
 
 Socket()->set_fp_server( $this->get_curl_server(), 1800 );
 
// @ pack : create command  .... 
 Socket()->set_fp_command("
	Action:send_sms\r\n".
	"MasterId:{$this->argvs_string['MasterId']}\r\n".
	"phone_number:{$this->argvs_string['phone_number']}\r\n".
	"templateId:{$this->argvs_string['templateId']}\r\n".
	"userid:{$this->argvs_string['userid']}\r\n".
	"location:{$this->argvs_string['location']}\r\n".
	"text_messages:{$this->argvs_string['text_messages']}\r\n"
 );

// @ send on to server  .... 
 if( Socket()->send_fp_comand() )
 {
	$conds++;
 }

 return $conds;
 
} 

// ----------------------- public function recsource  ----------------------------
/*
 *
 */
 
 public function get_curl_callback()
{
   if( $this->CURL_OUPUT )
  {
	  return $this->CURL_OUPUT;
  }	
}

// ----------------------- public function recsource  ----------------------------
/*
 * @ return get array model  
 */
 
 public function num_rows()
{
   $arr = $this->fetch_array();	 
   if( count($arr) > 0  ){
		return true;
   } else {
	   return false;
   }   
}

// ----------------------- public function recsource  ----------------------------
/*
 * @ return get array model  
 */
 
public function fetch_object()
{
   $arr = $this->fetch_array();	 
   if( count($arr) > 0  ){
		return (object)$arr;
   } else {
	   return false;
   }   
}


// ----------------------- public function recsource  ----------------------------
/*
 * @ return get array model  
 */
 
 public function fetch_array()
{
   $arr_json = array() ;
   if( !is_null($this->CURL_OUPUT) )  
  {
	if( function_exists('json_decode') ){    
		$arr_json = json_decode($this->CURL_OUPUT);	
	} else {
		die('service json not available'); 
	}
  } 
  if( is_object($arr_json) ){
	return (array)$arr_json;
  } else {
	  return FALSE;
  }
}


/*
 * @ pack : compile_curl()
 */
 
 public function compile_curl()
{
 $conds = 0;
 
 $ar_curl_server = $this->get_curl_server();
 $ar_curl_webserver = $this->get_server();
 $ar_curl_webparams = $this->get_curl();
 
// ---- test curl data  ---  
 
 $this->curl_data = sprintf("http://%s/%s/?%s", $ar_curl_server, $ar_curl_webserver, $ar_curl_webparams);
 if( $this->curl_data )
 {
	if( function_exists('curl_init') )
	{
		$this->CURL_REQUEST = curl_init( $this->curl_data );
	 // @ pack : user curl its 
	 
		curl_setopt($this->CURL_REQUEST, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($this->CURL_REQUEST, CURLOPT_BINARYTRANSFER, TRUE);
		
	// @ get ouput 
	
		$this->CURL_OUPUT = curl_exec($this->CURL_REQUEST);
		if( (trim($this->CURL_OUPUT)!='')) 
		{
			$this->reset_curl();
			$conds++;
		}
	} 
 }
 
return $this;
 
} 

// END CLASS 

 
 

}

?>