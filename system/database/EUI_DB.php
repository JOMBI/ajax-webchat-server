<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function &EUI_DB($params = '', $active_record_override = NULL)
{
	
	if (is_string($params) AND strpos($params, '://') === FALSE) {
		
		// open state Key : 
		$open =& KeyInstall(); 
		if(!is_object($open) ){
			show_error("invalid open key");
		}
		
		// then : 
		$configuration =& $open->getKey();
		if(!defined('ENVIRONMENT') OR !file_exists($file_path = BASEPATH ."keys/". ENVIRONMENT ."/". $configuration)){
			if ( ! file_exists($file_path = BASEPATH ."keys/". $configuration))  {
				show_error("The configuration file $configuration does not exist.");
			}
		}
		
		// then open run : 
		$BASEPATH = rtrim(BASEPATH, '/');
		$db =& $open->getKeyInstall( sprintf('%s/keys', $BASEPATH));
		
		if ( ! isset($db) OR count($db) == 0) {
			show_error('No database connection settings were found in the database config file.');
		}
		
		// jika ada parameters : 
		if ( $params != '' ) {
			$active_group = $params;
		}
		// then will get data : 
		else if( $active_group = $open->getString('active.group') ){
			$active_group = $active_group;
		}
		
		// check apakah yang active group OK atau tidak ?
		if ( !isset($active_group) OR !$open->getArray($active_group) ) {
			show_error('You have specified an invalid database connection group.');
		}
		
		// then willput params on database[onhere]
		$params = ( $open->getArray($active_group) ? 
					$open->getArray($active_group)  : NULL );
		
	}
	
	
	// No DB specified yet?  Beat them senseless...
	if ( ! isset($params['EUI_DB_driver']) OR $params['EUI_DB_driver'] == '') {
		show_error('You have not selected a database type to connect to.');
	}

	// Load the DB classes.  Note: Since the active record class is optional
	// we need to dynamically create a class that extends proper parent class
	// based on whether we're using the active record class or not.
	// Kudos to Paul for discovering this clever use of eval()

	if ($active_record_override !== NULL){
		$active_record = $active_record_override;
	}
	//debug($params);
	require_once(BASEPATH.'database/EUI_DB_driver.php');

	if ( ! isset($active_record) OR $active_record == TRUE) {
		require_once(BASEPATH.'database/EUI_DB_active_rec.php');

		if ( ! class_exists('EUI_DB')) {
			eval('class EUI_DB extends EUI_DB_active_record { }');
		}
	}
	else 
	{
		if ( ! class_exists('EUI_DB')){
			eval('class EUI_DB extends EUI_DB_driver { }');
		}
	}
	
	require_once(BASEPATH.'database/adapter/'. $params['EUI_DB_driver'] .'/EUI_'.$params['EUI_DB_driver'].'_driver.php');

	// Instantiate the DB adapter
	$driver = 'EUI_DB_'.$params['EUI_DB_driver'].'_driver';
	$DB = new $driver($params);

	if ($DB->autoinit == TRUE)
	{
		$DB->initialize();
	}

	if (isset($params['stricton']) && $params['stricton'] == TRUE)
	{
		$DB->query('SET SESSION sql_mode="STRICT_ALL_TABLES"');
	}

	return $DB;
}



/* End of file DB.php */
/* Location: ./system/database/DB.php */