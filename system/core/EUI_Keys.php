<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Properties Method [function]::set
 *
 * @Description []	private called by execute()
 * @Description []	string	an @prototype JS
 * @Description [] 	string
 */
 class EUI_Keys 
{
 
var $config = array();  
/*
 * Properties Method [function]::set
 *
 * @Description []	private called by execute()
 * @Description []	string	an @prototype JS
 * @Description [] 	string
 */
private $DefaultPath = array( BASEPATH, 'keys', 'PhysicalMacAddress.ini');

/*
 * Properties Method [function]::set
 *
 * @Description []	private called by execute()
 * @Description []	string	an @prototype JS
 * @Description [] 	string
 */
private static $Instance;

/*
 * Properties Method [function]::set
 *
 * @Description []	private called by execute()
 * @Description []	string	an @prototype JS
 * @Description [] 	string
 */
 public static function &Instance() {
   if(is_null( self::$Instance) ) {
		self::$Instance = new self();
   }
	return self::$Instance;
 } 
 
/*
 * Properties Method [function]::set
 *
 * @Description []	private called by execute()
 * @Description []	string	an @prototype JS
 * @Description [] 	string
 */
 protected function MacAddress()  {
	 
// get data from this class : 
 $this->AuthDefaultPath= implode('/', $this->DefaultPath);
 if( !file_exists($this->AuthDefaultPath) ) {
	show_error("No auth keys");
 }
 
 // then set an array data : 
 $fetchArray = @parse_ini_file($this->AuthDefaultPath);
 if(!isset( $fetchArray['mac.address'])) {
	show_error("No auth keys");
 }
 // will trigger_error 
 if( strlen(trim($fetchArray['mac.address']))==0 ) {
	show_error("No auth keys");
 }
 
 // then will run of data process : 
 $this->DefaultMacAddress = base64_decode(base64_decode($fetchArray['mac.address']));
 return $this->DefaultMacAddress; 
}
 
/*
 * Properties Method [function]::set
 *
 * @Description []	private called by execute()
 * @Description []	string	an @prototype JS
 * @Description [] 	string
 */
function getArray( $key = null ){
	return (array)$this->config[$key];
} 
/*
 * Properties Method [function]::set
 *
 * @Description []	private called by execute()
 * @Description []	string	an @prototype JS
 * @Description [] 	string
 */
function getString( $key = null ){
	return (string)$this->config[$key];
} 

/*
 * Properties Method [function]::set
 *
 * @Description []	private called by execute()
 * @Description []	string	an @prototype JS
 * @Description [] 	string
 */
  public function getKeyInstall( $path =NULL ) {
	$pathConfig = $this->getKey();
	if( !$pathConfig ){
		return 0;
	}
	
	// then willopen data: 
	$pathinfo = sprintf("%s/%s", $path, $pathConfig);
	if(!file_exists( $pathinfo ) ) {
		show_error( sprintf("No Keys Install <b>%s</b> does not exist.", $pathConfig));
	}
	
	// open configuration 
	
	$mArray = @parse_ini_file($pathinfo, true);
	if( is_array($mArray) ) 
	foreach( $mArray as $key => $val ) {
		// then will get key data : 
		if( !strcmp( $key, 'instalation') ){
			foreach( $val as $k => $v ){
				$this->config[$k] = $v;
			}
		}
		// version
		else if( !strcmp( $key, 'version') ){
			foreach( $val as $k => $v ){
				$this->config[$k] = $v;
			}
		}
		
		// system
		else if( !strcmp( $key, 'system') ){
			foreach( $val as $k => $v ){
				$this->config[$k] = $v;
			}
		}
		// active
		else if( !strcmp( $key, 'active') ){
			foreach( $val as $k => $v ){
				$this->config[$k] = $v;
			}
		}
		else {
			foreach( $val as $k => $v ){
				$this->config[$key][$k] = $v;
			}
		}
	}
	return $this->config;
	
 }
 
/*
 * Properties Method [function]::set
 *
 * @Description []	private called by execute()
 * @Description []	string	an @prototype JS
 * @Description [] 	string
 */
 public function getKey()  {
	 
// open decoede data : 	 
  $auth = base64_encode( $this->MacAddress() );
 
  if( !$auth ){
	  return 0;
  }
  // if open file success : 
  $open = md5($auth);
  $rest = sprintf("%s.conf", $open);
  return $rest; 
 }
 // END CLASS  -------------------------
}
?>