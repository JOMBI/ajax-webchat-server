<?php
/*
 * @def : read Text file
 *
 */
 
class EUI_ReadText
{

private static $instance  = null;
private $_filename_text = null;
private $_set_text_delimiter = null;
private static $_class_exponen_data = null;


/* get_instance **/

public static function &get_instance()
{
	if(is_null(self::$instance) ) {
		self::$instance  = new self();
	}
	
	return self::$instance;
}

/* 
 * @ def 	: aksesor read on location definition 
 * @ secure	: must be read file execute mode 0775 
 */

public function ReadText( $_FILE_TEXT_LOCATION = null  )
{
	if( is_null( $this -> _filename_text ) ) {
		$this -> _filename_text = $_FILE_TEXT_LOCATION;
	}
	
	$this -> getData();
}
	


/* 
 * @ def 	: settup delimiter of the text 
 * @ delimieter	: must valid OK 
 */
 
public function setDelimiter($_DELIMITER = null )
{
	if( is_null( $this -> _set_text_delimiter ) ) 
	{
		$this -> _set_text_delimiter = $_DELIMITER;
	}
	
}

/* 
 * @ def 	: settup delimiter of the text 
 * @ delimieter	: must valid OK 
 */
 
 public function getData( $_BUFFER =40960)
{
 $Delimiter = $this->_set_text_delimiter;
 
 if(is_null($this -> _filename_text) ) exit('File Not exist');
 else
 {
	$fhandle = @fopen($this -> _filename_text, "r");
	if ($fhandle)  
	{
		$line = 0; $_EXPLODE_LINE =null;
		
		while (($_READ_LINE = fgets($fhandle, $_BUFFER)) !== FALSE) 
		{
			$COLUMNS = EXPLODE($this -> _set_text_delimiter, $_READ_LINE);
			if( $COLUMNS = EXPLODE($this -> _set_text_delimiter, $_READ_LINE) 
				AND preg_match("/($Delimiter)/i", $_READ_LINE )!=FALSE )
			{
				$n = 1;
				foreach($COLUMNS as $keywords => $values ) {
					$_EXPLODE_LINE[$line][$n] = trim($values);
					$n++;
				}	
				$line++;
			}	
		}
			
		
		// class set data 
		
		if (!feof($fhandle)) { 
			exit("Error: unexpected fgets() fail\n");
		}
			
		fclose($fhandle);
		if( is_null(self::$_class_exponen_data) )
		{
			self::$_class_exponen_data = $_EXPLODE_LINE;
		}	
	 }
		
	}
}

/* 
 * @ def 	: settup delimiter of the text 
 * @ delimieter	: must valid OK 
 */
 
public function rowcount()
{
	$count = 0;

  if( !is_null(self::$_class_exponen_data) ) 
  {
	if( $data = self::$_class_exponen_data ){
		$count = count($data);
	}
 }
  
  
 return $count;
}



/* 
 * @ def 	: settup delimiter of the text 
 * @ delimieter	: must valid OK 
 */
 
public function Results()
{
 
 $result = array();

  if( !is_null(self::$_class_exponen_data) ) 
  {
	if( $data = self::$_class_exponen_data ){
		$result = $data;
	}
 }
  
  
 return $result;
}

/* 
 * @ def 	: settup delimiter of the text 
 * @ delimieter	: must valid OK 
 */
public function getHeader()
{

 $result = array();
  if( !is_null(self::$_class_exponen_data) ) 
  {
	if( $data = self::$_class_exponen_data ) {
		$result = $data[0];
	}
 }
 return $result;
}

/* 
 * @ def 	: settup delimiter of the text 
 * @ delimieter	: must valid OK 
 */
 

public function val( $r, $c )
{
 
 $result = array();
  if( !is_null(self::$_class_exponen_data) ) 
  {
	if( $data = self::$_class_exponen_data ) {
		$result = $data[$r][$c];
	}
 }
 return $result;
 
}

}

if( !function_exists('TextImport') ){
	
  function TextImport() 
  {
	if( class_exists('EUI_ReadText') )
	{
		return EUI_ReadText::get_instance();
	 }
  }
}

?>