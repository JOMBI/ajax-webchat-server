<?php 

/*
 * @ def 		:  EUI_helpers 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
class EUI_Flexible
{

/*
 * @ def 		:  get by campaign 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 private static $instance  = null;

/*
 * @ def 		:  get by campaign 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 public static function & get_instance()
{
	if( is_null(self::$instance) ) {
		self::$instance = new self();
	}
	return self::$instance;
}

/*
 * @ def 		:  get by campaign 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _FlexibleCampaignId( $CampaignId = 0 )
{ 
  $UI =& get_instance();
  
  $UI->db->select('*');
  $UI->db->from('t_gn_field_campaign a');
  $UI->db->where("a.CampaignId", $CampaignId);
  $UI->db->where("a.Field_Active", 1);
  
  if( $rows = $UI-> db -> get() -> result_first_assoc() )  
	return $rows;
  else
	return false;
}

/*
 * @ def 		:  get by campaign 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 public function _FlexibleFiedId( $LayoutId = 0 )
{ 
  $UI =& get_instance();
  
  $UI->db->select('a.*');
  $UI->db->from('t_gn_field_campaign a');
  $UI->db->where("a.Field_Id", $LayoutId);
  $UI->db->where("a.Field_Active", 1);

   if( $rows = $UI -> db -> get() -> result_first_assoc() )  
		return $rows;
	else
		return false;
}


/*
 * @ def 		:  get rows filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
 function _FlexibleLabelLayout( $Field_Id = 0 )
{
	
	$UI =& get_instance(); $Layout = array();
	
	$UI->db->select('*');
	$UI->db->from('t_gn_field_rowset a');
	$UI->db->where("a.Field_Id", $Field_Id);
	$UI->db->order_by("a.Rows_Orders","ASC");
	
	$i = 0;
	foreach( $UI ->db -> get() -> result_assoc() as $rows )
	{
		$Layout['Names'][$i]  = trim($rows['Rows_Names']);
		$Layout['Labels'][$i] = trim($rows['Rows_Labels']);
		$Layout['Ordes'][$i]  = trim($rows['Rows_Orders']);
		$i++;
	}
	
	return $Layout;
  }
  
}

// END OF OBJECT 

// START OF OBJECT

class _CreateLayout 
{

/*
 * @ def 		:  get by campaign 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
  
 private static $instance = null; 
 private static $LayouLabels = null;
 private static $LayoutHeaders = null;
 
 /*
 * @ def 		:  get by campaign 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
  
 private $CustomerId  = null;
 private $Tables  = null;
 private $primarykeys = null;
 
/*
 * @ def 		:  get by campaign 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
  
 public function _setLayoutLabels( $labels = null , $headers = null )
 {
	self::$LayouLabels = ( !is_null($labels) ? $labels : null );
	self::$LayoutHeaders = ( !is_null($headers) ? $headers : null );	
 }
 

/*
 * @ def 		:  get by campaign 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */

 public static function & get_instance()
{
	if( is_null(self::$instance) ) {
		self::$instance = new self();
	}
	return self::$instance;
}

/*
 * @ def 		:  get rows filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
  
 public function _getLabels() 
 {
	if( !is_null(self::$LayouLabels) ){
		return self::$LayouLabels;
	}
	else
		return false;
 }
 
/*
 * @ def 		:  get rows filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
  
 public function _getHeaderLayout() 
 {
	if( !is_null(self::$LayoutHeaders) ){
		return self::$LayoutHeaders;
	}
	else
		return false;
 }
  
 
 
/*
 * @ def 		:  get rows filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
  
 public function _getHeaderLabels() 
 {
	$match_labels = array();
	if( $labels = $this -> _getLabels() ){
		return $labels['Labels'];
	}
	else
		return false;
 } 
 
/*
 * @ def 		:  get rows filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
  
 public function _getFieldsLabels() 
 {
	$match_labels = array();
	
	$fields = $this -> _getFields();
	$labels = $this -> _getLabels();
	
	if( $fields )
	{
		if( $labels ) foreach( $labels['Names'] as $key => $label ) 
		{
			if(in_array($label, array_values($fields) )){	
				$match_labels[] = $label; 
			}
		}
	}
	
	// call back 
	
	if( count($match_labels) >  0 )	
		return $match_labels;
	else
		return false;
 }
   

/*
 * @ def 		:  get rows filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
  
 public function _getFields() 
 {
	if($labels = $this->_getLabels() ) 
	{
		$UI = & get_instance();
		$rows = $UI ->db->list_fields( $this -> _getTables() );
		return $rows;
	}
 }
  
/*
 * @ def 		:  get rows filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
  
 public function _getCustomerId() 
 {
	return $this -> primarykeys; 
 }
 
/*
 * @ def 		:  get rows filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
 
 public function _getTables(){
	return $this -> Tables;
 } 
 
 
 
/*
 * @ def 		:  get rows filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
  
 function _setCustomerId( $wheres = null ) 
 {
	if( is_array($wheres)) 
	{
		$this -> primarykeys  = $wheres;
	}
 }
 
/*
 * @ def 		:  get rows filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
 
  function _setTables( $tables = 't_gn_bucket_customers') {	
	$this -> Tables = $tables;
 }

/*
 * @ def 		:  get rows filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
  
  
function _getHeader()
{
	$Haders = null;
	$GroupHeader = $this -> _getHeaderLayout(); 
	if( is_array($GroupHeader) ) 
	{
		if(isset($GroupHeader['Field_Header']) ) 
		{
			$Haders = $GroupHeader['Field_Header'];
		}
	}
	
	return $Haders;
} 
 
/*
 * @ def 		:  get rows filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
  
 function _getData()
 {
	$_list_labels = null;
	if( $LayoutFields = $this -> _getFieldsLabels() ) 
	{
		$UI =& get_instance();
		$UI->db->select(array_values($LayoutFields));
		$UI->db->from($this->_getTables());
		$UI->db->where($this->_getCustomerId());
		
		if( $rows = $UI->db->get()->result_first_assoc() )
		{
			$i = 0;
			$_list_header = $this -> _getHeaderLabels();
			if(@is_array($_list_header))foreach( $rows as $key => $value ) 
			{
				$_list_labels[$key]['label']= $_list_header[$i]; 
				$_list_labels[$key]['value']= $value; 	
				$i++;
			}
		}
		// no data avaialebale
		else
		{
			$_list_fields = $this -> _getLabels();
			$_list_header = $this -> _getHeaderLabels(); 
			
			$i = 0;			
			if(@is_array($_list_fields['Names']) )foreach( $_list_fields['Names'] as $key => $value ) 
			{
				$_list_labels[$value]['label']= $_list_header[$i]; 
				$_list_labels[$value]['value']= null;	
				$i++;
			}
		}
	}
	
	
	return $_list_labels;
}

 
/*
 * @ def 		:  get rows filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */ 
 
 function _Compile() 
 {
	$UI = & get_instance();
	$UI->load->helper('EUI_Field');
	
	/** 
	 * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
	 * ||||||||||||||||||||||||||||**** load helper field **** |||||||||||||||||||||||||||||| 
	 * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
	 */
	 
	$FxData = & EUI_Field_value::get_instance();
	
	if( $Header = $this -> _getHeaderLayout()) 
	{
		$Field_Columns = (INT)$Header['Field_Columns'];
		$Field_Size = (INT)$Header['Field_Size'];
		$Group_Size = ceil($Field_Size/$Field_Columns);
		$Field_Data = $this -> _getData();
		$percent = floor(100/$Field_Columns);
		$asal='';
		if( !$Group_Size ) die('zero file ');
		else
		{
		 __("<fieldset class='corner' style='margin-top:-12px;margin-bottom:4px;'>\n");
		 __("<legend class='icon-customers'>&nbsp;&nbsp;{$this -> _getHeader()}</legend>\n");
		 __("<div style='overflow:auto;margin-top:1px;' class='activity-content'>\n");
			__("<table border=0 cellpadding='0' cellspacing=0 align='center' width='100%'>\n");
				__("<tr>\n");
			for( $p = 0; $p <$Field_Columns; $p++) 
			{
				__("<td width='$percent%' valign='top'>");
				__("<div style='border:0px solid #ddd;'>\n");
					__("<table border=0 width='99%' cellpadding='1px' cellspacing='1px' align='center'>\n");
					
					$start = ($p * $Group_Size);
						if( $list_arrays = array_slice($Field_Data, $start, $Group_Size)){
							$asal=$list_arrays;
							foreach( $list_arrays as $field => $rows )
						{
						
							__("<tr>");
								__("<td class='left text_caption bottom' nowrap valign='top'>". ( isset($rows['label']) ? $rows['label']: '-')."</td>\n");
								__("<td valign='top'>:</td>\n");
								__("<td class='bottom font-standars13' nowrap valign='top'>
									<span style='line-height:18px'>". wordwrap((isset($rows['value']) ? $FxData->FieldValue($field, $rows['value']) : '' ), 25, "<br/>\n") . "</span></td>\n");
							__("</tr>\n");
						}

					

				}
						__("</table>\n");
					__("</div>\n");
				__("</td>\n");
			}
				__("</tr>\n");
			__("</table>\n");	
			__("</div>\n");
			__("</fieldset>");

			
		}
	}
	
 }
 
 

 

}

// END OF OBJECT 

/*
 * @ def 		:  get rows filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
if(!function_exists('_cmpFlexibleLayout') ) 
{
 function _cmpFlexibleLayout($CampaignId= 0 ) 
 {
	$_create_layout = false;
	$flexi =& EUI_Flexible::get_instance();
	
	if( $rows = $flexi -> _FlexibleCampaignId( $CampaignId ) )
	{
		if(is_null($rows['Field_Id'])) die('No Campaign');
		else 
		{
			$labels = $flexi -> _FlexibleLabelLayout($rows['Field_Id']);
			if( $labels ) 
			{
				$_create_layout =& _CreateLayout::get_instance();
				$_create_layout -> _setLayoutLabels( $labels, $rows );
			}
		}	
	}
	
	return $_create_layout;
 }
}


/*
 * @ def 		:  get rows filed ID 
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
if(!function_exists('_fldFlexibleLayout') ) 
{
 function _fldFlexibleLayout( $Field_Id = 0 ) 
 {
	$_create_layout = false;
	$flexi =& EUI_Flexible::get_instance();
	if( $rows = $flexi -> _FlexibleFiedId( $Field_Id ) )
	{
		if(is_null($rows['Field_Id'])) die('No Field_Id');
		else 
		{
			$labels = $flexi -> _FlexibleLabelLayout($rows['Field_Id']);
			
			if( $labels ) 
			{
				$_create_layout =& _CreateLayout::get_instance();
				$_create_layout -> _setLayoutLabels( $labels, $rows );
			}
		}	
	}
	
	return $_create_layout;
  }
}
 
 ?>