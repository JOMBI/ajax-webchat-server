<?php  
// check path : 
 if (!defined( "DIR_ERR_PATH" )) 
 {
	define( "DIR_ERR_PATH", "/var/log/webchat");
 }

/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	 
  function writeloger($error= "", $EVENT = "EVENT REPORT" )
 {
    /* if directory not exits will create on here */ 
	if ( !is_dir(DIR_ERR_PATH) )
	{
		@mkdir(DIR_ERR_PATH, 0777, true);
		if (function_exists ('system'))
		{
			system (sprintf("chmod -R 0777 %s", DIR_ERR_PATH));
		}
	}
  
	/* then this will set path error log */
	$sp = sprintf("%s/%s_%s.log", DIR_ERR_PATH , "error", date('Ymd'));
	$fp = @fopen($sp, 'a');
	if (!$fp)
	{
		return 0;
	}
  
	// issource data OK 
	if ($fp)
	{
		fwrite($fp, sprintf("\n\r%s $EVENT >>\n%s", date('Y-m-d H:i:s'), $error));
		if ($fp)
		{
			@fclose($fp);
		}
	}
	return true;
}	

/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	 
if( !function_exists('is') ){
	function is( $data = array(), $field = 0 ) {
		if( isset( $data[$field] ) ) {
			return $data[$field];
		}
		return null;
	}
} 
// _setBoldColor 
// translate apakah "object" benar / salah

if( !function_exists('Call') ){
 function Call( $var = null, $val = null){
	if( !IsFunction( $val ) ){
		return $var;
	}		
	return call_user_func( $val, $var);
	
 }
}
// translate apakah "object" benar / salah

if( !function_exists('IsFunction') ){
 function IsFunction( $val ){ 
	return ( function_exists( $val ) ? true : false);	
 }
}

// translate apakah "object" benar / salah

if( !function_exists('IsCount') ){
 function IsCount( $val ){ 
	 if( is_array($val) and count($val) > 0 ){
		 return true;
	 }
	 return false;
 }
}

/* @brief CreateSession 
 * @details create session custom
 * @param string "session"
 * @retval mixed return 
 */
 if( !function_exists('CreateSession') ) 
{
	 function CreateSession(&$sessionid = false)  
	{
		$sessionid = sprintf("%016d",hexdec(uniqid()));
		return (string)$sessionid;
	}
} 

/* @brief SetDuration 
 * @details create session custom
 * @param string "session"
 * @retval mixed return 
 */
if (!function_exists('SetDuration') ) 
{
	 function SetDuration($duration = 0) 
	{
		return _getDuration($duration);
	}
}


/* @brief SetDuration 
 * @details create session custom
 * @param string "session"
 * @retval mixed return 
 */
 if (!function_exists('SetCurrency'))
{
	 function SetCurrency($currency = "")  
	{
		return _getCurrency($currency);
	}
}

// _setBoldColor 
// translate apakah "object" benar / salah

 if( !function_exists('SetBoldColor') ) {
	 function SetBoldColor(  $val = null )  {
		return _setBoldColor( $val );
	}
}
// _setBoldColor 
// translate apakah "object" benar / salah

 if( !function_exists('SetDateTime') ) {
	 function SetDateTime( $val = 0 )  {
		 $vals = explode(" ", $val);
		 return sprintf("%s %s", SetDate($vals[0]), $vals[1]);
	}
}
// SetAddressLine 
// translate apakah "object" benar / salah

function SetDOB( $val = null ){
	if( is_null( $val ) ){
		return '';
	} 
	// then :
	return date('Y-m-d', strtotime($val));
}
 
// RealIpAddress 
// translate apakah "object" benar / salah

if( !function_exists('RealIpAddress') ) {
function RealIpAddress(){ 
	return @call_user_func('_getIP', null);
}
}
// SetAddressLine 
// translate apakah "object" benar / salah

if( !function_exists('SetKalimat') ) {
	function SetKalimat( $val = '' )
	{
		$val = preg_replace('/\s+/S', " ", $val);
		return wordwrap($val, 50, " ");
	}
}
// translate apakah "object" benar / salah
if(!function_exists('SetDate') )  {
	function SetDate( $date = "" )  {
		if( strlen( $date ) <10){
			return "";
		}	
		
		if( strcmp( $date, "00-00-0000") == 0 ){
			return "";
		}
		
		if( strcmp( $date, "0000-00-00") == 0 ){
			return "";
		}
		
		$val = explode('-', $date );
		if( strlen( $val[0] ) == 4   ){ 
			return _getDateIndonesia( $date );
		}
		
		if( strlen( $val[0] ) == 2){ 
			return  _getDateEnglish( $date );
		}
		return " ";
	}
} 
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	 
 if(!function_exists('_getVersion') )
 {
	function _getVersion()
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _version();
		}	
	}
}


/* 
 * @ def : _getVersion of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
if(!function_exists('_getGender') )
 {
	function _getGender( $code )
	{
		$arr = array(1=> 'M', 2=>'F', 3 => 'U');
		if( in_array($code, array_values($arr) )) 
			return $code;
		else {
			return $arr[$code];
		}
	}
}




/* 
 * @ def : get name of month 
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('NameMonth') )
 {
	function NameMonth( $n = null )
	{
	   $UI =& get_instance();
	   if( !is_null($n) )
	   {
		 $n = (int)$n;
		 $month =& $UI->EUI_Tools->_getBulan('in'); // indonesia
		 return $month[$n];
	   }
	   else{
		return null;
	   }
	   
	}
 }
 
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 if(!function_exists('Objective') ) 
 {
   function Objective( $resultArray = null )  {
	
	$CI =&CI();
	if( !class_exists('EUI_Object') ){
		$CI->load->helper(array('EUI_Object'));	
	}
	// return data OK;
	$resultFetchArray = new EUI_Object( $resultArray );
	return $resultFetchArray;
  }
}

/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 if(!function_exists('CaptionKode') )  { 
 function CaptionKode( $val = ''){
	 if( stristr( $val, '-') ){
		 list( $caption, $CaptionKode ) = explode('-', $val);
		 return sprintf('%s',$CaptionKode);
	 }
	 return $val;
 }
}
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
if(!function_exists('FileSizeData') )  {    
 function FileSizeData( $size = 0 ){
	if ( !$size ){
		return 0;
	}
	// return data: 
	if(function_exists('_getFormatSize') ){
		return call_user_func( '_getFormatSize', $size);
	}
	return 0;
 } 
}
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if(!function_exists('FileDuration') )  {   
function FileDuration( $time = 0 ){
	// validation check :
	if ( !$time ){
		return sprintf('%s','00:00:00');
	}
	// return data: 
	if(function_exists('_getDuration') ){
		return call_user_func('_getDuration', $time);
	}
	// return this;
	return sprintf('%s','00:00:00');
	
 }
}
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if(!function_exists('AutoDateTime') )  {  
function AutoDateTime( $val = null  ){
	
	$value = null;
	if( is_null( $val ) ){
		return $value;
	}
	// then process : 
	list( $date, $time ) = explode(' ', $val );
	if( $date ){
		list( $s1, $s2, $s3 ) = explode( '-', $date );
		// then will convert : 
		if( strlen( $s1 ) == 4 ){
			// to local date 
			$value = date( 'd-m-Y H:i:s',  strtotime( $val ));
		}
		if( strlen( $s1 ) < 4 ){
			$value = date( 'Y-m-d H:i:s',  strtotime( $val ));
		}
	}	
	// return data : 
	return $value ;
  } 
}
 
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if(!function_exists('AutoDate') )  {  
function AutoDate( $val = null  ){
	
	$value = null;
	if( is_null( $val ) ){
		return $value;
	}
	// then process : 
	list( $date, $time ) = explode(' ', $val );
	if( $date ){
		list( $s1, $s2, $s3 ) = explode( '-', $date );
		// then will convert : 
		if( strlen( $s1 ) == 4 ){
			// to local date 
			$value = date('d-m-Y',  strtotime( $date ));
		}
		if( strlen( $s1 ) < 4 ){
			$value = date('Y-m-d',  strtotime( $date ));
		}
	}	
	// return data : 
	return $value ;
  } 
}
 
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 if(!function_exists('CaptionBold') )  { 
 function CaptionBold( $val = ''){
	 if( stristr( $val, '-') ){
		 list( $caption, $detail ) = explode('-', $val);
		 return sprintf('<span style="color:#2E244A;font-weight:bold;">%s</span> - %s', $caption, $detail);
	 }
	 return $val;
 }
 }
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 if(!function_exists('debug') ) 
 {
   function debug( $value = null )  {
	   
	   // check type str: 
		if( is_array($value) OR is_object($value) ){
			printf("%s", "<pre>");
				print_r( $value);
			printf("%s", "</pre>");
		}
		else {
			// then will get : 
			printf("<pre>%s</pre>", 
					$value);
		}
  }
}
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 if(!function_exists('NextMonth') )
 {
	function NextMonth( $date ) {
		$dates = explode("-", $date);
		$yyyy = $dates[0]; 
	$mm   = $dates[1];
	$mm++;
	
	if($mm>12){
		$mm = 1;
		$yyyy++;
	}

	if (strlen($mm)==1)$mm="0".$mm;
	return $yyyy."-".$mm;
	
	}
 }

/* 
 * @ def : _getBrowser of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getBrowser') )
 {
	function _getBrowser()
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _os_browser();
		}	
	}
}
 
/* 
 * @ def :_getOS UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getOS') )
 {
	function _getOS()
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _os_detected();
		}	
	}
} 

/* 
 * @ def : _getToHour instance of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getToHour') )
 {
	function _getToHour($integer = 0 )
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _set_float_hour( $integer );
		}	
	}
} 

/* 
 * @ def : _getToMinute instance of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getToMinute') )
 {
	function _getToMinute($integer = 0 )
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _set_float_minute( $integer );
		}	
	}
} 
/* 
 * @ def : _getDuration instance of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getDuration') )
 {
	function _getDuration($integer = 0 )
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _set_duration( $integer );
		}	
	}
} 


/* 
 * @ def : _getFormatSize of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getFormatSize') )
 {
	function _getFormatSize($integer = 0 )
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _get_format_size( $integer );
		}	
	}
} 

/* 
 * @ def : _getIP  of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getIP') )
 {
	function _getIP()
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _get_real_ip();
		}	
	}
} 


/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getCurrency') )
 {
	function _getCurrency($Integer=0)
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _set_rupiah($Integer);
		}	
	}
} 

/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getDateEnglish') )
 {
	function _getDateEnglish($Date=0)
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _date_english($Date);
		}	
	}
} 


/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getOptionDate') )
 {
	function _getOptionDate( $dates = null, $lang = 'en', $mod ='-')
	{
		$UI =& get_instance(); 
		$_dates = null; 
		if( !is_null($dates) )
		{
			$_lives = explode("{$mod}", $dates);
			switch( $lang ) 
			{
				case 'en' : 
					$_curdate = "{$_lives[0]}-{$_lives[1]}-{$_lives[2]}";
					$_dates = _getDateEnglish($_curdate);
				break;
				
				case 'in' : 
					$_curdate = "{$_lives[0]}-{$_lives[1]}-{$_lives[2]}";
					$_dates = _getDateIndonesia($_curdate);
				break;
			}
		}
		
		return $_dates;
	}
} 


/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getPhoneNumber') )
 {
	function _getPhoneNumber($String='')
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _getPhoneNumber($String);
		}	
	}
} 

/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getDateIndonesia') )
 {
	function _getDateIndonesia($Date=0)
	{
		$UI =& get_instance();
		if( $UI AND !is_null($Date) ) {
			return $UI -> EUI_Tools -> _date_indonesia( $Date );
		} else {
			return NULL;
		}	
	}
} 

/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getMasking') )
 {
	function _getMasking($v=null, $t='x')
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _set_masking($v,$t);
		}	
	}
} 

/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
if( ! function_exists('_setMasking') )
{
	function _setMasking($v=null, $t='x') 
	{
	    $UI =& get_instance();
		if( $UI ) {
			return $UI->EUI_Tools->_setToMasking($v,$t);
		}	
	}
} 




/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getNextDate') )
 {
	function _getNextDate( $Date = null )
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _NextDate($Date);
		}	
	}
} 


/* 
 * @ def : _getIP of UI // get range in date next 
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getNextCurrDate') )
 {
	function _getNextCurrDate( $Date = null, $n=0 )
	{
		$UI =& get_instance();
		if( $UI ) {
			return $UI -> EUI_Tools -> _NextCurrDate( $Date, $n );
		}	
	}
} 

/* 
 * @ def : _getIP of UI // get range in date next 
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
if( !function_exists('_getPrevDate') )
{ 
 function _getPrevDate( $Date=NULL, $n) 
 {
	$UI =& get_instance();
	if( $UI )
	{
		return $UI -> EUI_Tools -> _PrevDate( $Date, $n );
	}	
 }
 
}	

/* 
 * @ def : _getIP of UI // get range in date next 
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
if( ! function_exists('_getSizeDayMonth') )
{ 
	function _getSizeDayMonth( $Month = NULL, $Year = NULL ) 
 {
	$UI =& get_instance();
	if( $UI )
	{
		return $UI->EUI_Tools->_getSizeDayMonth( $Month, $Year );
	}	
 }
 
}




/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
 if(!function_exists('_getDateDiff') )
 {
	function _getDateDiff( $d1, $d2 )
	{
		$UI =& get_instance();
		if( $UI )
		{
			return $UI -> EUI_Tools -> _DateDiff( $d1, $d2 );
		}	
	}
}

/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
if(!function_exists('_getSortDate') )
{
	function _getSortDate( $bulan = 0 )
	{
		$_bulan = null;
		
		$UI =& get_instance();
		if( $UI )
		{
			$_list = $UI -> EUI_Tools ->_getBulan();
			$_bulan = $_list[(INT)$bulan];
		}
		
		return $_bulan;
	}
}	


/*
 * @ pack : public instance 
 */
 
if(!function_exists('getEventDate') )
{ 
  function getEventDate( $date = null ) 
  {
	$dates = NULL;
	
	if(!is_null($date) AND strlen($date) > 1 )
	{
		if( preg_match("/".  preg_quote($date, "/")  ."/", $date)) {
			$date = preg_replace('/\//','-',$date);	
		}
		$dates = date('Y-m-d', strtotime($date));
	}
	
	return $dates;
	
 }
} 

/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
if(!function_exists('_setPassword') )
{
	function _setPassword( $pwd = '' )
	{
		$_pwd= null;
		
		$UI =& get_instance();
		if( $UI )
		{
			$_pwd = $UI->EUI_Tools->_setPassword($pwd);
			
		}
		
		return $_pwd;
	}
}	

/* 
 * @ def : _getIP of UI
 * ---------------------------------------
 * @ param : EUI_Tools_helper 
 */
 
if(!function_exists('_getDateTime') )
{
  function _getDateTime( $date = null )
  {
	
	if(!is_null($date) and strlen($date) >1 )
	{
		return date('d-m-Y H:i:s', strtotime($date) );
	} else {
		return "-";
	}	
  }
}

/** @ pack : print empty string **/

if(!function_exists('__print') ) 
{ 
  function __print( $argv = null )  
{
	if( is_null($argv) ) {
		return "-";
	}
	else if( strlen($argv)==0 ) {
		return "-";
	}	
	else {
		return $argv;
	}
  } 
}




 
// ----------------------------------------------------------------------------------------------------------------

/*
 * @pack		 set data capital
 * @param		 array 
 * @param		 string
 * @return 		 string
 */
 
if(!function_exists('_setCapital') ) 
{ 
  function _setCapital( $arr_capital  = null )  
 {
	if( is_null($arr_capital) ){
		return __print($arr_capital);
	}	
	
	if( !is_array($arr_capital) 
		AND strlen($arr_capital) == 0 )
	{
		return __print($arr_capital);
	}
	 
	if( !is_array($arr_capital) )
	{
		return strtoupper($arr_capital);
	} 
	else
	{
		if( function_exists('array_map') ){
			return array_map('strtoupper', $arr_capital);
		} else{
			return $arr_capital;
		}
	}
	
  } 
}


// ----------------------------------------------------------------------------------------------------------------
/*
 * @ please print with its be convert to language 
 */
 
if( ! function_exists('_label') ) 
{ 
  function _label( $print = null )  
 {
	$arr_print =(string)$print;
	echo ( $arr_print ? $arr_print : "-");
  } 
}



// ----------------------------------------------------------------------------------------------------------------
/*
 * @ please print with its be convert to language 
 */
 
if( ! function_exists('_setBoldColor') ) 
{ 
  function _setBoldColor( $bold = null, $id= null)  
 {
	$arr_bold = null;
	$arr_id = (is_null($id) ? date('YmdHis') : $id );
	
	if( !is_null($bold) ) 
	{
		$arr_bold = "<span id=\"{$arr_id}\" class=\"left form-state-bold\">{$bold}</span>";	
	}
	return $arr_bold;
	
  } 
}



// ----------------------------------------------------------------------------------------------------------------
/*
 * @ please print with its be convert to language 
 */
 
if( ! function_exists('_setWordWrap') ) 
{ 
  function _setWordWrap( $text = null, $id= null)  
 {
	if( is_null($text) )  {
		return NULL;	
	}
	
	$val_length = strlen($text);
	$val_title = "";
	if( $val_length > 255 ){
		$text = substr($text,0, 255);
		$val_title = "clik row for detail";
	} 
	
	
	
	$text = _setWordCut($text);
	return sprintf("<div class=\"ui-widget-label-textmessage\" title=\"%s\">%s</div>", $val_title, $text);
	
  }
  
}
 
 // ----------------------------------------------------------------------------------------------------------------
/*
 * @ please print with its be convert to language 
 */
 
if( ! function_exists('_setWordCut') ) 
{ 
  function _setWordCut( $text= null, $len = 30 )  
 {
	return wordwrap($text,$len,"<br>");
	
  }
  
}





// ----------------------------------------------------------------------------------------------------------------
/*
 * @ please print with its be convert to language 
 */
 
if( ! function_exists('_image') ) 
{ 
  function _image( $image = NULL, $setting = NULL )  
 {
	$img_cetak_photo = null;
	if( is_null($image) OR  $image==FALSE ){
		return "3cm x 4cm";
	}
	
	 if( is_null($setting) )
	{
		$setting = array("type" => "data:image/jpeg;base64", "height" => "140", "width" => "110");
	 }
	if( !is_null($image) ){ 
		$img_cetak_photo = "<img src=\"{$setting[type]},". base64_encode($image) ."\" width=\"{$setting[width]}\" height=\"{$setting[height]}\"/>";
	}
	return $img_cetak_photo;
 }
 
}

// ----------------------------------------------------------------------------------------------------------------
/*
 * @ please print with its be convert to language 
 */
 
if( !function_exists('get_view') )
 {
   function get_view( $arr_view = array() )
   {
     $UI =& get_instance(); 
	  if(!is_array($arr_view) )
	 {
		$arr_view = array($arr_view);
	 }
	 $UI->load->view( implode("/", $arr_view) );
   }
}


// ----------------------------------------------------------------------------------------------------------------
/*
 * @ please print with its be convert to language 
 *
 * @ notes		get key array attrribute 
 */
 
if( !function_exists('_getKey') )
 {
   function _getKey( $arr = null )
  {
    if( is_array( $arr ) AND count($arr)!=0 ) {
		return array_keys($arr);
	} else {
		return null;
	}	
  }
  
}


// ----------------------------------------------------------------------------------------------------------------
/*
 * @ please print with its be convert to language 
 *
 * @ notes		get key array attrribute 
 */
 
if( !function_exists('first') )
 {
   function first( $arr = null )
  {
    if( is_array( $arr ) AND count($arr)!=0 ) 
	{
		return reset($arr);
	} else {
		return null;
	}	
  }
  
}

// --------------------------------------------------------------------------------

if (!function_exists('_setCallerMRN') )
{
	function _setCallerMRN( $MRN = '' )
	{
		$arr_mrn = array();
		if (strlen($MRN)> 3 ) 
		{
			$ln_sisa = ( DEFAULT_MRN_PREFIX_LENGTH - strlen($MRN) );
			for( $i=0; $i<$ln_sisa; $i++ ) {
				$arr_mrn[$i] = '0';	
			} 
			$Zero = join("",$arr_mrn);
			$MRN = join("", array($Zero,$MRN));
		} return _setCapital($MRN);
	} 
}


/* @brief encrypts
 * @param [type] $CustomerId [description]
 * @retval [type] [description]
 */
 function encrypts($str = "", &$ret = "") 
{
	$input = new stdclass();
	$input->str = (string)$str;
	$input->spl = str_split($input->str);   
	$input->buf = array();
	if (sizeof($input->spl)) foreach($input->spl as $ca){
		$input->buf[] = ord($ca);
	} if (is_array($input->buf) &&(sizeof($input->buf))){
		$ret = implode("\x", $input->buf);
	} return (string)$ret;    
}
	    
/* @brief StringToBinary
 * @param [type] $CustomerId [description]
 * @retval [type] [description]
 */
 function decrypts($str = "", &$ret= "")  
{
	$input = new stdclass();
	$input->str = (string)$str; 
	$input->ptr = array();
	$input->buf = array();
	$input->ptr = @explode("\x", $input->str);
	if (sizeof($input->ptr)) foreach ($input->ptr as $ca) {
		$input->buf[] = chr($ca); 
	} if (sizeof($input->buf) > 0){
		$ret = implode("", $input->buf);    
	} return (string)$ret;
}	

/* @brief xor_boolean 
 * @details create session custom
 * @param string "session"
 * @retval mixed return 
 */
 if (!function_exists("xor_boolean"))
{
	 function xor_boolean($objects= false, $key= null, $defaults= false)
	{
		if (is_array($objects))
		{
			if (array_key_exists($key, $objects)){
				if ($objects[$key]){
					return true;
				} else return false;
			} else return $defaults;
			
		} else if (is_object($objects)){
			if (property_exists($objects, $key)){
				if ($objects->$key){
					return true;
				} else return false;
			} else return $defaults;
		} 
		return $defaults;
	}
}

/* @brief xor_default 
 * @details create session custom
 * @param string "session"
 * @retval mixed return 
 */
 if (!function_exists("xor_default"))
{
	 function xor_default($objects= false, $key= null, $defaults= false)
	{
		if (is_array($objects)){
			if (array_key_exists($key, $objects)){
				return $objects[$key];
			} else return $defaults;
		} else if (is_object($objects)){
			if (property_exists($objects, $key)){
				return $objects->$key;
			} else return $defaults;
		}  
		return $defaults;
	}
}


/// ==============================================================>