<?php 
// -------------------------------------------------------------
/* 
 * Method 		_select_num_page
 *
 * @pack 		wellcome on eui first page 
 * @param		testing all 
 */
 
 
 class EUI_Encryption 
{
	
 private static $Instance = null;

// -------------------------------------------------------------
/* 
 * Method 		_select_num_page
 *
 * @pack 		wellcome on eui first page 
 * @param		testing all 
 */
 
 public static $salt = 'ZfTfbip&Gs0Z4yz34jFrG)Ha0gahptzLN7ROi%gy';
 

// -------------------------------------------------------------
/* 
 * Method 		_select_num_page
 *
 * @pack 		wellcome on eui first page 
 * @param		testing all 
 */
 
 public static function & Instance()
{
	if( is_null( self::$Instance ) ){
		self::$Instance = new self();
	}
	return self::$Instance;
}

// -------------------------------------------------------------
/* 
 * Method 		_hex2bin this will replace if not exist function 
 *
 * @pack 		wellcome on eui first page 
 * @param		testing all 
 */
 
 public function _hex2bin( $hexstr )
{
	$n = strlen( $hexstr );
	$sbin="";  
	$i=0;
	
	while($i<$n)
	{      
		$a = substr( $hexstr,$i,2);          
        $c = @pack("H*",$a);
		
        if ($i==0){
				$sbin=$c;}
        else { 
			$sbin.=$c; }
        $i+=2;
     }
        return $sbin;
 } 
	
// -------------------------------------------------------------
/* 
 * Method 		decrypted data 
 *
 * @pack 		wellcome on eui first page 
 * @param		testing all 
 */
 
  public function _decrypt( $encrypted = null )
 {
	$_base64_decode = "";
	if( !is_null( $encrypted ) )
 {
	$_base64_decode = base64_decode($encrypted);
  } 
  
  if(function_exists('hex2bin') ){
	return hex2bin( $_base64_decode );  
  } else {
	return self::_hex2bin( $_base64_decode  );  
  }
  
 }

// -------------------------------------------------------------
/* 
 * Method 		_select_num_page
 *
 * @pack 		wellcome on eui first page 
 * @param		testing all 
 */
 
  public function _encrypt( $string = null )
 {
	$_encrypted = "";
	if( !is_null( $string ) )
 {
	$_encrypted = bin2hex( $string );
  } 
	
  // ------------- encode to :: base64_encode -------------------------
   return base64_encode($_encrypted);
 }
	
} 
// =================================== END CLASS =======================================

if(!function_exists('Security') )
{
	function Security() 
  {
	return EUI_Encryption::Instance();
  }
}