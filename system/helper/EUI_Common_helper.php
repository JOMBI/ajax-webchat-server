<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// ------------------------------------------------------------------------

/**
 * Create URL Title
 *
 * Takes a "title" string as input and creates a
 * human-friendly URL string with either a dash
 * or an underscore as the word separator.
 *
 * @access	public
 * @param	string	the string
 * @param	string	the separator: dash, or underscore
 * @return	string
 */
 
if( ! function_exists('base_class_model') ) 
{
  function base_class_model( $model =null )
 {
	$prefix = sprintf('%s_', 'M'); 
	$name = null; 
	
	// check not null object: 
	if( !is_null( $model ) and is_object($model) ) {
		$name = sprintf('%s%s', $prefix, get_class($model));
	}
	else{
		show_error("No Model redirect");
	}	
	// return back : 
	return $name;
	//return ( !is_null($name) ?  $name : null );
  }
}


// ------------------------------------------------------------------------

/**
 * Header Redirect
 *
 * Header redirect in two flavors
 * For very fine grained control over headers, you could use the Output
 * Library's set_header() function.
 *
 * @access	public
 * @param	string	the URL
 * @param	string	the method: location or redirect
 * @return	string
 */
 
if ( ! function_exists('get_class_instance')) 
{
	function get_class_instance( $name=null, $method = 'Instance' ) {
		
		// check class : 
		if( is_object($name)){
			$name = get_class($name);
		}
		
		$singleton = null;
		// jika tidak null
		if( !is_null($method) ) {
			$singleton = call_user_func( array( $name, $method ));
			return $singleton;
		}
		else{
			// then will back data : 
			if( !is_null($name) and class_exists($name ) ) {
			// then will fork : 
				foreach( array( 'Instance', 'get_instance' ) as $key => $value ){
					if( is_class_method( $name, $value ) ){
						$singleton = call_user_func( array( $name, $value ));
						break;
					}
				}
			}
		}
		
		// then callback data 
		return $singleton;
	}
}

// ------------------------------------------------------------------------

/**
 * Header Redirect
 *
 * Header redirect in two flavors
 * For very fine grained control over headers, you could use the Output
 * Library's set_header() function.
 *
 * @access	public
 * @param	string	the URL
 * @param	string	the method: location or redirect
 * @return	string
 */
 
if ( ! function_exists('is_class_method')) 
{
	function is_class_method($class=null, $method = null ) {	
		if( method_exists( $class, $method) ){
			return true;
		} else {
			return FALSE;
		}
		
	}
}


// ------------------------------------------------------------------------

/**
 * Header Redirect
 *
 * Header redirect in two flavors
 * For very fine grained control over headers, you could use the Output
 * Library's set_header() function.
 *
 * @access	public
 * @param	string	the URL
 * @param	string	the method: location or redirect
 * @return	string
 */
 
if( !function_exists('base_temp') )
{
   function base_temp()
 {
	$base_temp = str_replace("system/", "application/temp", BASEPATH);
	if(is_dir($base_temp)  ){
		return $base_temp;
	}else {
		return FALSE;
	}
 }

}



// ------------------------------------------------------------------------
/**
 * Header Redirect
 *
 * Header redirect in two flavors
 * For very fine grained control over headers, you could use the Output
 * Library's set_header() function.
 *
 * @access	public
 * @param	string	the URL
 * @param	string	the method: location or redirect
 * @return	string
 */
 
if( !function_exists( 'Instance' ) ){
	function Instance( $name = null, $method = null ){
		// check validation function :
		if( !function_exists( 'get_class_instance' ) ){
			return false;
		}
		// then if true;
		$Instance = call_user_func('get_class_instance', $name, $method);
		return $Instance;
	}
}
// ------------------------------------------------------------------------

/**
 * Header Redirect
 *
 * Header redirect in two flavors
 * For very fine grained control over headers, you could use the Output
 * Library's set_header() function.
 *
 * @access	public
 * @param	string	the URL
 * @param	string	the method: location or redirect
 * @return	string
 */
if( !function_exists( 'Singgleton' ) ){
	function Singgleton( $name = null, $method = null ){
		// check validation function :
		if( !function_exists( 'get_class_instance' ) ){
			return false;
		}
		// then if true;
		return call_user_func_array('get_class_instance', array($name, $method ));
	}
}

// ------------------------------------------------------------------------

/**
 * Header Redirect
 *
 * Header redirect in two flavors
 * For very fine grained control over headers, you could use the Output
 * Library's set_header() function.
 *
 * @access	public
 * @param	string	the URL
 * @param	string	the method: location or redirect
 * @return	string
 */
if( !function_exists( 'Object' ) ){
	function Object( $data = null ){
		return new EUI_Object( $data );
	}
}



/**
 * Parse out the attributes 
 * Some of the functions use this 
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */ 
 
if (!function_exists("getParam"))
{ 
	 function getParam($value= false)
	{
		$output_add_rewrite_var = "";
		if (array_key_exists($value, $_REQUEST)){
			$output_add_rewrite_var = $_REQUEST[$value];
		} return (string)$output_add_rewrite_var;  
	}
} 

/**
 * Parse out the attributes 
 * Some of the functions use this 
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */  
if (!function_exists("getMixed"))
{ 
	 function getMixed($mixed = false)
	{
		$output_add_rewrite_var = "";
		if (is_array($mixed)) foreach ($mixed as $ca => $value){
			$output_add_rewrite_var .= getParam($value); 
		} return (string)$output_add_rewrite_var;
	}
} 

/**
 * Parse out the getArray 
 * Some of the functions use this 
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */  
if (!function_exists("getArray"))
{ 
	 function getArray($value= false)
	{
		if (false !== ($ret =  getParam($value))){
			return array_map("trim", explode( ",", $ret));
		} return false; 
	}
} 

// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
 
if ( ! function_exists('UR')){
	function UR() {
		return ObjectRequest();
	}
}
// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
if ( ! function_exists('CK')){
	function CK() {
		if( function_exists('Session') ){
			return call_user_func('Session');
		}
		return false;
	}
}
 
/**
 * Parse out the attributes 
 * Some of the functions use this 
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */ 
 if (!function_exists('CI'))
{
	 function CI(&$CI= false) 
	{ 
		if (function_exists( 'get_instance' )){
			$CI = call_user_func( 'get_instance' );
		} return $CI;
	}
}  

?>