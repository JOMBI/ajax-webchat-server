<?php
// -------------------------------------
/*
 * Module 			Helper 
 *
 * @param 			model action
 * @package     	reference global function for execute dropdown  	
 *					array return data .
 *				
 * @subproject  	enigmahelpdesk				
 */
 
// ============================================================================> 

if( !function_exists('EventLoger') )
{
  function EventLoger( $key = null, $value = "", $UserId = null)
 {
	$UI =& get_instance();
	if(!class_exists('M_UserActivity')){
		$UI->load->model(array('M_UserActivity'));
	}
	
	if( class_exists("M_UserActivity") ) {
		$obj_eventLoger =& get_class_instance("M_UserActivity");
		$obj_eventLoger->_set_user_event( $key, $value, $UserId);
	}
 }
} 

// tranport of dropdown ===================================> 
if( !function_exists('Dropdown') )
{
  function Dropdown()
 {
	$UI =& get_instance();
	if(!class_exists('M_Dropdown') ){
		$UI->load->model(array('M_Dropdown'));
	}
	
	if( class_exists("M_Dropdown") )
	{
		$dropdown =& get_class_instance("M_Dropdown");
		return $dropdown;
	}
 }
} 

// tranport of dropdown ===================================> 
if( !function_exists('UserMenuRole') )
{
  function UserMenuRole()
 {
	$UI =& get_instance();
	if(!class_exists('M_UserRole') ){
		$UI->load->model(array('M_UserRole'));
	}
	
	if( class_exists("M_UserRole") )
	{
		$dropdown =& get_class_instance("M_UserRole");
		return $dropdown;
	}
 }
} 

// ============================================================================> 
if( !function_exists('UserRole') )
{
  function UserRole(){
	return Dropdown()->_UserRole();
 }
} 
// ============================================================================> 
if( !function_exists('UserGroup') )
{
  function UserGroup(){
	return Dropdown()->_UserGroup();
 }
} 

// ============================================================================> 
if( !function_exists('LabelUnderGroup') )
{
  function LabelUnderGroup( $GroupId  = null ){
	return Dropdown()->_LabelUnderGroup( $GroupId );
 }
}

// ============================================================================> 
if( !function_exists('LevelUnderGroup') )
{
  function LevelUnderGroup( $GroupId  = null ){
	return Dropdown()->_LevelUnderGroup( $GroupId );
 }
} 

// ============================================================================> 
if( !function_exists('UserGroupId') )
{
  function UserGroupId(){
	return Dropdown()->_UserGroupId();
 }
} 
// ============================================================================> 
if( !function_exists('UserActiveEmploye') )
{
  function UserActiveEmploye( $val = null ){
	$resultArray = Dropdown()->_UserActiveEmploye();
	if( is_null( $val ) ){
		return (array)$resultArray;
	}
	return strtoupper($resultArray[$val]);
	
 }
} 

// ============================================================================> 
if( !function_exists('UserActiveEmployeid') )
{
  function UserActiveEmployeid( $val = null ){
	$resultArray = Dropdown()->_UserActiveEmployeid();
	if( is_null( $val ) ){
		return (array)$resultArray;
	}
	return strtoupper($resultArray[$val]);
	
 }
} 

// ============================================================================> 
if( !function_exists('UserNotActiveEmploye') )
{
  function UserNotActiveEmploye( $val = null ) {
	$resultArray = Dropdown()->_UserNotActiveEmploye();
	return $resultArray;
 }
} 
// ============================================================================> 
if( !function_exists('UserOnlineName') )
{
  function UserOnlineName() {
	return Dropdown()->_UserOnlineName();
 }
} 
// ============================================================================> 
if( !function_exists('AgentSkill') )
{
  function AgentSkill() {
	return Dropdown()->_AgentSkill();
 }
} 
// ============================================================================> 
if( !function_exists('AgentGroup') )
{
  function AgentGroup( $val = null ) {
	$resultArray = Dropdown()->_AgentGroup();
	if(is_null($val)){
		return $resultArray;
	}
	
	if(!is_null($val) 
	&&(isset($resultArray[$val]))){
		return $resultArray[$val];
	}
	return null;
 }
}

/**
 * [dropdown helper]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 
if( !function_exists('AllPbxId') ) {
  function AllPbxId( $val = null ) {
	$resultArray = Dropdown()->_AllPbxId();
	if(is_null($val)){
		return $resultArray;
	}
	if(!is_null($val) &&(isset($resultArray[$val]))){
		return $resultArray[$val];
	}
	return null;
 }
}
/**
 * [dropdown helper]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 
if( !function_exists('AllAgentId') ) {
  function AllAgentId( $val = null ) {
	$resultArray = Dropdown()->_AllAgentId();
	if(is_null($val)){
		return $resultArray;
	}
	if(!is_null($val) &&(isset($resultArray[$val]))){
		return @strtoupper($resultArray[$val]);
	}
	return null;
 }
}



// ============================================================================> 
if( !function_exists('CallCenterGroup') )
{
  function CallCenterGroup() {
	return Dropdown()->_CallCenterGroup();
 }
}

// ============================================================================> 
if( !function_exists('UserMenuGroup') )
{
  function UserMenuGroup() {
	return Dropdown()->_UserMenuGroup();
 }
} 
// ============================================================================> 
if( !function_exists('UserOrder') )
{
  function UserOrder($n = 50 ) {
	return Dropdown()->_UserOrder( $n );
 }
} 

/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if( !function_exists('Flags') )
{
  function Flags( $val = null ) {
	$resultArray = Dropdown()->_Flags();
	if( is_null( $val ) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val]) ? 
		$resultArray[$val] :  '');
 }
} 
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if( !function_exists('Toolbar') )
{
  function Toolbar()
 {
	return Dropdown()->_Toolbar();
 }
} 
// ============================================================================> 
if( !function_exists('Status') )
{
  function Status()
 {
	return Dropdown()->_Status();
 }
} 

// ============================================================================>
// ============================================================================>
// ============================================================================> 

if( !function_exists('Impact') )
{
  function Impact( $val = null ) {
	$resultArray = Dropdown()->_Impact();
	if( is_null( $val ) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val]) ? 
		$resultArray[$val] :  '');
		 
 }
} 
//=============================================================================>
if(!function_exists('AgentImpact') )
{
	 function AgentImpact()
	{
		$Impact=& Impact();
		
		$arr_listbox = array();
		if(is_array($arr_listbox))
			foreach( $Impact as $key => $val)
		{
			if( _get_session('CodeGroup') == 'DPT_CALLCENTER' )
			{
				if(@in_array($key, array(DEFAULT_TICKET_IMPACT_AGENT)) ) 
				{
					$arr_listbox[$key] = $val;
				}
			} else {
				$arr_listbox[$key] = $val;
			}
		}
		return 	(array)$arr_listbox;
	}	
}

// ============================================================================>
if( !function_exists('Source') )
{
  function Source( $val = null )
 {
	
	$resultArray = Dropdown()->_Source();
	//$resultArray['109'] = "Call Outgoing"; 
	if( is_null($val) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val]) ?
			$resultArray[$val]  : '');
 }
} 
// ============================================================================>
if( !function_exists('Tracker') )
{
  function Tracker()
 {
	return Dropdown()->_Tracker();
 }
} 

// ============================================================================>
if( !function_exists('AllTracker') )
{
  function AllTracker( $val = null )
 {
	//return Dropdown()->_AllTracker();
	$resultArray = Dropdown()->_AllTracker();
	if( is_null($val) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val])
		 ? $resultArray[$val]  : '' );
 }
} 
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if( !function_exists('AllTicketCategory') ) {
  function AllTicketCategory( $val = null ) {
	$resultArray = Dropdown()->_AllTicketCategory();
	if( is_null($val) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val])
		 ? $resultArray[$val]  : '' );
	
 }
} 



// ============================================================================>
if( !function_exists('CallTrack') )
{
  function CallTrack()
 {
	return Dropdown()->_CallTrack();
 }
} 

// ============================================================================>
if( !function_exists('Channel') )
{
  function Channel()
 {
	return Dropdown()->_Channel();
 }
} 

// ============================================================================>
if( !function_exists('AllCallTrack') )
{
  function AllCallTrack()
 {
	return Dropdown()->_AllCallTrack();
 }
} 


// ============================================================================>
if( !function_exists('AllTrackCall') )
{
  function AllTrackCall( $val = null ) {
	$resultArray = Dropdown()->_AllCallShowTrack();
	if( is_null( $val ) ){
		return (array)$resultArray;
	}
	return ( isset($resultArray[$val] ) ?
			$resultArray[$val] : '' );
 }
} 


// ============================================================================>
if( !function_exists('AllSource') )
{
  function AllSource( $val = null ) {
	$resultArray = Dropdown()->_AllSource();
	if( is_null( $val ) ){
		return (array)$resultArray;
	}
	return ( isset($resultArray[$val] ) ?
			$resultArray[$val] : '' );
 }
} 
// ============================================================================>
if( !function_exists('AllPriority') )
{
  function AllPriority( $val = null ) {
	$resultArray = Dropdown()->_AllPriority();
	if( is_null( $val ) ){
		return (array)$resultArray;
	}
	return ( isset($resultArray[$val] ) ?
			$resultArray[$val] : '' );
 }
} 


// ============================================================================>
if( !function_exists('Priority') )
{
  function Priority()
 {
	return Dropdown()->_Priority();
 }
} 
// ============================================================================>
if( !function_exists('RootCause') )
{
  function RootCause( $val = null )
 {
	$resultArray = Dropdown()->_RootCause();
	if( is_null( $val ) ){
		return (array)$resultArray;
	}
	// then : 
	return ( isset($resultArray[$val] ) ?
			$resultArray[$val] : '' );  
 }
} 


// ============================================================================>
 if( !function_exists('CallDirection') ) {
	 function CallDirection(  $val = null )  {
		 
		 $resultArray = array( 1 => 'Inbound', 2 => 'Outbound');
		 if( is_null( $val ) ){
			 return (array)$resultArray;
		 }
		 // return of callback data : 
		 return (isset($resultArray[$val]) 
			? call_user_func('strtoupper', $resultArray[$val]) : '');
	}
}

// ============================================================================>
if( !function_exists('CallerType') )
{
  function CallerType( $val = null)
 {
	$resultArray = Dropdown()->_CallerType();
	if( !is_null( $val ) ){
		return (string)$resultArray[$val];
	}
	return (array)$resultArray;
 }
} 

// ============================================================================>

if( !function_exists('CallerAllType') )
{
  function CallerAllType( $val = null)
 {
	$resultArray = Dropdown()->_CallerAllType();
	if( !is_null( $val ) ){
		return (string)$resultArray[$val];
	}
	return (array)$resultArray;
 }
} 
// ============================================================================>
if( !function_exists('Gender') )
{
  function Gender()
 {
	return Dropdown()->_Gender();
 }
} 
// ============================================================================>
if( !function_exists('Salutation') )
{
  function Salutation()
 {
	return Dropdown()->_Salutation();
 }
} 

// ============================================================================>
if( !function_exists('SMSStatus') )
{
  function SMSStatus()
 {
	return Dropdown()->_SMSStatus();
 }
} 

// ============================================================================>
if( !function_exists('Reply') )
{
  function Reply()
 {
	return Dropdown()->_ReplyStatus();
 }
} 

// ============================================================================>
if( !function_exists('Read') )
{
  function Read()
 {
	return Dropdown()->_ReadStatus();
 }
} 



// ============================================================================>
if( !function_exists('MasterSmsTemplate') )
{
  function MasterSmsTemplate()
 {
	return Dropdown()->_MasterSmsTemplate();
 }
} 

// ============================================================================>
if( !function_exists('AllSmsTemplate') )
{
  function AllSmsTemplate()
 {
	return Dropdown()->_AllSmsTemplate();
 }
} 
// ============================================================================>
if( !function_exists('ReportMode') ) {
  function ReportMode() {
	return Dropdown()->_ReportMode();
 }
} 

// ============================================================================>
if( !function_exists('Specialist') )
{
  function Specialist()
 {
	return Dropdown()->_Specialist();
 }
} 
// ============================================================================>
if( !function_exists('Salutation') )
{
  function Salutation( $val = null )
 {
	$resultArray = Dropdown()->_Salutation();
	if( !is_null( $val ) ){
		return $resultArray[$val];
	}
	return $resultArray;
	
 }
} 




// ============================================================================>
if( !function_exists('Doctors') )
{
  function Doctors( $code  = 0 )
 {
	return Dropdown()->_Doctors($code);
 }
} 
// ============================================================================>
if( !function_exists('Paket') )
{
  function Paket( $code  = 0 )
 {
	return Dropdown()->_Paket();
 }
} 
// ============================================================================>
if( !function_exists('ClassBooking') )
{
  function ClassBooking() {
	return Dropdown()->_ClassBooking();
 }
} 

// ============================================================================>
if( !function_exists('BookingCategory') )
{
  function BookingCategory() {
	return Dropdown()->_BookingCategory();
 }
} 


/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if( !function_exists('CallReport') ) {
  function CallReport( $val = null ) {
	  $resultArray = Dropdown()->_CallReport();
	  if( is_null( $val ) ){
		  return $resultArray;
	  }
	  return ( isset($resultArray[$val])
		? $resultArray[$val] : '' );
 }
}

if( !function_exists('ContactCenterReport') ) {
  function ContactCenterReport( $val = null ) {
	  $resultArray = Dropdown()->_ContactCenterReport();
	  if( is_null( $val ) ){
		  return $resultArray;
	  }
	  return ( isset($resultArray[$val])
		? $resultArray[$val] : '' );
 }
} 

/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if( !function_exists('TicketReport') ) {
  function TicketReport( $val = null ) {
	  $resultArray = Dropdown()->_TicketReport();
	  if( is_null( $val ) ){
		  return $resultArray;
	  }
	  return ( isset($resultArray[$val])
		? $resultArray[$val] : '' );
 }
} 
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	

// ============================================================================>
if( !function_exists('ExtStatus') )
{
  function ExtStatus() {
	return Dropdown()->_ExtStatus();
 }
} 

// ============================================================================>
if( !function_exists('ExtType') )
{
  function ExtType() {
	return Dropdown()->_ExtType();
 }
} 
// ============================================================================>
if( !function_exists('PBX') )
{
  function PBX() {
	return Dropdown()->_PBX();
 }
} 
// ============================================================================>
if( !function_exists('Direction') )
{
  function Direction() {
	return Dropdown()->_Direction();
 }
} 

// ============================================================================>
if( !function_exists('SubTrackAppointment') )
{
  function SubTrackAppointment() {
	return Dropdown()->_SubTrackAppointment();
 }
} 


// ============================================================================>
if( !function_exists('MRNRegister') )
{
  function MRNRegister() {
	return Dropdown()->_MRNRegister();
 }
} 

// ============================================================================>
if( !function_exists('WikiMasterId') )
{
  function WikiMasterId() {
	return Dropdown()->_WikiMasterId();
 }
} 

// ============================================================================>
if( !function_exists('EmailOutboxStatus') )
{
  function EmailOutboxStatus() {
	return Dropdown()->_EmailOutboxStatus();
 }
} 
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if( !function_exists('TicketSession') ){
	function TicketSession( $seq = 0, $prefix = null, $length = 4 ){
		$ticketSession = null; 
		if( !$seq ){
			return 0;
		}
		// konversi : 
		$prefix = date('dmy', strtotime( $prefix ));
		// then if not null proces then will get : 
		$ticketSession = sprintf("%s%s", $prefix, sprintf("%0{$length}d", $seq ));
		if( !strlen($ticketSession) ){
			return 0;
		}
		// then will backIts;
		return (string)$ticketSession;
	}
}



//=========================================
if( !function_exists('MCUType') )  
{
  function MCUType( $b = null ) {
	return Dropdown()->_MCUType( $b );
  }
	
} 
//=========================================
if( !function_exists('SubTicketCallTrack') )  
{
  function SubTicketCallTrack( $kd_parent = null ) {
	return Dropdown()->_SubTicketCallTrack( $kd_parent );
  }
	
} 

//=========================================
if( !function_exists('BookingStatus') )  
{
  function BookingStatus() 
  {
	$UI =& get_instance();
	$UI->load->model('M_BookingRole');
	return $UI->M_BookingRole->_select_role_booking_status();
  }
	
} 

//=========================================
if( !function_exists('TicketStatus') )  
{
  function TicketStatus() 
  {
	$UI =& get_instance();
	$UI->load->model('M_RoleTicket');
	return $UI->M_RoleTicket->_select_role_ticket_status();
  }
	
} 

//=========================================
if( !function_exists('AllTicketStatus') )  
{
  function AllTicketStatus( $val = null )  { 
	$resultArray = Dropdown()->_AllTicketStatus();
	if( is_null( $val ) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val]) ?
			$resultArray[$val] : '');
  }
	
} 

/**
 * @class  [@constructor]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
if( !function_exists('TrackerUserGroup') )  
{
  function TrackerUserGroup( $MasterKode = null, $DetailKode = null ) {
	  $resultArray = array();
	  $resultFetch = call_user_func('Dropdown');
	  
	  // then will back data OK;
	  if( !is_object( $resultFetch ) ){
		  return (array)$resultArray;
	  }
	  // then if obkject : 
	  $resultArray = $resultFetch->_TrackerUserGroup( $MasterKode, $DetailKode );
	  return (array)$resultArray;
  }
	
} 

/**
 * @class  [@constructor]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
if( !function_exists('SmsNotification') )  
{
  function SmsNotification() {
	return Dropdown()->_SmsNotification( $kd_parent );
  }
	
} 

//=========================================
if( !function_exists('LayoutName') )  
{
  function LayoutName() {
	return Dropdown()->_LayoutName();
  }
	
} 


//=========================================
if( !function_exists('LayoutThemes') )  
{
  function LayoutThemes() {
	return Dropdown()->_LayoutThemes();
  }
	
}
//=========================================
if( !function_exists('RoleLevelSystem') )  
{
	function RoleLevelSystem( $UserId = 0 ) { 
		return Dropdown()->_RoleLevelSystem( $UserId );
	}
}	
//=========================================
if( !function_exists('AllEmailAddress') )  
{
	function AllEmailAddress( $UserId = 0 ) { 
		return Dropdown()->_AllEmailAddress();
	}
}	

//=========================================
if( !function_exists('Keluhan') )  
{
	function Keluhan( $UserId = 0 ) { 
		return Dropdown()->_Keluhan();
	}
}	

//=========================================
if( !function_exists('Project') )  
{
	function Project( $Project = null ) 
 {
	$ar_project = array();
	$ar_project = Dropdown()->_Project();
	if( is_null($Project) ) {
		return $ar_project;
	}
	
	return (string)$ar_project[$Project];
 }
 
}	

//=========================================
if( !function_exists('ProjectAll') )  
{
	function ProjectAll() 
 {
	$ar_project = Project();
	$ar_project['9999'] = "Global";
	
	return $ar_project;
 }
 
}

//=========================================
if( !function_exists('SysCallerModul') )  
{
	function SysCallerModul( $Name = 0 ) 
{ 
	$UserRole =& UserMenuRole();
	$arr_role = $UserRole->_set_select_role_modul_helpdesk();
	 if( isset($arr_role[$Name]) ) {
		return ((int)$arr_role[$Name] ? TRUE : FALSE);
	}	
	return false;
}
 
}	

// customize with session callersessionsmsid
//=========================================
if( !function_exists('CallSesionSmsId') )  
{
	function CallSesionSmsId( $Id = 0 ) 
{ 
	if( !defined('DEFAULT_CALLER_SMSTRACK') ){
		return $Id;
	}
	
	$sms_source  = DEFAULT_CALLER_SMSTRACK;
	$max_session = "0000000000000000"; // 16 digit .
	$cal_session = substr($max_session, 0, strlen($max_session) - strlen($sms_source));
	$cal_session = join("", array($sms_source,$cal_session));
	$cal_session =substr($cal_session,0,(strlen($cal_session) - strlen($Id)));
	$cal_session = join("", array($cal_session, $Id));
	return (string)$cal_session;
}

 
}
// customize with session callersessionsmsid
//=========================================
if( !function_exists('CallSesionMailId') )  
{
	function CallSesionMailId( $Id = 0 ) 
{ 
	if( !defined('DEFAULT_CALLER_MAILTRACK') ){
		return $Id;
	}
	
	$sms_source  = DEFAULT_CALLER_MAILTRACK;
	$max_session = "0000000000000000"; // 16 digit .
	$cal_session = substr($max_session, 0, strlen($max_session) - strlen($sms_source));
	$cal_session = join("", array($sms_source,$cal_session));
	$cal_session =substr($cal_session,0,(strlen($cal_session) - strlen($Id)));
	$cal_session = join("", array($cal_session, $Id));
	return (string)$cal_session;
}

 
}



// customize with session callersessionsmsid
//=========================================
if( !function_exists('MailBodyMessage') )  
{
	function MailBodyMessage( $Body = "", $var = "<br>" ) { 
		$Body = explode("\n", $Body);
		return sprintf("%s", join($var, $Body));
	}
 
}

/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if( !function_exists('CallOutboundCategory') ){
 function CallOutboundCategory( $val = null ){ 
	$resultArray = array();
	$resultArray = Dropdown()->_CallOutboundCategory();
	if( is_null( $val) ){
		return $resultArray;
	}
	return $resultArray[$val];
	
 }
}
 /*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if( !function_exists('CallOutboundSubcategory') ){ 
 function CallOutboundSubcategory( $val = null ){
	 $resultArray = array();
	$resultArray =  Dropdown()->_CallOutboundSubcategory();
	 if( is_null( $val) ){
		return $resultArray;
	}
	return $resultArray[$val]; 
 }
}
 /*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if( !function_exists('CallOutboundReasonId') ){  
 function CallOutboundReasonId( $val = null){ 
	$resultArray = Dropdown()->_CallOutboundReasonId();
	if( is_null( $val) ){
		return $resultArray;
	}
	return $resultArray[$val];
 }
}
 /*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if( !function_exists('CallOutboundLevelStatusKode') ){  
 function CallOutboundLevelStatusKode( $val = null){ 
	$resultArray = Dropdown()->_CallOutboundLevelStatusKode( $val );
	return $resultArray;
 }
} 
 /*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if( !function_exists('CallerMasterAccount') ){  
 function CallerMasterAccount( $Account = null){ 
	$resultArray = Dropdown()->_CallerMasterAccount( $Account );
	return $resultArray;
 }
} 

 /*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 function Hour(){ 
	$resultArray = array();
	for( $h =0; $h<23; $h++){
		$hour = sprintf("%02d", $h);
		if( $hour ){
			$resultArray[$hour] = $hour;
		}
	}
	// return this;
	return (array)$resultArray;
 }
 
  /*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 function Minute(){ 
	$resultArray = array();
	for( $h =0; $h<60; $h++){
		$hour = sprintf("%02d", $h);
		if( $hour ){
			$resultArray[$hour] = $hour;
		}
	}
	// return this;
	return (array)$resultArray;
 }
 
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	 
if( !function_exists('StatusFormulir') ){   
 function StatusFormulir( $val = null ){
	 $resultArray = Dropdown()->_StatusFormulir();
	if( is_null( $val) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val])
		? $resultArray[$val]: '' );
 }
}
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	 
if( !function_exists('CallBackResult') ){   
 function CallBackResult( $val = null ){
	$resultArray = Dropdown()->_CallbackResult();
	if( is_null( $val) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val])
		? $resultArray[$val]: '' );
 }
}
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
function UserPrivilege( $val = null ){ 
	$resultArray = Dropdown()->_UserPrivilege();
	if( is_null( $val) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val])
		? $resultArray[$val]: '' );
}
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function AppVendor( $val = null ){ 
	$resultArray = Dropdown()->_AppVendor();
	if( is_null( $val) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val])
		? $resultArray[$val]: '' );
} 
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function AllTemplateVendor( $val = null ){ 
	$resultArray = Dropdown()->_AllTemplateVendor();
	if( is_null( $val) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val])
		? $resultArray[$val]: '' );
} 
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function TemplateVendor( $val = null, $vendor = null ){ 
	$resultArray = Dropdown()->_TemplateVendor($vendor);
	if( is_null( $val) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val])
		? $resultArray[$val]: '' );
} 
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function UserAdmin( $val = null ){
	$resultArray = Dropdown()->_UserAdmin();
	if( is_null( $val) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val])
		? $resultArray[$val]: '' );
	
}

function UserManager( $val = null ){
	$resultArray = Dropdown()->_UserManager();
	if( is_null( $val) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val])
		? $resultArray[$val]: '' );
}

function UserSupervisor( $val = null ){
	$resultArray = Dropdown()->_UserSupervisor();
	if( is_null( $val) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val])
		? $resultArray[$val]: '' );
}

function UserLeader( $val = null ){
	$resultArray = Dropdown()->_UserLeader();
	if( is_null( $val) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val])
		? $resultArray[$val]: '' );
}

function UserAgent( $val = null ){
	$resultArray = Dropdown()->_UserAgent();
	if( is_null( $val) ){
		return $resultArray;
	}
	return ( isset($resultArray[$val])
		? $resultArray[$val]: '' );
}

// IVRMenuSession
 function IVRMenuSession($callsession = 0 ){
	$Dropdown = Dropdown(); //->_UserAgent();
	return $Dropdown->_IVRMenuSession($callsession);
	
 }
// ===================== end helper =========================================


?>