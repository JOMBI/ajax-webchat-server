<?php 
/* @brief API Integrate
 * @version 1.0.1
 * @class Booking / apointment
 * @param type] $CustomerId [description]
 * @retval type description
 */	 
 class Authorization 
{ 
	/* set all parameter default */
	public $authorization = array(); 
	var $apikid = '35328fcd1b8cf9e101fc0e398de0be08';
	var $expired = ‭720‬;
	var $headers = NULL;
	
	/* @brief header
	 * @details get header default  
	 * @param none of param description
	 * @retval mixed return
	 */
	 function __construct($headers = false)
	{
		$this->headers = $headers;
		if (function_exists('getallheaders')){
			$this->headers = getallheaders(); 
		}
	} 
	
	/* @brief header
	 * @details get header default  
	 * @param none of param description
	 * @retval mixed return
	 */
	 function header()
	{
		$header = array();
		if (is_array($this->headers)) foreach ($this->headers as $ca => $va){
			$header[strtolower($ca)] = strtolower($va);
		} return Reader($header);
	}
	
	/* @brief extractHeaders
	 * @details get header default  
	 * @param none of param description
	 * @retval mixed return
	 */ 
	 function extractHeaders()
	{
		$localStorage = array();
		if (is_array($this->headers)) 
			foreach ($this->headers as $ca => $value)
		{ 	/* allowed */
			$allowed = strtolower($ca);
			if (!strcmp($allowed, "apikey")){
				$localStorage["ApiKey"] = $value; 
			} /* authorization */ 
			if (!strcmp($allowed,"authorization")){
				$localStorage["Authorization"] = $value;
			}
		} return (array)$localStorage;
		
	} 
	
	/* @brief extractAuthorization
	 * @details get header default  
	 * @param none of param description
	 * @retval mixed return
	 */  
	 function extractAuthorization()
	{ 
		$resultArray = $this->extractHeaders(); 
		if (!isset($resultArray['Authorization']) )
		{
			return false;
		} 
		
		// then if exist will to get to person;
		$authorization = $resultArray['Authorization'];
		if (!stristr($authorization, 'Basic') ) 
		{
			return false;
		}
		
		// jika process grep data 
		$authorization = str_replace( 'Basic ', '', $authorization );
		if (!$authorization ) {
			return false;
		}
		// get on base 64 decode.
		$authorization = base64_decode( $authorization );
		if (!stristr($authorization, ':') ){
			return false;
		}
		
		// jika semua benar 
		list( $username, $password ) = explode( ':', $authorization);
		$this->authorization['username'] = $username;
		$this->authorization['password'] = $password;
		// return back data 
		return $this->authorization;
		
	}
	
	/* @brief extractApiToken
	 * @details get header default  
	 * @param none of param description
	 * @retval mixed return
	 */  
	 function extractApiToken()
	{ 
		$localStorage = $this->extractHeaders();
		if (isset($localStorage['ApiKey']))
		{
			return $localStorage['ApiKey'];
			
		} return false;
	}
	
	/* @brief getUsername
	 * @details get header default  
	 * @param none of param description
	 * @retval mixed return
	 */ 
	 function getUsername()
	{
		$localStorage = $this->extractAuthorization();
		if (isset($localStorage['username']))
		{
			return $localStorage['username'];
			
		} return false;
	}
	
	/* @brief getPassword
	 * @details get header default  
	 * @param none of param description
	 * @retval mixed return
	 */ 
	 function getPassword()
	{
		$localStorage = $this->extractAuthorization();
		if (isset($localStorage['password']))
		{
			return $localStorage['password'];
			
		} return false;
	}
	
	/* @brief getUserToken
	 * @details get header default  
	 * @param none of param description
	 * @retval mixed return
	 */
	 function getUserToken()
	{
		return $this->extractApiToken();
	}
	
	/* @brief getValidation
	 * @details get header default  
	 * @param none of param description
	 * @retval mixed return
	 */ 
	 function getValidation()
	{ 
		$chat = new stdClass();
		$chat->username  = $this->getUsername();
		$chat->password  = $this->getPassword();
		$chat->usertoken = $this->getUserToken();
		return $chat;
	}	
	
	/* @brief register
	 * @param string  $username  username for login base on header 
	 * @param string  $password  password for login base on header 
	 * @param integer $interval  maximum password Interval akssess 
	 * @retval mixed return value
	 */ 
	 function register($username = "", $password = "", $interval = 0, &$authmodels = false, &$authorization_storage= false )
	{
		/* check for get register */
		$authorization_response = false;
		while ((strlen($username) && strlen($password))){
			/* get default interval base if failed process */
			$interval = (int)$interval;
			if (!$interval){
				$interval = (int)$this->expired;
			} /* then will started checked */ 
			$authorization_kid = $this->apikid;
			$authorization_now = strtotime('now');
			$authorization_interval = $interval;
			$authorization_storage = false;
			$authorization_session = false;
			$authorization_registered = false; 
			/* if success check on database */
			if (is_object($authmodels) &&($authmodels->client($username, $password, $authorization_session, $authorization_storage))){
				$authorization_registered = true;
			} 
			/* No in database */
			$authorization_response = array();
			if (!$authorization_registered){
				$authorization_response["Auth"] = sprintf("Basic %s", base64_encode(sprintf('%s:%s', $username, $password)));
				$authorization_response["Token"] = strtoupper(hash_hmac("sha256", implode(';', array($username, $authorization_now)), $authorization_kid));
				$authorization_response["Expired"] = date('Y-m-d H:i:s', strtotime(sprintf('+%d day', $authorization_interval) ));
				break;
			} /*  username client still exist will auto genrate day */
			if (!isset($authorization_storage->chat_client_expired)){
				break;
			} /* convert to intger process & look is ok not expired session*/
			$authorization_storage->chat_client_updated = false;
			$authorization_storage->chat_client_expired_time = strtotime($authorization_storage->chat_client_expired);
			if ($authorization_storage->chat_client_expired_time >= $authorization_now){
				$authorization_response["Auth"] = $authorization_storage->chat_client_auth;
				$authorization_response["Token"] = $authorization_storage->chat_client_token;
				$authorization_response["Expired"] = $authorization_storage->chat_client_expired; 
				break;
			} 
			/* sory session is expired will changed token */  
			$authorization_storage->chat_client_token = strtoupper(hash_hmac("sha256", implode(';', array($username, $authorization_now)), $authorization_kid));
			$authorization_storage->chat_client_expired = date('Y-m-d H:i:s', strtotime(sprintf('+%d day', (int)$authorization_interval)));
			$authorization_storage->chat_client_updated = true; 
			if ($authorization_storage->chat_client_token){	
				$authorization_response["Auth"] = $authorization_storage->chat_client_auth;
				$authorization_response["Token"] = $authorization_storage->chat_client_token;
				$authorization_response["Expired"] = $authorization_storage->chat_client_expired; 
			} break;  
		} /* authorization_response */
		return $authorization_response; 
	}
}

/* @brief Authorization
 * @details Main Page Controler for All process Attribute  
 * @header string Authorization  Token registration  
 * @header string ApiKey Basic for Login User & password 
 * @param  dynamic URI base from HTTP Server  
 * @retval mixed json response
 */
if (!function_exists('Authorization'))
{
	 function Authorization()
	{
		return new Authorization();
	}
}

/* @brief getToken
 * @details Main Page Controler for All process Attribute  
 * @header string Authorization  Token registration  
 * @header string ApiKey Basic for Login User & password 
 * @param  dynamic URI base from HTTP Server  
 * @retval mixed json response
 */
if (!function_exists('getToken'))
{
	 function getToken($value = "")
	{
		$authorization = Authorization();
		$exceptions = array (
		"username" => $authorization->getUsername(),
		"password" => $authorization->getPassword());
		if (isset($exceptions[$value])){
			return $exceptions[$value];
		} return false;
	}
}