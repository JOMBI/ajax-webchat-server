<?php
// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
if( !function_exists('base_layout'))
{
 function base_layout() 
 {
	$LYT =& get_instance();
	return $LYT ->Layout ->base_layout();
 }
}

// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
if( !function_exists('base_enigma'))
{  function base_enigma()
 {
	$LYT =& get_instance();
	$URI = $LYT ->Layout ->base_layout_enigma();
	return ( $URI ? $URI : null );
 }
}	

// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
if( !function_exists('base_web_editor')){
	
	function base_web_editor()
	{
		$LYT =& get_instance();
		return $LYT ->Layout ->base_web_editor();
	}
}
// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
if(!function_exists('base_jquery'))
{ 
 function base_jquery()
 {
	$LYT =& get_instance();
	return $LYT ->Layout ->base_layout_jquery();
 }
}	

// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
if( !function_exists('base_js_layout'))
{ 
 function base_js_layout()
 {
	$LYT =& get_instance();
	
	$spl_jsv_layout = array();
	if( ENVIRONMENT == 'DEPLOVMENT'){    
		$spl_jsv_layout = array($LYT->Layout->base_js_layout());
	} else {
		$spl_jsv_layout = array($LYT->Layout ->base_js_layout());
	}
	return (string)join("/", $spl_jsv_layout);
 }
}	

// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
if( !function_exists('base_image_layout'))
{
	function base_image_layout( $img = null)  {
		$LYT =& get_instance(); 
		if( is_object($LYT) &&(is_null($img))){
			return $LYT ->Layout ->base_image_layout();
		}
		else if(is_object($LYT) &&(!is_null($img))){
			return sprintf("%s/%s", $LYT ->Layout ->base_image_layout(), $img);
		}
		return null;
	}
}

// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
if( !function_exists('base_themes_style'))
{
 function base_themes_style( $style=null )
 {
	if( is_null($style) ){
		$style = defaultthemes();
	}
	$LYT =& get_instance(); 
	return $LYT->Layout->base_themes_style($style);
 }
}

// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
if( !function_exists('base_style'))
{
  function base_style() 
  {
	$LYT =& get_instance(); 
	return $LYT->Layout->base_style();
  }  
}
 
// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
 
if( !function_exists('base_menu_model') ) 
{
 function base_menu_model()
 {
	$_conds = null;
	$EUI =& get_instance();
	$EUI -> load -> Model('M_Menu');
	
	if( class_exists('M_Menu') )
	{
		$data= $EUI -> M_Menu -> _get_acess_menu();
		if( is_array($data) )
		{
			$_conds = $data;
		}
	}
	
	return $_conds;	
 }
}  

// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
if( !function_exists('base_layout_style'))
{
  function base_layout_style() 
  {
	$_URI = base_url() ."library/styles/". base_layout() ."/default";
	if( $_URI )
	{
		return $_URI;
	}	
  }
}

// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
if( !function_exists('base_library')) 
{ 
  function base_library() 
  {
	$LYT =& get_instance(); 
	return $LYT -> Layout ->base_library();
 }
 
}


// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
if( !function_exists('base_fonts_style')) 
{ 
  function base_fonts_style() 
  {
	$LYT =& get_instance(); 
	return $LYT->Layout->base_fonts_style();
 }
 
}

// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */

if( !function_exists('base_menu_layout') ) {
 function base_menu_layout( $m = null) 
 {
   $_compile = ''; 
  if(is_array($m) && !is_null($m))
  {
    if( isset($m['data']) && is_array($m) )
   {
	 $_compile .= '<div ';
	 
	// create class div menu 
	 if( isset($m['container']['class']) && !is_null($m['container']['class'])){
		$_compile .= ( $m['container']['class'] ? 'class="'. $m['data']['container']['class'] .'"':'');
	 }	
	
	// create class div id 	
	 if( isset($m['container']['id']) && !is_null($m['container']['id']) ){
		$_compile .= ( $m['container']['id'] ? 'id="'. $m['container']['id'] .'" ':'');
	 }
	
	// create class div extra 	
	 if( isset($m['container']['extra']) && !is_null($m['container']['extra']) ){
		$_compile .= ( $m['container']['extra'] ? $m['container']['extra'] : '' );
	 }
	
	 $_compile .= ">\n";
	
	if( isset($m['data']) && ($m['data']) )
	 {
		foreach( $m['data'] as $c => $r )
		{
			if( $c )
			{
				$_compile.= str_replace('{title}', $c, $m['parent']['ahref'] ) ."\n";
				
				$_compile.= '<ul'; // create ull
				
				if( isset($m['child']['show']) && ( $m['child']['show'] )){
					$_compile.= ( !is_null( $m['child']['class']['ul']) ? $m['child']['class']['ul'] : '' );
				}	
				
				$_compile.= ">";
				foreach( $r as $k => $d )
				{
					$_compile .= '<li';
					$_compile .= ( !is_null($m['child']['class']['li']) ? "class=\"{$m['child']['class']['li']}\"" : "" );  
					$_compile .= '>';
					$_compile .= "<a href=\"javascript:void(0);\" id=\"{$d['id']}\" class=\"{$d['style']}\"";
					$_compile .= (!is_null($m['click']['action'])? "onclick=\"javascript:{$m['click']['action']}('{$d['file_name']}','{$d['menu']}');\"" : "x")." >{$d['menu']}</a>";
					$_compile .= "</li>";	
				}
				
				$_compile .= "</ul>\n";	
			}
		}
	  }
	}
	
	$_compile .= "</div>\n";
 }
 
 return $_compile;
}

// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
 
 if( !function_exists('spl_core_helper') )
 {
	function spl_core_helper()
 { 
	$spl_core_helper = array( base_library(),'pustaka','Knowledge','library');
	return join('/', $spl_core_helper);
 }
 
 }
 
// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
 
 if( !function_exists('spl_core_helpers') )
 {
	function spl_core_helpers( $cores = null )
 { 
	
	if( ENVIRONMENT == 'DEPLOVMENT'){  
		$spl_core_helper = array( base_library(),'EUI','helper','dev');
	} else{
		$spl_core_helper = array( base_library(),'EUI','helper','min');
	}
	
	if( !is_null($cores) )
	{
		if( ENVIRONMENT == 'DEPLOVMENT'){  
			$cores = array($cores,'dev','js');
		} else {
			$cores = array($cores,'min','js');
		}
		
		if( ENVIRONMENT == 'DEPLOVMENT'){  
			$spl_core_helper = array( base_library(),'EUI','helper','dev', join('.', $cores));
		} else {
			$spl_core_helper = array( base_library(),'EUI','helper','min', join('.', $cores));
		}
	}
	
	return join('/', $spl_core_helper);
 }
 
 }
  
// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
 
 if( !function_exists('spl_core_view') )
 {
	function spl_core_view( $cores  = null )
 { 
	$spl_core_view = array( base_library(),'EUI','view');
	
	if( !is_null($cores) ){
		if( ENVIRONMENT == 'DEPLOVMENT'){  
			$cores = "{$cores}.dev.js";
		} else {
			$cores = "{$cores}.min.js";
		}
		
		if( ENVIRONMENT == 'DEPLOVMENT'){
			$spl_core_view = array( base_library(),'EUI','view','dev',$cores);
		} else {
			$spl_core_view = array( base_library(),'EUI','view','min', $cores);
		}
	}
	
	return join('/', $spl_core_view);
 }
 
 }  
 
// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
  
  if( !function_exists('base_js_view') ) {
	function base_js_view()
	{
		if( ENVIRONMENT == 'DEPLOVMENT'){
			$arr_js_eui_view = array( base_enigma(), "views", "dev");
		} else {
			$arr_js_eui_view = array( base_enigma(), "views", "min");
		}
		
		return join("/", $arr_js_eui_view);
	}
 }	
 
 // ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
  
  if( !function_exists('spl_core_login') ) {
	function spl_core_login()
	{
		if( ENVIRONMENT == 'DEPLOVMENT'){
			$spl_core_login = array(base_js_view(),"EUI.UserLogin.dev.js");
		} else {
			$spl_core_login = array(base_js_view(),"EUI.UserLogin.min.js");
		}
		
		return join("/", $spl_core_login);
	}
 }	
 
// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
 
 if( !function_exists('spl_core_plugin') )
 {
	function spl_core_plugin(){ 
		$spl_core_plugin = array(base_jquery(),"plugins");
		return join('/', $spl_core_plugin);
	}	
 }
 
// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
 
 if( !function_exists('spl_core_jquery') )
 {
	function spl_core_jquery() 
	{ 
		$jquery_spl = array("jquery", base_layout(), "spl", "js");
		if( ENVIRONMENT == 'DEPLOVMENT'){  
			$spl_jquery = array(base_jquery(), "spl","dev", join(".", $jquery_spl));
		} else {
			$spl_jquery = array(base_jquery(), "spl","min", join(".", $jquery_spl));
		}
		
		return join("/", $spl_jquery);
	}	
 }
 
// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
  if( !function_exists('spl_core_jqcore') )
 {
	function spl_core_jqcore() 
	{ 
		$spl_core_jqcore = array(base_jquery() , "core");
		return join("/", $spl_core_jqcore);
	}	
 }
 
 
// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
 
 if( !function_exists('spl_core_widget') ){
	function spl_core_widget()
  {
	if( ENVIRONMENT == 'DEPLOVMENT'){  
		$arr_core = array("EUI","1","3","15","dev","js");
	} else {
		$arr_core = array("EUI","1","3","15","min","js");
	}
	
	if( ENVIRONMENT == 'DEPLOVMENT'){
		$arr_enigma = array(base_enigma(),"cores","dev",  join(".", $arr_core));
	} else {
	  $arr_enigma = array(base_enigma(),"cores", "min", join(".", $arr_core));
	}
	
	return join("/", $arr_enigma);
	
  }
 }
 
// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
 
 if( !function_exists('spl_core_loader') ){
	function spl_core_loader()
  {
	if( ENVIRONMENT == 'DEPLOVMENT'){    
		$arr_core = array("EUI","Loader", "1","3","15","dev","js");
	} else {
		$arr_core = array("EUI","Loader", "1","3","15","min","js");
	}
	
	if( ENVIRONMENT == 'DEPLOVMENT'){
		$arr_enigma = array(base_enigma(),"cores","dev", join(".", $arr_core));
	} else {
		$arr_enigma = array(base_enigma(),"cores", "min", join(".", $arr_core));
	}
	
	return join("/", $arr_enigma);
	
  }
 } 
 
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if( !function_exists('spl_core_layout') ){
	function spl_core_layout()
  {
	$arr_core = array(base_layout(),"js");
	if( ENVIRONMENT == 'DEPLOVMENT'){
		$arr_enigma = array(base_js_layout(),join(".", $arr_core));
	} else {
		$arr_enigma = array(base_js_layout(),join(".", $arr_core));
	}
	
	return join("/", $arr_enigma); 
  }
 }
 /*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if( !function_exists( 'spl_core_other' ) ){
	
 function spl_core_other() {
	$other = array(base_layout(),"js");
	// convert data other : 
	$openURL = array();
	if( is_array( $other ) ){
		$openURL = array($other[0], 'other', $other[1]);
	}
	// then will get - out : 
	$splURL = null;
	if( defined( 'ENVIRONMENT' ) and ( ENVIRONMENT == 'DEPLOVMENT' ) ){
		$splURL = array(base_js_layout(), join( '.', $openURL));
	}
	// test data OK: 	
	else {
		$splURL = array(base_js_layout(),join( '.', $openURL));
	}
	// check validation : 
	if( is_array( $splURL ) ){
		return  implode( '/' , $splURL );
	}
	return null;
  }
  
}
 
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if( !function_exists('base_spl_cores'))
{ 
 function base_spl_cores()
 {
	$LYT =& get_instance();
	$base_spl_core = array($LYT->Layout->base_layout_jquery(),'cores', 'dev');
	if( ENVIRONMENT != 'DEPLOVMENT' ){
		$base_spl_core = array($LYT->Layout->base_layout_jquery(),'cores', 'min');
	}
	
	return (string)join('/', $base_spl_core);
 }
}

if( !function_exists('base_ext_cores'))
{ 
 function base_ext_cores()
 {
	$LYT =& get_instance();
	$base_ext_cores = array($LYT->Layout->base_layout_enigma(),'cores', 'dev');
	if( ENVIRONMENT != 'DEPLOVMENT' ){
		$base_ext_cores = array($LYT->Layout->base_layout_enigma(),'cores', 'min');
	}
	return (string)join('/', $base_ext_cores);
 }
}
	
 
 
// ------------------------------------------------------------------------

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
 
if( !function_exists('base_chat_layout') ) {
 function base_chat_layout( $c = null ) {
  $_compile = '<div';
  if( !is_null($c) && is_array($c) )
  {
	if( isset($c['container'])) {
		$_compile .= ( !is_null( $c['container']['id'] )?' id="'.$c['container']['id'].'"': null);
		$_compile .= ( !is_null( $c['container']['class'])?' class="'.$c['container']['class'].'"': null);
		$_compile .= ( !is_null( $c['container']['extra'])?' style="'.$c['container']['extra'].'"': null);
	}
	
	$_compile.= '>';
	if( isset($c['parent'])){
		$_compile .= ( !is_null($c['parent']['title'] )? $c['parent']['title'] : null);
	}
			
	$_compile.= '<ul';
	if( isset($c['ul'])) {
		$_compile .= ( !is_null( $c['ul']['id'] )?'id="'.$c['ul']['id'].'"': null);
		$_compile .= ( !is_null( $c['ul']['class'])?'class="'.$c['ul']['class'].'"': null);
		$_compile .= ( !is_null( $c['ul']['extra'])?'style="'.$c['ul']['extra'].'"': null);
	}
			
	$_compile.= '>'; $_compile.= '</ul>'; 
  }
   
   $_compile.= '</div>';
   
   return $_compile;
   
 }} 
}	


// ---------------------------------- end helper --------------------------------
?>