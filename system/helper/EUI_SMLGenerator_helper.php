<?php
/*
 * E.U.I Frame work 
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		E.U.I
 * @author		razaki team deplovment
 * @copyright	Copyright (c) 2007 - 2014, SALAM RAHMAT SEMESTA, Inc.
 * @since		Version 1.0
 */
 
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 * @truncate [
	truncate egs_outbox;
truncate egs_history;
truncate egs_destination;
truncate egs_queue;
truncate egs_copy_carbone;
truncate egs_attachment_url;

 ]
 */	
class EUI_SMLGenerator
{
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 protected $_post_to  		 = array();
 protected $_post_cc  		 = array();
 protected $_post_bcc 		 = array();
 protected $_post_attachment = array();
 protected $_post_title 	 = null;
 protected $_post_header 	 = null;
 protected $_post_body 		 = null;
 protected $_post_date 		 = null;
 protected $_post_config 	 = null;
 protected $_post_assign_id  = null;
 protected $_post_status 	 = null;
 protected $_post_path 		 = null; 
 protected $_post_userid 	 = null;
 protected $_post_project 	 = null;
 protected $_post_deleted 	 = false;
  
// -------- static properties ---------------------------------------------------------- 

 private static $Instance 	 = null;
 private static $Config 	 = null;
 
//---------------------------------------------------------------------------------------

/* properties		: Instance 
 *
 * @param 			: ${_REQUEST}
 * @author			: -
 */
 
 public static function &Instance()
{
  if( is_null(self::$Instance)) 
  {
	self::$Instance = new self();
  }
  
  return self::$Instance;
}

//---------------------------------------------------------------------------------------

/* properties		: select_config 
 *
 * @param 			: ${_REQUEST}
 * @author			: -
 */
 
 public function select_config( $Project = 0 )
{
    $UI =&get_instance();
    
	$UI->db->reset_select();
	$UI->db->select("*", FALSE);
	$UI->db->from("t_lk_configuration");
	$UI->db->where("ConfigCode", "V_MAIL_CONFIG");
	$UI->db->where("ConfigFlags", 1);
	
// --- test on project sender  --------------
	
	if( $Project ){
		$UI->db->where("ConfigProject", $Project);
	}
	
	$rs = $UI->db->get();
	if( $rs->num_rows() > 0 ) 
		foreach( $rs->result_assoc() as $row ) 
	{
		self::$Config[$row['ConfigName']] = $row['ConfigValue'];
	}
 
	return (array)self::$Config;	
}

//---------------------------------------------------------------------------------------

/* properties		: __construct 
 *
 * @param 			: ${_REQUEST}
 * @author			: -
 */
 
public function __construct()
{
 
  $this->_post_path = join('/',array(date('Y'),date('m'),date('d')));
  $this->_post_status = 1001;
  log_message('debug', "SMLGenerator Class Initialized");
}

//---------------------------------------------------------------------------------------

/* properties		: set_send_date 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
 public function set_send_date( $date = NULL )
{
	$this->_post_date = date('Y-m-d H:i:s');
	if( !is_null($date) )  {
		$this->_post_date = $date;	
	}
 }
 
 //---------------------------------------------------------------------------------------

/* properties		: set_send_date 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
 public function set_send_user( $user = NULL ) {
	$this->_post_userid = $user;
 }
 
 //---------------------------------------------------------------------------------------

/* properties		: reset_all_component 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
  public function set_send_project( $Project = 0 )
 {
	$this->_post_project = ( $Project ? $Project : 0 );
 }
 //---------------------------------------------------------------------------------------

/* properties		: set_send_date 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
 private function set_real_content( $data = NULL )
{
	if( function_exists('mysql_real_escape_string') )
	{
		return $data;
	}
	return $data;
 }

//---------------------------------------------------------------------------------------

/* properties		: set_send_date 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
  public function set_add_assign( $id = NULL ) 
 {
	$this->_post_assign_id = $id;
 }
 
 
//---------------------------------------------------------------------------------------

/* properties		: reset_all_component 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
  protected function reset_all_component()
{
 $this->_post_to = array();
 $this->_post_cc = array();
 $this->_post_bcc = array();
 $this->_post_title = NULL;
 $this->_post_header = NULL;
 $this->_post_body = NULL;
 $this->_post_assign_id = NULL;
 $this->_post_attachment = array();
 
}

//---------------------------------------------------------------------------------------

/* properties		: reset_all_component 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
 protected function _set_on_destination($UI, $OutboxId = 0 )
{
 if(is_array($this->_post_to) ) 
	foreach( $this->_post_to as $keys => $add_address )
 {
	$UI->db->set('EmailDestination',$add_address); 
	$UI->db->set('EmailReffrenceId', $OutboxId); 
	$UI->db->set('EmailCreateTs', $this->_post_date);	
	$UI->db->set('EmailDirection', 2);
	$UI->db->insert('egs_destination');
	if( $UI->db->affected_rows() > 0) 
	{
		$conds++;
	}
 }
 
 return $conds;
}
//---------------------------------------------------------------------------------------

/* properties		: reset_all_component 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
 protected function _set_on_copycarbone($UI, $OutboxId = 0 )
{
  if(is_array($this->_post_cc) ) 
	foreach( $this->_post_cc as $keys => $add_address )
  {
	$UI->db->set('EmailCC',$add_address); 
	$UI->db->set('EmailReffrenceId', $OutboxId); 
	$UI->db->set('EmailCreateTs', $this->_post_date);	
	$UI->db->set('EmailDirection', 2);
	$UI->db->insert('egs_copy_carbone');
	if( $UI->db->affected_rows() > 0) 
	{
		$conds++;
	}
 }
 
 return $conds;
 
}
//---------------------------------------------------------------------------------------

/* properties		: reset_all_component 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
 protected function _set_on_blindcarbone($UI, $OutboxId = 0 )
{
 if(is_array($this->_post_bcc) ) 
  foreach( $this->_post_bcc as $keys => $add_address )
  {
	if( $add_address!='' )
	{
		$UI->db->set('EmailBCC',$add_address); 
		$UI->db->set('EmailReffrenceId', $OutboxId); 
		$UI->db->set('EmailDirection', 2);
		$UI->db->set('EmailCreateTs', $this->_post_date);	
		$UI->db->insert('egs_blindcopy_carbone');
		if( $UI->db->affected_rows() > 0)  
		{
			$conds++;
		}
	}	
 }
 
 return $conds;
}
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 protected function _set_on_attachment($CI, $OutboxId = 0 ) 
{
	
 $total  = 0;
 if(is_array($this->_post_attachment) ) 
	foreach( $this->_post_attachment as $attachment )  
 {
	
	$attach = array();
	$attach = $this->set_write_attachment($attachment, $OutboxId);
	
	// tehn will save on this folder : 
	
	if( is_array($attach) 
	and sizeof( $attach ) ) 
	{
		$CI->db->reset_write();
		$CI->db->set('EmailAttachmentPath',$attach['path']); 
		$CI->db->set('EmailAttachmentSize',$attach['size']);
		$CI->db->set('EmailAttachmentType',$attach['mime']);
		$CI->db->set('EmailReffrenceId', $OutboxId); 
		$CI->db->set('EmailCreateTs', $this->_post_date);	
		$CI->db->set('EmailDirection', 2); 
		
		// then will get its: 
		
		if( $CI->db->insert('egs_attachment_url') 
		and ( $CI->db->affected_rows() > 0 ) 
		and( $atachId = $CI->db->insert_id() ) )  {
			$total ++;
		}
		// if have error_get_last" 
		else{
			if(function_exists('writeloger') ){
				writeloger( sprintf('write attachment email error : %s, FILE : %s, LINE : %s', mysql_error(), __FILE__, __LINE__ ));
			}
		}
	}
}
// then will test :
 return $total;
}
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 
 protected function _set_on_queue( $UI, $OutboxId = 0 )
{
  $conds = 0;
  if(($OutboxId) AND ($this->_post_date))
  {
	$UI->db->set('QueueMailId', $OutboxId );
	$UI->db->set('QueueStatusTs',$this->_post_date);
	$UI->db->set('QueueCreateTs',$this->_post_date); 
	$UI->db->set('QueueStatus', $this->_post_status);
	$UI->db->set('QueueTrying',0);
	$UI->db->insert('egs_queue');
	
	if( $UI->db->affected_rows()> 0 ) 
	{
		$conds++;
	}
  }
  
  return $conds;
 } 
 
 
//---------------------------------------------------------------------------------------

/* properties		: Submit 
 *
 * @param 			: ${_REQUEST}
 * @author			: -
 */
 
 private function _set_event_status( $OutboxId=0 ) 
 {
	$CI=& get_instance();
	
	$sql = sprintf("update t_gn_customer a
					inner join egs_outbox b on a.CustomerId=b.EmailAssignDataId
					set a.EmailStatus = b.EmailStatus
					where b.EmailOutboxId='%s'", $OutboxId);					
	$CI->db->query($sql);
}

//---------------------------------------------------------------------------------------

/* properties		: reset_all_component 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
 public function set_compile()
{
	
  $this->_post_config = $this->select_config( $this->_post_project );
  
  $conds = 0;
  $OutboxId = 0;
  
  if( !is_array($this->_post_config) ) {
	 log_message('debug', "Configuration is not found.");
	 return false;
  }
  
  // get instance configure;
  
	$UI =& get_instance();
	$UI->db->set('EmailSender', $this->_post_config['OUTBOX_SMTP_AUTH']); // on configuratio 
	$UI->db->set('EmailContent', $this->set_real_content($this->_post_body)); // html or text 
	$UI->db->set('EmailSubject', $this->set_real_content($this->_post_title)); // subject 
	$UI->db->set('EmailAssignDataId', $this->set_real_content($this->_post_assign_id)); // subject 
	$UI->db->set('EmailCreateById', $this->set_real_content($this->_post_userid)); // user create
	$UI->db->set('EmailProjectCode', $this->set_real_content($this->_post_project)); // new propeties object 
	$UI->db->set('EmailStatus', $this->_post_status); // Ready status
	$UI->db->set('EmailCreateTs', $this->_post_date); 
	
	// then insert to db 
	
	$UI->db->insert('egs_outbox');
	 if( $UI->db->affected_rows() > 0 ) 
	{
		$OutboxId = $UI->db->insert_id();
		$this->_set_event_status( $OutboxId );
		
		if( $OutboxId )
		{
		
		/** post destination **/
			$this->_set_on_destination($UI, $OutboxId);
			
		/** post copy carbone **/
			$this->_set_on_copycarbone($UI, $OutboxId);
			
		/** post blind copy carbone **/
			$this->_set_on_blindcarbone($UI, $OutboxId);
			
		/** _set_on_attachment **/
			$this->_set_on_attachment($UI, $OutboxId);
			
		/** _set_on_queue **/
			$this->_set_on_queue($UI, $OutboxId);
		}
	}
	
	// then willCallback By Return OutboxId:
	$this->reset_all_component();
	return $OutboxId;
 
}


 
 
//---------------------------------------------------------------------------------------

/* properties		: reset_all_component 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
  public function set_add_to( $_address =NULL, $_name = null )
 {
	if(!is_array($_address) ){
		$_address = array($_address => $_address);	
	}	
		
	foreach($_address as $add_address => $add_name ) {
		$this->_post_to[trim($add_address)] = $add_name;	
	}
 }
 
//---------------------------------------------------------------------------------------

/* properties		: reset_all_component 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
  public function set_add_cc( $_address =NULL, $_name = null )
 {
	if(!is_array($_address) ){
		$_address = array($_address => $_address);	
	}	
		
	foreach($_address as $add_address => $add_name ) {
		$this->_post_cc[trim($add_address)] = $add_name;	
	}
 }
//---------------------------------------------------------------------------------------

/* properties		: reset_all_component 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
  public function set_add_bcc( $_address =NULL, $_name = null )
 {
	if(!is_array($_address) ){
		$_address = array($_address => $_address);	
	}	
		
	foreach($_address as $add_address => $add_name ) {
		$this->_post_bcc[trim($add_address)] = $add_name;	
	}
 }
//---------------------------------------------------------------------------------------

/* properties		: reset_all_component 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
  public function set_add_title( $title = NULL )
 {
	if(!is_array($title) 
		AND is_null($title) ==FALSE ) 
	{
		$this->_post_title = $title;
	}
 } 
//---------------------------------------------------------------------------------------

/* properties		: reset_all_component 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
 public function set_add_header( $header = null )
{
	if(!is_array($header) 
		AND is_null($header) ==FALSE ) 
	{
		$this->_post_header = $header;
	}
 }  
 
//---------------------------------------------------------------------------------------

/* properties		: reset_all_component 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
 public function set_add_body( $set_add_body =NULL )
{
	$this->_post_body = $set_add_body;
 }  
 
 
 
//---------------------------------------------------------------------------------------

/* properties		:  will setup date from tmp file 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
 public function set_add_temp_attachment( $original, $filename ='' )
{
	$pathtemp = OUTBOX_ATTACHMENT_PATH;
	if( !is_array($original) ){
		$original = array( $original => $filename );
	}
	
	 foreach( $original as $path_tmp => $filename ) {
		$temp_eskalate = sprintf("%s/%s", $pathtemp, $filename);
		if( move_uploaded_file($path_tmp, $temp_eskalate )) {
			$this->_post_attachtmpt[$temp_eskalate] = $temp_eskalate;
		}
	}
	
// -- then push and deleted .	
	if( is_array( $this->_post_attachtmpt ) 
		and count($this->_post_attachtmpt) > 0 )
	{ 
		$this->set_add_attachment( $this->_post_attachtmpt, true );
	}
} 
 
 
//---------------------------------------------------------------------------------------

/* properties		: reset_all_component 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
 public function set_add_attachment( $_add_atachment = NULL, $flag = FALSE )
{
	$this->_post_deleted = (bool)$flag;
	if( !is_array($_add_atachment) 
		AND !is_null($_add_atachment) )
		$this->_post_attachment[trim($_add_atachment)] = $_add_atachment;
	else 
	{
		if( !is_null( $_add_atachment) )
		{
			foreach( $_add_atachment as $attach_url => $attach_name ) 
			{
				$this->_post_attachment[trim($attach_name)] = $attach_name;	
			}
		}	
	}
 }  
 
 
//---------------------------------------------------------------------------------------

/* properties		: set_write_attachment 
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
 private function set_write_attachment( $BASE_PATH = NULL , $OutboxId  = 0) 
{
	
 $resultArray = array();
 if( ( $OutboxId > 0 )  and is_array($this->_post_config)  
	 and count($this->_post_config) > 0) 
 { 

 // get file of name : 
	$BASE_PATH_OUTBOX  = $this->_post_config['OUTBOX_ATTACHMENT_PATH'];
	$BASE_PATH_ATTACHMENT = implode ( '/',  array( $BASE_PATH_OUTBOX, $this->_post_path, $OutboxId ) ); 
	$BASE_FILE_NAME = basename($BASE_PATH);
		
// check if not dir will create : 		
	if(!is_dir($BASE_PATH_ATTACHMENT)){
		@mkdir($BASE_PATH_ATTACHMENT, 0777,TRUE); 	
	}
	
// grep to convert file : 	
	$BASE_FILE_EXIST = sprintf( '%s/%s', $BASE_PATH_ATTACHMENT, $BASE_FILE_NAME);
	
	// copy file to tmp process : 
	if( @copy( $BASE_PATH, $BASE_FILE_EXIST ) ) {	
		$BASE_FILE_SIZE = @filesize($BASE_FILE_EXIST);
		$BASE_FILE_MIME = @mime_content_type ( $BASE_FILE_EXIST );
		
		// get mime Type : 
		if( !$BASE_FILE_MIME){
			$BASE_FILE_MIME= sprintf('%s', 'application/vnd.ms-excel');
		}
		// set on process DB : 
		if( file_exists($BASE_FILE_EXIST) and $BASE_FILE_SIZE > 0 )  {
			$resultArray['path'] = $BASE_FILE_EXIST;
			$resultArray['size'] = $BASE_FILE_SIZE;
			$resultArray['mime'] = $BASE_FILE_MIME;
		}	
	}
	
	// if true then deleted tmp  ---------------------
	if( ($this->_post_deleted  == TRUE) and ( file_exists($BASE_PATH) ))  {
		@unlink( $BASE_PATH );
	}
	
 }
 // return array : 
 return $resultArray; 
} 

// =========================================== END OF CLASS =============================

}

//---------------------------------------------------------------------------------------

/* properties		: call &argv :: SML_Generator
 *
 * @param 			: $argv1 <string>
 * @author			: -
 */
 
if(!function_exists('SML_Generator') )
{  function SML_Generator()  
{
	if(!class_exists('EUI_SMLGenerator')) {
		return FALSE;
	}
	
	return EUI_SMLGenerator::Instance();
	
  }
}

// -------------------------------------------EXAMPLE : usage ---------------------
/*
	$this->load->helper('EUI_SMLGenerator');
	SML_Generator()->set_send_date(date('Y-m-d H:i:s'));
	SML_Generator()->set_add_assign('10');
	SML_Generator()->set_add_title('hello world');
	SML_Generator()->set_add_body('test html');
	SML_Generator()->set_add_to('jombi_par@yahoo.com');
	SML_Generator()->set_add_bcc('jombi_par@yahoo.com');
	SML_Generator()->set_add_bcc('wisnu@yahoo.com');
	SML_Generator()->set_add_attachment('/opt/enigma/www/test1.xls');
	SML_Generator()->set_add_attachment('/opt/enigma/www/test2.xls');
	SML_Generator()->set_add_attachment('/opt/enigma/www/test3.xls');
	SML_Generator()->set_compile();
	print_r(SML_Generator())
*/
		

?>