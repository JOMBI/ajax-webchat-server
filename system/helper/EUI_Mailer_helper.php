<?php
/*
 * @packages : EUI_Mailer_helper 
 * --------------------------------------------
 
 * @ author  : < omens >
 * @librarry : - /system/libraries/EUI_MailerSmtp.php, 
			   - /system/libraries/EUI_Mailer.php 
			   - /system/libraries/EUI_MailerPop3.php	
 */ 

 
 /*
  * @ configuration setting :
  * @ param : array()
  */
  
 if(!function_exists('SendMailConfiguration') )  
 {
  function SendMailConfiguration($param = null ) 
  {
	 $EUI =& get_instance();
	 
	 if( !is_null($param) AND is_array($param) )
	 {
		if( !class_exists('EUI_PHPMailer') )  return false;
		else 
		{	
			$EUI -> EUI_PHPMailer -> IsSMTP();
			foreach( $param as $keys => $values ) 
			{
				if( strtolower($keys) == 'host') $EUI -> EUI_PHPMailer -> Host = $values;	
				if( strtolower($keys) == 'debugs') $EUI -> EUI_PHPMailer -> SMTPDebug = $values;	
				if( strtolower($keys) == 'auth') $EUI -> EUI_PHPMailer -> SMTPAuth = $values;	
				if( strtolower($keys) == 'secure') $EUI -> EUI_PHPMailer -> SMTPSecure = $values;	
				if( strtolower($keys) == 'port') $EUI -> EUI_PHPMailer -> Port = $values;
				if( strtolower($keys) == 'username') $EUI -> EUI_PHPMailer -> Username = $values;	
				if( strtolower($keys) == 'password') $EUI -> EUI_PHPMailer -> Password = $values;	
				if( strtolower($keys) == 'contact') $EUI -> EUI_PHPMailer -> ContactCenter = $values;	
				
			}	
			
			
			/** set default parameter **/
			
			$EUI -> EUI_PHPMailer -> ConfirmReadingTo = $EUI -> EUI_PHPMailer -> Username;
			$EUI -> EUI_PHPMailer -> SetFrom($EUI -> EUI_PHPMailer -> Username, $EUI -> EUI_PHPMailer -> ContactCenter);
		}
	}
	
	return $EUI -> EUI_PHPMailer;
	}
 }
?>