<?php

/*
 * @ pack   : IMAP Helper 
 *
 * @ param  : include file EUI_ImapApi libraries on context helper 
 */ 
 
 class EUI_ImapApi_helper 
{
 
 var $config_imap_api = array(); 
 var $ui_extends_api = null;
 var $ui_extends_project = '9999';
 
 // var $imap_path_inbox  = null;
 // var $imap_path_user  = null;
 // var $imap_path_password  = null;
 // var $imap_path_attachment = null;
			
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 instance of class 
 *
 *  @ param   		 
 *  @ notes      	
 */
 
 var $class_imap_api = null;
 
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 instance of class 
 *
 *  @ param   		 
 *  @ notes      	
 */
 
 private static $Instance = null;
 
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 instance of class 
 *
 *  @ param   		 
 *  @ notes      	
 */
 
 public static function &Instance()
{
  if( is_null(self::$Instance) )
 {
	self::$Instance = new self();
  }
  return self::$Instance;
}

// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 __construct
 *
 *  @ param   		 $this argc 
 *  @ notes      	 - 
 */
 
  function __construct()
 { 
	$this->ui_extends_api =& get_instance();
	$this->ui_extends_api->load->helper('EUI_IMAP_Api');
	
 }
 
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : Inialize
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
 
 public function Inialize( $key = '', $val = '' )
{
   if( !is_array($key) )	
  {
	$this->config_imap_api = array($key => $val );    
  }
  
   $this->config_imap_api = $key;
 
}

// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : interface
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
 
 public function Imap_set_row_error( $err )
{
	//var_dump( $err );
}

// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : interface
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
 
function imap_set_project( $ProjectCode  = '' ){
	$this->ui_extends_project = $ProjectCode; 
}

// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : interface
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
 
 public function Imap_get_row_id( $out )
{
	$this->Imap_get_row_id = base64_encode(base64_encode((base64_encode($out->id))));;
	return $this->Imap_get_row_id;
 } 
 
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : interface
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
  
  public function Imap_get_row_from( $out )
 {
	$this->Imap_get_row_from = $out->fromAddress;
	
	return $this->Imap_get_row_from;
 } 
 
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : interface
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
  
  public function Imap_get_row_project()
 {
	$Ci = & get_instance();
	
	$ar_imap_row_project = array();
	
	$sql = "select 
			a.ConfigValue as EmailAddress,
			a.ConfigProject as ProjectCode
			from t_lk_configuration a where a.ConfigCode='V_MAIL_CONFIG'
			and a.ConfigName='INBOX_IMAP_USER'";
	$res = $Ci->db->query($sql);
	if( $res->num_rows() > 0 ) 
		foreach( $res ->result_assoc() as $row )
	{
		$ar_imap_row_project[$row['EmailAddress']] = $row['ProjectCode']; 
	} 
	return $ar_imap_row_project;
 }
 

// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : interface
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
  
  
 public function Imap_set_row_project( $Ci, $out, $InboxId )
{
	$ar_imap_row_project = self::Imap_get_row_project();
	if( !is_array($ar_imap_row_project) 
		OR count($ar_imap_row_project) == 0 ) {
		return false;
	}
	
	if( is_array($out->to) AND count($out->to) > 0 ) 
		foreach( $out->to as $address => $name )
	{
		$ar_row_code = ( isset($ar_imap_row_project[$address] ) ? $ar_imap_row_project[$address] : 0 );
		if( $ar_row_code )
		{
			$Ci->db->reset_write();
			$Ci->db->set('EmailProjectCode', $ar_row_code);
			$Ci->db->where("EmailInboxId", $InboxId);
			$Ci->db->update("egs_inbox");
		}
	}
	return true;
} 

// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : interface
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
  
  public function Imap_get_row_subject( $out )
 {
	$this->Imap_get_row_subject = $out->subject;
	return $this->Imap_get_row_subject;
 } 
  
 // --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : interface
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
  
 public function Imap_get_row_sent_date( $out )
 {
	$this->Imap_get_row_sent_date = $out->date;
	return $this->Imap_get_row_sent_date;
 } 
 
 
 
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : interface
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
  
  
 public function Imap_get_row_header( $out )
{
	$this->Imap_get_row_header = null;
	if(($out->textPlain) AND ( strlen($out->textPlain) > 0 ) ){
		$this->Imap_get_row_header = $out->textPlain;
	}
	
	return $this->Imap_get_row_header;
 } 

 
 
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : interface
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
  
 public function Imap_get_row_body( $out )
{
	$this->Imap_get_row_body = "";
	if(($out->textHtml) AND ( strlen($out->textHtml) > 0 ) ){
		$this->Imap_get_row_body = 	$out->textHtml;
	} else{
		$this->Imap_get_row_body = $this->Imap_get_row_header($out);
	}
	return $this->Imap_get_row_body;
 } 

 // --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : interface
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
  
private function Imap_get_row_mime_type($Ci, $file_path = null ) 
{
  $mime_type = 'application/octet-stream'; // default  

 /** 
  * @ def : load config for get mime type extension look on config .. application 
  * --------------------------------------------------------------------------
   
  * @ param : mime type OK 
  */
  
 $Ci->_zlib_oc = @ini_get('zlib.output_compression');
 
 if(defined('ENVIRONMENT') AND file_exists(APPPATH.'config/'.ENVIRONMENT.'/EUI_Config.php')) 
	include APPPATH.'config/'.ENVIRONMENT.'/EUI_Config.php';
 else
	include APPPATH.'config/EUI_Config.php';
		

/** set all mime types **/

	$this->IMAP_mimetype =& $mimes;	
	if( !is_null($file_path) AND is_array($mimes)) 
	{
		$parts = pathinfo($file_path);
		if( is_array($parts))
		{
			$extension = $parts['extension']; 
			if (isset($this->IMAP_mimetype[$extension]))
			{
				$mime_type =& $this->IMAP_mimetype[$extension];
				if (is_array($mime_type))
				{
					$mime_type = current($mime_type);
				}
			}
		}	
	}
	
	return $mime_type;
}
 
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : interface
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
 
 
 function Imap_set_row_content( $out, $Ci, $ImapInboxId )
{
	$InboxId = 0;
	$Ci->db->reset_write();
	$Ci->db->set("EmailImapId", $ImapInboxId);
	$Ci->db->set("EmailSender", $this->Imap_get_row_from($out) );
	$Ci->db->set("EmailMessageId", $this->Imap_get_row_id($out) );
	$Ci->db->set("EmailSubject", $this->Imap_get_row_subject($out));
	$Ci->db->set("EmailContent", $this->Imap_get_row_body($out));
	$Ci->db->set("EmailHeader", $this->Imap_get_row_header($out));
	$Ci->db->set("EmaiReceiveDate", $this->Imap_get_row_sent_date($out));
	$Ci->db->set("EmailContentType", 'multipart/mixed');
	$Ci->db->set("EmailCreateTs", date('Y-m-d H:i:s'));
	
// --- this not available for diffrent domain Account ----
	//$Ci->db->duplicate("EmailContent", $this->Imap_get_row_body($out));
	//$Ci->db->duplicate("EmailHeader", $this->Imap_get_row_header($out));
	//$Ci->db->duplicate("EmailImapId",$InboxId);
	
	$Ci->db->insert("egs_inbox");
	if( $Ci->db->affected_rows() > 0 ) {
		$InboxId = $Ci->db->insert_id();
	}
	
	return $InboxId;
	
 }  
 
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : interface
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
 
 public function Imap_set_row_to( $Ci, $out, $InboxId)
 {
	if( is_array($out->to) AND count($out->to) > 0 ) 
		foreach( $out->to as $address => $name )
	{
		$Ci->db->reset_write();
		$Ci->db->set("EmailDestination", $address);
		$Ci->db->set("EmailReffrenceId",$InboxId);
		$Ci->db->set("EmailDirection",1);
		$Ci->db->set("EmailCreateTs", date('Y-m-d H:i:s'));
		$Ci->db->insert("egs_destination");
	}	
	
	return true;
 } 
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : interface
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
  
 public function Imap_set_row_cc( $Ci, $out, $InboxId )
 {
	if( is_array($out->cc) AND count($out->cc) > 0 ) 
		foreach( $out->cc as $address => $name )
	{
		$Ci->db->reset_write();
		$Ci->db->set("EmailCC", $address);
		$Ci->db->set("EmailReffrenceId",$InboxId);
		$Ci->db->set("EmailDirection",1);
		$Ci->db->set("EmailCreateTs",date('Y-m-d H:i:s'));
		$Ci->db->insert("egs_copy_carbone");
	}	
	
	return true;
 } 
 
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : interface
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
 
 public function Imap_set_row_bcc( $Ci, $out, $InboxId )
 {
	return true; 
 } 
 
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : interface
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
  
  public function Imap_set_row_attachment( $Ci, $out, $InboxId )
 {
	$Attachment = $out->getAttachments();
	
	if( is_array($Attachment) AND count($Attachment) > 0 ) 
		foreach( $Attachment as $k => $Attach )
	{
		$Ci->db->reset_write();
		$Ci->db->set("EmailAttachmentPath",$Attach->filePath);
		$Ci->db->set("EmailReffrenceId",$InboxId);
		$Ci->db->set("EmailAttachmentSize",filesize($Attach->filePath));
		$Ci->db->set("EmailAttachmentType",$this->Imap_get_row_mime_type($Ci, $Attach->filePath ));
		$Ci->db->set("EmailDirection",1);
		$Ci->db->set("EmailCreateTs",date('Y-m-d H:i:s'));
		$Ci->db->insert("egs_attachment_url");
	}	
	
	return true;  
 } 
 
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : Inialize
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
 
 public function Imap_open_buffer()
{

// --------- if not have class this close -----------------------------------

   $SML = new EUI_SMLGenerator();
   $this->config_imap_api = $SML->select_config(  $this->ui_extends_project );
  
   if(!class_exists('EUI_ImapApi') ) {
		return FALSE;
   }	
// --------- then called of this class --------------------------------------	
	
	$this->imap_path_inbox 		= sprintf("{%s:%s/%s}INBOX", $this->config_imap_api['INBOX_IMAP_HOST'], $this->config_imap_api['INBOX_IMAP_PORT'], $this->config_imap_api['INBOX_IMAP_SECURE']);
	$this->imap_path_user 		= $this->config_imap_api['INBOX_IMAP_USER'];
	$this->imap_path_password 	= $this->config_imap_api['INBOX_IMAP_PASSWORD'];
	$this->imap_path_attachment = $this->config_imap_api['INBOX_IMAP_ATTACHMENT'];
	$this->imap_path_encoding   = 'utf-8';
	
	if( is_null( $this->class_imap_api ) )
	{		
		$this->class_imap_api = new EUI_ImapApi( 
			$this->imap_path_inbox, 
			$this->imap_path_user,  
			$this->imap_path_password, 
			$this->imap_path_attachment,
			$this->imap_path_encoding	
		);  
	}
	
	return $this->class_imap_api;
 }

 
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : Imap_while_loop 
 *
 *  @ param   		 : main content of helper 
 *  @ notes      	 : -  	
 */
 public function Imap_get_row_failed( $header  = null )
{
	$this->ar_response = array();
	if(is_array( $header )) 
		foreach( $header as $line ) 
	{
		 if( !strncasecmp($line, "Action: ", 6) ) $this->ar_response['reason'] = trim(substr($line, 7));
		 else if( !strncasecmp($line, "To: ", 3) ) $this->ar_response['address'] = preg_replace("(\")","", trim(substr($line, 3)));
	     else if( !strncasecmp($line, "Message-ID: ", 11) ) $this->ar_response['msgid'] =  preg_replace("[<|>]","", trim(substr($line, 11)));
	}
	return $this->ar_response;
} 

 
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : Imap_while_loop 
 *
 *  @ param   		 : main content of helper 
 *  @ notes      	 : -  	
 */
 
 public function Imap_get_row_section( $Imap, $ImapInboxId )
{	
   $EmailStatus = 1007; // not receive  
   
  if( $out = $Imap->getMail( $ImapInboxId  ) )  
  {
	$EmailMessageId = $this->Imap_get_row_id($out);  
	$InboxId = $this->Imap_set_row_content( $out, $this->ui_extends_api, $ImapInboxId );
	
	// --- will update email not receive by spesific address  --- 
	// --- on setup mailer daemon mchine  --- 
	$MailerDaemonAddress  = $this->Imap_get_row_from($out);
	//print_r($MailerDaemonAddress);
	
	
	if( in_array($MailerDaemonAddress, 
		array(INBOX_MAILER_DAEMON)) ) 
	{
		$header  = explode("\n", $this->Imap_get_row_header($out));
		$content = $this->Imap_get_row_failed( $header );
		if( is_array( $content ) )
		{
			$row = Objective( $content );
			
			if( $row->find_value('msgid') )
			{
				$EmailOutboxDataId = $row -> get_value('msgid');
				
				$Ci = & get_instance();
				$Ci->db->reset_write();
				//$Ci->db->set('EmailStatus',$EmailStatus); // -- email not receive -- 
				$Ci->db->set('EmaiUpdateTs', date('Y-m-d H:i:s'));
				$Ci->db->where("EmailMsgId", $EmailOutboxDataId);
				$Ci->db->update("egs_outbox");
				
				if( $Ci->db->affected_rows() > 0 ){
					
					// -------- update if have assign ID --- 
					 $sql = sprintf("select a.EmailMsgId, a.EmailAssignDataId from egs_outbox a 
									where a.EmailMsgId='%s'",$EmailOutboxDataId);
					 $res = $Ci ->db->query($sql);
					 if( $res->num_rows() > 0 
						AND $oux = Objective( $res->result_first_assoc()) )
					 {
						 	$EmailAssignDataId = $oux->get_value('EmailAssignDataId');
						// --- update inbox failed  --- 
						
							$Ci->db->query(sprintf("
									UPDATE egs_inbox a 
									SET a.EmailAssignDataId='%s', 
										a.EmailMsgId='%s' WHERE a.EmailMessageId='%s'", 
									
									$EmailAssignDataId,
									$EmailOutboxDataId,
									$EmailMessageId
							));
								
					 }
				}
				
			}
		}
	}
		
	//var_dump($out);
	
	if( $InboxId )
	{
		$this->Imap_set_row_to( $this->ui_extends_api, $out, $InboxId );
		$this->Imap_set_row_cc( $this->ui_extends_api, $out, $InboxId);
		$this->Imap_set_row_attachment( $this->ui_extends_api, $out, $InboxId);
		$this->Imap_set_row_project( $this->ui_extends_api, $out, $InboxId );
		
		// -------- if this like its -- 
		
	}
  }
}  
// --------------------------------------------------------------------------------- 
/* 
 *  propertie   	 : Imap_while_loop 
 *
 *  @ param   		 : @$key = '' , @$val = ''
 *  @ notes      	 : -  	
 */
 
 public function Imap_while_loop( $flag = 'UNSEEN' )
{
	$ImapBuffer =& self::Imap_open_buffer();
	try  {
	   	
	  // uid on inbox mail 	
		$this->UID = $ImapBuffer->searchMailBox($flag);
		
		if( !$this->UID ) {
			$this->Imap_set_row_error("Mailbox is empty");
			die('Mailbox is empty');	
		}
		
		
		if( !is_array($this->UID) ) { 
			return false; 
		}
		
		
	  // dibaca satu persatu
		if( is_object( $ImapBuffer ) AND ( $this->InboxId = reset($this->UID)) ){
			$this->Imap_get_row_section($ImapBuffer, $this->InboxId);
		}
	} 
	catch( ImapMailboxException $e ){ 
		$this->Imap_set_row_error($e->errorMessage());
	}	
 } 

// ============================== END CLASS  ==========================

} 
?>
