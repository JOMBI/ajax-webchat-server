<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Enigma User Interface 
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		Enigma User Interface 
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://razakitechnology.com/user_guide/license.html
 * @link		http://razakitechnology.com
 * @since		Version 1.2.0
 * @filesource
 */

 


// -------------------------------------------------------------------------------------------------
/* is valid key */

if( ! function_exists('eui_webapplication') )
{
   function eui_webapplication() 
  {
	 $arr_class =& get_instance();
	 $arr_class->load->model("M_WebApplication");
	 if( class_exists("M_WebApplication"))
	 {
		return get_class_instance("M_WebApplication");
	 }
	 else {
		return FALSE;
	 }	
  }
}
 

// -------------------------------------------------------------------------------------------------
/* is valid key */

if( ! function_exists('eui_version') )
{
   function eui_version() 
  {
	 $UI=& get_instance();
	 return EUI_Version::Instance(); 
  }
}

// -------------------------------------------------------------------------------------------------
/* is valid key */

if( ! function_exists('isvalid') )
{
   function isvalid() 
  {
	 $Ver=& eui_version();
	 return $Ver->isvalid(); 
	 
  }
}


// -------------------------------------------------------------------------------------------------
/* is valid key */

if( ! function_exists('title') )
{
   function title() 
  {
	 $Ver=& eui_version();
	 return $Ver->title(); 
	 
  }
}

// -------------------------------------------------------------------------------------------------
/* is  version */

if( ! function_exists('version') )
{
   function version() 
  {
	$Ver=&eui_version();
	 return $Ver->version(); 
	 
  }
}

// -------------------------------------------------------------------------------------------------
/* is author */

if( ! function_exists('author') )
{
   function author() 
  {
	 $Ver=&eui_version();
	 return $Ver->author(); 
	 
  }
}

// -------------------------------------------------------------------------------------------------
/* is dsinstall */

if( ! function_exists('dsinstall') )
{
   function dsinstall() 
  {
	$Ver=&eui_version();
	return $Ver->dsinstall(); 
	 
  }
}


// -------------------------------------------------------------------------------------------------
/* is description */
if( ! function_exists('description') )
{
   function description() 
  {
	 $Ver=&eui_version();
	 return $Ver->description(); 
	 
  }
}

// -------------------------------------------------------------------------------------------------
/* is copyright on model */

if( ! function_exists('copyright') )
{
   function copyright() 
  {
	 $Ver=&eui_webapplication();
	 return $Ver->_web_copyright(); 
	 
  }
}


// -------------------------------------------------------------------------------------------------
/* is copyright on model */

if( ! function_exists('company') )
{
   function company() 
  {
	 $Ver=&eui_webapplication();
	 return $Ver->_web_company_name(); 
	 
  }
}


// -------------------------------------------------------------------------------------------------
/* is copyright on model */

if( ! function_exists('website') )
{
   function website() 
  {
	 $Ver=&eui_webapplication();
	 return $Ver->_web_company_website(); 
	 
  }
}


// -------------------------------------------------------------------------------------------------
/* is copyright on model */

if( ! function_exists('email') )
{
   function email() 
  {
	 $Ver=&eui_webapplication();
	 return $Ver->_web_company_email(); 
	 
  }
}


// -------------------------------------------------------------------------------------------------
/* is copyright on model */

if( ! function_exists('defaultthemes') )
{
   function defaultthemes() 
  {
	 $Ver=&eui_webapplication();
	 return $Ver->_web_themes(); 
	 
  }
}

if( ! function_exists('title_header') )
{
   function title_header( $title = '' ) 
  {
	 $arr_header = array( description(), $title);
	 return join(" :: ", $arr_header);
  }
}










?>