<?php
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
class EUI_Object  {

private $arr_val = "";
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
var $find_vals_args = "";
var $arr_rows = array();
var $arr_func_arg = array();
 
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 private static $Instance = null;

 
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 public static function &Instance()
{
  if( is_null(self::$Instance) )
  {
	 self::$Instance = new self();
  }
  
  return self::$Instance;
  
 }

/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 public function EUI_Object( $arr_object = null )
{
 
  if( !is_null($arr_object) )
  {
	 $this->Inialize( $arr_object );	
  }  
}

/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 public function fetch_ready()
{
 if( is_array( $this->arr_rows ) 
	AND count( $this->arr_rows ) > 0  )
 {
	return true;
  }	
 return false;
}


/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
public function Inialize( $arr_object = null )
{
	$this->arr_rows =(array)$arr_object;
}

/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 protected function _value( $value = null ) 
{
	
	
  $value = trim($value);
  if( is_null($value) ) {
	return NULL;
  }
 
  if( isset($this->arr_rows[$value]) AND empty($this->arr_rows[$value]) 
	  AND !$this->arr_rows[$value] == 0 ) 
 {
	return NULL;
  }
  
 if( isset($this->arr_rows[$value]) AND is_null($this->arr_rows[$value]) ) {
	return NULL;
 } 
 
 if( !isset($this->arr_rows[$value]) ) {
	return NULL;
 } 
   
   return $this->arr_rows[$value];
  
}

/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 protected function _event_value( $value = null, $event=null, $context = null ) 
{
  if( is_null( $context ) OR strlen($context)==0 ){	
	$_value = $this->_value( $value );
  } else {
	  $_value = $context;
  }
  
  if(!function_exists( $event ) )
  {
	return $_value; 
  }
  
   return call_user_func_array( $event, array($_value) );  
  
}
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 public function get_array_order( $value = null, $asc = 'sort' ) 
{
  $arr_val = $this->get_splits_value($value);

  if( count($arr_val) ==0 ){
	return NULL;
  }	  
  
  $arr_sorter = array('asort', 'arsort', 'krsort','ksort','sort','rsort');
  if( in_array($asc, $arr_sorter )){
	if(function_exists( $asc ) ){  
		$asc($arr_val);
	}
  }	
  return (array)$arr_val;
  
}
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 public function get_value_order( $value =''  )
{
	$arr_order = $this->get_array_order( $value );
	if( is_array($arr_order) and count($arr_order) > 0  ){
		return (string)join(",", $arr_order);
	}	
	return NULL;
 } 
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 public function get_splits_value( $value = null , $splite="," ) 
{
 $arr_value  = array();
  if( count($arr_value) == 0 AND  !is_null($value) )
 {
	if( $this->find_value($value) ){
		$arr_value = explode($splite,  $this->_value($value) );
	}
 }
 
 return (array)$arr_value;
	
}
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 
public function get_array_value( $value = null , $event = NULL ) 
{
 $arr_value = array();
 $evt_value = $this->_value( $value );
  
 if( preg_match("/\;/", $evt_value ) ){
	$arr_value = explode(";", $evt_value);
 }
 else if( preg_match("/\,/", $evt_value ) ){
	$arr_value = explode(",", $evt_value);
 }
 else {
	$arr_value = explode(",", $evt_value);  
 }
  
 $arr_outer = array();
 if( is_null($event) ) 
 {
	foreach( $arr_value as $k => $val ) {
		if( strlen( trim($val) ) > 0 ){
			$arr_outer[$k] = $val;
		}
	}	
 }
 
  if( !is_null($event) ) 
 {
	foreach( $arr_value as $k => $val ) 
	{
	  if( strlen( trim($val) ) > 0 )
	  {	
		if(function_exists($event) ){
			$arr_outer[$k] = call_user_func_array($event, array($val)); 
		} else {
			$arr_outer[$k] = $val;
		}
	  }	
	}	
 }
 
 return $arr_outer;
}
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 function findkey( $key = null ){
	 if( isset($this->arr_rows[$key]) ){
		 return true;
	 }
	 return false;
 }
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 public function find_value() 
{
	
 $this->find_vals_args  = '';	
 $this->arr_list_args  = func_get_args();
 $this->arr_num_args   = func_num_args();
 
 if( !$this->arr_num_args ){
	return FALSE;
 }
 
 if(!is_array($this->arr_list_args) ){
	return FALSE;
 }	
 
 $this->find_vals_args = $this->get_value($this->arr_list_args[0]);
  if( !is_array($this->find_vals_args) 
	AND !is_object($this->find_vals_args)  )
 {
	$str_val = $this->find_vals_args;
	return ( strlen($str_val) > 0 ? TRUE : FALSE ); 
	 
 }
 
 return TRUE;
 
}
 
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 
 function get_value( $value = null , $event = NULL )  { 
 if( is_null($event) ) {
	return $this->_value( $value );
 }

 if(!is_array($event) ){
	$event = array( $value => $event);	
 }
	
  if ( isset($arr_val) ) {
  	 $arr_val = '';
  } else {
  	 $arr_val = '';
  }

  $arr_val = $arr_val;

  foreach( $event as $n => $argc ) 
  {
	if( !$n ) {
		$arr_val= $this->_event_value($value, $argc);
	}
	 else {
		
		$arr_val = $this->_event_value($value, $argc,  $arr_val);
	}
  }
  
 return  $arr_val;
}

/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 protected function _object( $arr_object = null ) 
{
   return (object)$arr_object;
}
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 function fetchAssoc(){
	 if( is_array($this->arr_rows) ){
		 return (array)$this->arr_rows;
	 }
	 // null array;
	 return array();
 }
 
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 public function fetch_rows( $n = 0 )
{
   $arr_rows = $this->get_value( $n ); // first_rows 
    if( is_array( $arr_rows ) ) 
   {
	  $this->arr_header_value = (array)$arr_rows;
   }
   
   return $this->arr_header_value;
} 

/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
public function debug_label()
{
	echo "<pre>";
		print_r($this->fetch_label());
	echo "</pre>";
		
} 
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
public function debug_field()
{
	echo "<pre>";
		print_r($this->fetch_field());
	echo "</pre>";
		
} 
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 public function fetch_field()
{
  $resultField = array();	 
  $resultArray = $this->fetch_rows();
  
  if( is_array( $resultArray )) {
	  $resultField = (array)_getKey($resultArray);
	  
	  // how is that ?
	  if(is_array($resultField))
	  foreach($resultField as $k => $value ) {
		  $this->arr_label[$value] = $value;
	  }
  }	
  
  return $this->arr_label;
} 

/* @brief  object
 * @param  [type] $CustomerId [description]
 * @retval [type] [description]
 */
 public function fetch_label()
{
  return array_keys($this->arr_rows);
} 

/* @brief  object
 * @param  [type] $CustomerId [description]
 * @retval [type] [description]
 */
 public function get_object() 
{
   return $this->_object($this->arr_rows);
}

/* @brief  object
 * @param  [type] $CustomerId [description]
 * @retval [type] [description]
 */
 public function object() 
{
   return $this->_object($this->arr_rows);
}

/* @brief  object
 * @param  [type] $CustomerId [description]
 * @retval [type] [description]
 */
 function add( $key, $value  = '' )
{
	if (!is_array($key)){
		$key = array($key => $value);
	} if (is_array($key)) foreach ($key as $ca => $va){
		$this->arr_rows[$ca] = $va;
	} return $this;
 }
 
/* @brief  dell
 * @details delete data source
 * @param  [type] $CustomerId [description]
 * @retval [type] [description]
 */
 function dell($key =  NULL)
{
	if (isset( $this->arr_rows[$key] ) ){
		unset( $this->arr_rows[$key] );
	} return $this;
}
 
 
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 function valid( $key = null ){
	 $valid = ( isset( $this->arr_rows[$key] ) ? 
					   $this->arr_rows[$key] : '' );
	// data type string :				   
	if( is_string($valid) and !strlen($valid)  ){
		return false;
	}
	// jika array: 
	//var_dump($valid);
	if( is_array($valid) and !count($valid)  ){
		return false;
	}
	// then : 
	return true;
 }
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 
 function values($key, $event = null ) {
	 return $this->get_array_value($key, $event); 
 }
 
 /*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 function find( $key  = null ){
	if( !$this->findkey( $key ) ){
		return false;
	}
	return $this->get_value( $key );
 }
 /*
 * @class  [session::iterator]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 function session(){
	 $CI = &CI();
	 if( is_object( $CI ) ){
		 return $CI->EUI_Session;
	 }
	 return false;
 }
 
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 
 function fields($key, $event = null ) {
	 return $this->values($key, $event); 
 }
 /*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 // function translation object :URI 
 function segment( $i = 0 ){
	$CI=&CI();
	$value = null;
	if( is_object( $CI->URI ) ){
		$value = $CI->URI->segment( $i );
	}
	// return segment from parse URI:
	return $value;
 }
 
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 function get( $key = null, $calbacks = null ){
	return $this->field($key, $calbacks);
 }
/*
 * @class  [get_queue]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 function field( $key = null, $calbacks = null ){
	 $value = null;
	 $value = ( isset( $this->arr_rows[$key] ) ? $this->arr_rows[$key] : null );
	 
	 // then exist function : 
	 if( !is_null($calbacks) and function_exists( $calbacks ) ){
		return call_user_func( $calbacks, $value );
	 }
	 // then : 
	 if( !is_null($calbacks) and !function_exists( $calbacks ) ){
		 return $value;			
	 }
	 // then : 
	 return $value;
 }
 
// END CLASS  
}
if(!function_exists('Reader')){
function Reader( $param ){
	return new EUI_Object($param);
}
}
if(!function_exists('fd_item')){
function fd_item( $param ){
	return new EUI_Object($param);
}
}
?>