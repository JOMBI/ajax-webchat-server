<?php
/* @ def 	: E.U.I Session Helper base on EUI_Session libraries 
 * ---------------------------------------------------------------
 * @ param 	: modified
 * @ author : omens
 */
 
// ------------------------------------------------------------
 
/* @ def 	: _have_get_session base on views type 
 * ---------------------------------------------------------------
 * @ param 	: modified
 * @ author : omens
 */
 
if( !function_exists('_have_get_session') )
{
	function _have_get_session($param)
	{
		$EUI =& get_instance();
		return $EUI -> EUI_Session -> _have_get_session($param);
	}
} 

 
// ------------------------------------------------------------
 
/* @ def 	: _have_get_session base on views type 
 * ---------------------------------------------------------------
 * @ param 	: modified
 * @ author : omens
 */
 
if( !function_exists('_is_login') )
{
	function _is_login()
	{
		$EUI =& get_instance();
		return $EUI->EUI_Session->_have_get_session("UserId");
	}
} 

 
// ------------------------------------------------------------
 
/* @ def 	: _have_get_session base on views type 
 * ---------------------------------------------------------------
 * @ param 	: modified
 * @ author : omens
 */
 
if( !function_exists('_is_expired') )
{
	function _is_expired()
	{
		$EUI =& get_instance();
		return $EUI->EUI_Session->_have_get_session("UserExpired");
	}
} 
/**
 * @class  [@constructor]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
if( !function_exists('setSession') ) {
	function setSession($name, $value) {
		return @call_user_func('_set_session', $name, $value);
	}
} 
/**
 * @class  [@constructor]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
if( !function_exists('_set_session') )
{
	function _set_session($name, $value)
	{
		$EUI =& get_instance();
		return $EUI->EUI_Session->_set_session($name, $value);
	}
} 

// ------------------------------------------------------------
 
/* @ def 	: _get_session base on views type 
 * ---------------------------------------------------------------
 * @ param 	: modified
 * @ author : omens
 */
 
if( !function_exists('_get_session') )
{
	function _get_session($param)
	{
		$EUI =& get_instance();
		return $EUI -> EUI_Session -> _get_session($param);
	}
} 

// ------------------------------------------------------------
 
/* @ def 	: _get_session base on views type 
 * ---------------------------------------------------------------
 * @ param 	: modified
 * @ author : omens
 */
 
 if( !function_exists('_deleted_session') )
{
	function _deleted_session($param)
	{
		$EUI =& get_instance();
		return $EUI->EUI_Session->_unset_session($param);
	}
} 
 

/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 
 if( !function_exists('_get_exist_session') ) {
	function _get_exist_session($param) {
		$EUI =& get_instance();
		return (_have_get_session($param)?_get_session($param):$EUI->URI->_get_post($param));
	}
}


/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
 if( ! function_exists('_get_real_session') )
{
	
	function _get_real_session()
	{
		$EUI =& get_instance();
		return $EUI->EUI_Session->get_real_session();
	}
}

// ------------------------------------------------------------
 
/* @ def 	: _get_session base on views type 
 * ---------------------------------------------------------------
 * @ param 	: modified
 * @ author : omens
 */
 
if( !function_exists('_set_key_session') ){
	
	function _set_key_session( $session = null  )
	{
		$EUI =& get_instance();
		return $EUI->EUI_Session->_set_key_session( $session );
	}
}
// ------------------------------------------------------------
 
/* @ def 	: _get_session base on views type 
 * ---------------------------------------------------------------
 * @ param 	: modified
 * @ author : omens
 */
 
if( !function_exists('_destroy_session') ){
	
	function _destroy_session( $session = null  )
	{
		$EUI =& get_instance();
		return $EUI->EUI_Session->_destroy_session();
	}
}

/**
 * Parse out the attributes
 *
 * Some of the functions use this
 *
 * @access	private
 * @param	array
 * @param	bool
 * @return	string
 */
 
if ( ! function_exists('Session'))
{
	function Session() 
	{
		if( !class_exists('EUI_Object') ) {
			$CI =& CI();
			$CI->load->helper(array('EUI_Object'));
		}
		
		// class exist on her3
		if( class_exists('EUI_Object') )  {
			
			// then get resultArray 
			$resultArray = call_user_func('_get_real_session');
			if( !is_array( $resultArray ) ){
				return false;
			}
			// then : 
			return new EUI_Object( $resultArray);
			
		}
		return false;
	}
}
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */	
if(!function_exists('getReadySession') ){
	function getReadySession( $val  = null ){
		if( function_exists('_get_exist_session') ){
			return call_user_func('_get_exist_session', $val);
		}
		return $val;
	}
}
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
if(!function_exists('set_session') ){
 function set_session( $key = null, $val = '' ) {
	if(function_exists('_set_key_session')){
		return setSession($key, $val);
	}
	return null;
 }
}
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function destroy_session(){
	$EUI =& get_instance();
	return $EUI->EUI_Session->_destroy_session();
} 
/*
 * @class  [get array values : ]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
if(!function_exists('set_key_session') ){
 function set_key_session( $key = null  ) {
	if(function_exists('_set_key_session')){
		return _set_key_session($key);
	}
	return null;
 }
}
 