<?php
 error_reporting(E_ALL);
 ini_set("display_errors",1);
 
 /* set_time_limit */
 if (function_exists('set_time_limit')) {
	set_time_limit(0);
 }   
 /* ob_implicit_flush */
 if (function_exists('ob_implicit_flush')) {
	ob_implicit_flush();
 } 
 /* date_default_timezone_set */
 if (function_exists('date_default_timezone_set')) {
	date_default_timezone_set('Asia/Jakarta');
 }
 
 /* ini_set */
 if (function_exists('ini_set')){
	ini_set('memory_limit', '-1');
	ini_set('session.cookie_samesite', 'None');
	ini_set('session.cookie_secure', 'true');
 
 } 
 /* base appear */
 $system_path = 'system';
 $application_folder = 'application';
 /* PRODUCT */
 if (!defined( 'PRODUCT' )) {
	define( 'PRODUCT','service');
 }
 /* ENVIRONMENT */ 
 if ( !defined( 'ENVIRONMENT' )) {
	define('ENVIRONMENT','PRODUCTION');
 }
 /* STDIN */  
 if (defined('STDIN')) {
	chdir(dirname(__FILE__));
 }
 /* realpath */
 if (realpath($system_path) !== FALSE) {
	$system_path = realpath($system_path).'/';
 } 
 $system_path = rtrim($system_path, '/').'/'; 
 if (!is_dir($system_path)) {
	exit( "Your system folder path does not appear to be set correctly. Please open the following file and correct this: ".pathinfo(__FILE__, PATHINFO_BASENAME));
 }
 /* SELF */
 define( 'SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
 define( 'EXT', '.php');
 define( 'BASEPATH', str_replace("\\", "/", $system_path));
 define( 'FCPATH', str_replace(SELF, '', __FILE__));
 define( 'SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));
 /* The path to the "application" folder */
 if (is_dir($application_folder)) { 
	define( 'APPPATH', $application_folder .'/'); 
 } else {
	if (!is_dir(BASEPATH.$application_folder.'/')) {
		exit( "Your application folder path does not appear to be set correctly. Please open the following file and correct this: ".SELF);
	} 
	define( 'APPPATH', BASEPATH.$application_folder.'/');
 } 
 require_once BASEPATH.'core/EUI_Core.php'; 