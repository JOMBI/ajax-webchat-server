<!DOCTYPE html>
<html lang="en">
<head> 
<title><?php echo description();?> <?php echo version();?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<link type="text/css" rel="shortcut icon"  href="<?php echo base_url();?>library/styles/service/images/livechat.ico?ver=<?php echo version();?>&amp;time=<?php echo time();?>">
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>library/styles/service/default/screen.src.css?ver=<?php echo version();?>&amp;time=<?php echo time();?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>library/styles/themes/fonts/font-awesome.min.css?ver=<?php echo version();?>&amp;time=<?php echo time();?>"/>
</head>
<body>
