<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desktop Class Controller Base On CI 
 */ 
class Desktop extends EUI_controller {

	/* @brief Construtor of class 
	 * @param none 	none none 
	 * @retval mixed json resposne  
	 */ 
	 public function __construct() 
	{
		parent::__construct();  
		$this->load->model(array("M_Desktop"));
	}   
	
	/* @brief index
	 * @details enable feature for desktop clients 
	 * @param string  $username  username for login base on header 
	 * @param string  $password  password for login base on header 
	 * @param integer $interval  maximum expired Interval   
	 * @retval mixed json resposne  
	 */ 
	 function index()
	{
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");
		/* get for check return callbacks */
		$desktop_client_id = getParam("cid");
		$desktop_client_env = false;
		$desktop_register_clients = 0;
		/* get for check return callbacks */
		$desktop_enable_global_settings = 0; 
		/* global settings for sip for audio callas config */
		$desktop_sip_settings = false;
		/* global settings for buttons */
		$desktop_enable_button = false;
		/* global settings for buttons */
		$desktop_image_settings = false;
		/* engine settings for buttons */
		$desktop_engine_settings = false; 
		/* global settings for per buttons clients  */
		$desktop_disable_features = false; 
		// set default response 
		$response = array ( 
		"success" => false, 
		"message" => "failed", 
		"data" => array (
			"desktop_sip_settings" => false,
			"desktop_enable_button" => false, 
			"desktop_image_settings" => false,
			"desktop_engine_settings" => false,
			"desktop_disable_features" => false
		)); 
		// check if success process  
		$this->M_Desktop->desktop_register_clients($desktop_register_clients, $desktop_client_id);
		while ($desktop_register_clients){
			$this->M_Desktop->desktop_engine_clients($desktop_engine_settings, $desktop_client_id);  
			$this->M_Desktop->desktop_enable_button($desktop_enable_button, $desktop_client_id); 
			$this->M_Desktop->desktop_image_settings($desktop_image_settings, $desktop_client_id);
			$this->M_Desktop->desktop_disable_feture($desktop_disable_features, $desktop_client_id);  
			// desktop_engine_settings
			if ($desktop_engine_settings){
				$response["data"]["desktop_engine_settings"] = $desktop_engine_settings;
				++ $desktop_enable_global_settings;
			} // desktop_enable_button 
			if ($desktop_enable_button){
				$response["data"]["desktop_enable_button"] = $desktop_enable_button;
				++ $desktop_enable_global_settings;
			} // desktop_disable_features
			if ($desktop_disable_features){
				$response["data"]["desktop_disable_features"] = $desktop_disable_features;
				++ $desktop_enable_global_settings;
			} // desktop_image_settings
			if ($desktop_image_settings){
				$response["data"]["desktop_image_settings"] = $desktop_image_settings;
				++ $desktop_enable_global_settings;
			} // for return success;
			if ($desktop_enable_global_settings){
				$response["success"] = true;
				$response["message"] = "success";
			} break;
		} // set return this message 
		exit(json_encode($response));
	} 
	
	/* @brief engine
	 * @details this for test and check 
	 * @param string  $username  username for login base on header 
	 * @param string  $password  password for login base on header 
	 * @param integer $interval  maximum expired Interval   
	 * @retval mixed json resposne  
	 */ 
	 function engine()
	{
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");
		/* get for check return callbacks */
		$desktop_client_id = getParam("cid");
		$this->M_Desktop->desktop_engine_clients($desktop_engine_clients, $desktop_client_id);	
		echo json_encode($desktop_engine_clients);
	}
	
}
?>