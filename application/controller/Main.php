<?php
/** 
 * @properties  [description]
 * @Methode	 	[@param]		[construtor class]
 * @param 		[uknown]
 * @author		[uknown]
 */
class Main extends EUI_Controller
{
/** 
 * @properties  [description]
 * @Methode	 	[@param]		[construtor class]
 * @param 		[uknown]
 * @author		[uknown]
 */
var $Centerback = null;
/** 
 * @properties  [description]
 * @Methode	 	[@param]		[construtor class]
 * @param 		[uknown]
 * @author		[uknown]
 */
 public function __construct() 
 {
	parent::__construct();
	$this->load->model(array('M_Menu','M_Centerback'));
	$this->load->helpers('EUI_Object');
}

/** 
 * @properties  [description]
 * @Methode	 	[@param]		[construtor class]
 * @param 		[uknown]
 * @author		[uknown]
 */
 
 function index() 
{
	$_header_website  = null;
 
	// check validation login :  
	if( !_is_login() ) {
		redirect("Auth/?login=(false)");
	}
 
	// get menu for all application  
	 $resultFetchMenu =&Instance("M_Menu");
	 $resultFetchApplet =&Instance('M_Centerback');
	  
	// sent data to view process : 
	 $resultFetchApplication = array( 'arr_role' 	=> $resultFetchMenu->_MenuMasterRole(),
									  'arr_master'	=> $resultFetchMenu->_MenuMasterName(),
									  'centerback' 	=> $resultFetchApplet );
	 
	 // get end substr: 
	 if(is_array( $resultFetchApplication ) ) {
		$this->load->layout( $this->Layout->base_layout() .'/UserMain', $resultFetchApplication);	
	  } 
	}
// END CLASS :
}
?>