<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * Chat Class controller 
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Security
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/security.html
 */  
if ( !defined( 'BASEDIR_DOWNLOAD' )){
	define ( 'BASEDIR_DOWNLOAD', rtrim(rtrim(rtrim(BASEPATH, "/"), "system"), "/") . DIRECTORY_SEPARATOR . rtrim(APPPATH, "/") . "/temp");
}
class Document extends EUI_Controller {
	/* @brief __construct   
	 * @details no description 
	 * @retval mixed return 
	 */
	function __construct(){
		parent::__construct();
		$this->load->model(array('M_Chat', 'M_Upload'));
	}
	
	/* @brief index   
	 * @details no description 
	 * @retval mixed return 
	 */
	function index(){}
	
	/* @brief file   
	 * @details no description 
	 * @retval mixed return 
	 */
	 function file()
	{
		$response = array("success" => false, "message" => "failed", "data" => false); // new parameter
		$bytes = new stdclass(); // new parameter 
		$bytes->_get_stream = false; // new parameter
		$bytes->_get_action = getParam("action");
		while (false !== ($requestor = UR())){
			// validation request from user
			if (false === ($documentid = $requestor->get("id"))){
				$response["message"] = "wrong parameter";
				break;
			} // validation request from user
			if (!strlen($documentid)){
				$response["message"] = "wrong parameter";
				break;
			} // next check to database  
			$byte_stream = false;
			if (!$this->M_Upload->download($documentid, $byte_stream)){
				$response["message"] = "file not found";
				break;
			} // set global process 
			$bytes->_mimetype = false;
			$bytes->_directory = $byte_stream["file_path"];
			$bytes->_filename = $byte_stream["file_name"];
			$bytes->_filehash = $byte_stream["file_hash"];
			$bytes->_filepath = $bytes->_directory . DIRECTORY_SEPARATOR . $bytes->_filehash;
			if ( @file_exists($bytes->_filepath) === false){
				$response["message"] = "file not found";
				$bytes->_get_stream = false;
				break;
			} // get mime to open file process 
			if (false !== ($infopath = @pathinfo($bytes->_filepath))){
				$bytes->_mimetype = isset($infopath["extension"]) ? $byte_stream["file_mime"]: false;
			} // get real size  
			if (false !== ($bytes->_filesize = @filesize($bytes->_filepath))){
				$bytes->_get_stream = true;
			} break;
		} 
		// if file false process will response on JSON stream 
		if (!$bytes->_get_stream){
			header("X-Content-Type-Options: nosniff");
			header("Content-Type: application/json;charset=UTF-8");
			exit( json_encode($response) );
		}
		// if file is true content in my server will show data to stream   
		header("Content-Type: ". $bytes->_mimetype);
		if (!strcmp($bytes->_get_action, "download")){ 
			header("Content-Disposition: attachment; filename=". $bytes->_filename);
		} header("Content-Length: " . $bytes->_filesize);
		exit ( @file_get_contents($bytes->_filepath) );
	} 
	
	
	/* @brief export   
	 * @details no description 
	 * @retval mixed return 
	 */
	 function export()
	{
		$bytes = new stdclass(); // new parameter  
		$bytes->_get_stream = true;
		$bytes->_mimetype = false;
		$bytes->_get_file = getParam("file", false); // get file process 
		$bytes->_get_path = BASEDIR_DOWNLOAD . DIRECTORY_SEPARATOR . $bytes->_get_file; // get file
		// get header response for client 
		$response = array("success" => false, "message" => "failed", "data" => false);
		while ($bytes->_get_file !== "") {
			// get file process  
			if (! @file_exists($bytes->_get_path)) {
				$bytes->_get_stream = false; 
				break;
			} // get size process 
			$bytes->_get_filename = basename($bytes->_get_path); 
			if (false === ($bytes->_get_size = @filesize($bytes->_get_path))){
				$bytes->_get_stream = true;
				break;
			} // get info file 
			if (false !== ($pathinfo = pathinfo($bytes->_get_path))){
				$bytes->_get_type = isset($infopath["extension"]) ? $byte_stream["file_mime"]: false;
				$bytes->_get_stream = true; 
			} break;
		} // if got message stream 
		if (!$bytes->_get_stream){
			header("X-Content-Type-Options: nosniff");
			header("Content-Type: application/json;charset=UTF-8");
			exit( json_encode($response) );
		} // force download thend delete file 
		header("Content-Type: ". $bytes->_get_type); // type 
		header("Content-Disposition: attachment; filename=". $bytes->_get_filename); // file name 
		header("Content-Length: " . $bytes->_get_size); // use this to open files directly
		header("Cache-control: private"); // use this to open files directly
		$fd = fopen($bytes->_get_path, "r");
		while (!feof($fd)) {
			$buffer = fread($fd, 2048);
			echo $buffer;
		} fclose ($fd); 
		@unlink($bytes->_get_path); 
	}
}
?>