<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Chat Class controller 
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Security
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/security.html
 */
 
 class Guest extends EUI_Controller
{
	/* @brief Constructor Method this class  
	 * @return mixed html content  
	 * @details none
	 */ 
	 function __construct()
	{
		parent::__construct(); 
		$this->load->model(array("M_Chat", "M_Guest"));
	}
	
	/* @brief index
	 * @details Index Page Layout for Visitor  
	 * @return mixed this html content  
	 * @details Show layout page on Visitor user 
	 */
	 function index()
	{ 
		/* get Moduls Url */
		$this->requestor = UR();  
		$this->cid = $this->requestor->get('cid');
		$this->host = $this->requestor->get('host'); 
		
		/* get cid && host Parameter */
		if (empty($this->cid) OR  empty($this->host)) {
			exit(0);
		} 
		
	   /* view folder akan di routing berdasarkan nama domain yang ada jika
		* Tidak ada maka akan di tampilkan Error 
		*/
		$this->output = (string)$this->host . DIRECTORY_SEPARATOR . "content";
		$this->load->view ($this->output, 
		array (
			"cid"  => $this->requestor->get("cid"),
			"host" => $this->requestor->get("host")
		));
	}
	
	/* @brief headers 
	 * @details Emoticon layout  
	 * @return mixed html content  
	 * @details none
	 */ 
	 function headers()
	{
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");
		header("Access-Control-Allow-Methods: OPTIONS,POST,GET");
		header("Access-Control-Max-Age: 3600");
		header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, apikey, authorization, ApiKey, Authorization, X-Requested-With");
		return 0;
	}

	/* @brief auths 
	 * @details Emoticon layout  
	 * @return mixed html content  
	 * @details none
	 */ 
	 function clients() 
	{
		$this->headers();
		$response = array("success" => 0, "message" => "failed", "data" => false);
		while ($this->M_Guest->clients($cid, $host, $clients)){
			$response["success"] = 1;
			$response["message"] = "success";
			$response["data"] = (array)$clients;
			break;
		} 
		exit (json_encode($response));
	}
	
	/* @brief auth 
	 * @details Emoticon layout  
	 * @return mixed html content  
	 * @details none
	 */ 
	 function auth() 
	{
		$this->headers();
		$cid = getParam("cid"); // clientid 
		$host = getParam("host"); // hostname 
		$response = array("success" => 0, "message" => "failed", "data" => false);
		while ($this->M_Guest->auth($cid, $host, $ret)){ 
			$response["success"] = 1;
			$response["message"] = "success";
			$response["data"] = array();
			$response["data"]["a"] = encrypts($ret["chat_client_auth"]);
			$response["data"]["t"] = encrypts($ret["chat_client_token"]);
			$response["data"]["s"] = encrypts($ret["chat_client_session"]);  
			$response["data"]["f"] = encrypts($ret["chat_client_files"]); 
			break;
		} 
		exit (json_encode($response));
	} 
	
	/* @brief emotion 
	 * @details Emoticon layout  
	 * @return mixed html content  
	 * @details none
	 */ 
	 function emotion() 
	{
		$this->load->view("chat_view_guest/emotion", array());
	} 
	 
	/* @brief chat  
	 * @details Chat is Redirect page index Controler  
	 * @return mixed this html content  
	 * @details Show layout page on Visitor user 
	 */
	 function chat() 
	{ 
		$this->index();
	}
	
	/* @brief destroy
	 * @details Chat is Redirect page index Controler  
	 * @return mixed this html content  
	 * @details Show layout page on Visitor user 
	 */
	 function destroy()
	{
		session_start();
		session_destroy();
		$this->headers();
		$response = array("error" => 0, "message" => "success", "data"=> false);
		exit (json_encode($response) );
	}
	 
	/* @brief verbose
	 * @details Chat is Redirect page index Controler  
	 * @return mixed this html content  
	 * @details Show layout page on Visitor user 
	 */
	 function verbose()
	{
		session_start(); 
		$this->headers();
		$response = array("error" => 1, "message" => "success", "session"=> $_SESSION);
		if (isset($_SESSION)){
			$response["error"] = 0;
			$response["message"] = "success";
			$response["session"] = (array)$_SESSION; 
		} 
		exit (json_encode($response) );
	}
	 
	 
	/* @brief download
	 * @details Download file from visitor Or Agents  
	 * @param [in] $uid Id session chat from guest
	 * @return Mixed is true download stream  
	 * @details no more details
	 */
	function download($uid= "") { 
		$download = new stdclass();
		$download->uid = (string)$uid; 
		/* false param uid */
		if ($download->uid == "") {
			return 0;
		} /* empty uid */
		if (empty($download->uid)){
			return 0;
		} /* if empty data from database sction  */
		if (false === ($download->output = $this->M_Chat->gettmp($download->uid))){
			return 0;
		} /* invalid source of data */
		if (is_array($download->output) && !sizeof($download->output)){
			return 0;
		} /* check values of data source */
		$download->values = isset($download->output['value']) ? $download->output['value'] : false;
		if (!$download->values){
			return 0;
		} /* get row data process */
		if (!$this->M_Chat->getfile($download->values, $download->row)){
			return 0;
		} /* get path to download process */
		$download->path = $download->row["filepath"] . DIRECTORY_SEPARATOR . $download->row["filename"];  
		if (!file_exists($download->path)) {
			return 0;
		} /* Starting Header download file */
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='. basename($download->path));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: private');
		header('Pragma: private');
		header('Content-Length: ' . filesize($download->path));
		ob_clean();
		flush();
		readfile($download->path);
		exit;
	}   
} 
?>