<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* CHAT_EXPIRED_INTERVAL */
 if (!defined ('CHAT_EXPIRED_INTERVAL') )
{
	define ('CHAT_EXPIRED_INTERVAL', 360);	
}

/**
 * Auth Class Controller Base On CI 
 */ 
class Auth extends EUI_controller {

	/* @brief Construtor of class 
	 * @param none 	none none 
	 * @retval mixed json resposne  
	 */ 
	 public function __construct() 
	{
		parent::__construct(); 
		$this->load->model(array("M_Auth"));
	}  
	
	/* @brief headers
	 * @param string  $username  username for login base on header 
	 * @param string  $password  password for login base on header 
	 * @param integer $interval  maximum expired Interval   
	 * @retval mixed json resposne  
	 */
	function headers()
	{
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");
		header("Access-Control-Allow-Methods: OPTIONS,POST,GET");
		header("Access-Control-Max-Age: 3600"); 
		header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, apikey, authorization, ApiKey, Authorization, X-Requested-With");
		return 0;
	} 
	
	/* @brief Generate Token from user public process  
	 * @param string  $username  username for login base on header 
	 * @param string  $password  password for login base on header 
	 * @param integer $interval  maximum expired Interval   
	 * @retval mixed json resposne  
	 */ 
	 function token()
	{	
		$this->headers();
		$response = array("error" => 1, "message" => "failed", "data" => false);
		while (false !== ($requestor = UR())) { 
			
			/* default set all parameter */
			$username = $requestor->get("username");
			$password = $requestor->get("password");
			
			/* default token expired */
			$interval = (int)$requestor->get("interval");
			if (!$interval){
				$interval = CHAT_EXPIRED_INTERVAL;
			} /* validation 'username' */
			if (!$username OR (empty($username))){
				$response["message"] = "username failed";
				break;
			} /* validation 'password' */
			if (!$password OR (empty($password))){
				$response["message"] = "Password failed";
				break;
			} /* check moduls 'Authorization' */ 
			if (!function_exists('Authorization')){
				$response["message"] = "moduls 'Authorization' failed";
				break;
			} /* check moduls 'Authorization' */
			if (false === ($authorization = Authorization())){
				$response["message"] = "Authorization failed";
				break;
			}  /* test to register */
			if (false === ($ret = $authorization->register($username, $password, $interval, $this->M_Auth, $sessionStorage))){
				$response["message"] = "register failed";
				break;
			} /* not allowed if not an array */ 
			if (!is_array($ret)){
				break;
			}   /* update new token for this user */ 
			if (isset($sessionStorage->chat_client_updated) && $sessionStorage->chat_client_updated){
				$this->M_Auth->change($sessionStorage);
				break;
			} /* sent to resposne */
			$response['error']   = 0;
			$response['message'] = "success";
			$response['data']    = $ret;
			break;
		}
		echo json_encode($response);
		return false; 
	} 
	
	/* @brief Check Client Token to View Status  
	 * @param string  $username  username for login base on header 
	 * @param string  $password  password for login base on header  
	 * @retval mixed json response  
	 */
	 function client()
	{
		$this->headers(); /* ambil semua request yang di minta */
		$requestor = UR();  /* create new object  */
		$storage = new stdClass(); /* create new object  */ 
		$storage->response = array ( "error" => 1,  "message" => "failed",  "data" =>  false);
		$storage->username = $requestor->get('username');
		$storage->password = $requestor->get('password'); 
		
		if ((empty($storage->username)) OR (empty($storage->password))) {
			$storage->response["message"] = "Wrong Username Or Password !"; 
			echo json_encode($storage->response);
			return false;
		} 
		/* if successs data will show this */ 
		while ($this->M_Auth->client($storage->username, $storage->password, $session, $ret))
		{
			/* if true process generate then will input data */
			while (false == $ret->chat_client_auth) {   
				$authorization = sprintf("Basic %s", base64_encode(sprintf("%s:%s", $ret->chat_client_id, $ret->chat_client_pass)));
				if (false !== ($session['client_auth'] = $authorization)){  
					$this->M_Auth->update($session);
				} break;
			} /* deleted don show to user */
			if (isset($session['client_pass'])) {
				unset($session['client_pass']); 
			} /* sent to resposne */
			$storage->response['error']   = 0;
			$storage->response['message'] = "success";
			$storage->response['data'] 	  = $session;
			break;
		} 
		echo json_encode($storage->response);
		return false; 
	}
	
	/* @brief Register New Client for get Base Auth And Token process 
	 * @param string  $username  username for login base on header 
	 * @param string  $password  password for login base on header 
	 * @param integer $interval  maximum password Interval akssess 
	 * @retval mixed json resposne 
	 */
	 function register()
	{ 
		$this->headers(); /* set default response */
		$response = array("error" => 0, "message" => "failed", "data" => false);
		while (false !== ($requestor = UR())) { 
		
			/* default set all parameter */
			$username = $requestor->get("username");
			$password = $requestor->get("password");
			$interval = $requestor->get("interval");
			
			/* validation 'username' */
			if (!$username OR (empty($username))){
				$response["message"] = "username failed";
				break;
			} /* validation 'password' */
			if (!$password OR (empty($password))){
				$response["message"] = "Password failed";
				break;
			} /* check moduls 'Authorization' */ 
			if (!function_exists('Authorization')){
				$response["message"] = "moduls 'Authorization' failed";
				break;
			} /* check moduls 'Authorization' */
			if (false === ($authorization = Authorization())){
				$response["message"] = "Authorization failed";
				break;
			}  /* test to register */
			if (false === ($ret = $authorization->register($username, $password, $interval, $this->M_Auth, $sessionStorage))){
				$response["message"] = "register failed";
				break;
			} /* not allowed if not an array */ 
			if (!is_array($ret)){
				break;
			}   /* update new token for this user */ 
			if (isset($sessionStorage->chat_client_updated) && $sessionStorage->chat_client_updated){
				$this->M_Auth->change($sessionStorage);
				break;
			} /* sent response to client */
			$response["error"] = 0;
			$response["message"] = "success";
			$response["data"] = $ret; 
			break;  
		}
		echo json_encode($response);
		return false; 
	} 	
	
	/* @brief Test for Login Authorization from base URL  
	 * @param  none  none 
	 * @retval mixed json resposne  
	 */
	 function login()
	{
		$this->headers(); /* set default response */
		$resposne = array( "error" => 0, "message" => "failed", "data" => false); 
		echo json_encode($resposne);
		return false; 
	}
	
	/* @brief Base Page Aksess with empty Authorization
	 * @param  none  none 
	 * @retval mixed json resposne  
	 */
	 function index() 
	{ 
		$this->load->view("default/welcome", array());
	}
}
?>