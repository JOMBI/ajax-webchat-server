<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * Chat Class controller 
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Security
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/security.html
 */ 
 
 
 /* CHAT_LOGIN */
 if (!defined('CHAT_LOGIN')) {
	define ('CHAT_LOGIN', 1);
 } /*CHAT_LOGOUT */
 if (!defined('CHAT_LOGOUT')) {
	define ('CHAT_LOGOUT', 0); 
 } /* CHAT_READY */
 if (!defined( 'CHAT_READY')) {
	define ('CHAT_READY', 1); 
 } /* CHAT_NOTREADY */
 if (!defined( 'CHAT_NOTREADY')) {
	define ('CHAT_NOTREADY', 2);
 } /* CHAT_BUSY */
 if (!defined( 'CHAT_BUSY')) {
	define ('CHAT_BUSY', 4);
 } /* CHAT_ACTIVE */
 if (!defined( 'CHAT_ACTIVE')) {
	define ('CHAT_ACTIVE', 1);
 } /*CHAT_ONLINE */ 
 if (!defined( 'CHAT_ONLINE')) {
	define ('CHAT_ONLINE', 'Online');
 } /*CHAT_OFFLINE */ 
 if (!defined( 'CHAT_OFFLINE')) {
	define ('CHAT_OFFLINE', 'Offline');
 } /* CHAT_RETVAL_SUCCESS */ 
 if (!defined( 'CHAT_RETVAL_SUCCESS')) {
	define ('CHAT_RETVAL_SUCCESS', 1);
 } /* CHAT_RETVAL_FAILED */ 
 if (!defined( 'CHAT_RETVAL_FAILED')) {
	define ('CHAT_RETVAL_FAILED', 0);
 } /* SESSION_ROUTE_TIMEOUT */
 if (!defined('SESSION_ROUTE_TIMEOUT')){
	define ('SESSION_ROUTE_TIMEOUT', 8); /*! expired timeout session */
 } if (!defined( 'BASEDIR_DOWNLOAD' )){
	define ( 'BASEDIR_DOWNLOAD', rtrim(rtrim(rtrim(BASEPATH, "/"), "system"), "/") . DIRECTORY_SEPARATOR . rtrim(APPPATH, "/") . "/temp");
 }
 
 /* Chat */
 class Chat extends EUI_Controller 
{ 
	Const CHAT_LOGIN 	 = 1;	
	Const CHAT_LOGOUT 	 = 0;
	Const CHAT_READY 	 = 1;
	Const CHAT_NOTREADY = 2;
	Const CHAT_BUSY 	 = 4;
	Const CHAT_ACTIVE 	 = 1;
	 
	/* @brief __construct	
	 * @details Constructor default class 
	 * @header [type] [name] [description]	  
	 * @header [type] [name] [description]	  
	 * @param  [type] [name] [description]	  
	 * @retval [type] [name] [description]	  
	 */
	 
	var $auth = NULL;	
	var $http = NULL;
	var $head = NULL;
	
	/* @brief __construct
	 * @details Constructor default class  
	 * @header [type] [name] [description]	  
	 * @header [type] [name] [description]	   
	 * @param  [type] [name] [description]	  
	 * @retval [type] [name] [description]	  
	 */
	 public function __construct()
	{
		parent::__construct(); 
		session_start();
		$this->load->model(array('M_Chat', 'M_Upload')); 
	} 
	
	/* @brief headers
	 * @details set header response to client request 
	 * @retval mixed json response
	 */
	 function headers()
	{
		/* set header if incorect will exit */
		header("Access-Control-Allow-Origin: *");
		header("X-Content-Type-Options: nosniff");
		header("Content-Type: application/json;charset=UTF-8");
		header("Access-Control-Allow-Methods: POST,GET,OPTIONS");
		header("Access-Control-Max-Age: 3600");
		header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers,ApiKey, Authorization, apikey, authorization, X-Requested-With");
		return false;
	}	
	
	/* @brief index
	 * @details Main Page Controler for All process Attribute  
	 * @header string Authorization  Token registration  
	 * @header string ApiKey Basic for Login User & password 
	 * @param  dynamic URI base from HTTP Server  
	 * @retval mixed json response
	 */
	 function index()
	{
		$this->headers(); // set header if incorect will exit 
		$this->auth = Authorization(); // Authorization: Check session header from clients */
		$this->head = $this->auth->header(); // Reader: check header for authorize data process  */  
		$this->http = array("success" => 0, "message" => "expired session", "data" => false); // default response for client state   
		if (!is_object($this->head)) {
			exit (json_encode($this->http));   
		} // Authorization: if "authorization" failed */ 
		else if (is_object($this->head) &&(!$this->head->get('authorization'))) {
			$this->http["message"] = "invalid authorization";
			exit (json_encode($this->http));
		} // Authorization: check apakah token expired ? !(next : end) */
		if (false === ($this->token = $this->auth->getValidation())){
			$this->http["message"] = "invalid token"; 
			exit (json_encode($this->http));
		} // Authorization: check apakah token expired ? !(next : end) */
		if (!$this->M_Chat->validation($this->token)) {
			$this->http["message"] = "invalid token"; 
			exit (json_encode($this->http));
		} // Authorization: make new 'session' */
		if (!isset($this->Cok)) {
			$this->Cok= CK();
		} // Authorization: make new 'requestor' */
		if (!isset($this->requestor)){
			$this->Req= UR();
			$this->requestor = $this->Req; 
		} // Authorization: make new 'requestor' */
		if (!is_object($this->Req)) {
			exit (json_encode($this->http));
		} // start session 
		while (is_object($this->requestor)) {
			// get Action forward to 'e(event_handler)' */
			if (false == ($this->events = $this->requestor->get("action"))) {
				break;
				
			} // GUEST:routeAppChat  
			if (!strcmp($this->events, 'routeAppChat')) {
				/* login dari applikasi GUEST */
				$this->chatRoute($this); 
				break;
				
			} // get methode from request [GUEST:activeAppChat] 
			if (!strcmp($this->events, 'activeAppChat')) {
				$this->chatActive($this); 
				break;
				
			} // GUEST: Check Session 
			if (!strcmp($this->events, 'sessionAppChat')) {
				$this->chatSession($this); 
				break;
				
			} // GUEST: Check Session 
			if (!strcmp($this->events, 'historyAppChat')) {
				$this->chatHistory($this);
				break;
				
			} // GUEST: signout application 
			if (!strcmp($this->events, 'signoutAppChat')) {
				$this->chatSignout($this); 
				break;
				
			} // GUEST: get methode from request
			if (!strcmp($this->events, 'chatGsActive')) {
				$this->chatGuested($this); 
				break;
				
			} // GUEST:  uidAppChat  */
			if (!strcmp($this->events, 'uidAppChat')) { 
				$this->chatAppUid();	
				break;
				
			} // CRM: get methode from request [stateAppChat] */
			if ((!strcmp($this->events, 'stateAppChat'))){
				$this->chatState($this); 
				break;
				
			} // CRM: get methode from request [loginAppChat] */
			if ((!strcmp($this->events, 'loginAppChat'))) {
				$this->chatLogin($this); // lgin dari applikasi CRM :
				break;
				
			} // CRM: get methode from request [readyAppChat] */
			if (!strcmp($this->events, 'readyAppChat'))  {
				$this->chatReady($this); // Ready dari applikasi CRM :
				break;
				
			} // CRM: get methode from request [notreadyAppChat] */
			if (!strcmp($this->events, 'notreadyAppChat'))  {
				$this->chatNotReady($this); // Ready dari applikasi CRM :
				break;
				
			} // CRM: get methode from request [LogoutAppChat] */
			if (!strcmp($this->events, 'logoutAppChat')) {
				$this->chatLogout($this); // lgin dari applikasi CRM :
				break;
				
			} // CRM: get methode from request [onlineAppChat] */
			if (!strcmp($this->events, 'onlineAppChat'))  {
				$this->chatOnline($this); // lgin dari applikasi CRM :
				break;
				
			} // CRM: get methode from request [conversationAppChat] */
			if (!strcmp($this->events, 'conversationAppChat')) {
				$this->chatStartConversation($this);	
				break;
				
			} // Call this methode "startAppChat" from agents 
			if (!strcmp($this->events, 'startAppChat')) {
				$this->chatStartSession($this);	
				break;
				
			} // CRM: get methode from request [chatAppHeartbeat] */
			if (!strcmp($this->events, 'heartbeatAppChat')) {
				$this->chatHeartbeat();	
				break;
				
			} // CRM:  get methode from request [sendAppChat] */
			if (!strcmp($this->events, 'sendAppChat')) { 
				$this->chatSending();	
				break;
				
			} // AGENT|VISITOR:  uploadAppChat */
			if (!strcmp($this->events, 'onTypingAppChat')) { 
				$this->chatOnTyping($this);	
				break;
				
			} // AGENT|VISITOR:  uploadAppChat */
			if (!strcmp($this->events, 'uploadAppChat')) { 
				$this->chatUpload();	
				break;
				
			} // AGENT|VISITOR: get methode from request [closeAppChat] */
			if (!strcmp($this->events, 'closeAppChat')) {
				$this->chatClosing($this);	
				break;
				
			} // AGENT|VISITOR: get methode from request [destroyAppChat] */
			if (!strcmp($this->events, 'destroyAppChat')) {
				$this->chatDestroy();	
				break;
				
			} // AGENT
			if (!strcmp($this->events, "typingAppChat")) { 
				$this->chatSentTyping($this);	
				break;  
				
			} // AGENT|VISITOR
			if (!strcmp($this->events, "readAppChat")) {
				$this->chatRead();	
				break; 
			} // AGENT|VISITOR
			if (!strcmp($this->events, "deleteAppChat")) {
				$this->chatDeleted();	
				break; 
			} // AGENT|VISITOR
			if (!strcmp($this->events, "sendLeaveMessageChat")) {
				$this->chatLeaveMessage();	
				break; 
			} 
			// This Global Methods from "Visitor"  
			if (!strcmp($this->events, "exportVisitorChat")) {
				$this->chatVisitorExport();	
				break; 
			} 
			// This Global Methods from "chatHistory"
			if (false !== ($this->chatHistory = set_key_session('chatHistory'))) {   
				if (!isset($_SESSION[$this->chatHistory])) {
					$_SESSION[$this->chatHistory] = array();
				} 
			} // This Global Methods from "openChatBoxes" */
			if (false !== ($this->chatBoxes = set_key_session('openChatBoxes'))) { 	
				if (!isset($_SESSION[$this->chatBoxes])) {
					$_SESSION[$this->chatBoxes] = array(); 
				} 
			} break;
		} return 0;
	} 
	/* @brief read of message from by id  
	 * @param session dynamic dynamic session 	  
	 * @retval mixed none contents html 	  
	 */
	 function chatRead()
	{
		$response = array( "success" => false, "message" => "failed", "data" => false);
		$msgid = (int)getParam("msgid"); // get message id 
		while ($msgid !== ""){
			if (!$msgid){
				$response["message"] = "incorrect id";
				break;
			} // get all parameter process   
			$msgtime = false;
			$userid = (string)getMixed(array("userid", "email")); 
			$session = (string)getParam("session");
			if (!$this->M_Chat->updatesession($session)){
				$response["message"] = "Invalid session"; 
				break;
			}  // if got message of session 
			$response["data"] = array(); 
			$response["data"]["msgid"] = $msgid;
			$response["data"]["msgtime"] = 0;
			if (!$this->M_Chat->read($msgid, $userid, $msgtime)){
				$response["message"] = "update failed"; 
				break;
			} // update chat is success 
			$response["success"] = true;
			$response["message"] = "success";
			$response["data"]["msgtime"] = $msgtime; 
			break; 
		}  exit (json_encode($response));
	}
	
	/* @brief chatVisitorExport 
	 * @details chat leave message from visitor
	 * @param session dynamic dynamic session 	  
	 * @retval mixed none contents html 	  
	 */
	 function chatVisitorExport() 
	{
		$visitor = new stdclass(); /* make new object this */
		$visitor->_download = false; /* download path process */
		$visitor->_session = getParam("session", "addslashes"); /* session */
		$visitor->_client = getToken("username", "addslashes"); /* client token */
		$visitor->_userid = getParam("visitor", "addslashes"); /* nama visitor */ 
		$visitor->_email = getParam("email", "addslashes"); /* emai address  */
		$response = array( "success" => false, "message" => "failed", "data" => false );  
		while ($visitor->_client &&(strlen($visitor->_userid))) {  
			if ($this->M_Chat->exportmessage($visitor, $build_res, $build_ret)) {
				$response["data"] = array(); 
				if (CreateSession($visitor->_download)){ 
					$visitor->_download = sprintf("%s_%s.JSON", date('YmdH'), $visitor->_download);
					$response["data"]["file"] = $visitor->_download;
				} $body_content = BASEDIR_DOWNLOAD . DIRECTORY_SEPARATOR . $visitor->_download;   
				while ( @file_exists ($body_content) === false ) {
					if (false !== ( @file_put_contents($body_content, @json_encode($build_res)))) {
						$response["success"] = true;
						$response["message"] = "success"; 
						$response["data"]["url"] = rtrim(base_url(), "/") ."/index.php/Document/export/?file=". $visitor->_download; 
					} else {
						$response["message"] = "create file failed";
					} break;
				}  
			} break; 
		} exit (json_encode($response)); 
	}
	
	/* @brief chat leave message from visitor
	 * @param session dynamic dynamic session 	  
	 * @retval mixed none contents html 	  
	 */
	 function chatLeaveMessage()
	{	
		// make new object this 
		$visitor = new stdclass(); 
		$visitor->_msgid = getParam("msgid"); // get from table of message
		$visitor->_create = getParam("create"); // create now of message
		$visitor->_date = getParam("date"); // create date of message
		$visitor->_time = getParam("time"); // create time of message 
		$visitor->_session = getParam("session"); // session of message 
		$visitor->_client = getToken("username", "addslashes"); // session of message 
		$visitor->_visitor = getParam("visitor", "addslashes"); // session of message 
		$visitor->_email = getParam("email", "addslashes"); // session of message 
		$visitor->_phone = getParam("phone", "addslashes"); // session of message 
		$visitor->_message = getParam("phone", "addslashes"); // session of message 
		
		// set default for response to clients 
		$response = array("success" => false, "message" => "failed", "data" => false);  
		while ($visitor->_client && (strlen($visitor->_visitor))) { 	
			// createed unicode_decode "default with now"
			$create_date = date('Y-m-d H:i:s'); 
			if (CreateSession($visitor->_session)){
				$visitor->_create = $create_date; // full time 
				$visitor->_date = date('Y-m-d', strtotime($create_date)); // date  
				$visitor->_time = date('H:i:s', strtotime($create_date)); // time 
			} // default for response if failed
			$response["data"] = array();
			$response["data"]["msgid"] = $visitor->_msgid;
			$response["data"]["session"] = $visitor->_session;
			$response["data"]["visitor"] = $visitor->_visitor;
			$response["data"]["email"] = $visitor->_email;
			$response["data"]["phone"] = $visitor->_phone;
			$response["data"]["message"] = $visitor->_message;
			$response["data"]["create"] = $visitor->_create; 
			if ($this->M_Chat->leavemessage($visitor)){
				$response["success"] = true;
				$response["message"] = "success"; 
				$response["data"]["msgid"] = $visitor->_msgid;
				break;
			} // if proces save data success  
			break;
		} // set response
		exit (json_encode($response)); 
	}
	
	/* @brief deleted message from by id  
	 * @param session dynamic dynamic session 	  
	 * @retval mixed none contents html 	  
	 */
	 function chatDeleted()
	{ 
		$response = array( "success" => false, "message" => "failed", "data" => false);
		$msgid = (int)getParam("msgid"); // get message id 
		while ($msgid !== ""){
			if (!$msgid){
				$response["message"] = "incorrect id";
				break;
			} // get all parameter process  
			$msgtime = false;
			$userid = (string)getMixed(array("userid", "email"));
			$session = (string)getParam("session");
			if (!$this->M_Chat->updatesession($session)){
				$response["message"] = "Invalid session"; 
				break;
			} // if got message of session 
			$response["data"] = array(); 
			$response["data"]["msgid"] = $msgid;
			$response["data"]["msgtime"] = 0; 
			if ($this->M_Chat->delete($msgid, $userid, $msgtime)){
				$response["success"] = true;
				$response["message"] = "success";  
			} break;
		} exit (json_encode($response));
	}
	
	/* @brief Destroy session on chat Program 
	 * @param  session dynamic dynamic session 	  
	 * @retval mixed  none contents html 	  
	 */
	 function chatDestroy()
	{
		if (function_exists('destroy_session')) {
			destroy_session();
			session_destroy();
		} return false;
	} 
	
	/* @brief Make Chat Box Session with Agent & Visitor  
	 * @param [in] $guest Name Of Guest / Visitor 
	 * @param [in] $userid Name Of Agent / Client Application 
	 * @return Object Array Respons to JSON   
	 */
	 function chatBoxSession($visitor = NULL, $agents = NULL, &$items= "")
	{
		$chat = new stdclass();  
		$chat->items =& $items; // get default for reff process;
		$chat->boxes = false; // set default value reff 
		// if get return is false then will break process 
		if (!$this->M_Chat->getboxsession($agents, $visitor, $chat->boxes)){
			return $chat->items;
		} // set to order chat message 
		krsort ($chat->boxes); /* assign data buf then exec */
		foreach ($chat->boxes as $row){ 
			/* define all session on agent */
			$message = (string)$row["chat_message"];
			$message = (string)$this->chatSanitize($message);
			$session = (string)$row["chat_session"];
			$chatid = (int)$row["chat_data_id"];
			$chatfid = (int)$row["chat_fid"];
			$email = (string)$row["chat_email"];
			$from = (string)$row["chat_from"];
			$type = (int)$row["chat_type"];
			$sent = (string)$row["chat_sent"]; 
			$to = (string)$row['chat_to']; 
			$now = (time()-strtotime($sent));
			$docs = array();
			$files = 0;
			
			// get contains file from this chat process like this
			while ($type &&($this->M_Chat->getfile($chatfid, $file_documents))){ 
				$docs["id"]   = $file_documents["id"]; 
				$docs["cid"]  = $file_documents["chat_id"]; 
				$docs["uid"]  = $file_documents["file_uid"]; 
				$docs["name"] = $file_documents["file_name"]; 
				$docs["type"] = $file_documents["file_type"]; 
				$docs["mime"] = $file_documents["file_mime"]; 
				$docs["size"] = $file_documents["file_size"]; 
				$docs["url"]  = $file_documents["file_url"]; 
				break;
			} // convert to json_encode  
			if (sizeof($docs)){
				$files = @json_encode($docs); 
			} // from visitor to agent  
			$x = 0;
			if (!strcmp($from, $visitor)) {
				$s = 0;
				$n = $chatid;
				$j = $type;
				$i = $session;
				$f = $from;
				$g = $to;  
				$m = $message;
				$x = $files;
				$d = strtotime($sent);
				$t = sprintf('Receive at %s', date('g:iA M dS', strtotime($sent))); 
			} // from agent to visitor*/
			else if (!strcmp($from, $agents)){
				$s = 1;
				$n = $chatid;
				$j = $type;
				$i = $session;
				$f = $to; 
				$g = $from; 
				$m = $message;
				$x = $files;
				$d = strtotime($sent);
				$t = sprintf('Sent at %s', date('g:iA M dS', strtotime($sent)));
			} // sent to response process 
			$chat->items .= "{\"s\":\"{$s}\", \"n\":\"{$n}\", \"i\": \"{$i}\", \"d\":\"{$d}\", \"f\":\"{$f}\", \"g\":\"{$g}\", \"m\": \"{$m}\", \"j\":\"${j}\", \"x\": ${x}, \"t\":\"{$t}\"},";
		} return $chat->items;
	}
	
	/* @brief chatStartConversation
	 * @details  Get All chat Histry base on Username  
	 * @param [in] $me current user session 
	 * @param [in] $to to destination of chat  
	 * @return Mixed response JSON  
	 */
	function chatStartConversation($ctm = false)
	{
		$chat = new stdclass();
		$chat->client = getToken("username"); // client 
		$chat->agents = getParam("userid"); // local agents 
		/* AGENT: get active conversation */
		$response = array("error" => 1, "message" => "failed", "data" => false);
		if ($this->M_Chat->conversation($visitor, $chat->client, $chat->agents, 7200)){ 
			$response["error"] = 0;
			$response["message"] = "success";
			$response["data"] = $visitor;
		} 
		exit( json_encode($response));
		
	}

	/* @brief chatStartSession
	 * @details  Get All chat Histry base on Username  
	 * @param [in] $me current user session 
	 * @param [in] $to to destination of chat  
	 * @return Mixed response JSON  
	 */
	 function chatStartSession($ctm= false)
	{
	    $chat = new stdclass(); // guest: dummy,Budy && session_start(); 
		$chat->client = getToken("username"); // client id 
		$chat->agents = getParam("userid"); // local agents
		$chat->visitor = getArray("guest"); // visitor array 
		// AGENT:  open box session of chats && check visitor chat interaction
		$chat->items = ""; // new string akomdation
		$chat->openChatBoxes = set_key_session("openChatBoxes");
		if (is_array($chat->visitor) && (count($chat->visitor) > 0)) {
			foreach ($chat->visitor as $i => $visitor) { 
				if (!$visitor) {
					continue;
				} if ($visitor) {
					$this->chatBoxSession($visitor, $chat->agents, $chat->items);
				}
			}
		} 
		if ($chat->items != '')  {
			$chat->items = substr($chat->items, 0, -1);
		}   
		header('Content-type: application/json'); ?> 
		{
			"username"	: "<?php echo $chat->userid;?>",
			"codeuser" 	: "<?php echo $chat->userid;?>",
			"items"		: [<?php echo $chat->items;?>]
		}
		<?php exit(0);	
	}

	/* @brief chatHistory
	 * @details Get All chat Histry base on Username  Action from visitor
	 * @param [in] $me current user session 
	 * @param [in] $to to destination of chat 
	 * @return Mixed response JSON  
	 */
	 function chatHistory()
	{
		$session = new stdclass(); // new make object
		$session->response = array("success" => 0, "message" => "failed", "data" => false);
		if (false === ($session->client = getToken("username"))){
			exit (json_encode($session->response)); 
		} // validation from "visitor/agent"
		if (false === ($session->me = getParam("me"))){
			exit (json_encode($session->response)); 
		} // validation to "visitor/agent"
		if (false === ($session->to = getParam("to"))){
			exit (json_encode($session->response)); 
		} // set default global parameter 
		$session->items = ""; 
		$session->chatBoxes = array();
		$session->tsChatBoxes = set_key_session('tsChatBoxes');
		$session->chatHistory = set_key_session('chatHistory');
		$session->openChatBoxes = set_key_session('openChatBoxes');  
		// get history data from this session 
		while ($this->M_Chat->gethistory($session->client, $session->me, $session->to, $history)){
			if (is_array($history)) {
				krsort($history); 
			} foreach ($history as $chat) { 
				// convert data to object */
				$chat_id = (int)$chat["chat_data_id"];
				$chat_fid = (int)$chat["chat_fid"];
				$chat_to = (string)$chat["chat_to"];
				$chat_from = (string)$chat["chat_from"];
				$chat_type = (int)$chat["chat_type"];
				$chat_session = (string)$chat["chat_session"];
				$chat_time = (string)$chat["chat_sent"];  
				$chat_message = (string)$chat["chat_message"];
				$chat_message = (string)$this->chatSanitize($chat_message);  
				$chat_aliasfrom = (string)$chat["chat_from"];
				$chat_create = strtotime($chat_time); 
				$file_documents =  false;
				$chat_docs = array(); 
				$chat_files = 0; 
				// get contains file from this chat process like this
				while ($chat_type &&($this->M_Chat->getfile($chat_fid, $file_documents))){ 
					$chat_docs["id"]   = (int)$file_documents["id"]; 
					$chat_docs["cid"]  = (int)$file_documents["chat_id"]; 
					$chat_docs["uid"]  = (string)$file_documents["file_uid"]; 
					$chat_docs["name"] = (string)$file_documents["file_name"]; 
					$chat_docs["type"] = (string)$file_documents["file_type"]; 
					$chat_docs["mime"] = (string)$file_documents["file_mime"]; 
					$chat_docs["size"] = (string)$file_documents["file_size"]; 
					$chat_docs["url"]  = (string)$file_documents["file_url"]; 
					break;
				}
				// if documents is true will get this data;
				if (sizeof($chat_docs) > 0){
					$chat_files = json_encode($chat_docs);
				} // then will insert into session user:
				if (!strcmp($chat['chat_from'], $session->me)){
					$chat['chat_sent'] = sprintf("Sent at %s", date('g:iA M dS', $chat_create));
				} else {
					$chat['chat_sent'] = sprintf("Receive at %s", date('g:iA M dS', $chat_create));
				} if (!isset($_SESSION[$session->openChatBoxes][$chat_from]) && (isset($_SESSION[$session->chatHistory][$chat_from]))) {
					$session->items = $_SESSION[$session->chatHistory][$chat_from];
				} 
				$chat_sent = $chat['chat_sent']; 
				// 0:receive, 1:sent
				$s = 0;
				// receive
				if (!strcmp($chat_to, $session->me)){
					$s = 0;
					$f = $chat_from;
					$g = $chat_to; 
					$n = $chat_id;
					$i = $chat_session;
					$d = $chat_create;
					$m = $chat_message;
					$t = $chat_sent;
					$x = $chat_files;
					$j = $chat_type; 
				} 
				// sent  
				if (!strcmp($chat_from, $session->me)){
					$s = 1;
					$f = $chat_from;
					$g = $chat_to;
					$n = $chat_id;
					$i = $chat_session;
					$d = $chat_create;
					$m = $chat_message;
					$t = $chat_sent;
					$x = $chat_files;
					$j = $chat_type;
				} 
				$session->items .= "{\"s\":\"{$s}\", \"n\": \"{$n}\", \"i\":\"{$i}\", \"d\":\"{$d}\", \"f\":\"{$f}\", \"g\":\"{$g}\", \"m\":\"{$m}\", \"j\":\"{$j}\", \"x\":${x}, \"t\":\"{$t}\"},";
				if (!isset($_SESSION[$session->chatHistory][$chat_from])) {
					$_SESSION[$session->chatHistory][$chat_from] = "";
				} // check my session 
				$_SESSION[$session->chatHistory][$chat_from] .= "{\"s\":\"{$s}\", \"n\": \"{$n}\", \"i\":\"{$i}\", \"d\":\"{$d}\", \"f\": \"{$f}\", \"g\":\"{$g}\", \"m\":\"{$m}\", \"j\":\"{$j}\", \"x\":${x}, \"t\":\"{$t}\"},"; 
				if (isset($_SESSION[$session->tsChatBoxes][$chat_from])){
					unset($_SESSION[$session->tsChatBoxes][$chat_from]);
				} $_SESSION[$session->openChatBoxes][$chat_from] = array( "time" => $chat_time, "aliasfrom" => $chat_aliasfrom); 
			} break;
		}
		// then will get data from session delegate 
		if (!empty($_SESSION[$session->openChatBoxes])) {
			foreach ($_SESSION[$session->openChatBoxes] as $chatbox => $row ){
				$row['aliasfrom'] = $row['aliasfrom']; 
				if (!isset($_SESSION[$session->tsChatBoxes][$chatbox])) {
					$now = time() - strtotime($row['time']);
					$time = date('g:iA M dS', strtotime($row['time']));
					$message = "Sent at $time";
					if ($now > 180) { 
						$items .= "{\"s\":\"2\",\"f\":\"{$chatbox}\", \"g\":\"{$chatbox}\", \"m\":\"{$message}\"},";	 
						if (!isset($_SESSION[$session->chatHistory][$chatbox])) {
							$_SESSION[$session->chatHistory][$chatbox] = '';
						}
						$_SESSION[$session->chatHistory][$chatbox] .="{\"s\":\"2\", \"f\": \"{$chatbox}\", \"g\":\"{$chatbox}\", \"m\": \"{$message}\"},"; 
						$_SESSION[$session->tsChatBoxes][$chatbox]  = 1;
					}
				}
			}
		}  
		/* then */
		if ($session->items != '')  {
			$session->items = substr($session->items, 0, -1);
		}
		?>
		{
			"items": [
				<?php echo $session->items;?>
			]
		}
		<?php
		exit(0);	
	}

	/* @brief chatHeartbeat
	 * @details Pool Thread check message every time / with interval client process 
	 * @param [in] $client client code from API  
	 * @param [in] $userid user to check 
	 * @return Mixed response JSON  
	 */
	 function chatHeartbeat()
	{
		$session = new stdclass(); // default for response */
		$response = array("success" => 0, "message" => "failed", "data" => false);
		if (false === ($session->client = getToken("username"))){
			exit(json_encode($response));
		} // check userid (visitor|agents) */
		if (false === ($session->userid = getParam("userid"))){
			exit(json_encode($response));	 
		} // manipulasi untuk session haertbeat  
		if ($session->userid){
			set_session("UserId", $session->userid);
		} 
		// NOTE: this process for new version 
		// new performance process on set all response header of chats 
		$response = "";
		$heartbeat = false; 
		$openchatboxes = set_key_session('openChatBoxes'); 
		$chathistory = set_key_session('chatHistory');
		$tschatboxes = set_key_session('tsChatBoxes'); 
		// get heartbeat data from this session 
		while ($this->M_Chat->getheartbeat($session->client, $session->userid, $heartbeat)) { 
			foreach ($heartbeat as $chat) {
				// convert data to object 
				$chat["chat_create"] = strtotime($chat["chat_sent"]);
				$chat["AliasFrom"] = (string)$chat["chat_from"]; // then will insert into session user:
				$chat["chat_sent"] = sprintf("Receive at %s", date("g:iA M dS", strtotime($chat["chat_sent"]))); 
				// set all reff before set to response 
				$chat_id = isset($chat["chat_id"]) ? (int)$chat["chat_id"]: 0;
				$chat_fid = isset($chat["chat_fid"]) ? (int)$chat["chat_fid"]: 0;
				$chat_to = isset($chat["chat_to"]) ? (string)$chat["chat_to"]: "";
				$chat_email = isset($chat["chat_email"]) ? (string)$chat["chat_email"]: "";
				$chat_sent = isset($chat["chat_sent"])? (string)$chat["chat_sent"] : "";
				$chat_from = isset($chat["chat_from"])? (string)$chat["chat_from"] : "";
				$chat_session = isset($chat['chat_session']) ? (string)$chat['chat_session']: "";
				$chat_message = (string)$chat["chat_message"];
				$chat_message = (string)$this->chatSanitize($chat_message); 
				$chat_create = isset($chat["chat_create"])? $chat["chat_create"] : "";
				$chat_type = isset($chat["chat_type"])? 1 : 0;
				$chat_docs = array();
				$chat_files = 0; 
				// get contains file from this chat process like this
				while ($chat_type &&($this->M_Chat->getfile($chat_fid, $file_documents))){ 
					$chat_docs["id"]   = (int)$file_documents["id"]; 
					$chat_docs["cid"]  = (int)$file_documents["chat_id"]; 
					$chat_docs["uid"]  = (string)$file_documents["file_uid"]; 
					$chat_docs["name"] = (string)$file_documents["file_name"]; 
					$chat_docs["type"] = (string)$file_documents["file_type"]; 
					$chat_docs["mime"] = (string)$file_documents["file_mime"]; 
					$chat_docs["size"] = (string)$file_documents["file_size"]; 
					$chat_docs["url"]  = (string)$file_documents["file_url"]; 
					break;
				} 
				// convert to json stream 
				if (sizeof($chat_docs)){
					$chat_files = json_encode($chat_docs);
				} // get all session of chat 
				if (!isset($_SESSION[$openchatboxes][$chat_from]) && (isset($_SESSION[$chathistory][$chat_from])))  {
					$response = $_SESSION[$chathistory][$chat_from];
				} // then oput data from here:
				$response .= "{\"s\": \"0\", \"n\":\"{$chat_id}\", \"i\":\"{$chat_session}\", \"d\":\"{$chat_create}\", \"f\":\"{$chat_from}\", \"g\":\"{$chat_to}\", \"m\": \"{$chat_message}\", \"j\":\"{$chat_type}\", \"x\":{$chat_files}, \"t\":\"{$chat_sent}\"},";
				if (!isset($_SESSION[$chathistory][$chat_from])) {
					$_SESSION[$chathistory][$chat_from] = "";
				} // check my session :	
				$session_handler_response = array ("s" => 0, "n" => $chat_id, "i"=> $chat_session, "d" => $chat_create, "f" => $chat_from, "g" => $chat_to, "m" => $chat_message, "j"=> $chat_type, "x" => $chat_files, "t" => $chat_sent);
				$_SESSION[$chathistory][$chat['chat_from']] .= (string)json_encode($session_handler_response) . ",";
				if (isset($_SESSION[$tschatboxes][$chat_from])){
					unset($_SESSION[$tschatboxes][$chat_from]);
				} // check my session
				$_SESSION[$openchatboxes][$chat_from] = array('time' => $chat['sent'], 'aliasfrom' => $chat['AliasFrom']);
			} break;
		}  
		
		// then will get data from session delegate[box]
		if (!empty($_SESSION[$openchatboxes]))  {
			foreach ($_SESSION[$openchatboxes] as $from => $row){ 
				$row['aliasfrom'] = $row['aliasfrom'];
				if (!isset($_SESSION[$tschatboxes][$from])) {
					$now = time()-strtotime($row['time']);
					$time = date('g:iA M dS', strtotime($row['time']));
					$message = "Sent at $time";
					if ($now > 180) {
						$items .= "{\"s\":\"2\", \"f\":\"{$from}\", \"g\":\"{$from}\", \"m\":\"{$message}\"},";  
						if (!isset($_SESSION[$chathistory][$from])) {
							$_SESSION[$chathistory][$from] = '';
						}
						$_SESSION[$chathistory][$from] .=" {\"s\":\"2\", \"f\": \"{$from}\", \"g\": \"{$from}\", \"m\":\"{$message}\"}, ";	 
						$_SESSION[$tschatboxes][$from] = 1;
					} 
				}
			}
		} 
		// update chat to read session 
		if ($this->M_Chat->setheartbeat($session->client, $session->userid, $ret)){
			// NOTE: this for check success only 
			// if check for loger session process 
		} 
		// remove end of line 
		if ($response != '') {
			$response = substr($response, 0, -1);
		}
		?>
		{
			"items": [
				<?php echo $response;?>
			]
		}
		<?php
		exit(0);	
	}

	/* @brief Sent message from Gust / Agents  
	 * @param [in] $from from user message  
	 * @param [in] $to to sent message user 
	 * @param [in] $message text message to sent    
	 * @return Mixed response JSON  
	 */
	 function chatSending()
	{
		// create new object
		$chat 			= new stdclass(); 
		$chat->client   = getToken("username"); // client
		$chat->username = getParam("from"); // visitor|agents
		$chat->from 	= getParam("from"); // visitor|agents
		$chat->to 		= getParam("to");  // visitor|agents
		$chat->message  = getParam("message"); 
		$chat->files	= getParam("files");
		$chat->quote    = getParam("quote");
		$chat->date 	= date('Y-m-d H:i:s');
		$chat->docs	 	= 0; 	
		$chat->type 	= 0;
		$chat->uid  	= 0;
		
		// set default response && /* set session By ID 
		$this->http = array("success" => 0, "message" => "failed", "msg"=> false); 
		if (!$this->Cok->get('UserId')) {
			set_session("UserId", $chat->to); 
		} // get "box" session 
		$chat->uniqid = $this->M_Chat->sessionid($chat->client, $chat->from, $chat->to);
		if (!$chat->uniqid){
			$chat->uniqid = $this->M_Chat->sessionid($chat->client, $chat->to, $chat->from);
		} // no uniqid to set on database session 
		if (!$chat->uniqid){
			$this->http["success"] = 3; // new code expired is false;
			$this->http["message"] = "expired session";
			exit (json_encode($this->http));
			return false;
		} // set new session start 
		if (false !== ($chat->openChatBoxes = set_key_session('openChatBoxes'))) {
			$_SESSION[$chat->openChatBoxes][$chat->to]["time"] = date('Y-m-d H:i:s', time());
			$_SESSION[$chat->openChatBoxes][$chat->to]["aliasto"] = $chat->username;
		} // cleant from any char hex 
		$now = date('Y-m-d H:i:s');
		$chat->msg = $this->chatSanitize($chat->message);
		$chat->time = sprintf('Sent at %s', date('g:iA M dS', strtotime($now)));
		$chat->chatHistory = set_key_session('chatHistory');  
		if (!isset($_SESSION[$chat->chatHistory][$chat->to])) {
			$_SESSION[$chat->chatHistory][$chat->to] = '';
		} // upload file before write chat  
		if ($this->chatUpload($chat, $body_documents)){
			if ($this->M_Upload->upload($chat, $body_documents)){
				$chat->docs["id"]	= (int)$body_documents['id'];   // fil upload id on table 
				$chat->docs["cid"]	= (int)$body_documents['cid'];  // chat id 
				$chat->docs["uid"]  = (string)$body_documents["uid"];  // base uid for url get data 
				$chat->docs["name"] = (string)$body_documents["name"]; // name file description 
				$chat->docs["type"] = (string)$body_documents["type"]; // type of file 
				$chat->docs["size"] = (string)$body_documents["size"]; // size of file 
				$chat->docs["url"]  = (string)$body_documents["url"];  // base url to read or download 
			}
		} // sent message to database 
		while ($this->M_Chat->write($chat, $ret)) { 
			// after success then get return docs 
			if ($chat->docs &&(isset($chat->docs["id"]))){
				$chat->docs["cid"] = (int)$chat->uid;
			} // set on server session 
			$chat->date = strtotime($chat->date);
			$session_handler = array("s" => 1, "n" => $chat->uid, "i" => $chat->uniqid, "d" => $chat->date, "f" => $chat->to, "g" => $chat->to, "m" => $chat->msg, "j"=> $chat->type, "x" => $chat->docs, "t" => $chat->time);
			$_SESSION[$chat->chatHistory][$chat->to] .= (string)json_encode($session_handler) . ",";
			// create last time chat 
			$chat->tsChatBoxes = set_key_session('tsChatBoxes'); 
			if (isset($_SESSION[$chat->tsChatBoxes][$chat->to])){
				unset($_SESSION[$chat->tsChatBoxes][$chat->to]);
			} // set response for guest callback 
			$this->http["msg"] = array();
			$this->http["success"] = 1;
			$this->http["message"] = "success"; // message
			$this->http["msg"]["s"] = 1; // send 
			$this->http["msg"]["n"] = $chat->uid; // uid 
			$this->http["msg"]["i"] = $chat->uniqid;  // uniqid 
			$this->http["msg"]["d"] = $chat->date;  // date of chat 
			$this->http["msg"]["j"] = $chat->type; // type text|file 
			$this->http["msg"]["f"] = $chat->from; // from send 
			$this->http["msg"]["g"] = $chat->to; // chat to 
			$this->http["msg"]["m"] = $chat->msg; // message 
			$this->http["msg"]["x"] = $chat->docs; // docs if exits 
			$this->http["msg"]["t"] = $chat->time;  // time of chat 
			$this->http["msg"]["q"] = (int)$chat->quote; // quote of chat  
			break;
		} // sent data process on here
		exit( json_encode($this->http) );

	}
	
	/* @brief Closing chat  
	 * @param [in] $CI Global Object framework  
	 * @return Mixed response JSON  
	 */
	 function chatClosing($ctm = NULL)
	{
		$response = array("success" => 0, "message" => "failed", "data"=> array()); 
		while (false !== ($agents = getMixed(array("userid", "email")))){ 
			// get detail for user chating  
			$session = getMixed(array("chatbox", "session"));
			if (!strlen($session)){
				$response["message"] = "invalid chat session";
				break;
			} // get detail process 
			if (false === ($chatbox = set_key_session("openChatBoxes"))){
				$response["message"] = "invalid box session";
				break;
			} // get detail of session 
			if (isset($_SESSION[$chatbox])) {
				unset($_SESSION[$chatbox][$session]);
			} // get detail for checksum  
			$response["data"]["userid"]  = $agents;
			$response["data"]["chatbox"] = $session; 
			$response["message"] = "failed, close session";	
			if ($this->M_Chat->close($session, $agents)) {
				$response["success"] = 1;
				$response["message"] = "success";	
			} break;
		} // return transfer 
		exit (json_encode($response)); 
	}
	
	/* @brief Sanitize fro html content Of text   
	 * @param [in] $message to user on active chat    
	 * @return Mixed response JSON  
	 */
	 function chatSanitize($message = NULL )
	{
		$message = str_replace(array("'", "\\"), array("\"", "\\\\"),$message);
		$message = htmlspecialchars($message, ENT_QUOTES);
		$message = str_replace("\n\r","\n",$message);
		$message = str_replace("\r\n","\n",$message);
		$message = str_replace("\n","<br>",$message);
		return (string)$message;
	}

	/* @brief Start Sessio  from Guest / Public 
	 * @details test for get state online process 
	 * @param [in] $client to user on active chat  
	 * @param [in] $user user on check 
	 * @param [in] none  
	 * @return Mixed response JSON  
	 */
	 function chatSession($ctm = NULL)
	{
		session_start();
		$ctm = new stdClass(); /* default response from server */
		$ctm->response = array("success"  => 0,  "message"  => "failed", "data" => false); 
		/* get all parameter from here && handler chrome tidak bisa crossite */
		$ctm->to = CK()->get("GuestChat") ? CK()->get("GuestChat") : getParam("to");
		$ctm->from = CK()->get("GuestName") ? CK()->get("GuestName") : getParam("from");
		$ctm->email = CK()->get("GuestEmail") ? CK()->get("GuestEmail") : getParam("email"); 
		$ctm->uniqid = CK()->get($ctm->email) ? CK()->get($ctm->email): getParam("session");
		$ctm->datenow = strtotime('now');
		
		/* get uniqid for check first */
		if ($ctm->uniqid) {
			$ctm->response["data"] = array();
			$ctm->response["success"] = 1;
			$ctm->response["message"] = "success";
			$ctm->response["data"]["update"] = $ctm->datenow;
			$ctm->response["data"]["session"] = $ctm->uniqid;
			$ctm->response["data"]["email"] = $ctm->email;
			$ctm->response["data"]["from"] = $ctm->from;
			$ctm->response["data"]["to"] = $ctm->to; 
		} /* response on JSON values */
		exit (json_encode($ctm->response));
	}

	/* @brief test for get state online process  
	 * @param [in] $client to user on active chat  
	 * @param [in] $user user on check 
	 * @param [in] none  
	 * @return Mixed response JSON  
	 */
	 function chatOnline($ctm = NULL)
	{ 
		$chat = new stdClass();
		$chat->user = getParam("user");
		$chat->status = getParam("status");
		$chat->client = getToken("username"); 
		$chat->data = array(); 
		$chat->data[0] = array (
			'chat_user' => 'ROOT',
			'chat_client' => 'C001',
			'chat_status' =>  1
		); 
		exit (json_encode($chat->data));
	}

	/* @brief Check User Atcive Or Not Active like "Online" 
	 * @param [in] $to to user on active chat  
	 * @return Mixed response JSON 
	 * @details No more details
	 */ 
	 function chatGuested($ctm = NULL)
	{
		$session = new stdclass();
		$session->guest = getParam("guest");
		$session->userid = getParam("userid");
		$session->client = getToken("username"); 
		
		// check data from separator this object */
		$session->response = array("success" => 0, "data" => false);
		if (false == ($session->users = @explode(",", $session->guest) )) {
			exit (json_encode($session->response));
		} // get data from this */
		$session->retval = $this->M_Chat->guestactive($session->users, $session->userid, $session->client); 
		if (((is_array($session->retval) && count($session->retval)) ? true : false)) {
			$session->response["success"] = 1;
			$session->response["data"] = $session->retval;
		}  // get data from this */
		exit (json_encode($session->response));	
	}

	/* @brief Check User Atcive Or Not Active like "Online" 
	 * @param [in] $to to user on active chat  
	 * @return Mixed response JSON 
	 * @details No more details
	 */ 
	 function chatActive($ctm = NULL)
	{
		$o = new stdclass(); 
		$o->to = getParam("to");
		$o->client  = getToken("username"); 
		$o->message = CHAT_OFFLINE;
		$o->success = CHAT_RETVAL_FAILED; 
		/* set default response */
		$o->res 	 = array (
			'success' 	=> $o->success, 
			'to' 		=> $o->to, 
			'message' 	=> $o->message 
		);
		
		/*  Pool with interval Process set on Client process */ 
		if ( $this->M_Chat->active($o->to, $o->client) )
		{
			$o->res[ 'success' ] = CHAT_RETVAL_SUCCESS;
			$o->res[ 'to' ] = $o->to;
			$o->res[ 'message' ] =  CHAT_ONLINE; 
		}
		/*! return back for response */
		exit (json_encode($o->res));
	}
	
	/* @brief chatOnTyping
	 * @details Chat Event "OnTyping" from Guest Or Agent 
	 * @param [in] $from form user typing 
	 * @param [in] $user user identification  
	 * @param [in] $to to user on active chat  
	 * @return Mixed  
	 * @retval mixed return
	 */
	 function chatOnTyping($ctm = NULL)
	{
		$session = new stdclass();
		$session->_response = array ("error" => 1, "to" => $session->_to, "typing" => 0);
		if (false === ($session->_cid = getToken("username"))){
			exit(json_encode($session->_response));
		} // get data from request 
		$session->_from = getParam("from"); // local user channel 
		$session->_type = getParam("user");	// type visitor|agents 
		$session->_to = getParam("to");  // remote user channel   
		// default response from server  
		while ($session->_cid &&($this->M_Chat->gettyping($session->_cid, $session->_to, $session->_type, $get))){
			$session->_response["typing"] = $get["chat_user_typing"];
			$session->_response["error"] = 0;
			break; 
		}
		// response json 
		exit (json_encode($session->_response));
	}
	
	/* @brief chatSentTyping
	 * @details Chat Event set typing from (Agent to Vistor) usage webhook session
	 * @param [in] $from form user typing 
	 * @param [in] $user user identification  
	 * @param [in] $to to user on active chat  
	 * @retval mixed return   
	 */
	 function chatSentTyping($ctm = null )
	{
		$storage = new stdclass();
		$storage->_cid = getToken("username"); 
		$storage->_from = getParam("from"); // user current typing 
		$storage->_to = getParam("to");	// user to info in typing
		$storage->_session = getParam("session"); // get current session 
		$storage->_typing = getParam("typing");	 // type typing = 1|0
		// set response data process on here like this 
		$response = array("success" => false, 
		/* "event" => "typing", */ "message" => "failed", "data" => false );  
		// get from this "visitor|agent"
		while ($storage->_cid &&(strlen($storage->_typing))) { 
			$response["data"] = array();
			$response["data"]["event"] = "typing";
			$response["data"]["session"] = $storage->_session;
			$response["data"]["from"] = $storage->_from;
			$response["data"]["to"] = $storage->_to;
			$response["data"]["typing"] = $storage->_typing;
			// invalid current session 
			if (!$this->M_Chat->updatesession($storage->_session)){
				$response["message"] = "Invalid session"; 
				break;
			} // successfuly  
			if ($this->M_Chat->settyping($storage->_cid, $storage->_session, $storage->_to, $storage->_agent, $storage->_typing)){
				$response["success"] = true;
				$response["message"] = "success"; 
			} break; 
		} // sent response to client request */
		exit (json_encode($response));
	}
	
	/**
	 * @brief chatWorkhour
	 * @details check jam kerja beradasrakn clients
	 * @param [in] $obj dump Of Process 
	 * @return Mixed   
	 */
	 function chatWorkhour($ctm = NULL)
	{
		// check hari kerja
		// check hari libur 
		// check jam kerja 
		return true;
	}

	/**
	 *  @brief chatRoute
	 *  @details Chat Route from Guest Then Will destroy of session  
	 *  @param [in] $obj dump Of Process 
	 *  @return Mixed  
	 *  @details No more details
	 */
	 function chatRoute($ctm = NULL)
	{
		$localStorage = new stdclass(); 
		$localStorage->response= array("success" => 0, "message" => "failed", "data" => false, "from" => "", "to" => "");
		$localStorage->email   = getParam("email"); // visitor: email 
		$localStorage->visitor = getParam("from");  // visitor: username
		$localStorage->agents  = false; // get routing agents 
		$localStorage->uniqid  = false; // get routing uniqid 
		
		// check if request handler is successfuly 
		while($ctm->requestor){ 
			// create session for new started 
			if (function_exists( "CreateSession" )) {
				CreateSession($localStorage->uniqid);
			} // if user get request not required 
			if	(!$localStorage->visitor OR $localStorage->visitor === "") {
				$localStorage->response["message"] = "failed identify";
				break;
			} // if user get request not required 
			if	(!$localStorage->email OR $localStorage->email === "") {
				$localStorage->response["message"] = "failed identify";
				break;
			}  
			// NOTE: 
			// Untuk Uniqid akan di check dari session dulu 
			// kalau belum ada akan di generate yang baru kalau sudah ada ambil dari session  saja 
			$localStorage->session = 0; // not found 
			$sessionid = CK()->get($localStorage->email) ? CK()->get($localStorage->email) : $ctm->requestor->get("session");
			if ($sessionid) {
				$localStorage->uniqid = $sessionid; 
				$localStorage->session ++;  /* kondisi jika user ini pernah punya session */
			} // if success Guest insert / update then starting Routing for get Agent Ready */
			if ($localStorage->founds = $this->M_Chat->guest($localStorage->visitor, $localStorage->email, $ctm->token->username, $localStorage->uniqid)){
				$localStorage->router = false;	
			}  
			// Jika Visitor / Guest Yang melakukan request pernah meliki session chat dengan user agent maka
			// Agent tetap Akan di Ambil yang seblumnya tanpa melakukan Routing Ulang 
			// dengan Mengambil nilai Session sebelumnya  
			if (!$localStorage->session){
				$localStorage->router = $this->M_Chat->routing($localStorage->visitor, $ctm->token->username);
			} // Ambil session dari nila sebelumnya */
			else if ($localStorage->session){
				$localStorage->router = $this->M_Chat->routing($localStorage->visitor, $ctm->token->username, $localStorage->uniqid);
			} // starting manipulation process Routing */
			$datenow = strtotime('now'); $uniqid = $localStorage->agents = false; 
			while (!$uniqid) {
				/* copy to new object */
				$uniqid = $localStorage->uniqid; 
				$email  = $localStorage->email;
				$client = $ctm->token->username; 
				/* if response check route not an array */ 
				if (!$localStorage->router) {
					$this->M_Chat->guestend($localStorage->visitor, $ctm->token->username, $localStorage->email);
					break;
				} /* if an array check of size */
				if (is_array($localStorage->router) && !sizeof($localStorage->router)) {
					$this->M_Chat->guestend($localStorage->visitor, $ctm->token->username, $localStorage->email);
					break;
				} /* then will process data */ 
				foreach ($localStorage->router as $visitor => $agents) {	
					/* set default values */
					$localStorage->visitor = (string)$visitor;
					$localStorage->agents = (string)$agents;
					if (false == ($nick = $this->M_Chat->nickname($agents))){
						continue;
					} // then will update this */ 
					$localStorage->response["data"] = array(); 
					if ($this->M_Chat->insertbox($client, $uniqid, $agents, $visitor, $email))  {
						$localStorage->response["success"] = 1; 
						$localStorage->response["message"] = "success";
					} // old version */
					$localStorage->response["session"] = $uniqid;
					$localStorage->response["update"] = $datenow;
					$localStorage->response["from"] = $visitor;
					$localStorage->response["nick"] = $nick;
					$localStorage->response["to"] = $agents;
					// new version */
					$localStorage->response["data"]["session"] = $uniqid;
					$localStorage->response["data"]["update"] = $datenow;
					$localStorage->response["data"]["from"] = $visitor;
					$localStorage->response["data"]["nick"] = $nick;
					$localStorage->response["data"]["to"] = $agents;
				} 
				// break level top:2
				break;
			} 
			// break level top:1 
			break;
		}  
		// jika berhasil set session hanya untuk Guest Saja 
		if ($localStorage->agents &&($localStorage->uniqid)) {   
			session_start();
			// set session by uniqid exceptions "email" is "unique"
			if ($localStorage->emai){
				set_session($localStorage->email, $localStorage->uniqid); 
			} // set other reff 
			set_session("Uniqid", $localStorage->uniqid); 
			set_session("GuestId", $localStorage->uniqid); 
			set_session("GuestName", $localStorage->visitor); 
			set_session("GuestChat", $localStorage->agents);
			set_session("GuestEmail", $localStorage->email);  
		}  
		// return Respons for User reuested 
		exit (json_encode($localStorage->response)); 
	}
	
	/* @brief Signout from Guest Then Will destroy of session 
     * @details No more details
	 * @return Mixed  
	 */
	 function chatSignout($obj = NULL)
	{
		$session = new stdclass();  
		$session->uid = getParam("uid");
		$session->from = getParam("from");
		$session->client = getToken("username");  
		$session->user = CK()->get('GuestName') ? CK()->get('GuestName'): getParam("from");
		$session->email = CK()->get('GuestEmail') ? CK()->get('GuestEmail') : getParam("email");
		$session->uniqid = CK()->get($session->email) ? CK()->get($session->email) : getParam("session");
		// reset sesion and destroy table */
		$session->response = array("success" => 0,"message" => "failed","data"=> false); 
		if ($session->uniqid){
			$this->M_Chat->guestend($session->user, $session->client, $session->email, $session->uniqid); 
			$session->response["data"] = array();
			$session->response["success"] = 1;
			$session->response["message"] = "success";
			$session->response["data"]["uid"] = $session->uniqid;
			$session->response["data"]["from"] = $session->user;
			$session->response["data"]["email"] = $session->email;
			$session->response["data"]["update"] = strtotime('now'); 
			if (isset($_SESSION)){
				destroy_session();
			}  
		} exit (json_encode($session->response));
	}
	
	/* @brief chatState from Guest Then Will destroy of session  
	 * @param [in] $obj dump Of Process 
	 * @return Mixed  
	 * @details No more details
	 */
	 function chatState($uri = NULL)
	{ 
		$session = new stdclass();
		$session->userid = (string)getParam("userid");
		$session->states = (int)getParam("states");
		$session->client = (string)getToken("username");
		/* set default response */
		$session->response  = array("success" => 0, "message" => "state invalid", "data" => false); 
		while ($session->client) {
			/* if false process */
			if (false === ($get = $this->M_Chat->state($session->userid, $session->client, $session->states))){
				break;
			} /* sent message data to process */
			$session->response["success"] = 1;
			$session->response["message"] = "success";
			$session->response["data"] = array();
			$session->response["data"]["userid"] = $session->userid;
			$session->response["data"]["client"] = $session->client;
			$session->response["data"]["states"] = $session->states; 
			break; 
		} exit (json_encode($session->response));
	}

	/* @brief Signout from Guest Then Will destroy of session  
	 * @param [in] $obj dump Of Process 
	 * @return Mixed  
	 * @details No more details
	 */
	 function chatLogout($obj = NULL)
	{ 
		$session = new stdclass();
		$session->userid = getParam("userid");
		$session->client = getToken("username");
		$session->status = Chat::CHAT_LOGOUT;
		// then sent insert into DB:
		$session->response = array( "success" => 0, "message" => "failed", "data" => false);
		if (!$session->client) { 
			exit (json_encode($session->response));
		} // then if client is contract promise with server:
		else if ($session->client){
			while ( false !== ($get = $this->M_Chat->logout($session->userid, $session->client, $session->status))){
				$session->response["success"] = 1;
				$session->response["message"] = "success";
				$session->response["data"] = array (
				"client" => $session->client,
				"userid" => $session->userid );
				break;
			}	 
		} exit (json_encode($session->response));
		
	}

	/* @brief Signout from Guest Then Will destroy of session  
	 * @param [in] $obj dump Of Process 
	 * @return Mixed  
	 * @details No more details
	 */
	 function chatLogin($obj = NULL )
	{
		$session = new stdclass();
		$session->client = getToken("username");
		$session->userid = getParam("userid");
		$session->status = Chat::CHAT_LOGIN;
		/* then sent insert into DB */
		$session->response = array( "success" => 0, "message" => "invalid", "data" => false); 
		if (!$session->client) {
			$session->response["message"] = "wrong authorize";
			exit (json_encode($session->response));
		} /* then if client is contract promise with server */
		while ($session->client && ($this->M_Chat->login($session, $get))){ 
			/* get result detail of agent login */
			if (!is_object($get)){
				break;
			} /* set detail for response data */
			$session->response["success"] = 1;
			$session->response["message"] = "success";
			$session->response["data"] = array (
			"client"   => $get->get("chat_user_client"),
			"userid"   => $get->get("chat_user_kode"),
			"update"   => $get->get("chat_user_update"),
			"username" => $get->get("chat_user_name"));
			/* update session */
			if ($get->get("chat_user_id")){
				set_session( "UserId",  $get->get("chat_user_kode"));
			} break; 
		} exit (json_encode($session->response));
	}
	 
	/* @brief Signout from Guest Then Will destroy of session  
	 * @param [in] $obj dump Of Process 
	 * @return Mixed  
	 * @details No more details
	 */
	 function chatReady($obj = NULL )
	{ 
		$session = new stdclass();
		$session->userid = getParam("userid");
		$session->client = getToken("username");
		$session->status = Chat::CHAT_LOGIN;
		
		// then sent insert into DB:
		$session->response = array("success" => 0, "message" => "failed", "data" => false);
		if (!$session->client) { 
			exit (json_encode($session->response)); 
		} // then if client is contract promise with server:
		else if ($session->client){
			while (false !== ($get = $this->M_Chat->ready($session))){
				$session->response["success"] = 1;
				$session->response["message"] = "success";
				$session->response["data"] = array (
				"client" => $session->client,
				"userid" => $session->userid );	
				break;
			} 
		} exit (json_encode($chat->data));
	} 

	/* @brief chatNotReady
	 * @details Signout from Guest Then Will destroy of session  
	 * @param [in] $obj dump Of Process 
	 * @return Mixed  
	 * @details No more details
	 */
	 function chatNotReady($obj = NULL )
	{ 
		$session = new stdclass();
		$session->userid = getParam("userid");
		$session->client = getToken("username");
		$session->status = Chat::CHAT_LOGIN; 
		// then sent insert into DB:
		$session->response = array("success" => 0, "message" => "failed", "data" => false);
		if (!$session->client) { 
			exit (json_encode($session->response));
		} // then if client is contract promise with server:
		else if ($session->client){
			while (false !== ($get = $this->M_Chat->notready($session))){
				$session->response["success"] = 1;
				$session->response["message"] = "success";
				$session->response["data"] = array (
				"client" => $session->client,
				"userid" => $session->userid ); 
				break;
			}
		} exit (json_encode($session->response));
	} 

	/* @brief chatUpload
	 * @details Signout from Guest Then Will destroy of session  
	 * @param [in] $obj dump Of Process 
	 * @return Mixed  
	 * @details No more details
	 */
	 function chatUpload(&$chat = false, &$body_documents = false)
	{
		// default for set directory  
		if (!defined( "BASEUPLOAD" ) ){
			define( 'BASEUPLOAD', rtrim(rtrim(rtrim(BASEPATH,'/'),'system'),'/'). "/application/upload");
		} // get base directory 
		
		$basedir = BASEUPLOAD; 
		if (!is_dir($basedir)){
			return false;
		} // base url to open and download file 
		$baseurl = rtrim(base_url(), "/") ."/index.php/Document/file"; 
		$danger_documents = array("php", "exe", "cgi", "sh", "js", "py", "html", "htaccess", "c");
		$body_documents = array(); 
		while ($chat){ 
			if (!isset($chat->files)){
				$chat->files = false;
				break;
			} // next to step process;
			if (!strlen($chat->files)){
				$chat->files = false;
				break;
			} // if is not array but is strings 
			if (false === ($body_documents = json_decode($chat->files, true))){
				$chat->files = false;
				break;
			} // this unique file rename  
			list($type, $data) = explode(';', $body_documents["data"]);
			list(, $data) = explode(',', $data);
			// custom file for save on local 
			$body_filetype = false;
			$body_default_type = "txt";
			$body_data = base64_decode($data);  
			$body_mimetype = $body_documents["mime"];
			$body_filename = $body_documents["name"];
			if ($info = @pathinfo($body_filename)){
				$body_filetype = strtolower($info["extension"]);	
			} // validation file type is danger 
			$body_upload_filename = $body_documents["name"];
			if (in_array($body_filetype, $danger_documents)){
				if (false !== ($body_index = @strpos($body_upload_filename, "."))){
					$body_documents["name"] = sprintf("%s.%s", substr($body_upload_filename, 0, $body_index), $body_default_type);
				} else {
					$body_documents["name"] = sprintf("%s.%s", $body_upload_filename, $body_default_type);
				} $body_filetype = $body_default_type;
			} else {
				$body_filetype = $body_default_type;
				$body_documents["name"] = sprintf("%s.%s", $body_upload_filename, $body_default_type);
			} 
			$body_tmpid = "DOC_". strtotime('now') ."_". md5(uniqid());
			$body_pathinfo = $basedir . DIRECTORY_SEPARATOR . $body_tmpid .".". $body_filetype; 
			$body_documents["path"] = $basedir;
			if (false === ( @file_put_contents($body_pathinfo, $body_data))){
				$chat->files = false;
				break;
			}  
			$body_documents["uid"] = @file_exists($body_pathinfo) ? $body_tmpid : false;
			$body_documents["size"] = @filesize($body_pathinfo);
			if (false !== ($body_pathinfos = @pathinfo($body_pathinfo))){ 
				$body_documents["hash"] = $body_pathinfos["basename"];
			} 
			$body_buildurls = @http_build_query(array("action" => "download", "id" => $body_tmpid, "type" => $body_filetype, "name" => $body_filename), "", "&");
			$body_pathurls = $baseurl ."/?". $body_buildurls; 
			$body_documents["url"] = $body_pathurls; 
			$body_documents["type"] = $body_filetype;
			break;
		} // remove base asci 64 
		unset($body_documents["data"]);
		if (false !== ($ret = isset($body_documents["uid"]))){
			$chat->docs = array();
		} return $ret;
	} 

	/* @brief chatUpload
	 * @details Signout from Guest Then Will destroy of session  
	 * @param [in] $obj dump Of Process 
	 * @return Mixed  
	 * @details No more details
	 */
	 function chatAppUid()
	{
		$session = new stdclass();
		$session->uid = getParam("uid");
		$session->qid = strtotime("now");  
		$session->response = array("success"=> 0, "message" => "failed", "msg" => false);
		while (false !== ($get = $this->M_Chat->createtmp($session->qid, $session->uid))) {
			$session->response["success"] = 1;
			$session->response["message"] = "success";
			$session->response["msg"] = array ("uid" => $session->qid); 
			break;
		}	
		exit (json_encode($session->response));
	}
	
}
?>