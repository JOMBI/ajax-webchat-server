<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * M_Desktop Class controller 
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Security
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/security.html
 */  
class M_Desktop extends EUI_Model {
	private static $Instance = NULL;
	/* @brief Instance   
	 * @details no description
	 * @param none none description
	 * @retval mixed return 
	 */ 
	 public static function &Instance() 
	{
		if (is_null(self::$Instance)){
			self::$Instance = new self();
		} return self::$Instance; 
	} 
	
	/* @brief desktop_register_clients   
	 * @details no description
	 * @param "desktop_disable_feture" reff callback
	 * @param "client" string client_id | cid
	 * @retval mixed return 
	 */ 
	 function desktop_register_clients(&$desktop_register_clients = 0, $client= false)
	{
		$build_str = sprintf("select count(chat_client_id) as jml ". 
		"from pds_chat_client where chat_client_kode = '%s'", addslashes($client));
		if ($build_qry = $this->db->query($build_str)) {
			if ($build_ret = $build_qry->result_first_assoc()){
				$desktop_register_clients = (int)$build_ret["jml"];
			}
		} return $desktop_register_clients;
	}
	
	/* @brief desktop_disable_feture   
	 * @details no description
	 * @param "desktop_disable_feture" reff callback
	 * @param "client" string client_id | cid
	 * @retval mixed return 
	 */ 
	 function desktop_disable_feture(&$desktop_disable_feture = false, $client= false)
	{
		$desktop_disable_feture = array(); 
		$this->db->reset_select();
		$build_str = "select * from pds_chat_disabled";
		if ($client){
		$build_str .= sprintf(" where client= '%s'", addslashes($client));
		} if ($build_qry = $this->db->query($build_str)) { 
			foreach ($build_qry->result_assoc() as $row) {
				$cid = isset($row["client"]) ? $row["client"]: false;
				if (!$cid) {
					continue;
				} // disabled buttons property_exists  
				$buttons_disabled_files		 = xor_boolean($row, "files"	 , true);  
				$buttons_disabled_expression = xor_boolean($row, "expression", true); 
				$buttons_disabled_photo 	 = xor_boolean($row, "photo"	 , true);  
				$buttons_disabled_dictate 	 = xor_boolean($row, "dictate"	 , true);  
				$buttons_disabled_audiocalls = xor_boolean($row, "calls"	 , true);  
				$buttons_disabled_videocalls = xor_boolean($row, "video"	 , true);  
				$buttons_disabled_deleted 	 = xor_boolean($row, "deleted"	 , true);  
				$buttons_disabled_download 	 = xor_boolean($row, "download"	 , true);  
				$buttons_disabled_exports 	 = xor_boolean($row, "exports"	 , true);  
				$buttons_disabled_refresh 	 = xor_boolean($row, "refresh"	 , true);  
				$buttons_disabled_preview 	 = xor_boolean($row, "preview"	 , true);  
				$buttons_disabled_playing 	 = xor_boolean($row, "playing"	 , true);  
				$buttons_disabled_quotes 	 = xor_boolean($row, "quotes"	 , true); 
				$buttons_disabled_console 	 = xor_boolean($row, "console"	 , true);
				$buttons_disabled_leavemsg 	 = xor_boolean($row, "leavemsg"	 , true);
				
				// settings buttons property_exists 
				$buttons_settings_files 	 = xor_default($row, "", array());
				$buttons_settings_expression = xor_default($row, "", array());
				$buttons_settings_photo 	 = xor_default($row, "", array());
				$buttons_settings_dictate 	 = xor_default($row, "", array());
				$buttons_settings_audiocalls = xor_default($row, "", array());
				$buttons_settings_videocalls = xor_default($row, "", array());
				$buttons_settings_deleted 	 = xor_default($row, "", array());
				$buttons_settings_download 	 = xor_default($row, "", array());
				$buttons_settings_exports 	 = xor_default($row, "", array());
				$buttons_settings_refresh 	 = xor_default($row, "", array()); 
				$buttons_settings_preview 	 = xor_default($row, "", array()); 
				$buttons_settings_playing 	 = xor_default($row, "", array()); 
				$buttons_settings_quotes 	 = xor_default($row, "", array()); 
				$buttons_settings_console 	 = xor_default($row, "", array()); 
				$buttons_settings_leavemsg 	 = xor_default($row, "", array()); 
				
				// buttons files 
				$desktop_disable_feture[$cid]["files"] = array();
				$desktop_disable_feture[$cid]["files"]["disabled"] = $buttons_disabled_files;
				$desktop_disable_feture[$cid]["files"]["settings"] = $buttons_settings_files; 
				
				// expression 
				$desktop_disable_feture[$cid]["expression"] = array();
				$desktop_disable_feture[$cid]["expression"]["disabled"] = $buttons_disabled_expression;
				$desktop_disable_feture[$cid]["expression"]["settings"] = $buttons_settings_expression; 
				
				// photo 
				$desktop_disable_feture[$cid]["photo"] = array();
				$desktop_disable_feture[$cid]["photo"]["disabled"] = $buttons_disabled_photo;
				$desktop_disable_feture[$cid]["photo"]["settings"] = $buttons_settings_photo; 
				
				// dictate 
				$desktop_disable_feture[$cid]["dictate"] = array();
				$desktop_disable_feture[$cid]["dictate"]["disabled"] = $buttons_disabled_dictate;
				$desktop_disable_feture[$cid]["dictate"]["settings"] = $buttons_settings_dictate; 
				
				// calls
				$desktop_disable_feture[$cid]["calls"] = array();
				$desktop_disable_feture[$cid]["calls"]["disabled"] = $buttons_disabled_audiocalls;
				$desktop_disable_feture[$cid]["calls"]["settings"] = $buttons_settings_audiocalls; 
				
				// video
				$desktop_disable_feture[$cid]["video"] = array();
				$desktop_disable_feture[$cid]["video"]["disabled"] = $buttons_disabled_videocalls;
				$desktop_disable_feture[$cid]["video"]["settings"] = $buttons_settings_videocalls; 
				
				// deleted
				$desktop_disable_feture[$cid]["deleted"] = array();
				$desktop_disable_feture[$cid]["deleted"]["disabled"] = $buttons_disabled_deleted;
				$desktop_disable_feture[$cid]["deleted"]["settings"] = $buttons_settings_deleted; 
				
				// download
				$desktop_disable_feture[$cid]["download"] = array();
				$desktop_disable_feture[$cid]["download"]["disabled"] = $buttons_disabled_download;
				$desktop_disable_feture[$cid]["download"]["settings"] = $buttons_settings_download; 
				
				// exports
				$desktop_disable_feture[$cid]["exports"] = array();
				$desktop_disable_feture[$cid]["exports"]["disabled"] = $buttons_disabled_exports;
				$desktop_disable_feture[$cid]["exports"]["settings"] = $buttons_settings_exports; 
				
				// refresh
				$desktop_disable_feture[$cid]["refresh"] = array();
				$desktop_disable_feture[$cid]["refresh"]["disabled"] = $buttons_disabled_refresh;
				$desktop_disable_feture[$cid]["refresh"]["settings"] = $buttons_settings_refresh;  
				
				// preview
				$desktop_disable_feture[$cid]["preview"] = array();
				$desktop_disable_feture[$cid]["preview"]["disabled"] = $buttons_disabled_preview;
				$desktop_disable_feture[$cid]["preview"]["settings"] = $buttons_settings_preview;  
				
				// playing
				$desktop_disable_feture[$cid]["playing"] = array();
				$desktop_disable_feture[$cid]["playing"]["disabled"] = $buttons_disabled_playing;
				$desktop_disable_feture[$cid]["playing"]["settings"] = $buttons_settings_playing;  
				
				// quotes
				$desktop_disable_feture[$cid]["quotes"] = array();
				$desktop_disable_feture[$cid]["quotes"]["disabled"] = $buttons_disabled_quotes;
				$desktop_disable_feture[$cid]["quotes"]["settings"] = $buttons_settings_quotes;  
				
				// console
				$desktop_disable_feture[$cid]["console"] = array();
				$desktop_disable_feture[$cid]["console"]["disabled"] = $buttons_disabled_console;
				$desktop_disable_feture[$cid]["console"]["settings"] = $buttons_settings_console;  
				
				// leavemsg
				$desktop_disable_feture[$cid]["leavemsg"] = array();
				$desktop_disable_feture[$cid]["leavemsg"]["disabled"] = $buttons_disabled_leavemsg;
				$desktop_disable_feture[$cid]["leavemsg"]["settings"] = $buttons_settings_leavemsg;  
				
			}
		} return $desktop_disable_feture; 
	}
	
	/* @brief desktop_enable_button   
	 * @details no description
	 * @param "desktop_enable_button" reff callback
	 * @param "client" string client_id | cid
	 * @retval mixed return 
	 */ 
	 function desktop_enable_button(&$desktop_enable_button = false, $client= false)
	{
		$desktop_enable_button = array(); 
		$this->db->reset_select();
		$build_str = "select * from pds_chat_buttons";
		if ($build_qry = $this->db->query($build_str)) { 
			foreach ($build_qry->result_assoc() as $row) {
				$desktop_enable_button[$row["buttons"]] = xor_boolean($row, "enabled", false); 
			}
		} return $desktop_enable_button;
	}
	
	/* @brief desktop_image_settings   
	 * @details no description
	 * @param "desktop_image_settings" reff callback
	 * @param "client" string client_id | cid
	 * @retval mixed return 
	 */ 
	 function desktop_image_settings(&$desktop_image_settings = false, $client= false)
	{
		$desktop_image_settings = array(); 
		$this->db->reset_select();
		$build_str = "select * from pds_chat_images ";
		if ($client){
			$build_str .= sprintf("where client= '%s'", addslashes($client));
		} if ($build_qry = $this->db->query($build_str)) { 
			foreach ($build_qry->result_assoc() as $row) {
				$desktop_image_settings[$row["client"]]["css"] = xor_default($row, "css", false);
				$desktop_image_settings[$row["client"]]["img"] = xor_default($row, "img", false); 
			}
		} return $desktop_image_settings;
	}
	
	/* @brief desktop_engine_settings   
	 * @details get default engine settings for chat 
	 * @param "desktop_engine_settings" reff callback
	 * @param "envi" string client_id | cid
	 * @retval mixed return 
	 */ 
	 function desktop_engine_settings(&$desktop_engine_settings = false, $envi= "production")
	{
		/**
		$desktop_engine_settings = array(); 
		$this->db->reset_select();
		$build_str = "select * from pds_chat_engine ";
		if ($envi){
			$build_str .= sprintf("where envi= '%s'", addslashes($envi));
		} if ($build_qry = $this->db->query($build_str)) { 
			foreach ($build_qry->result_assoc() as $row) {
				$desktop_engine_settings[$row["envi"]]["directory"] = xor_default($row, "path", "");
				$desktop_engine_settings[$row["envi"]]["sock"] = xor_default($row, "socket", ""); 
			}
		} */
		return $desktop_engine_settings;
	} 
	
	/* @brief desktop_engine_clients   
	 * @details get default engine settings for chat 
	 * @param "desktop_engine_settings" reff callback
	 * @param "envi" string client_id | cid
	 * @retval mixed return 
	 */
	 function desktop_engine_clients(&$desktop_engine_clients = false, $client= false)
	{
		$this->db->reset_select();
		$desktop_engine_clients = array();
		$desktop_engine_clients["development"] = array();
		$desktop_engine_clients["production"] = array(); 
		// started check by client register 
		$build_str = sprintf( "select production, development from pds_chat_settings ". 
		"where client= '%s'", addslashes($client));
		if (false === ($build_qry = $this->db->query($build_str))){
			return $desktop_engine_clients;
		} // then get by code reff 
		foreach ($build_qry->result_assoc() as $ret) {
			foreach ($ret as $envi => $code){ 
				$build_str = sprintf( "select engine_name, engine_value from pds_chat_engine ". 
				"where engine_code ='%s'", addslashes($code));
				if (false === ($build_qry = $this->db->query($build_str))){
					continue;
				} // get all engine by code user 
				foreach ($build_qry->result_assoc() as $engine){ 
					// directory 
					if (!strcmp(xor_default($engine, "engine_name", ""), "path")){
						$desktop_engine_clients[$envi]["directory"] = xor_default($engine, "engine_value", "");
					} // visitor
					if (!strcmp(xor_default($engine, "engine_name", ""), "visitor")){
						$desktop_engine_clients[$envi]["sock"] = xor_default($engine, "engine_value", "");
						$desktop_engine_clients[$envi]["visitor"] = xor_default($engine, "engine_value", "");
					} // agent 
					if (!strcmp(xor_default($engine, "engine_name", ""), "agent")){
						$desktop_engine_clients[$envi]["agent"] = xor_default($engine, "engine_value", "");
					}
				}
			} 
		} return $desktop_engine_clients;
	}
	
}	