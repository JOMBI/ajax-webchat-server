<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Chat Class controller 
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Security
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/security.html
 */  
class M_Guest extends EUI_Model {
	/* @brief Instance   
	 * @details no description
	 * @param none none description
	 * @retval mixed return 
	 */ 
	private static $Instance = NULL;
	/* @brief Instance   
	 * @details no description
	 * @param none none description
	 * @retval mixed return 
	 */ 
	 public static function &Instance()
	{
		if (is_null(self::$Instance)){
			self::$Instance = new self();
		} return self::$Instance; 
	}
	
	/* @brief Construct 
	 * @details no description
	 * @param none none description
	 * @retval mixed return 
	 */
	function __construct(){ } 
	
	/* @brief clients 
	 * @details no description
	 * @param none none description
	 * @retval mixed return 
	 */
	function clients($cid= "", $host = "", &$ret= 0)
	{
		$this->db->reset_select();
		$this->db->select("chat_client_kode, chat_client_host, ".
		"chat_client_token, chat_client_auth", false);
		
		$this->db->from("pds_chat_client"); 
		// get and bindec 
		if (false === ($get = $this->db->get())){
			return 0;
		} // get number 
		if ($get->num_rows() <= 0){
			return 0;
		} // get result of array 
		$ret = array(); $x = 0;
		foreach ($get->result_assoc() as $row){
			// get kode if exist 
			if (false === ($cid = $row["chat_client_kode"])){
				continue;
			} // host 
			$ret[$cid]["ca"] = $row["chat_client_host"]; 
			// auth 
			$ret[$cid]["cb"] = $row["chat_client_auth"];
			// token 
			$ret[$cid]["cc"] = $row["chat_client_token"]; 
			// session 
			$ret[$cid]["cd"] = "ctm_public_client_session_". $cid; 
			// files 
			$ret[$cid]["ce"] = "ctm_public_client_files_". $cid; 
		} return $ret;
	}
	
	/* @brief auth 
	 * @details no description
	 * @param none none description
	 * @retval mixed return 
	 */
	function auth($cid= "", $host = "", &$ret= 0)
	{
		$this->db->reset_select();
		$this->db->select("chat_client_kode, chat_client_token, chat_client_auth", false);
		$this->db->from("pds_chat_client");
		$this->db->where("chat_client_kode", $cid);
		$this->db->where("chat_client_host", $host);
		// get and bindec 
		if (false === ($get = $this->db->get())){
			return 0;
		} // get number 
		if ($get->num_rows() <= 0){
			return 0;
		} // get result of array 
		if (false !== ($ret = (array)$get->result_first_assoc())){
			$ret["chat_client_session"] = sprintf("ctm_publish_client_session_%s_%s", $ret["chat_client_kode"], date('Ym')); 
			$ret["chat_client_files"] = sprintf("ctm_publish_client_files_%s_%s", $ret["chat_client_kode"], date('Ym'));
		} return $ret;
	}
}
?>