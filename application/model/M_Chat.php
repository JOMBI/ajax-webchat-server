<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Chat Class controller 
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Security
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/security.html
 */  
class M_Chat extends EUI_Model {
	
	 private static $Instance = NULL;
	/* @brief Instance   
	 * @details no description
	 * @param none none description
	 * @retval mixed return 
	 */ 
	 public static function &Instance()
	{
		if (is_null(self::$Instance)){
			self::$Instance = new self();
		} return self::$Instance; 
	}
	
	/* @brief Construct 
	 * @details no description
	 * @param none none description
	 * @retval mixed return 
	 */
	 function __construct(){ }
	
	/* @brief sessionid
	 * @details get session from room conversation
	 * @param string "guest" User Guested 
	 * @param string "agent" User Agents  
	 * @return mixed Uniqueid 
	 */
	 function sessionid($client= false, $visitor = NULL, $agents = NULL)
	{
		$sessionid = 0; 
		$sql = sprintf(
		"select b.user_uniqid as uniquid from pds_chat_serv b ".
		"where b.user_id=( ".
		"select max(a.user_id) as id from pds_chat_serv a ". 
		"where a.user_client= '%s' ".
		"and a.user_guest='%s' ".
		"and a.user_agent='%s' ".
		"and a.user_status=1 )", 
		$client, $visitor, $agents);
		writeloger($sql, "EVENT SESSION");
		$qry = $this->db->query($sql);
		if ($qry &&($qry->num_rows() > 0)) 
		foreach ($qry->result_assoc() as $get){
			$sessionid = isset($get["uniquid"]) ? $get["uniquid"] : 0;
		} return $sessionid;
	}  
	
	/* @brief validation
	 * @details Validation User Token & Auth 
	 * @param [in] $requestor userid 
	 * @return mixed data  
	 */
	 function validation($requestor = false)
	{
		/*! then result on boolean only */	
		$chat = new stdClass();  
		$chat->condition = 0;
		
		/* invalid object process */
		if (!is_object($requestor)) {
			return $chat->condition;
		}   
		/* get data from source database */
		$this->db->reset_select();
		$this->db->select("count(chat_client_id) as total", false);
		$this->db->from("pds_chat_client");
		$this->db->where("chat_client_kode" , $requestor->username);
		$this->db->where("chat_client_pass" , $requestor->password);
		$this->db->where("chat_client_token", $requestor->usertoken);
		$this->db->where("(unix_timestamp(chat_client_expired) - unix_timestamp(now())) > 0", null, false);
		$qry = $this->db->get(); 
		if ($qry &&($qry->num_rows()>0) &&($get = $qry->result_first_assoc())) {
			$condition = isset($get["total"]) ? $get["total"]: 0;
			if ($condition) {
				$chat->condition ++;
			}
		}  /* write to loger if expired of token */ 
		if (!$chat->condition){
			writeloger(sprintf("Validation client.kode=%s client.password = %s client.token= %s", 
			$requestor->username, $requestor->password, $requestor->usertoken));
		} /* then will result data */
		return (int)$chat->condition;  
	}

	/* @brief Signout from Guest/ Vistor User  
	 * @param [in] $userid userid
	 * @param [in] $client client API 
	 * @param [in] $email email from session 
	 * @param [in] $uniqid session id  
	 * @return mixed data  
	 */
	 function guestactive ($guest = NULL, $agent= NULL, $client = NULL)
	{
		$resultArray = array();
		$this->db->reset_select();
		$this->db->select("a.chat_user_kode, a.chat_user_status", false);
		$this->db->from("pds_chat_guest a");
		$this->db->join("pds_chat_serv b", "a.chat_user_uniquid=b.user_uniqid", "INNER");
		$this->db->where("a.chat_user_update >= subdate(now(), 1)", '', false);
		/* if have guest */
		if (is_array($guest) &&(count($guest)>0)){
			$this->db->where_in("a.chat_user_kode", $guest);
		} /* if have client session */
		if (!is_null($client)){
			$this->db->where("a.chat_user_client", $client);
		} /* get data */
		$qry = $this->db->get();
		if ($qry &&($qry->num_rows() > 0)) 
		foreach($qry->result_assoc() as $row ){
			$resultArray[] = array(
				'user' => $row['chat_user_kode'], 
				'status' => $row['chat_user_status']
			);
		} /* testing OK */
		return (array)$resultArray;
	}	

	/* @brief guestend
	 * @details Signout from Guest/ Vistor User  
	 * @param string $userid userid
	 * @param string $client client API 
	 * @param string $email email from session 
	 * @param string $uniqid session id  
	 * @retval mixed return  
	 */
	 function guestend($userid = NULL, $client = NULL, $email = NULL, $uniqid = NULL)
	{
		/* update session and stop */ 
		if ($uniqid) {
			$this->db->reset_write();
			$this->db->set("user_status", 0);
			$this->db->set("user_close", "now()", false);
			$this->db->where("user_uniqid", $uniqid);
			if ($this->db->update("pds_chat_serv")){
				writeloger(sprintf("Guest end process uniqueid = %s", $uniqid), "EVENT CLOSE(0)");
			}
		}  
		// new update 2024 where by "session.id"
		if ($client) {
			$this->db->reset_write(); 
			$this->db->set("chat_user_status"   , 0); 
			$this->db->set("chat_user_update"   , "now()", false);
			$this->db->where("chat_user_client" , $client);
			$this->db->where("chat_user_kode"   , $userid);	
			$this->db->where("chat_user_email"  , $email);
			$this->db->where("chat_user_uniquid", $uniqid); 
			if ($this->db->update("pds_chat_guest") 
			&& ($this->db->affected_rows() > 0))  {
				return $userid;
			}
		} return 0;
	}

	/* @brief guest
	 * @details Handling visitor to state 
	 * @param string $userid userid
	 * @param string $client client API 
	 * @param string $email email from session 
	 * @param string $uniqid session id  
	 * @retval mixed return  
	 */
	 function guest(&$userid = NULL, $email = NULL, $client = NULL, $uniqid = NULL, &$visitors = false, &$retval = 0)
	{
		$datenow = date('Y-m-d H:i:s'); /* local varibale */ 
		$this->db->reset_write(); /* update if success then will get */
		$where = array("chat_user_client" => $client, "chat_user_email" => $email);
		$update = array("chat_user_update" => $datenow, "chat_user_uniquid" => $uniqid, "chat_user_status" => CHAT_ACTIVE); 
		$this->db->update("pds_chat_guest", $update, $where); /* get callback for response login session */
		while ($client){
			/* jika process update berhasil */
			if ($this->db->affected_rows() > 0){
				$this->db->reset_select(); 
				$this->db->where("chat_user_client", $client);
				$this->db->where("chat_user_email" , $email);
				if ($get = $this->db->get("pds_chat_guest")){
					if ($visitors = Reader($get->result_first_assoc())){
						$userid = $visitors->get("chat_user_kode");
						$retval = $visitors->get("chat_user_id");
					}
				} break;
			}
			/* user terindikasi sebagai user baru */
			$this->db->reset_write();
			$insert_array = array (
				"chat_user_client" 	 => $client,
				"chat_user_kode" 	 => $userid,
				"chat_user_name" 	 => $userid,
				"chat_user_email" 	 => $email,
				"chat_user_uniquid"  => $uniqid,
				"chat_user_createts" => $datenow,
				"chat_user_status" 	 => CHAT_ACTIVE 
			); 
			$this->db->insert("pds_chat_guest", $insert_array);
			if ($this->db->affected_rows() > 0){
				$chat_user_id = $this->db->insert_id();
				$insert_array["chat_user_id"] = $chat_user_id; 
				if ($visitors = Reader($insert_array)){
					$retval = $visitors->get( "chat_user_id" );
				} 
			} break; 
		} return $retval; 
	} 

	/* @brief nickname
	 * @details Get Nikname from userid Guest Or Agents  
	 * @param  object  $userkode process   
	 * @retval mixed return
	 */
	 function nickname($userkode = NULL)
	{	
		$nickname = NULL;
		$sql = sprintf (
		"select chat_user_name as nickname ". 
		"from pds_chat_user where chat_user_kode= '%s'", $userkode);
		$qry = $this->db->query($sql);
		if ($qry &&($qry->num_rows()>0)) 
		foreach ($qry->result_assoc() as $row ){
			$nickname = $row['nickname'];
		}
		return (string)$nickname;
	}	
							 
	/* @brief routing
	 * @details Pencarian Agent di Applikasi (Routing) Dari Guest  
	 * @param object $from process 
	 * @param object $clientid process 
	 * @param object $session  process  
	 * @retval mixed return 
	 */
	 function routing(&$from = "", $clientid = NULL, $session= 0)
	{
		/* set all parameter */
		$routing = new stdclass(); 
		$routing->session = $session;
		$routing->datas   = array(); 
		$routing->route   = array();
		
		/* AGENT: Belum memiliki session, This Process Normal Route get Agents */
		if (!$routing->session)
		{
			/* NOTE: 20220922 
			 * Jika Waktu (now()- login) >= 24 jam skip process 
			 */  
			$sql = sprintf(
			"select a.chat_user_kode as userid, ".
			"floor(((now() - a.chat_user_update)/3600)/24) as _timeout ". 
			"from pds_chat_user a ". 
			"where a.chat_user_status > 0 ". 
			"and a.chat_user_client = '%s' ". 
			"order by a.chat_user_update asc ". 
			"limit 1", $clientid);  
			  
			$qry = $this->db->query($sql);
			if ($qry &&($qry->num_rows())) foreach ($qry->result_assoc() as $row){ 
				/* get Storage from this section */
				$user_id = isset($row['userid']) ? $row['userid'] : false; /* get user id */
				$user_timeout = isset($row['_timeout']) ? $row['_timeout'] : 0; /* get timeout */ 
				/* NOTE: jika nilai (timeout <= session_route_timeout) */
				if ($user_id &&($user_timeout <= SESSION_ROUTE_TIMEOUT)){
					$routing->datas[$user_id] = $user_id; 
				} /* timeout > session_route_timeout */
				else if ($user_id &&($user_timeout > SESSION_ROUTE_TIMEOUT)){ 
					/* NOTE: update ke database menjadi logout */
					if ($this->logout($user_id, $clientid, CHAT_LOGOUT)){ 
						/* NOTE: process loger */
						writeloger(sprintf( "SESSION_ROUTE_TIMEOUT(%d) = %s", $user_timeout, $user_id), "EVENT ROUTING"); 
					} 	
				}
			} 
			
		} /* AGENT: Sudah memiliki session sebelumnya */
		else if ($routing->session) { 
			$sql =  sprintf( 
			"select b.chat_user_kode as userid, ". 
			"a.user_guest as guest_code from pds_chat_serv a ".
			"left join pds_chat_user b on a.user_agent = b.chat_user_kode ".
			"where a.user_uniqid = '%s'", $session);
			
			$qry = $this->db->query($sql);
			if ($qry &&( $qry->num_rows() )) foreach($qry->result_assoc() as $row ){
				$routing->datas[$row['userid']] = $row['userid']; 
			}
		} 
		/* for Route loger state */
		writeloger($sql, "EVENT ROUTING"); /* After Routing is success */
		if (is_array($routing->datas) && sizeof($routing->datas)) {
			/* Ada kondisi dimana ketika Guest  sedang chat tiba2 agent logout */
			$routing->user_status = false;
			while (false !== ($routing->kode = end($routing->datas))){  
				/* user is logout from session before answered message */
				if (false == ($routing->user_status = $this->userstate($routing->kode, $clientid))){
					$routing->route[$from]= $routing->kode; /* agent kode from db: */
					writeloger($routing->kode, sprintf("EVENT ROUTING STATE(%d)", CHAT_LOGOUT));
					break;
					
				} /* user is still connected with server not notice */
				$sql = sprintf (
				"update pds_chat_user set chat_user_status=%d, chat_user_update= now() ".
				"where chat_user_client= '%s' and chat_user_kode= '%s' ".
				"and chat_user_status > 0", Chat::CHAT_BUSY, $clientid, $routing->kode);
				if ($this->db->query($sql) &&($this->db->affected_rows() > 0)) { 
					$routing->route[$from]= $routing->kode; /* agent kode from db: */
					if ($routing->kode){ /*! catat setiap process yang terjadi */
						writeloger($routing->kode, sprintf("EVENT ROUTING STATE(%d)", CHAT_BUSY));
					}
				} break;	
			} 
		}  /*! return of data to response */
		return $routing->route;
	}	
	
	/* @brief state
	 * @details state from CRM Or Agents Application  
	 * @param object  $userid process 
	 * @param object  $client process 
	 * @param object  $status process  
	 * @retval mixed return
	 */
	 function state($userid = NULL, $client= NULL, $status = 0)
	{
		$sql = sprintf(
		"update pds_chat_user set chat_user_status = %d ".
		"where chat_user_client= '%s' ".
		"and chat_user_kode = '%s'", $status, $client, $userid);
		writeloger($sql, sprintf("EVENT STATE(%d)", $status));
		return $this->db->query($sql);
	}

	/* @brief logout
	 * @details Logout from CRM Or Agents Application  
	 * @param object  $userid process 
	 * @param object  $client process 
	 * @param object  $status process  
	 * @retval mixed return
	 */
	 function logout($agents = NULL, $client= NULL, $status = 0, &$ret= 0)
	{
		// NOTE: Update on master user identified by client & agent 
		// session from login session 
		//
		$this->db->reset_write(); 
		$this->db->set('chat_user_status', 0);
		$this->db->set('chat_user_typing', 0);
		$this->db->set('chat_user_update', 'now()', false);
		$this->db->where('chat_user_client', $client);
		$this->db->where('chat_user_kode', $agents);  
		$this->db->update('pds_chat_user'); 
		
		// NOTE: Check apakah ketika melakukan logout masih / sedang 
		// handle visitor  
		//
		$this->db->reset_write();
		$this->db->set('user_status', 0);
		$this->db->set('user_close', 'now()', false);
		// $this->db->set('user_datets', 'now()', false); /* fixed bugs */
		$this->db->where('user_status', 1); // in active only 
		$this->db->where('user_client', $client);
		$this->db->where('user_agent', $agents); 
		$this->db->update('pds_chat_serv');
		
		// write to loger 
		$ret++;
		writeloger("Agent {$agents} Sent Signal Logout", "EVENT STATE(0)");
		return $ret;  
	}
	
	/* @brief conversation
	 * @details get active conversation on started session 
	 * @param object $userid process 
	 * @param object $client process   
	 * @retval mixed return
	 */
	 function conversation(&$ret= 0, $client =false, $userid = false, $expired= 7200)
	{
		$ret = array();
		$sql = sprintf( 
		"select ".
		"a.user_uniqid as `session`, ".
		"b.chat_user_client AS `channel`,".
		"b.chat_user_name as `name`, ".
		"b.chat_user_email as email, ".
		"b.chat_user_status as `status`, ". 
		"unix_timestamp(b.chat_user_update) as `update` ".
		"from pds_chat_serv a ".
		"left join pds_chat_guest b on a.user_uniqid=b.chat_user_uniquid ".
		"where a.user_client = '%s' ".
		"and a.user_agent = '%s' ".
		"and a.user_status = 1 ".
		"and (unix_timestamp(now()) - unix_timestamp(a.user_update)) <= %d", 
		$client, $userid, $expired );
		
		/* get build source query is false */
		if (false === ($bind = $this->db->query($sql))){
			return 0;
		} /* if get result data success */
		if ($bind->num_rows()) foreach ($bind->result_assoc() as $visitor){
			$ret[$visitor["session"]] = $visitor;
		} /* check of return*/
		return (is_array($ret) && sizeof($ret)) ? $ret : 0; 
	}
	

	/* @brief active
	 * @details Active from CRM Or Agents Application  
	 * @param object $userid process 
	 * @param object $client process   
	 * @retval mixed return
	 */
	 function active($userid = NULL, $client = 0)
	{
		$ret = 0; /* build of query data process */
		$sql = sprintf("select count(chat_user_kode) as total from pds_chat_user where chat_user_kode='%s' and chat_user_status>0 and chat_user_client='%s'", $userid, $client);
		$qry =  $this->db->query($sql);
		if ($qry &&($get = $qry->result_first_assoc())) {
		   $ret = (int)$get['total'];
		} return $ret;
	}

	/* @brief userstate
	 * @details get user state by client & user  
	 * @param object $userid process 
	 * @param object $client process   
	 * @param reff $ret integer callback
	 * @retval integer value 
	 */
	 function userstate($userid = NULL, $client = 0, &$ret = 0)
	{
		$sql = sprintf("select chat_user_status from pds_chat_user ". 
		"where chat_user_kode= '%s' and chat_user_client='%s'", $userid, $client);
		$qry = $this->db->query($sql);
		if ($qry &&($get = $qry->result_first_assoc())) {
		   $ret = (int)$get['chat_user_status'];
		} return $ret;
	}

	/* @brief write
	 * @details Accepted Sent Message from Vistor OR Agents 
	 * @param object $s process 
	 * @param integer $retval return
	 * @retval mixed values 
	 */
	 function write(&$chat = NULL, &$ret = 0)
	{
		// chat_uid_return */ 
		if (!defined('CHAT_UID_RETURN') ){
			define ('CHAT_UID_RETURN', 0);
		} // invalid chat "object" */
		if (!is_object($chat)){
			return 0;
		} // invalid chat "from" */
		if (!isset($chat->from) && strlen($chat->from) < 1){
			return 0;
		} // invalid chat "from" */
		if (!isset($chat->to)){
			return 0;
		} // check ui 
		if (!isset($chat->uid)){
			$chat->uid = CHAT_UID_RETURN;
		} // new add quote 
		if (!isset($chat->quote)){
			$chat->quote = 0;
		} // check if message contains message  
		$chat->uid =& $ret; // reff return callback
		$chat->type = 0; // type dokument 
		$chat->files = 0; // file id from chat files 
		if ($chat->docs){
			$chat->files = isset($chat->docs["id"]) ? $chat->docs["id"] : 0; 
		} 
		// jika data type docs 
		if ($chat->docs &&($chat->files)){
			$chat->type = 1;
		} 
		// default insert Id  set data for UTF-8 like Icon generator */
		$chat->uid = CHAT_UID_RETURN; 
		$chat->message = $this->db->escape_real_string($chat->message); 
	 
		
		// set data for UTF-8 like */
		$this->db->db_set_charset('UTF-8', 'utf8_general_ci');
		
		// start to insert process 
		$this->db->reset_write();  
		$this->db->set("chat_data_session", $chat->uniqid);
		$this->db->set("chat_data_from"	  , $chat->from);
		$this->db->set("chat_data_to"	  , $chat->to);
		$this->db->set("chat_data_msg"	  , $chat->message); 
		$this->db->set("chat_data_client" , $chat->client);
		$this->db->set("chat_data_create" , $chat->date);
		$this->db->set("chat_data_type"	  , (int)$chat->type);
		$this->db->set("chat_data_files"  , (int)$chat->files);
		$this->db->set("chat_data_quote"  , (int)$chat->quote);
		$this->db->insert("pds_chat_data"); 
		
		// get return "affected_rows" 
		if ($this->db->affected_rows() > 0) {
			$chat->uid = $this->db->insert_id();
			if ($chat->files){
				$this->M_Upload->chatid($chat->uid, $chat->files);
			}
		} // get uniqid id from this session 
		if ($chat->uniqid){  
			$this->db->reset_write();
			$this->db->set("user_update", "now()", false);
			$this->db->where("user_uniqid", $chat->uniqid);
			$this->db->update("pds_chat_serv"); 
		} // then set of return this;
		return $chat->uid;
	}
	
	/* @brief read
	 * @details read chat message from guest / visitor 
	 * @param string  $from agent from apps 
	 * @param string  $guest user from public domain 
	 * @retval mixed values 
	 */
	 function read($msgid= 0, $userid= 0, &$read = false)
	{
		$this->db->reset_write(); // build query update string
		$build_str = sprintf("update pds_chat_data ".
		"set chat_data_read=1, chat_data_update= now() ".
		"where chat_data_read=0 and chat_data_id= %d", $msgid); 
		if (false !== ($build_ret = $this->db->query($build_str))){
			if ($this->db->affected_rows() > 0){
				$read = strtotime('now');
				return $msgid;
			}
		} return 0;
	}
	
	/* @brief delete
	 * @details delete chat message from guest / visitor 
	 * @param string  $from agent from apps 
	 * @param string  $guest user from public domain 
	 * @retval mixed values 
	 */
	 function delete($msgid= 0, $userid= 0, &$updated = false)
	{
		$this->db->reset_write(); // build query update string
		$build_str = sprintf("update pds_chat_data ".
		"set chat_data_delete=1, chat_user_delete= '%s', chat_time_delete= now() ".
		"where chat_data_id= %d", addslashes($userid), $msgid); 
		if (false !== ($build_qry = $this->db->query($build_str))){
			if ($this->db->affected_rows() > 0){
				$updated = strtotime('now');
				return $msgid;
			}
		} return 0;
	}
	
	/* @brief complete
	 * @details complete chat will insert to this table 
	 * @param string "session" agent from apps 
	 * @param string "userid" user from public domain 
	 * @retval mixed values 
	 */
	 function complete($session = NULL, $userid = NULL, &$build_ret= 0)
	{
		$build_str = sprintf( "insert into pds_chat_close (`session`, userid, `status`, created) ". 
		"values ('%s', '%s', 0, now()) ", $session, $userid);
		if ($this->db->query($build_str) &&($this->db->affected_rows() > 0)){
			++ $build_ret;
		} return $build_ret;
	}
	
	/* @brief close
	 * @details Close Sent Message from Vistor OR Agents 
	 * @param string $from agent from apps 
	 * @param string $guest user from public domain 
	 * @retval mixed values 
	 */
	 function close($session = NULL, $userid = NULL, &$build_ret= 0)
	{
		/* NOTE: 
		 * untuk sementara proces force close dari local channel / agent di hiden 
		 * dulu  harus di buat syncron dengan method di sisi visitor 
		 * next auto compalaints   
		 */ 
		$this->db->reset_write();
		$build_str = sprintf ("update pds_chat_serv set user_status= 0, user_close= now() ".
		"where user_uniqid= '%s'", $session);  // force close 
		if ($this->db->query($build_str) && ($this->db->affected_rows() > 0)){
			$build_ret ++; // insert into table "pds_chat_close"   
			if ($this->complete($session, $userid)){
				$build_ret ++; 
			}
		} if ($build_ret){
			writeloger("close session {$session} by local channel {$userid} success", "EVENT CLOSE(0)"); 
		} else {
			writeloger("close session {$session} by local channel {$userid} failed", "EVENT CLOSE(0)");
		} return $build_ret; 
	}	

	/* @brief login
	 * @details login Sent Message from Vistor OR Agents 
	 * @param object  $data process 
	 * @retval mixed values 
	 */
	 function login($requestor = false, &$ret = false)
	{
		/* Add New object */ 
		$response = 0; 	/* set default for check process */
		if (!isset($requestor->datenow)){
			$requestor->datenow = date('Y-m-d H:i:s');
		} /* write loger process */
		writeloger(sprintf( "AGENT: %s", $requestor->userid),"EVENT STATE(3)");
		while ($requestor->userid) {  
			/* updated data from selection login process  */
			$this->db->reset_write();
			$this->db->set("chat_user_status"  , $requestor->status);
			$this->db->set("chat_user_update"  , $requestor->datenow);
			$this->db->where("chat_user_client", $requestor->client);
			$this->db->where("chat_user_kode"  , $requestor->userid);
			$this->db->update("pds_chat_user");
			if ($this->db->affected_rows() > 0){ 
				$response ++;
				break;
			}  
			/* If  agent not register will setup No Registered */
			$this->db->reset_write(); 
			$this->db->set("chat_user_status", $requestor->status);
			$this->db->set("chat_user_client", $requestor->client);
			$this->db->set("chat_user_kode"  , $requestor->userid);
			$this->db->set("chat_user_name"  , $requestor->userid);
			$this->db->set("chat_user_update", $requestor->datenow);
			$this->db->insert("pds_chat_user"); 
			if ($this->db->affected_rows() > 0){ 
				$response ++;
			}  /* stop go to end process */
			break;
		}
		/* get details for check all process */
		if ($response &&(!$this->agent($requestor->userid, $requestor->client, $ret))){
			return 0;
		} return $ret; 
	}	

	/* @brief ready
	 * @details Ready Sent Message from Vistor OR Agents 
	 * @param  object  $data process 
	 * @retval mixed values 
	 */
	 function ready($requestor = false)
	{
		$ret = 0; /* return check */
		$sql = sprintf("update pds_chat_user a ".
		"set a.chat_user_status=1, a.chat_user_update= now() ".
		"where a.chat_user_kode = '%s' and a.chat_user_client='%s'", 
		$requestor->userid, $requestor->client); 
		
		$bind = $this->db->query($sql);  
		writeloger($sql, "EVENT STATE(1)");
		if ($bind &&($this->db->affected_rows() > 0)){
			$ret ++; 
		} return $ret;
	} 

	/* @brief notready
	 * @details NotReady Sent Message from Vistor OR Agents 
	 * @param  object  $data process 
	 * @retval mixed values 
	 */
	 function notready($requestor = false)
	{
		/* build to bind query */
		$sql = sprintf("update pds_chat_user a ". 
		"set a.chat_user_status=2, a.chat_user_update= now() ". 
		"where a.chat_user_kode = '%s' and a.chat_user_client='%s'", 
		$requestor->userid, $requestor->client); 
		
		$bind = $this->db->query($sql);
		writeloger($sql, "EVENT STATE(2)");
		if ($bind &&($this->db->affected_rows() > 0)){	
			return true;
		} return false;
		
	} 

	/* @brief createtmp
	 * @details Create Tmp Sent Message from Vistor OR Agents 
	 * @param  object  $data process 
	 * @retval mixed values 
	 */
	 function createtmp($session = "", $values = "")
	{
		$this->db->reset_write();
		$this->db->set("session" , $session);
		$this->db->set("value"	 , $values);
		$this->db->set("createts", 'now()', false);
		$this->db->set("expired" , 0);
		if ($this->db->insert("pds_chat_tmp") 
		&& ($this->db->affected_rows() > 0)){
			return $this->db->insert_id(); 
		} return 0;
	}
	
	/* @brief gettmp 
	 * @details get details user by client session 
	 * @param  object "session" process 
	 * @retval mixed values 
	 */
	 function agent($userid = false, $client = NULL, &$ret = false)
	{
		$this->db->reset_select();
		$this->db->where("chat_user_kode", $userid); 
		$this->db->where("chat_user_client", $client);
		if (false !== ($get = $this->db->get("pds_chat_user")))
		{   if ($get->num_rows() > 0){
				$ret = Reader($get->result_first_assoc());
			}
		} return $ret; 
	}
	
	/* @brief gettmp 
	 * @details gettmp Sent Message from Vistor OR Agents 
	 * @param  object "session" process 
	 * @retval mixed values 
	 */	
	 function gettmp($session = "")
	{ 
		$get = array();
		$uid = @mysql_real_escape_string($session);
		$sql = sprintf("select * from pds_chat_tmp where session='%s' and expired = 0", $uid);
		$res = $this->db->query($sql);
		if (($res->num_rows() > 0) &&($get = $res->result_first_assoc())){
			$get = (array)$get;
			if (is_array($get) &&(sizeof($get) > 0)) {
				$update = array("expired" => 1);
				$wheres = array("session" => $get["session"]); 
				$this->db->reset_write();
				$this->db->update('pds_chat_tmp', $update, $wheres);
			}
		} /* return get data */
		return sizeof($get) > 1 ? $get : 0;
	}

	/* @brief getfile
	 * @details Get File Sent Message from Vistor OR Agents 
	 * @param object  $data process 
	 * @retval mixed values 
	 */		
	 function getfile($id= 0, &$get= false)
	{ 
		$get = array(); /* get Uid data to download process */ 
		if (!$id){
			return 0;
		} /* Build Query data from database */
		$sql = sprintf("select * from pds_chat_files where id=%d", $id);  
		$res = $this->db->query($sql);  
		if ($res &&($res->num_rows())){
			$get = $res->result_first_assoc();
		} /* return of data if success */
		if (sizeof($get)){
			return $get;
		} return 0;
	} 
	
	/* @brief gettyping
	 * @details get typing if use ajax clients  
	 * @retval mixed values 
	 */	
	 function gettyping($cid= false, $to = false, $type= "visitor", &$typing = false)
	{
		$sql = false; //  get bin query process */
		// fo visitor typing
		if (!strcmp($type, "visitor")){
			$sql = sprintf (
			"select chat_user_typing ".
			"from pds_chat_user where chat_user_client= '%s' ".
			"and chat_user_kode = '%s'", $cid, $to);
		} // fo agents typing
		else if (!strcmp($type, "agent")){
			$sql = sprintf ( 
			"select chat_user_typing ".
			"from pds_chat_guest where chat_user_client = '%s' ".
			"and date(chat_user_update) = curdate() ".
			"and chat_user_kode = '%s'", $cid, $to);
		}  
		// no sql build process 
		if (!$sql) {
			return 0;
		} // get source data process 
		if (false === ($bind = $this->db->query($sql))){
			return 0;
		}  // get source data process  
		if ($bind->num_rows() <= 0){
			return 0;
		} // get all row process to this 
		$typing = (array)$bind->result_first_assoc();
		return $typing; 
	} 
	
	/* @brief gethistory
	 * @details get history form session user login 
	 * @retval mixed values 
	 */	
	 function gethistory($cid= false, $me= false, $to= false, &$history = false)
	{
		$sql = sprintf ( 
		"select chat_data_id, chat_data_session as chat_session, chat_data_to as chat_to, ". 
		"chat_data_from as chat_from, coalesce(chat_data_type,0) as chat_type, ". 
		"chat_data_msg as chat_message, chat_data_create as chat_sent, ".
		"chat_data_files as chat_fid ".
		"from pds_chat_data ".
		"where chat_data_client = '%s' ".
		"and chat_data_to = '%s' ".
		"and chat_data_from='%s' ".
		"and chat_data_delete= 0 ".
		"union ".
		"select chat_data_id, chat_data_session as chat_session, chat_data_to as chat_to, ". 
		"chat_data_from as chat_from, coalesce(chat_data_type,0) as chat_type, ". 
		"chat_data_msg as chat_message, chat_data_create as chat_sent,  ".
		"chat_data_files as chat_fid ".
		"from pds_chat_data ".
		"where chat_data_client = '%s' ".
		"and chat_data_to = '%s' ".
		"and chat_data_from='%s' ".	
		"and chat_data_delete= 0 ".	
		"order by chat_data_id desc limit 10", 
		$cid, $to, $me, $cid, $me, $to);
		
		// get source from this session 
		if (false === ($bind = $this->db->query($sql))){
			return 0;
		} // no num_rows then return "0";
		if ($bind->num_rows() <= 0 ){
			return 0;
		} // if success then 
		$y = 0; $history = array(); 
		foreach ($bind->result_assoc() as $row){
			$history[$y] = $row;	
			$y++;
		} return $history;
	}
	
	
	/* @brief getheartbeat
	 * @details Get File Sent Message from Vistor OR Agents  
	 * @retval mixed values 
	 */	
	 function getheartbeat($client = 0, $userid = 0, &$heartbeat = false)
	{
		$this->db->reset_select();
		$this->db->select(
		"a.chat_data_id as chat_id, a.chat_data_type AS chat_type, ".
		"a.chat_data_session as chat_session, b.chat_user_email as chat_email, ".
		"a.chat_data_to as chat_to, a.chat_data_from as chat_from, ".
		"a.chat_data_files as chat_fid, a.chat_data_msg as chat_message, ".
		"a.chat_data_create as chat_sent",  FALSE);
		$this->db->from("pds_chat_data a");
		$this->db->join("pds_chat_guest b", "a.chat_data_session=b.chat_user_uniquid", "LEFT");
		$this->db->join("pds_chat_serv c", "a.chat_data_session  = c.user_uniqid", "LEFT");
		$this->db->where("c.user_status",1);
		$this->db->where("a.chat_data_read", 0);
		$this->db->where("a.chat_data_client",$client);
		$this->db->where("a.chat_data_to",$userid); 
		$this->db->order_by("a.chat_data_id","ASC");
		// get bind data process 
		if (false === ($bind = $this->db->get())){
			return false;
		} // if num_rows is false return 
		if ($bind->num_rows() <= 0){
			return false;
		} // make new array fo return process 
		$heartbeat = array();
		foreach ($bind->result_assoc() as $row){
			$heartbeat[] = $row;
		} return $heartbeat;
	} 
	
	/* @brief exportmessage
	 * @details insert into to leavemessage 
	 * @param string "visitor" object request 
	 * @param string "ret" integer if success  
	 * @retval mixed values 
	 */ 
	 function exportmessage(&$exports = false, &$build_res = false, &$build_ret = 0)
	{	
		$build_str = sprintf ( 
		"select b.chat_data_id as msg_id, ".
		"b.chat_data_session as msg_session, ".
		"b.chat_data_create as msg_create, ".
		"b.chat_data_from as msg_from, ".
		"b.chat_data_to as msg_to, ".
		"b.chat_data_msg as msg_body,".
		"b.chat_data_files as msg_file, ".
		"b.chat_data_delete as msg_delete,".
		"b.chat_data_read as msg_read ".
		"from pds_chat_guest a ".
		"left join pds_chat_data b on a.chat_user_kode= b.chat_data_to ".
		"where a.chat_user_email = '%s' ".
		"and a.chat_user_client = '%s' ".
		"and b.chat_data_delete = 0  ".
		"and b.chat_data_session = '%s' ". 
		"UNION ".
		"select ".
		"b.chat_data_id as msg_id, ".
		"b.chat_data_session as msg_session,".
		"b.chat_data_create as msg_create,".
		"b.chat_data_from as msg_from,".
		"b.chat_data_to as msg_to,".
		"b.chat_data_msg as msg_body, ".
		"b.chat_data_files as msg_file, ".
		"b.chat_data_delete as msg_delete,".
		"b.chat_data_read as msg_read ".
		"from pds_chat_guest a ".
		"left join pds_chat_data b on a.chat_user_kode= b.chat_data_from ".
		"where a.chat_user_email = '%s' ".
		"and a.chat_user_client= '%s' ".
		"and b.chat_data_delete = 0 ".
		"and b.chat_data_session = '%s' ". 
		"order by msg_id asc", 
		$exports->_email, $exports->_client, $exports->_session,
		$exports->_email, $exports->_client, $exports->_session);   
		$build_get = $this->db->query($build_str); 
		if (false !== ($build_ret = $build_get->num_rows())){ 
			$build_res = array(); /* get return proess here like this */ 
			foreach ($build_get->result_assoc() as $row){
				/* convert integer "msg_id" */
				$row["msg_id"] = (int)$row["msg_id"]; 
				$row["msg_file"] = (int)$row["msg_file"];
				$row["msg_read"] = (int)$row["msg_read"];
				$row["msg_delete"] = (int)$row["msg_delete"]; 
				if (xor_default($row, "msg_id", false)){
					$build_res[] = $row;
				}  
			} 
		} return $build_res; 
	}		
	
	/* @brief leavemessage
	 * @details insert into to leavemessage 
	 * @param string "visitor" object request 
	 * @param string "ret" integer if success  
	 * @retval mixed values 
	 */
	 function leavemessage(&$visitor= false, &$ret= 0)
	{
		$this->db->reset_write(); // build query update string
		$this->db->set("session", $visitor->_session);
		$this->db->set("client", $visitor->_client);
		$this->db->set("visitor", $visitor->_visitor);
		$this->db->set("email", $visitor->_email);
		$this->db->set("phone", $visitor->_phone);
		$this->db->set("message", $visitor->_message);
		$this->db->set("create_date", $visitor->_date);
		$this->db->set("create_time", $visitor->_time);
		$this->db->set("update", $visitor->_create); 
		if ($this->db->insert("pds_chat_message")){
			if ($this->db->affected_rows() <= 0){
				$visitor->msgid = 0;
				return 0;
			} // get last insert id 
			$visitor->msgid= (int)$this->db->insert_id();
			if ($visitor->msgid) {
				$ret ++;
			}
		} // get return this value 
		return $ret;
	}
	
	/* @brief updatesession
	 * @details update get session return 
	 * @param string  $from agent from apps 
	 * @param string  $guest user from public domain 
	 * @retval mixed values 
	 */
	 function updatesession($session = 0)
	{
		$this->db->reset_write(); // build query update string
		$build_str = sprintf("update pds_chat_serv a set user_update = now() where user_uniqid = '%s'", addslashes($session));
		if (false !== ($build_ret = $this->db->query($build_str))){
			if ($this->db->affected_rows() > 0){
				return $session;
			}
		} return 0;
	}
	 
	/* @brief getboxsession
	 * @details Get File Sent Message from Vistor OR Agents  
	 * @retval mixed values 
	 */	
	 function getboxsession($agent = NULL, $visitor = NULL, &$chatboxes = 0)
	{
		$this->db->reset_select();
		$bind = "select ".
		"a.chat_data_id as chat_data_id,".
		"b.chat_user_email as chat_email,".
		"a.chat_data_session as chat_session,". 
		"a.chat_data_to as chat_to, ".
		"a.chat_data_from as chat_from,". 
		"a.chat_data_msg as chat_message,".
		"a.chat_data_type as chat_type,".
		"a.chat_data_create as chat_sent,".
		"a.chat_data_files as chat_fid ".
		"from pds_chat_data a ".
		"left join pds_chat_guest b on a.chat_data_session=b.chat_user_uniquid ".
		"where a.chat_data_delete=0 and a.chat_data_to = '$agent' and a.chat_data_from='$visitor' ".
		"union all ".
		"select ".
		"a.chat_data_id as chat_data_id,".
		"b.chat_user_email as chat_email,".
		"a.chat_data_session as chat_session,".
		"a.chat_data_to as chat_to,".
		"a.chat_data_from as chat_from,".
		"a.chat_data_msg as chat_message,".
		"a.chat_data_type as chat_type,".
		"a.chat_data_create as chat_sent,".
		"a.chat_data_files as chat_fid ".
		"from pds_chat_data a ".
		"left join pds_chat_guest b on a.chat_data_session=b.chat_user_uniquid ".
		"where a.chat_data_delete=0 and a.chat_data_to = '$visitor' and a.chat_data_from='$agent' ".
		"order by chat_data_id desc ".
		"limit 10 ";  
		 
		// then will get source data from sql 
		if (false === ($get = $this->db->query($bind))){
			return 0;
		} // check num_rows 
		if ($get &&($get->num_rows() <= 0)){
			return 0;
		} // set return to array 
		$chatboxes = array(); $x = 0;
		foreach ($get->result_assoc() as $row) {
			$chatboxes[$x] = $row;	
			$x ++;
		}  // then return this 
		return (array)$chatboxes;
	} 
	 
	/* @brief setheartbeat
	 * @details Get File Sent Message from Vistor OR Agents 
	 * @param object  $data process 
	 * @retval mixed values 
	 */	
	 function setheartbeat($client = 0, $userid = 0, &$heartbeat = false)
	{
		$this->db->reset_write();
		$this->db->set("chat_data_read", 1);
		$this->db->set("chat_data_update", 'now()', false);
		$this->db->where("chat_data_client", $client);
		$this->db->where("chat_data_to", $userid);
		$this->db->where("chat_data_read", 0);
		if (false === ($heartbeat = $this->db->update("pds_chat_data"))){
			return 0;
		} return $heartbeat;
	}  
	
    /* @brief settyping
	 * @details updated kondition if agent / visitor on typing flag , 
	 * if feature not usage websocket transport or webhook only 
	 * @param "channel" client code 
	 * @param "type" jenis subscribe (agent|visitor)
	 * @param "typing" status typing 1:typing, 0: untyping
	 * @retval mixed values 
	 */	
	 function settyping($channel = NULL, $session= NULL, $visitor= NULL, $agent = NULL, $typing = 0, &$build_ret = 0)
	{
		/* NOTE:
		 * chat ini berasal dari applikasi "agent"   
		 * + yang masih active saja yang di updated 
		 * + update untuk visitor di table "pds_chat_guest.chat_user_typing2" typing2 
		 * + update untuk agent di table "pds_chat_user.chat_user_typing" pada typing 
		 */
		$this->db->reset_write(); // update untuk disisi "visitor"  
		$build_str = sprintf ("update pds_chat_guest set chat_user_typing2= %d ".
		"where chat_user_status > 0 and chat_user_client= '%s' ". // chat user yang masih active saja
		"and (chat_user_kode= '%s' OR chat_user_email= '%s')", 
		$typing, $channel, $visitor, $visitor);
		if ($this->db->query($build_str)){
			if ($this->db->affected_rows() > 0) 
				$build_ret ++;
		} // update untuk disisi "agent" 
		$build_str = sprintf ("update pds_chat_user set chat_user_typing= %d ".
		"where chat_user_status > 0 and chat_user_id= %d and chat_user_client= '%s'", 
		$typing, $agent, $channel); // chat user yang masih active saja 
		if ($this->db->query($build_str)){
			if ($this->db->affected_rows() > 0) 
				$build_ret ++;
		} return $build_ret;
	}		
	
	/* @brief insertbox
	 * @details insert service box ssssion 
	 * @param object  $data process 
	 * @retval mixed values 
	 */	
	 function insertbox($client= null, $uniqid = false, $agent = null, $visitor = null, $email = false,  &$ret = 0)
	{
		$this->db->reset_write();
		$this->db->set("user_datets", "now()", false);
		$this->db->set("user_uniqid", $uniqid);
		$this->db->set("user_agent" , $agent);
		$this->db->set("user_guest" , $visitor);
		$this->db->set("user_email" , $email);
		$this->db->set("user_client", $client); 
		
		// on duplicate updated this rows 
		$this->db->duplicate("user_datets", date('Y-m-d H:i:s')); 
		$this->db->insert_on_duplicate("pds_chat_serv"); 
		if ($this->db->affected_rows() > 0){ 
			if (false === ($ret = (int)$this->db->insert_id())){
				return 0;
			} // next to process response
		} return $ret; 
	}
}
?>