<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * Chat Class controller 
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Security
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/security.html
 */  
class M_Upload extends EUI_Model {
	private static $Instance = NULL;
	/* @brief Instance   
	 * @details no description
	 * @param none none description
	 * @retval mixed return 
	 */ 
	public static function &Instance() {
		if (is_null(self::$Instance)){
			self::$Instance = new self();
		} return self::$Instance; 
	} 
	/* @brief __construct 
	 * @details no description 
	 * @retval mixed return 
	 */
	 function __construct(){ }
	
	/* @brief delete 
	 * @details no description 
	 * @retval mixed return 
	 */
	function delete(){ }
	
	/* @brief chatid 
	 * @details update reff chat id form files 
	 * @retval mixed return 
	 */
	function chatid($cid= 0, $id= 0){
		$this->db->reset_write("");
		$this->db->set("chat_id", $cid);
		$this->db->where("id", $id);
		$this->db->update("pds_chat_files");
		if ($this->db->affected_rows() > 0){
			return $id;
		} return 0;
	} 
	 
	/* @brief upload 
	 * @details no description 
	 * @retval mixed return 
	 */
	function upload(&$chat= false, &$file_documents = false) { 
		// set all reff data process 
		$file_documents["id"]  = 0; // file chat id 
		$file_documents["cid"] = (int)$chat->uid; // chat id 
		$file_documents["sid"] = $chat->uniqid; // session id 
		$file_documents["now"] = date('Y-m-d H:i:s'); 
		// bind query data process 
		$this->db->reset_write();
		$bind = sprintf( 
		"insert into pds_chat_files (".
		"chat_id, ".
		"session, ".
		"file_uid,".
		"file_create, ".
		"file_name, ".
		"file_hash, ".
		"file_type, ".
		"file_mime, ".
		"file_size, ".
		"file_path, ".
		"file_url) ".
		"values (".
		"'%d',". // chat_id
		"'%s',". // session
		"'%s',". // file_uid
		"'%s',". // file_create
		"'%s',". // file_name
		"'%s',". // file_hash
		"'%s',". // file_type
		"'%s',". // file_mime
		"'%d',". // file_size
		"'%s',". // file_path
		"'%s')", // file_url  
		$file_documents["cid"],
		$file_documents["sid"],
		$file_documents["uid"], 
		$file_documents["now"],
		$file_documents["name"], 
		$file_documents["hash"],
		$file_documents["type"],
		$file_documents["mime"],
		$file_documents["size"], 
		$file_documents["path"], 
		$file_documents["url"]);    
		// then sent to insert data to table of files 
		if (!$this->db->query($bind)){
			return 0;
		} // if success insert data then get last insert;
		if ($this->db->affected_rows() > 0){
			$file_documents["id"] = $this->db->insert_id(); 
		} return $file_documents["id"];
	}
	
	/* @brief download 
	 * @details no description 
	 * @retval mixed return 
	 */
	function download($id= "", &$bytes= false) { 
		$this->db->reset_select();
		$this->db->select("*", false);
		$this->db->from("pds_chat_files");
		$this->db->where("file_uid", $id); 
		// get and binding process;
		if (false === ($bind = $this->db->get())){
			return 0;
		} // if success process 	
		if ($bind->num_rows()){
			if ($bytes = $bind->result_first_assoc()){
				$this->db->reset_write();
				$this->db->set("file_hit", "(coalesce(file_hit, 0)+1)", false);
				$this->db->where("id", $bytes["id"]);
				$this->db->update("pds_chat_files");
			}
		} return $bytes;
	}  
}
?>