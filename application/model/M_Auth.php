<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter 
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package 	CodeIgniter
 * @author 		PDS Development Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0 
 */
 class M_Auth extends EUI_Model
{
	/* @brief Instance of class 
	 * @param none 	none none 
	 * @retval mixed json resposne  
	 */
	 public static function &Instance()
	{
		if (is_null(self::$Instance)) {
			self::$Instance = new self();	
		} return self::$Instance;
	}  
	
	/* @brief  __construct()
	 * @details Construtor of class 
	 * @param none 	none none 
	 * @retval mixed json resposne  
	 */
	function __construct(){ }
	
	/* @brief client 
	 * @details Check Client On Database to view process 
	 * @param string  $username  username for login base on header 
	 * @param string  $password  password for login base on header 
	 * @param integer $s array return reff
	 * @param integer $o object return reff   
	 * @retval mixed json response   
	 */ 
	 function client($username = NULL, $password = NULL, &$session= false, &$ret= false)
	{
		/* get build of query */
		$this->db->reset_select(); 
		$sql = sprintf(
		"select * from pds_chat_client ". 
		"where chat_client_kode='%s' ". 
		"and chat_client_pass= '%s'", $username, $password); 
		if (false == ($qry = $this->db->query($sql))){ 
			return $session;
		} /* get all result data process */
		if ($qry->num_rows()){
			$ret = $qry->first_row();
		} /* check data object */ 
		if (is_object($ret)){
			$session = array();
			$session['client_cid'] = $ret->chat_client_kode;
			$session['client_name'] = $ret->chat_client_name;
			$session['client_pass'] = $ret->chat_client_pass;
			$session['client_host'] = $ret->chat_client_host;
			$session['client_token'] = $ret->chat_client_token;
			$session['client_auth'] = $ret->chat_client_auth;
			$session['client_expired'] = $ret->chat_client_expired;
			$session['client_status'] = $ret->chat_client_status; 
		} /* get of return value */
		return is_array($session) ? $session : false;
	}
	
	/* @brief update 
	 * @details Update field Auth  
	 * @param string  $username  username for login base on header 
	 * @param string  $password  password for login base on header 
	 * @param integer $interval  maximum expired Interval   
	 * @retval mixed json resposne  
	 */ 
	 function update(&$auth = false)
	{
		if (!is_array($auth)){
			return false;
		} /* update this "client" */
		$this->db->reset_write();
		$this->db->set("chat_client_auth", $auth['client_auth']);
		$this->db->where("chat_client_kode", $auth['client_cid']);
		return $this->db->update("pds_chat_client"); 
	} 
	
	/* @brief change 
	 * @details auto update if user register but is exist then expired 
	 * @auth object "auth" object reff 
	 * @param integer $interval  maximum expired Interval   
	 * @retval mixed return
	 */ 
	 function change(&$auth = false)
	{
		if (!is_object($auth)){
			return false;
		} /* update this "client" */
		$this->db->reset_write();
		$this->db->set("chat_client_token"  , $auth->chat_client_token);
		$this->db->set("chat_client_expired", $auth->chat_client_expired); 
		$this->db->where("chat_client_kode" , $auth->chat_client_kode);
		$this->db->where("chat_client_status", 1);
		return $this->db->update("pds_chat_client"); 
	} 
}

?>